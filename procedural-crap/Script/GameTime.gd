extends Node

const NYCHTHEMERON = 24 * 60 * 1000 # 24 minutes
const YEAR = NYCHTHEMERON * 365

var time_start

func elapsed():
	return OS.get_unix_time() - time_start

func elapsedMs():
	return OS.get_ticks_msec()

func elapsedUs():
	return OS.get_ticks_usec()

func _ready():
	time_start = OS.get_unix_time()
