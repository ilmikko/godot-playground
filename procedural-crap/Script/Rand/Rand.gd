extends RandomNumberGenerator

# randfneg returns a random number between -1 and 1.
func randfneg():
	return (self.randf()-0.5)*2.0

# randfneg2 returns a Vector2 with randfneg values.
func randfneg2():
	return Vector2(randfneg(), randfneg())

# randfneg3 returns a Vector3 with randfneg values.
func randfneg3():
	return Vector3(randfneg(), randfneg(), randfneg())

# randf2 returns a Vector2 with randf values.
func randf2():
	return Vector2(self.randf(), self.randf())

# randf3 returns a Vector3 with randf values.
func randf3():
	return Vector3(self.randf(), self.randf(), self.randf())

# weighed_rand returns an index weighed randomly based on sums of the indices.
func weighed_rand(a):
	var sum = 0.0
	for i in a:
		sum += a[i]
	return weighed_rand_precalc(a, sum)

func weighed_rand_arr(a):
	var sum = 0.0
	for i in a:
		sum += i
	return weighed_rand_precalc_arr(a, sum)

# weighed_rand_precalc is like weighed_rand except it uses a precalculated sum.
func weighed_rand_precalc(a, sum):
	var targetIndex = self.randf()*sum
	for i in a:
		targetIndex -= a[i]
		if targetIndex <= 0:
			return i

func weighed_rand_precalc_arr(a, sum):
	var targetIndex = self.randf()*sum
	for i in range(a.size()):
		targetIndex -= a[i]
		if targetIndex <= 0:
			return i
