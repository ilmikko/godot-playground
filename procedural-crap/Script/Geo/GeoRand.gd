extends "res://Script/Rand/Rand.gd"
class_name GeoRand

# bounds generates a set of random points inside the bounding box of a polygon.
func bounds(poly : Polygon2D, options):
	return polygon(Geo.bounds(poly), options)

# triangle generates a set of random points inside a triangle.
func triangle(poly : Polygon2D, options):
	var points = []
	
	# Define polygon triangle transform
	var a = poly.polygon[0]
	var b = poly.polygon[1]
	var c = poly.polygon[2]
	var m1 = Transform2D(b-a, c-a, a)
	
	for _i in range(options.count):
		# Generate random point in unit triangle based on our distribution.
		var pos = options.distribution.apply()
		
		# Translate this to the polygon triangle
		pos.x = 1.0-pos.x
		pos = m1.xform(pos)
		points.append(pos)
	
	return points

# polygon generates a set of random points inside a polygon.
func polygon(poly : Polygon2D, options):
	var points = []
	
	var tris = Geo.triangulate(poly)
	
	var areas = []
	var areasSum = 0
	for t in tris:
		var area = Geo.triangle_area(t)
		areasSum += area
		areas.append(area)
	
	# Randomize which areas the n points are to fall first.
	# This is done by weighed random sampling based on the areas of the triangles.
	var buckets = []
	for i in areas:
		buckets.append(0)
	for _i in range(options.count):
		var tid = weighed_rand_precalc_arr(areas, areasSum)
		buckets[tid] += 1
	
	for i in range(buckets.size()):
		if buckets[i] <= 0:
			continue
		
		points += triangle(tris[i], {
			"count": buckets[i],
			"distribution": options.distribution,
		})
	
	return points
