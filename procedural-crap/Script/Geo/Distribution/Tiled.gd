extends Node

var generator
var tiling = Vector2(16.0, 16.0)
# max_offset is a percentage of the cell based on tiling.
var max_offset = Vector2(0.0, 0.0)

func apply():
	var pos = generator.randf2()
	pos.x = floor(pos.x * tiling.x) / tiling.x
	pos.y = floor(pos.y * tiling.y) / tiling.y
	pos += generator.randf2() * (max_offset / tiling)
	if pos.y > pos.x:
		pos = Vector2(1.0, 1.0)-pos
	return pos

func _init(options):
	generator = options.rng
	if 'tiling' in options:
		tiling = options.tiling
	if 'max_offset' in options:
		max_offset = options.max_offset
