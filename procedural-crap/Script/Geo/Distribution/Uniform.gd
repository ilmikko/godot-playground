extends Node

var generator

func apply():
	var pos = generator.randf2()
	if pos.y > pos.x:
		pos = Vector2(1.0, 1.0)-pos
	return pos

func _init(options):
	generator = options.rng
