extends Node

const DistributionUniform = preload("res://Script/Geo/Distribution/Uniform.gd")
const DistributionTiled = preload("res://Script/Geo/Distribution/Tiled.gd")

# tiled defines an uniform distribution of points.
func tiled(rng):
	return DistributionTiled.new(rng)

# uniform defines an uniform distribution of points.
func uniform(rng):
	return DistributionUniform.new(rng)
