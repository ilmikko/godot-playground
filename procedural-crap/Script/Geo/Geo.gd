# Geo is a collection of helpers to deal with geometry.
extends Node

static func ceil2(vec : Vector2) -> Vector2:
	return Vector2(ceil(vec.x), ceil(vec.y))

static func floor2(vec : Vector2) -> Vector2:
	return Vector2(floor(vec.x), floor(vec.y))

static func round2(vec : Vector2) -> Vector2:
	return Vector2(round(vec.x), round(vec.y))

# bounding_box calculates the bounding box for any Polygon2D.
static func bounding_box(poly : Polygon2D) -> Rect2:
	var topleft = Vector2(INF, INF)
	var bottomright = Vector2(-INF, -INF)
	
	for p in poly.polygon:
		topleft[0] = min(topleft[0], p.x)
		topleft[1] = min(topleft[1], p.y)
		bottomright[0] = max(bottomright[0], p.x)
		bottomright[1] = max(bottomright[1], p.y)
	
	return Rect2(topleft, bottomright-topleft)

# bounds returns the bounding box of a Polygon2D as a Polygon2D.
static func bounds(poly : Polygon2D) -> Polygon2D:
	var rect = bounding_box(poly)
	
	var p = Polygon2D.new()
	
	var a = rect.position
	var b = rect.position + Vector2(rect.size.x, 0)
	var c = rect.position + rect.size
	var d = rect.position + Vector2(0, rect.size.y)
	
	p.polygon = PoolVector2Array([a, b, c, d])
	
	return p

# math_line_from_points returns a line in the form [a, b, c] where PQ is ax+by=c
static func math_line_from_points(p : Vector2, q : Vector2):
	var a = q.y - p.y
	var b = p.x - q.x
	var c = a*p.x + b*p.y
	return [a, b, c]

# math_line_intersection returns the point where two lines of ax+by=c intersect.
# This function returns null if the two lines are parallel.
static func math_line_intersection(l1, l2):
	var a1 = l1[0]
	var b1 = l1[1]
	var c1 = l1[2]
	
	var a2 = l2[0]
	var b2 = l2[1]
	var c2 = l2[2]
	
	var d = a1*b2 - a2*b1
	if (d == 0):
		return null

	var x = (b2*c1 - b1*c2)/d
	var y = (a1*c2 - a2*c1)/d
	return Vector2(x, y)

static func math_perpendicular_bisector_from_line(p : Vector2, q : Vector2):
	var mid_point = Vector2((p.x+q.x)/2.0, (p.y+q.y)/2.0)
	var line = math_line_from_points(p, q)
	
	var aa = line[0]
	var bb = line[1]
	
	var a = -bb
	var b = aa
	var c = -bb*mid_point.x + aa*mid_point.y
	return [a, b, c]

# triangle_area calculates the area for a Polygon2D triangle.
static func triangle_area(t : Polygon2D) -> float:
	var a = t.polygon[0]
	var b = t.polygon[1]
	var c = t.polygon[2]
	var ab = (a-b).length()
	var bc = (b-c).length()
	var ca = (c-a).length()
	var s = (ab + bc + ca)/2
	return sqrt(s*(s-ab)*(s-bc)*(s-ca))

# triangle_circumcenter calculates the circumcenter for a Polygon2D triangle.
static func triangle_circumcenter(t : Polygon2D) -> Vector2:
	var a = t.polygon[0]
	var b = t.polygon[1]
	var c = t.polygon[2]
	
	var l1 = math_perpendicular_bisector_from_line(a, b)
	var l2 = math_perpendicular_bisector_from_line(b, c)
	
	return math_line_intersection(l1, l2)

# triangulate returns a list of Polygon2D triangles that make up this polygon.
# If you require raw points, please use Geometry.triangulate_polygon directly.
static func triangulate(poly : Polygon2D):
	var tris = []
	var tri = Geometry.triangulate_polygon(poly.polygon)
	for i in range(0, tri.size(), 3):
		var p = Polygon2D.new()
		
		var a = poly.polygon[tri[i+0]]
		var b = poly.polygon[tri[i+1]]
		var c = poly.polygon[tri[i+2]]
		
		p.polygon = PoolVector2Array([a, b, c])
		
		tris.append(p)
	return tris

# triangulate returns a list of Polygon2D triangles triangulated by the delaunay algorithm.
# If you require raw points, please use Geometry.triangulate_delaunay_2d directly.
static func delaunay(poly : Polygon2D):
	var tris = []
	var tri = Geometry.triangulate_delaunay_2d(poly.polygon)
	for i in range(0, tri.size(), 3):
		var p = Polygon2D.new()
		
		var a = poly.polygon[tri[i+0]]
		var b = poly.polygon[tri[i+1]]
		var c = poly.polygon[tri[i+2]]
		
		p.polygon = PoolVector2Array([a, b, c])
		
		tris.append(p)
	return tris
