extends Node

func update(rot):
	var rot01 = rot / TAU
	var hours = int(24 * rot01)
	var minutes = int(60 * 24 * rot01) % 60
	Debug.show("Time", "%02d:%02d" % [hours, minutes])
