extends Node

# We are in Greece.
const MEAN_YEARLY_TEMPERATURE = 15 # Celsius
const YEAR_VARIATION = 10 # Celsius
const NYCHTHEMERON_VARIATION = 10 # Celsius

var current = 0

func update(t, phase):
	var year_phase = t / float(GameTime.YEAR)
	current = MEAN_YEARLY_TEMPERATURE + YEAR_VARIATION * year_phase
	current += phase * NYCHTHEMERON_VARIATION
	Debug.show("Temperature", "%fC" % current)
