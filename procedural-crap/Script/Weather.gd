extends Spatial

signal updated

var time = preload("res://Script/Weather/Time.gd").new()
var temperature = preload("res://Script/Weather/Temperature.gd").new()

var generator = Seed.generator(get_name())

# Offset to force a time of day. This can be random, but for debug you might
# want to set this to these values:
# NIGHT:   0
# MORNING: NYCHTHEMERON / 4
# NOON:    NYCHTHEMERON / 2
# EVENING: NYCHTHEMERON / 2 + NYCHTHEMERON / 4
var offset = 0

func update():
	var t = offset + GameTime.elapsedMs()
	# rot is 0 and TAU at midnight, and PI at noon.
	var rot = fmod(t / float(GameTime.NYCHTHEMERON) * TAU, TAU)
	# phase is <0 at night, 0 at morning and evening, 1 at noon.
	var phase = -cos(rot)
	
	time.update(rot)
	temperature.update(t, phase)
	emit_signal("updated", t, rot, phase)

func _ready():
	offset = generator.randi()%GameTime.YEAR
	update()
