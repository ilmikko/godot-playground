extends Node

const SEED = 77

const Rand = preload("res://Script/Rand/Rand.gd")
const GeoRand = preload("res://Script/Geo/GeoRand.gd")

func _seed_from_basis(basis_seed : Basis) -> int:
	return _seed_from_int(
		_seed_from_int(
			(_seed_from_vec3(basis_seed.x)+0)
			-
			(_seed_from_vec3(basis_seed.y)+1)
		)
		-
		(_seed_from_vec3(basis_seed.z)+2)
	)

func _seed_from_int(int_seed : int) -> int:
	var s = SEED + int_seed
	if s < 0:
		return -s
	return s

func _seed_from_string(string_seed : String) -> int:
	return _seed_from_int(string_seed.hash())

func _seed_from_transform(transform_seed : Transform) -> int:
	return _seed_from_int(
		(_seed_from_basis(transform_seed.basis)+0)
		-
		(_seed_from_vec3(transform_seed.origin)+1)
	)

func _seed_from_vec2(vec2_seed : Vector2) -> int:
	return _seed_from_string("%s,%s" % [vec2_seed.x, vec2_seed.y])

func _seed_from_vec3(vec3_seed : Vector3) -> int:
	return _seed_from_string("%s,%s,%s" % [vec3_seed.x, vec3_seed.y, vec3_seed.z])

func seed_from(any_seed):
	if typeof(any_seed) == TYPE_BASIS:
		return _seed_from_basis(any_seed)
	elif typeof(any_seed) == TYPE_INT:
		return _seed_from_int(any_seed)
	elif typeof(any_seed) == TYPE_STRING:
		return _seed_from_string(any_seed)
	elif typeof(any_seed) == TYPE_TRANSFORM:
		return _seed_from_transform(any_seed)
	elif typeof(any_seed) == TYPE_VECTOR2:
		return _seed_from_vec2(any_seed)
	elif typeof(any_seed) == TYPE_VECTOR3:
		return _seed_from_vec3(any_seed)
	else:
		Error.fatal("Unknown type for seed.")

func generator(any_seed):
	var generator = Rand.new()
	generator.seed = seed_from(any_seed)
	return generator

func georand(any_seed):
	var georand = GeoRand.new()
	georand.seed = seed_from(any_seed)
	return georand

func simplex_noise(any_seed):
	var simplex_noise = OpenSimplexNoise.new()
	simplex_noise.seed = seed_from(any_seed)
	return simplex_noise
