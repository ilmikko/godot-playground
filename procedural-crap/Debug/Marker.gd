extends Spatial

var color = Color(1, 0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	var m = SpatialMaterial.new()
	m.albedo_color = color
	m.emission_enabled = true
	m.emission = color
	$X.material_override = m
	$Y.material_override = m
	$Z.material_override = m
