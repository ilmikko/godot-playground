extends Spatial

var color = Color(1, 0, 0)

# Called when the node enters the scene tree for the first time.
func _ready():
	var m = SpatialMaterial.new()
	m.albedo_color = color
	m.emission_enabled = true
	m.emission = color
	$Mesh.material_override = m
