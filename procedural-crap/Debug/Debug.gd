extends Node

signal update

var markers = {}

const Marker = preload("res://Debug/Marker.tscn")
const Dot = preload("res://Debug/Dot.tscn")

func line(id, start, end, color = Color(1, 0, 0)):
	var ler_max = 7.0
	for i in range(ler_max):
		marker_dot(id+("%s"%i), lerp(start, end, i/ler_max), color)

func line_2d(id, start, end, color = Color(1, 0, 0)):
	line(id, Vector3(start.x, 0, start.y), Vector3(end.x, 0, end.y), color)

func marker_cross(id, coords, color = Color(1, 0, 0)):
	marker(id, coords, Marker, color)

func marker_dot(id, coords, color = Color(1, 0, 0)):
	marker(id, coords, Dot, color)

func marker(id, coords, type, color = Color(1, 0, 0)):
	if not (id in markers):
		var marker = type.instance()
		marker.color = color
		markers[id] = marker
		add_child(marker)
	
	markers[id].global_transform.origin = coords

func marker_2d(id, vec, color = Color(1, 0, 0)):
	marker_cross(id, Vector3(vec.x, 0, vec.y), color)

func marker_rect(id, rect, color = Color(1, 0, 0)):
	marker_2d(id+"TOPLEFT", rect.position, color)
	marker_2d(id+"BOTRIGHT", rect.position+rect.size, color)

func show(id, s):
	emit_signal("update", id, s)

func show_text(s):
	show("DEBUG", s)
