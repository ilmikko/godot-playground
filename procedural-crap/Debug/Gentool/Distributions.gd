extends Node2D

onready var Marker = $"Marker"

var GeoRand = Seed.georand(get_name())

func _generate(tile, distrib):
	var poly = tile.get_node("Polygon")
	var points = GeoRand.polygon(poly, {
		"count": 50,
		"distribution": distrib,
	})
	for p in points:
		var dot = Marker.duplicate()
		dot.position = p
		poly.add_child(dot)

func generate():
	_generate($"Uniform", Distribution.uniform({
		"rng": GeoRand,
	}))
	
	_generate($"Tiled", Distribution.tiled({
		"rng": GeoRand,
		"tiling": Vector2(4.0, 4.0),
	}))
	
	_generate($"TiledRandom", Distribution.tiled({
		"rng": GeoRand,
		"tiling": Vector2(4.0, 4.0),
		"max_offset": Vector2(0.25, 0.25),
	}))

func _on_Button_pressed():
	generate()
