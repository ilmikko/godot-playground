extends Node2D

onready var GenPoly = $"Polygon"
onready var Marker = $"Marker"

var GeoRand = Seed.georand(get_name())

func _on_Button_pressed():
	var points = GeoRand.polygon(GenPoly, {
		"count": 1500,
		"distribution": Distribution.uniform({
			"rng": GeoRand,
		}),
	})
	for p in points:
		var dot = Marker.duplicate()
		dot.position = p
		add_child(dot)
