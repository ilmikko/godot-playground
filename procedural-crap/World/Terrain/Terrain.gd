extends Spatial

const CELL_SIZE = 10

var cells = {}
var previous_rect = Rect2()

onready var Cells = $Cells
onready var CELL = preload("res://World/Terrain/Cell.tscn")
onready var HeightMap = $HeightMap

func cell(pos):
	if (pos in cells) and (cells[pos] != null):
		return cells[pos]

	var cell = CELL.instance()
	cell.position = pos
	cell.heightmap = HeightMap
	cells[pos] = cell
	add_child(cell)
	return cell

func terrain_rect(rect):
	rect = Rect2(rect.position/CELL_SIZE, rect.size/CELL_SIZE)
	rect.position = Geo.floor2(rect.position)
	rect.size = Geo.ceil2(rect.size)
	return rect

func view_changed(rect):
	rect = terrain_rect(rect)
	if rect.position == previous_rect.position and rect.size == previous_rect.size:
		return
	
	var cells_to_deactivate = {}
	for x in range(previous_rect.position.x, previous_rect.position.x + previous_rect.size.x + 1):
		for y in range(previous_rect.position.y, previous_rect.position.y + previous_rect.size.y + 1):
			var c = cell(Vector2(x, y))
			cells_to_deactivate[c] = true

	Debug.show("Terrain Rect", rect)
	for x in range(rect.position.x, rect.position.x + rect.size.x + 1):
		for y in range(rect.position.y, rect.position.y + rect.size.y + 1):
			var c = cell(Vector2(x, y))
			c.activate()
			cells_to_deactivate[c] = false
	
	for c in cells_to_deactivate:
		if cells_to_deactivate[c] == true:
			c.deactivate()

	previous_rect = rect

func _on_World_camera_rect_changed(rect):
	view_changed(rect)
