extends Spatial

var noise = Seed.simplex_noise(get_name())

func _init():
	noise.octaves = 6
	noise.period = 80

func height(pos):
	return noise.get_noise_3d(pos.x, 0, pos.y)
