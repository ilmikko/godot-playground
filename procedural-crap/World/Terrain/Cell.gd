extends Spatial

const HEIGHT_INTENSITY = 10.0
const SUBDIVISIONS = 2
const SIZE = 10

# position in the cell space.
var position = Vector2(0, 0)
export(Material) var material
var heightmap

var active = false

onready var mesh_instance = $MeshInstance

func activate():
	if active:
		return
		
	Debug.show("Last activated cell", position)
	active = true
	
	generate_mesh()

func deactivate():
	if !active:
		return
	
	active = false
	
	Debug.show("Last deactivated cell", position)
	queue_free()

func generate_mesh():
	var mesh = PlaneMesh.new()
	
	mesh.size = Vector2(1, 1) * SIZE
	mesh.subdivide_depth = SUBDIVISIONS
	mesh.subdivide_width = SUBDIVISIONS

	var surface_tool = SurfaceTool.new()
	surface_tool.create_from(mesh, 0)

	var array_plane = surface_tool.commit()

	var data_tool = MeshDataTool.new()
	data_tool.create_from_surface(array_plane, 0)

	for i in range(data_tool.get_vertex_count()):
		var vertex = data_tool.get_vertex(i)
		vertex.y = heightmap.height(Vector2(translation.x + vertex.x, translation.z + vertex.z))
		vertex.y *= HEIGHT_INTENSITY
		data_tool.set_vertex(i, vertex)

	for i in range(array_plane.get_surface_count()):
		array_plane.surface_remove(i)

	data_tool.commit_to_surface(array_plane)
	surface_tool.begin(Mesh.PRIMITIVE_POINTS)
	surface_tool.create_from(array_plane, 0)
	surface_tool.generate_normals()

	mesh_instance.mesh = surface_tool.commit()
	mesh_instance.material_override = material

func _ready():
	translate(Vector3(position.x, 0, position.y) * SIZE)
