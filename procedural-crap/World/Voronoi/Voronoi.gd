extends Spatial

signal cell_generated

const CELL_SIZE = 100
const POINTS_PER_CELL_AVERAGE = 10
const POINTS_PER_CELL_VARIATION = 2

var cells_active = {}
var previous_rect = Rect2()

func is_cell_active(cell):
	if cell.y in cells_active and cell.x in cells_active[cell.y]:
		return true
	return false

func cell_activate(pos):
	if is_cell_active(pos):
		return
	
	if not pos.y in cells_active:
		cells_active[pos.y] = {}
	if not pos.x in cells_active[pos.y]:
		cells_active[pos.y][pos.x] = true
	
	var generator = Seed.generator(pos)
	
	# Generate voronoi points from pos -> pos + Vector2(1, 1)
	# TODO: Use a grid instead
	var point_count = POINTS_PER_CELL_AVERAGE + ((generator.randi()%POINTS_PER_CELL_VARIATION)-POINTS_PER_CELL_VARIATION/2.0)
	
	var points = PoolVector2Array()
	for _i in range(point_count):
		var p = pos
		p += generator.randf2()
		p *= CELL_SIZE
		points.append(p)

	var poly = Polygon2D.new()
	poly.polygon = points

	var triangles = Geo.delaunay(poly)
	
	emit_signal("cell_generated", pos, triangles)
	#for t in triangles:
		#var tpos = Geo.triangle_circumcenter(t)
		#Debug.line_2d("%s,%sl1" % [tpos.x,tpos.y], t.polygon[0]*CELL_SIZE, t.polygon[1]*CELL_SIZE, Color(0, 1, 0))
		#Debug.line_2d("%s,%sl2" % [tpos.x,tpos.y], t.polygon[1]*CELL_SIZE, t.polygon[2]*CELL_SIZE, Color(0, 1, 0))
		#Debug.line_2d("%s,%sl3" % [tpos.x,tpos.y], t.polygon[0]*CELL_SIZE, t.polygon[2]*CELL_SIZE, Color(0, 1, 0))
		#Debug.marker_2d("%s,%st" % [tpos.x,tpos.y], tpos*CELL_SIZE, Color(1, 0, 1))

func voronoi_rect(rect):
	rect = Rect2(rect.position/CELL_SIZE, rect.size/CELL_SIZE)
	rect.position = Geo.floor2(rect.position)
	rect.size = Geo.ceil2(rect.size)
	return rect
	
func view_changed(rect):
	rect = voronoi_rect(rect)
	if rect.position == previous_rect.position and rect.size == previous_rect.size:
		return
	previous_rect = rect
	
	Debug.show("Voronoi Rect", rect)
	for x in range(rect.position.x, rect.position.x + rect.size.x + 1):
		for y in range(rect.position.y, rect.position.y + rect.size.y + 1):
			cell_activate(Vector2(x, y))

func _on_World_camera_rect_changed(rect):
	view_changed(rect)
