extends Spatial

const INTENSITY = 2.2

export(Gradient) var gradient

onready var yaw = $Yaw
onready var pitch = $Yaw/Pitch
onready var light = $Yaw/Pitch/DirectionalLight

func _ready():
	var status = Weather.connect("updated", self, "_on_Weather_updated")
	assert(status == OK)

func _update_brightness(phase):
	var phase01 = (0.5 + phase/2)
	light.light_color = gradient.interpolate(phase)
	light.light_energy = phase01 * INTENSITY
	Debug.show("Sun Energy", light.light_energy)

func _update_sun_position(t, rot):
	pitch.rotation = Vector3(PI / 2 + rot, 0, 0)
	yaw.rotation = Vector3(0, t / float(GameTime.YEAR) * TAU, 0)

func update(t, rot, phase):
	Debug.show("Sun Phase", phase)
	Debug.show("Sun Rotation", rot)
	_update_sun_position(t, rot)
	_update_brightness(phase)

func _on_Weather_updated(t, rot, phase):
	update(t, rot, phase)
