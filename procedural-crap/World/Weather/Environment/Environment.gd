extends Spatial

const INTENSITY = 0.15

export(Gradient) var environment_gradient

onready var environment = $WorldEnvironment

func _ready():
	var status = Weather.connect("updated", self, "_on_Weather_updated")
	assert(status == OK)

func _update_brightness(phase):
	environment.environment.ambient_light_energy = cos(phase) * INTENSITY
	Debug.show("Environment Energy", environment.environment.ambient_light_energy)

func _update_colours(phase):
	var phase01 = (0.5 + phase/2)
	environment.environment.ambient_light_color = environment_gradient.interpolate(phase01)

func update(phase):
	_update_brightness(phase)
	_update_colours(phase)

func _on_Weather_updated(_t, _rot, phase):
	update(phase)
