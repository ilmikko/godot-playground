extends Spatial

const TREE = preload("res://Tree/Tree.tscn")

var rng = Seed.georand(get_name())

func add_tree(cell, x, y):
	var height = 0
	var tree = TREE.instance()
	tree.translation = Vector3(x, height, y)
	cell.add_child(tree)

func generate_for(cell, poly):
	var points = rng.polygon(poly, {
		"count": 50,
		"distribution": Distribution.uniform({
			"rng": rng,
		}),
	})
	
	for p in points:
		add_tree(cell, p.x, p.y)
