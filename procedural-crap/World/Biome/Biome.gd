extends Node

enum {
	NONE
	FOREST
}

var probabilities = {
	NONE: 0.6,
	FOREST: 0.3,
}

var generators = {
	FOREST: preload("res://World/Biome/Forest/Forest.gd").new(),
}

var rng = Seed.generator(get_name())

func generate_for(cell, poly):
	var b = random()

	if !(b in generators):
		return

	generators[b].generate_for(cell, poly)
	
func random():
	return rng.weighed_rand(probabilities)
