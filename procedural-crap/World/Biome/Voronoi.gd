extends Spatial

const CELL_SIZE = 100
const CELL = preload("res://World/Biome/Cell.tscn")

var BIOME = preload("res://World/Biome/Biome.gd").new()

var cells = {}
var previous_rect = Rect2()

func cell(pos):
	if (pos in cells) and (cells[pos] != null):
		return cells[pos]

	var cell = CELL.instance()
	cell.biome = BIOME
	cell.position = pos
	cells[pos] = cell
	add_child(cell)
	return cell

func voronoi_rect(rect):
	rect = Rect2(rect.position/CELL_SIZE, rect.size/CELL_SIZE)
	rect.position = Geo.floor2(rect.position)
	rect.size = Geo.ceil2(rect.size)
	return rect

var cells_to_deactivate = {}
func view_changed(rect):
	rect = voronoi_rect(rect)
	if rect.position == previous_rect.position and rect.size == previous_rect.size:
		return
	previous_rect = rect
	
	var cells_activated = {}
	Debug.show("Voronoi Rect", rect)
	for x in range(rect.position.x, rect.position.x + rect.size.x + 1):
		for y in range(rect.position.y, rect.position.y + rect.size.y + 1):
			var c = cell(Vector2(x, y))
			c.activate()
			cells_to_deactivate[c] = false
			cells_activated[c] = true
	
	for c in cells_to_deactivate:
		if !(c in cells_activated):
			c.deactivate()
	
	cells_to_deactivate = cells_activated

func _on_World_camera_rect_changed(rect):
	view_changed(rect)
