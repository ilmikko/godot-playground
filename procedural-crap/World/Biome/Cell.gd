extends Spatial

const SIZE = 100
const POINTS_PER_CELL_AVERAGE = 10
const POINTS_PER_CELL_VARIATION = 2

var biome
var position = Vector2(0, 0)

var active = false

func deactivate():
	queue_free()

func activate():
	if active:
		return
	active = true
	
	var generator = Seed.generator(transform)
	
	# Generate voronoi points from pos -> pos + Vector2(1, 1)
	# TODO: Use a grid instead
	var point_count = POINTS_PER_CELL_AVERAGE + ((generator.randi()%POINTS_PER_CELL_VARIATION)-POINTS_PER_CELL_VARIATION/2)
	
	var points = PoolVector2Array()
	for _i in range(point_count):
		var p = Vector2(transform.origin.x, transform.origin.z)
		p += generator.randfneg2() * SIZE
		points.append(p)

	var poly = Polygon2D.new()
	poly.polygon = points

	var triangles = Geo.delaunay(poly)
	
	for t in triangles:
		biome.generate_for(self, t)

func _ready():
	translation = Vector3(position.x, 0, position.y) * SIZE
