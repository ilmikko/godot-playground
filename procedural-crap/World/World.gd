extends Spatial

signal camera_rect_changed

const MARGIN = 1.5

func camera_rect(transform):
	var viewport = get_viewport().size
	var size = Vector2(viewport.x / viewport.y, 1) * transform.origin.y * MARGIN
	
	var center = Vector2(transform.origin.x, transform.origin.z)
	var topleft = center - size/2
	
	return Rect2(topleft, size)

func _on_Controller_camera_moved(transform):
	# topleft is world coordinates for top left
	# bottomright is world coordinates for bottom right
	var rect = camera_rect(transform)
	Debug.show("Camera", transform)
	Debug.show("Camera Rect", rect)
	emit_signal("camera_rect_changed", rect)
