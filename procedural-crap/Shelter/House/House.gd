extends StaticBody

const MAXIMUM_OCCUPANCY = 3

var number_of_occupants = 0

func add_occupant():
	if number_of_occupants >= MAXIMUM_OCCUPANCY:
		return false

	number_of_occupants += 1
	return true
