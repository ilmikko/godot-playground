extends Spatial

const APPLE_COUNT_AVERAGE = 5
const APPLE_COUNT_VARIATION = 5

const APPLE = preload("res://Tree/Apple.tscn")

var active = false

func activate():
	if active:
		return
	active = true

	var rng = Seed.generator(transform)
	var apple_count = APPLE_COUNT_AVERAGE + ((rng.randi()%APPLE_COUNT_VARIATION)-APPLE_COUNT_VARIATION/2)
	for _i in range(apple_count):
		var apple = APPLE.instance()
		apple.translation = $Leaves.translation + rng.randfneg3()
		add_child(apple)
