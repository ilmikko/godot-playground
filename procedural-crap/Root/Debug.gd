extends Spatial

func _ready():
	if OS.is_debug_build():
		Debug.show("Version", "DEBUG")
	else:
		Debug.show("Version", "PROD")
