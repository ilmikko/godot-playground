extends Control

onready var canvas = $CanvasLayer
onready var camera = get_viewport().get_camera()

const Stats = preload("res://UI/Stats.tscn")

var stat_boxes = {}

func display_stats(id, position, current_stats):
	if abs(position.y - camera.global_transform.origin.y) > 20:
		return
	var stats_box = Stats.instance()
	stat_boxes[id] = stats_box
	stats_box.set_stats(current_stats)
	stats_box.rect_global_position = camera.unproject_position(position)
	add_child(stats_box)

func connect_stats_update_signal(id, signal_emitter, signal_name):
	if not stat_boxes.has(id):
		return
	stat_boxes[id].set_stats_updated_signal(signal_emitter, signal_name)

func hide_stats(id):
	if not stat_boxes.has(id):
		return
	var stats_box = stat_boxes[id]
	remove_child(stats_box)
	stat_boxes.erase(id)
