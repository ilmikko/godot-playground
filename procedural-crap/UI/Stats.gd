extends Control

func set_stats_updated_signal(resource, signal_name):
	resource.connect(signal_name, self, "_on_stats_updated")

func set_stats(stats):
	var text = ""
	for stat in stats:
		text += "[b]" + stat + "[/b]: " + str(stats[stat]) + "\n"
	$Text.set_bbcode(text)

func _on_stats_updated(stats):
	set_stats(stats)
