extends Spatial

signal camera_moved

const CAMERA_SPEED = 1.0
const CAMERA_ZOOM_SPEED = 1.0
const CAMERA_ZOOM_FALLOFF = 1.1

onready var kinematicBody = $"KinematicBody"

var zoom = 0.0

func _input(event):
	if (!event is InputEventMouseButton):
		return

	if Input.is_action_just_released("ui_scroll_up"):
		zoom = -CAMERA_ZOOM_SPEED
	if Input.is_action_just_released("ui_scroll_down"):
		zoom = CAMERA_ZOOM_SPEED

func _physics_process(_delta):
	var direction = Vector2(0, 0)

	if Input.is_action_pressed("ui_left"):
		direction.x -= CAMERA_SPEED
	if Input.is_action_pressed("ui_right"):
		direction.x += CAMERA_SPEED
	if Input.is_action_pressed("ui_up"):
		direction.y -= CAMERA_SPEED
	if Input.is_action_pressed("ui_down"):
		direction.y += CAMERA_SPEED

	direction = direction.normalized()
	
	var translate = Vector3(direction.x, zoom, direction.y)
	
	zoom /= CAMERA_ZOOM_FALLOFF

	kinematicBody.move_and_collide(translate)
	emit_signal("camera_moved", kinematicBody.global_transform)
