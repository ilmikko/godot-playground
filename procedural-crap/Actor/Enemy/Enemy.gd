extends KinematicBody

const ENEMY_SPEED = 1.0

onready var Direction = $"Direction"

func _physics_process(delta):
	move_and_collide(-Direction.global_transform.basis.z * ENEMY_SPEED * delta)

func _ai_process(delta):
	var v = nearest_villager()

	if v == null:
		return
	
	Direction.look_at(v.global_transform.origin, Direction.global_transform.basis.y)

func kill():
	var dead = preload("res://Actor/Enemy/EnemyDead.tscn").instance()
	
	var head = dead.get_node("Head")
	var body = dead.get_node("Body")
	
	dead.global_transform = global_transform
	head.transform = Direction.transform
	
	get_parent().add_child_below_node(self, dead)
	queue_free()
	
	body.add_force(Vector3(0, 0.8, 0), Vector3(0, 0, -180.0))
	body.add_force(Vector3(0, 0, 0), Vector3(0, 0, -180.0))

func nearest_villager():
	var villagers = get_tree().get_nodes_in_group("villager")
	if villagers.size() == 0:
		return null

	var nearest = villagers[0]
	var nearestDistance = INF
	for v in villagers:
		var distance = v.global_transform.origin.distance_to(global_transform.origin)
		if distance < nearestDistance:
			nearest = v
			nearestDistance = distance
	
	return nearest

func _on_Timer_timeout():
	_ai_process(0)
