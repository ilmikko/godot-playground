extends "res://Actor/Goals/Goal.gd"

func _init(actor).(actor): pass

func velocity():
	return -_actor.global_transform.basis.z*2

func update_direction():
	var nearest_shelter = _closest_shelter_within_view()
	if nearest_shelter == null:
		return
	var direction = nearest_shelter.transform.origin
	_actor.look_at(direction, _actor.global_transform.basis.y)

func encountered_body(body):
	if body.is_in_group("shelter"):
		var entered_shelter = body.add_occupant()
		if entered_shelter:
			_actor.queue_free()

func _closest_shelter_within_view():
	var shelters = _actor.get_tree().get_nodes_in_group("shelter")
	if shelters.size() == 0:
		return null
	var nearest_shelter = shelters[0]
	var nearest_shelter_distance = INF
	var global_position = _actor.global_transform.origin
	for shelter in shelters:
		var distance = shelter.global_transform.origin.distance_to(global_position) < nearest_shelter_distance
		nearest_shelter = shelter
		nearest_shelter_distance = distance

	return nearest_shelter
