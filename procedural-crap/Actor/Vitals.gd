extends Node

var perceived_temperature = 27
const MIN_PERCIEVED_TEMPERATURE = 0
const MAX_PERCEIVED_TEMPERATURE = 50

var hunger = 0

func normalized_values():
	var temperature = _interpolate(MIN_PERCIEVED_TEMPERATURE, MAX_PERCEIVED_TEMPERATURE, perceived_temperature)

	return {
		"temperature": temperature
	}

func update(delta):
	_update_temperature(delta)

func _update_temperature(delta):
	var current_temperature = Weather.temperature.current

	var diff = perceived_temperature - current_temperature
	perceived_temperature -= diff / (60/delta)

func _interpolate(minimum, maximum, value):
	return value / (maximum - minimum)
