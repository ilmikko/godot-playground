extends KinematicBody

var velocity = Vector3(0, 0, 0)

signal vitals_updated(stats)

const SeekShelter = preload("res://Actor/Goals/SeekShelter.gd")
const DummyGoal = preload("res://Actor/Goals/DummyGoal.gd")

onready var gravity = -ProjectSettings.get_setting("physics/3d/default_gravity")
onready var vitals = preload("res://Actor/Vitals.gd").new()
onready var ui = get_tree().get_root().get_node("Production/UI")

var current_goal = DummyGoal.new(self)

func _physics_process(_delta):
	velocity = current_goal.velocity()

	# velocity.y = gravity if is_on_floor() else velocity.y + gravity

	var _velocity = move_and_slide(velocity, Vector3.UP)

func _on_Timer_timeout():
	_pick_new_goal()
	vitals.update(1)
	emit_signal("vitals_updated", vitals.normalized_values())
	current_goal.update_direction()


func _pick_new_goal():
	if vitals.perceived_temperature < 20 or vitals.perceived_temperature > 35:
		current_goal = SeekShelter.new(self)


func _die():
	var dead = preload("res://Actor/Villager/VillagerDead.tscn").instance()
	
	dead.global_transform = global_transform
	dead.transform = transform
	
	get_parent().add_child_below_node(self, dead)
	queue_free()
	
	var direction = Vector3(randf()*1.0, randf()*1.0, randf()*1.0)
	dead.add_force(Vector3(0, 0.8, 0), direction * 100.0)

func _on_Area_body_entered(body):
	if body.is_in_group("void"):
		queue_free()
	if body.is_in_group("enemy"):
		_die()
	current_goal.encountered_body(body)

func _on_Area_mouse_entered():
	var position = global_transform.origin
	var current_vitals = vitals.normalized_values()
	ui.display_stats(self, position, current_vitals)
	ui.connect_stats_update_signal(self, self, "vitals_updated")

func _on_Area_mouse_exited():
	ui.hide_stats(self)
