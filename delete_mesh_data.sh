#!/usr/bin/env bash
# Cleans up VolatileMultiMesh data from Godot projects.
cd $(dirname $0);

all_scenes() {
	find . -name '*.tscn';
}

clean_up() {
	while read SCENE; do
		[ -f "$SCENE" ] || continue;
		( cat "$SCENE"; echo; ) | mark_for_deletion | delete_if_marked > "$SCENE.tmp";
		OLD_SIZE=$(get_size "$SCENE");
		NEW_SIZE=$(get_size "$SCENE.tmp");
		if [ "$NEW_SIZE" -lt "$OLD_SIZE" ]; then
			mv "$SCENE.tmp" "$SCENE";
			echo "Cleaned up $SCENE ($((OLD_SIZE - NEW_SIZE + 1)) byte(s) removed)";
		else
			rm "$SCENE.tmp";
			echo "Untouched  $SCENE (OK)";
		fi
	done
}

delete_if_marked() {
	awk '{ if ($1 == 1) { if ($2 == "custom_data_array" || $2 == "transform_array") { next; } } sub("^[01] ", "", $0); print $0 }';
}

diff_scenes() {
	git diff --name-only HEAD | grep '.tscn$'
}

get_size() {
	stat --format %s "$@";
}

mark_for_deletion() {
	awk 'BEGIN { delete_mesh_data=0; } { block[i]=$0; i++ } /delete_mesh_data = true/ { delete_mesh_data=1; } /^\s*$/ { for (line in block) { print delete_mesh_data,block[line]; } delete block; delete_mesh_data=0; }';
}

case $@ in
	-all|--all)
		all_scenes | clean_up;
		;;
	-diff|--diff)
		diff_scenes | clean_up;
		;;
	-*)
		echo "Unknown command: $@" 1>&2;
		exit 1;
		;;
	*)
		diff_scenes | clean_up;
		;;
esac
