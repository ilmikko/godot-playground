tool
extends MultiMeshInstance

const GrassFactory = preload("res://script/factory/GrassFactory.gd")

export(float) var patch_size = 5.0 setget _set_patch_size
func _set_patch_size(_patch_size):
	patch_size = _patch_size
	rebuild()

export(int) var count = 1000 setget _set_count
func _set_count(_count):
	count = _count
	rebuild()

export(Vector2) var width_min_max = Vector2(0.01, 0.02) setget _set_width_min_max
func _set_width_min_max(_width_min_max):
	width_min_max = _width_min_max
	rebuild()

export(Vector2) var height_min_max = Vector2(0.04, 0.08) setget _set_height_min_max
func _set_height_min_max(_height_min_max):
	height_min_max = _height_min_max
	rebuild()

func rebuild():
	if !multimesh:
		multimesh = VolatileMultiMesh.new()
		# Set format for multimesh data.
		multimesh.transform_format = MultiMesh.TRANSFORM_3D
		multimesh.set_custom_data_format(MultiMesh.CUSTOM_DATA_FLOAT)
		multimesh.set_color_format(MultiMesh.COLOR_NONE)

	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh = GrassFactory.grass_blade()
	multimesh.instance_count = count

	for index in (multimesh.instance_count):
		var basis = Basis(Vector3.UP, randf()*TAU)
		var origin = Vector3(rand_range(-patch_size, patch_size), 0.0, rand_range(-patch_size, patch_size))
		multimesh.set_instance_transform(index, Transform(basis, origin))
		multimesh.set_instance_custom_data(index, Color(
			rand_range(width_min_max.x, width_min_max.y),
			rand_range(height_min_max.x, height_min_max.y),
			0, # Unused channel
			0  # Unused channel
		))

func _init():
	rebuild()

func _ready():
	rebuild()
