extends MultiMeshInstance

onready var CollisionTop = $StaticBody/CollisionTop
var BlobMesh = preload("res://mesh/tree/Blob.tscn").instance()

export(int) var blob_count = 5 setget _set_blob_count
func _set_blob_count(_blob_count):
	blob_count = _blob_count
	rebuild()

export(float, 0.0, 2.0) var radius_max = 1.0 setget _set_radius_max
func _set_radius_max(_radius_max):
	radius_max = _radius_max
	rebuild()

export(float, 0.0, 1.0) var scale_variation = 0.5 setget _set_scale_variation
func _set_scale_variation(_scale_variation):
	scale_variation = _scale_variation
	rebuild()

func rebuild():
	if !multimesh:
		multimesh = MultiMesh.new()
		multimesh.transform_format = MultiMesh.TRANSFORM_3D

	var worldpos = Vector3(0, 0, 0)
	if is_inside_tree():
		if CollisionTop:
			# Calculate collisions.
			var radius = BlobMesh.get_aabb().size.length() / 2;
			var sphere = SphereShape.new()
			sphere.radius = radius_max + radius
			CollisionTop.shape = sphere

		# World pos is used for hashing.
		worldpos = global_transform.origin

	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh =  BlobMesh.mesh
	multimesh.instance_count = blob_count

	var offset = Vector3(0, 0, 0)
	for i in (multimesh.instance_count):
		var rand = GL.random3(worldpos+offset)
		offset = rand.normalized()
		var basis = Basis(Vector3.UP, sin(rand.length())*TAU)
		basis = basis.scaled(Vector3(1.0, 1.0, 1.0) + offset*scale_variation)
		var origin = Vector3(0.0, 6.0, 0.0)
		origin += offset * radius_max
		multimesh.set_instance_transform(i, Transform(basis, origin))

func _ready():
	rebuild()

func _init():
	rebuild()
