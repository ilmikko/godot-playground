shader_type spatial;

// The billboard shader keeps the vertices facing a certain direction regardless
// of camera positioning.
void vertex() {
	vec3 pos = VERTEX.xzy; // Rearrange axes to determine rotation
	pos *= vec3(1.0, -1.0, 1.0); // Multiply by negative 1 to invert normals
	VERTEX.xyz = (CAMERA_MATRIX * vec4(pos, 0.0)).xyz;
}