shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// cam_to_world translates from camera coordinates to world coordinates.
//  vec: Vector to translate
//  cam: Camera matrix (godot: CAMERA_MATRIX)
vec3 cam_to_world(vec3 vec, mat4 cam) {
	return (vec4(vec, 0) * cam).rgb;
}

// specular_phong calculates a Phong specular for a pixel.
//      shininess: Phong shininess factor.
//     reflection: Direction of normal reflection from light.
//  eye_direction: Direction of viewer's eye in camera coordinates.
vec3 specular_phong(float shininess, vec3 reflection, vec3 eye_direction) {
	float specular_intensity = max(dot(reflection, eye_direction), 0);
	return vec3(1.0) * pow(specular_intensity, shininess);
}

void light() {
	// Note: Godot's ATTENUATION has the 0..1 lighting calculation done already,
	// but I (ilmikko) want to learn about shading this myself, from the basics.
	
	// eye_direction points forwards out of the camera.
	vec3 eye_direction = vec3(0, 0, -1);
	// light_direction points towards the light source.
	vec3 light_direction = vec3(cos(TIME), 0, sin(TIME));
	// Use world angles, not camera angles
	light_direction = cam_to_world(light_direction, CAMERA_MATRIX);
	// Light bouncing off the normal
	vec3 reflection = reflect(light_direction, NORMAL);
	
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 specular = specular_phong(150.0, reflection, eye_direction);
	
	DIFFUSE_LIGHT = ambient + specular;
}