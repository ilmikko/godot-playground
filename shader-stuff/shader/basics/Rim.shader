shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// fresnel calculates the fresnel factor for a pixel.
// This is a value which will be closer to 1.0 if a normal is pointing towards
// the eye direction, and closer to 0.0 if it points further away.
//      shininess: Factor for fresnel falloff.
//  eye_direction: Direction of viewer's eye in camera coordinates.
//         normal: Direction of surface normal.
float fresnel(float shininess, vec3 eye_direction, vec3 normal) {
	float fresnel_intensity = max(dot(normal, -eye_direction), 0);
	return pow(fresnel_intensity, shininess);
}

void light() {
	vec3 eye_direction = vec3(0, 0, -1);
	
	float rim = 1.0 - fresnel(0.25, eye_direction, NORMAL);
	
	DIFFUSE_LIGHT = vec3(1.0) * rim;
}