shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// cam_to_world translates from camera coordinates to world coordinates.
//  vec: Vector to translate
//  cam: Camera matrix (godot: CAMERA_MATRIX)
vec3 cam_to_world(vec3 vec, mat4 cam) {
	return (vec4(vec, 0) * cam).rgb;
}

// diffuse calculates the diffuse colour for a pixel.
//           albedo: The base colour for this pixel in full light.
//  light_direction: Direction of light.
//           normal: Direction of surface normal.
vec3 diffuse(vec3 albedo, vec3 light_direction, vec3 normal) {
	float diffuse_intensity = max(dot(normal, light_direction), 0);
	return albedo * diffuse_intensity;
}

void light() {
	// Note: Godot's ATTENUATION has the 0..1 lighting calculation done already,
	// but I (ilmikko) want to learn about shading this myself, from the basics.
	
	// light_direction points towards the light source.
	vec3 light_direction = vec3(cos(TIME), 0, sin(TIME));
	// Use world angles, not camera angles
	light_direction = cam_to_world(light_direction, CAMERA_MATRIX);
	
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 diffuse = diffuse(ALBEDO, light_direction, NORMAL);
	
	DIFFUSE_LIGHT = ambient + diffuse;
}