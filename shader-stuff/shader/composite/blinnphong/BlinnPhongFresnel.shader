shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// cam_to_world translates from camera coordinates to world coordinates.
//  vec: Vector to translate
//  cam: Camera matrix (godot: CAMERA_MATRIX)
vec3 cam_to_world(vec3 vec, mat4 cam) {
	return (vec4(vec, 0) * cam).rgb;
}

// diffuse calculates the diffuse colour for a pixel.
//           albedo: The base colour for this pixel in full light.
//  light_direction: Direction of light.
//           normal: Direction of surface normal.
vec3 diffuse(vec3 albedo, vec3 light_direction, vec3 normal) {
	float diffuse_intensity = max(dot(normal, light_direction), 0);
	return albedo * diffuse_intensity;
}

// fresnel calculates the fresnel factor for a pixel.
// This is a value which will be closer to 1.0 if a normal is pointing towards
// the eye direction, and closer to 0.0 if it points further away.
//      shininess: Factor for fresnel falloff.
//  eye_direction: Direction of viewer's eye in camera coordinates.
//         normal: Direction of surface normal.
float fresnel(float shininess, vec3 eye_direction, vec3 normal) {
	float fresnel_intensity = max(dot(normal, -eye_direction), 0);
	return pow(fresnel_intensity, shininess);
}

// specular calculates a Blinn-Phong specular for a pixel.
//      shininess: Blinn-Phong shininess factor.
//        halfway: Direction to halfway point between eye_direction and light.
//         normal: Direction of surface normal.
vec3 specular(float shininess, vec3 halfway, vec3 normal) {
	float specular_intensity = max(dot(normal, halfway), 0);
	return vec3(1.0) * pow(specular_intensity, shininess);
}

void light() {
	// Note: Godot's ATTENUATION has the 0..1 lighting calculation done already,
	// but I (ilmikko) want to learn about shading this myself, from the basics.
	
	// eye_direction points forwards out of the camera.
	vec3 eye_direction = vec3(0, 0, -1);
	// light_direction points towards the light source.
	vec3 light_direction = vec3(cos(TIME), 0, sin(TIME));
	// Use world angles, not camera angles
	light_direction = cam_to_world(light_direction, CAMERA_MATRIX);
	// Vector pointing halfway between the light and eye vectors.
	vec3 halfway = normalize(light_direction - eye_direction);
	
	// Blinn-Phong shading is very similar to Phong, but uses a halfway vector
	// and the surface normal to calculate the light reflection.
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 diffuse = diffuse(ALBEDO, light_direction, NORMAL);
	vec3 specular = specular(150.0, halfway, NORMAL);
	// Use fresnel to fade out specular when we're not perpendicular to the
	// surface.
	specular *= fresnel(4.0, eye_direction, NORMAL);
	
	DIFFUSE_LIGHT = ambient + diffuse + specular;
}