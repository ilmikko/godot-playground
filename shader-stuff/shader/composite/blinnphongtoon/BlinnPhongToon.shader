shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// cam_to_world translates from camera coordinates to world coordinates.
//  vec: Vector to translate
//  cam: Camera matrix (godot: CAMERA_MATRIX)
vec3 cam_to_world(vec3 vec, mat4 cam) {
	return (vec4(vec, 0) * cam).rgb;
}

// step2 calculates a step function between two floats.
float step2(float a, float b, float f) {
	if (f < a) {
		return 0.0;
	} else if (f < b) {
		return 0.5;
	} else {
		return 1.0;
	}
}

// step3 calculates a step function between three floats.
float step3(float a, float b, float c, float f) {
	if (f < a) {
		return 0.0;
	} else if (f < b) {
		return 0.33;
	} else if (f < c) {
		return 0.66;
	} else {
		return 1.0;
	}
}


// fresnel calculates the fresnel factor for a pixel.
// This is a value which will be closer to 1.0 if a normal is pointing towards
// the eye direction, and closer to 0.0 if it points further away.
//      shininess: Factor for fresnel falloff.
//  eye_direction: Direction of viewer's eye in camera coordinates.
//         normal: Direction of surface normal.
float fresnel(float shininess, vec3 eye_direction, vec3 normal) {
	float fresnel_intensity = max(dot(normal, -eye_direction), 0);
	return pow(fresnel_intensity, shininess);
}

// diffuse_toon calculates the diffuse colour for a pixel with hard thresholds.
//           albedo: The base colour for this pixel in full light.
//  light_direction: Direction of light.
//           normal: Direction of surface normal.
vec3 diffuse_toon(vec3 albedo, vec3 light_direction, vec3 normal) {
	float diffuse_intensity = max(dot(normal, light_direction), 0);
	return albedo * step3(0.2, 0.5, 0.8, diffuse_intensity);
}

// specular_toon calculates a Blinn-Phong specular for a pixel with hard thresholds.
//      shininess: Blinn-Phong shininess factor.
//        halfway: Direction to halfway point between eye_direction and light.
//         normal: Direction of surface normal.
vec3 specular_toon(float shininess, vec3 halfway, vec3 normal) {
	float specular_intensity = max(dot(normal, halfway), 0);
	return vec3(1.0) * step(0.5, pow(specular_intensity, shininess));
}

void light() {
	// Note: Godot's ATTENUATION has the 0..1 lighting calculation done already,
	// but I (ilmikko) want to learn about shading this myself, from the basics.
	
	// eye_direction points forwards out of the camera.
	vec3 eye_direction = vec3(0, 0, -1);
	// light_direction points towards the light source.
	vec3 light_direction = vec3(cos(TIME), 0, sin(TIME));
	// Use world angles, not camera angles
	light_direction = cam_to_world(light_direction, CAMERA_MATRIX);
	// Vector pointing halfway between the light and eye vectors.
	vec3 halfway = normalize(light_direction - eye_direction);
	
	// Blinn-Phong shading is very similar to Phong, but uses a halfway vector
	// and the surface normal to calculate the light reflection.
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 diffuse = diffuse_toon(ALBEDO, light_direction, NORMAL);
	vec3 specular = specular_toon(250.0, halfway, NORMAL);
	// Use fresnel to fade out specular when we're not perpendicular to the
	// surface.
	specular *= fresnel(2.0, eye_direction, NORMAL);
	
	DIFFUSE_LIGHT = ambient + diffuse + specular;
}