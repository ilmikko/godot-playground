shader_type spatial;

uniform sampler2D albedo_texture;
uniform sampler2D normal_texture;

void fragment() {
	NORMALMAP = texture(normal_texture, UV).rgb;
	   ALBEDO = texture(albedo_texture, UV).rgb;
}

// cam_to_world translates from camera coordinates to world coordinates.
//  vec: Vector to translate
//  cam: Camera matrix (godot: CAMERA_MATRIX)
vec3 cam_to_world(vec3 vec, mat4 cam) {
	return (vec4(vec, 0) * cam).rgb;
}

// diffuse calculates the diffuse colour for a pixel.
//           albedo: The base colour for this pixel in full light.
//  light_direction: Direction of light.
//           normal: Direction of surface normal.
vec3 diffuse(vec3 albedo, vec3 light_direction, vec3 normal) {
	float diffuse_intensity = max(dot(normal, light_direction), 0);
	return albedo * diffuse_intensity;
}

// specular calculates a Phong specular for a pixel.
//      shininess: Phong shininess factor.
//     reflection: Direction of normal reflection from light.
//  eye_direction: Direction of viewer's eye in camera coordinates.
vec3 specular(float shininess, vec3 reflection, vec3 eye_direction) {
	float specular_intensity = max(dot(reflection, eye_direction), 0);
	return vec3(1.0) * pow(specular_intensity, shininess);
}

void light() {
	// Note: Godot's ATTENUATION has the 0..1 lighting calculation done already,
	// but I (ilmikko) want to learn about shading this myself, from the basics.
	
	// eye_direction points forwards out of the camera.
	vec3 eye_direction = vec3(0, 0, -1);
	// light_direction points towards the light source.
	vec3 light_direction = vec3(cos(TIME), 0, sin(TIME));
	// Use world angles, not camera angles
	light_direction = cam_to_world(light_direction, CAMERA_MATRIX);
	// Light bouncing off the normal
	vec3 reflection = reflect(light_direction, NORMAL);
	
	// Phong shading consists of these three factors.
	// It is fast and looks good enough for simple geometry, especially with
	// normal maps and high shininess factor.
	vec3 ambient = vec3(0.0, 0.0, 0.0);
	vec3 diffuse = diffuse(ALBEDO, light_direction, NORMAL);
	vec3 specular = specular(150.0, reflection, eye_direction);
	
	DIFFUSE_LIGHT = ambient + diffuse + specular;
}