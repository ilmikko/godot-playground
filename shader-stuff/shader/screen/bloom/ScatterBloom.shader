shader_type spatial;
render_mode unshaded, cull_disabled;

vec2 rand_2(vec2 p) {
	p = vec2(
		dot(p, vec2(-127.1284271, 9.2718)),
		dot(p, vec2(200.11842, -30042.32))
	);
	return fract(cos(p)*58392.9492122);
}

// blur_scatter applies a very fast scattering blur effect to a texture.
//     scatter: Amount of scatter for the blur effect.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_scatter(float scatter, sampler2D tex, vec2 uv) {
	return texture(tex, uv + scatter*(rand_2(uv)-0.5), scatter);
}

// apply_threshold_channel_max makes a colour 0 if none of its channels is above
// the threshold.
vec4 apply_threshold_channel_max(float threshold, vec4 col) {
	if (max(col.r, max(col.g, col.b)) < threshold) { col = vec4(0); }
	return col;
}

// bloom_scatter computes a bloom with a scatter blur for a texture.
//     scatter: Amount of scatter in the blurring effect.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to bloom.
//          uv: UV coordinates for the center pixel to bloom.
vec4 bloom_scatter(float scatter, float threshold, sampler2D tex, vec2 uv) {
	return apply_threshold_channel_max(threshold, blur_scatter(scatter, tex, uv));
}

void fragment() {
	vec3 screen = texture(SCREEN_TEXTURE, SCREEN_UV).xyz;
	float intensity = 32.0;
	
	vec3 bloom = bloom_scatter(0.025, 0.25, SCREEN_TEXTURE, SCREEN_UV).xyz;
	screen += bloom * bloom * bloom * bloom * bloom * intensity;
	
	ALBEDO = screen;
}
