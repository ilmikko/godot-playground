shader_type spatial;
render_mode unshaded, cull_disabled;

// apply_threshold_channel_max makes a colour 0 if none of its channels is above
// the threshold.
vec4 apply_threshold_channel_max(float threshold, vec4 col) {
	if (max(col.r, max(col.g, col.b)) < threshold) { col = vec4(0); }
	return col;
}

// bloom computes a simple bloom for a texture.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to bloom.
//          uv: UV coordinates for the center pixel to bloom.
vec4 bloom(float threshold, sampler2D tex, vec2 uv) {
	return apply_threshold_channel_max(threshold, texture(tex, uv));
}

void fragment() {
	vec3 screen = texture(SCREEN_TEXTURE, SCREEN_UV).xyz;
	float intensity = 40.0;
	
	vec3 bloom = bloom(0.8, SCREEN_TEXTURE, SCREEN_UV).xyz;
	screen += bloom * bloom * intensity;
	
	ALBEDO = screen;
}
