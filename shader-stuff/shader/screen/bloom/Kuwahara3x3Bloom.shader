shader_type spatial;
render_mode unshaded, cull_disabled;

// apply_threshold_channel_max makes a colour 0 if none of its channels is above
// the threshold.
vec4 apply_threshold_channel_max(float threshold, vec4 col) {
	if (max(col.r, max(col.g, col.b)) < threshold) { col = vec4(0); }
	return col;
}

// bloom computes a simple bloom for a texture.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to bloom.
//          uv: UV coordinates for the center pixel to bloom.
vec4 bloom(float threshold, sampler2D tex, vec2 uv) {
	return apply_threshold_channel_max(threshold, texture(tex, uv));
}

// find_color_variance_1 finds a mean colour of a 2x2 region, and a variance
// between that mean in the colours.
// The pixels are indexed from top-left ([0,0], [0,1], [1,0], [1,1]).
// This uses a vec4 to return both the variation (float) and mean colour (vec3).
vec4 find_color_variance_1(int separation, int x0, int y0, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 mean = vec4(0);
	
	vec4 sort[4];
	
	// Calculate mean as usual, storing the original colours as well.
	for (int y = 0; y <= 1; y++) {
		for (int x = 0; x <= 1; x++) {
			vec4 col = texture(tex, uv + vec2(ivec2(x0+x, y0+y)*separation)/texsize);
			sort[x+y*2] = col;
			mean += col;
		}
	}
	mean /= 4.0;
	
	// Calculate how much each colour varies from the mean.
	vec4 variance = vec4(0);
	for (int i = 0; i < 4; i++) {
		vec4 sm = sort[i]-mean;
		variance += sm * sm;
	}
	variance /= 4.0;
	
	return vec4(mean.rgb, length(variance));
}

// bloom_kuwahara_1 calculates a bloom with a 3x3 kuwahara filter for a texture.
//  separation: Separation between sample points.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec3 bloom_kuwahara_1(int separation, float threshold, sampler2D tex, vec2 uv) {
	float minVariance = 99999.0;
	
	vec3 col = vec3(0);
	for (int y = -1; y <= 0; y++) {
		for (int x = -1; x <= 0; x++) {
			vec4 colvar = find_color_variance_1(separation, x, y, tex, uv);
			if (colvar.w < minVariance) {
				minVariance = colvar.w;
				col += apply_threshold_channel_max(threshold, colvar).rgb;
			}
		}
	}
	
	return col;
}

void fragment() {
	vec3 screen = texture(SCREEN_TEXTURE, SCREEN_UV).xyz;
	float intensity = 4.0;
	
	vec3 bloom = bloom_kuwahara_1(15, 0.6, SCREEN_TEXTURE, SCREEN_UV).xyz;
	screen += bloom * bloom * intensity;
	
	ALBEDO = screen;
}
