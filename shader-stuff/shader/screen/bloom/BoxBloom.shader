shader_type spatial;
render_mode unshaded, cull_disabled;

// apply_threshold_channel_max makes a colour 0 if none of its channels is above
// the threshold.
vec4 apply_threshold_channel_max(float threshold, vec4 col) {
	if (max(col.r, max(col.g, col.b)) < threshold) { col = vec4(0); }
	return col;
}

// bloom computes a simple bloom for a texture.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to bloom.
//          uv: UV coordinates for the center pixel to bloom.
vec4 bloom(float threshold, sampler2D tex, vec2 uv) {
	return apply_threshold_channel_max(threshold, texture(tex, uv));
}

// bloom_box computes a box blurred bloom for a texture.
//        size: Half of the length of the sample box.
//              A box size of 2 will have 2*2+1 = 25 samples.
//  separation: Amount of separation between sample points to create more blur
//              without the need of increasing samples.
//   threshold: Values below this will not be lit in the bloom filter.
//         tex: Texture to bloom.
//          uv: UV coordinates for the center pixel to bloom.
vec4 bloom_box(int size, int separation, float threshold, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 bcol = vec4(0);
	for (int y = -size; y <= size; y++) {
		for (int x = -size; x <= size; x++) {
			bcol += bloom(threshold, tex, uv + vec2(ivec2(x, y) * separation)/texsize);
		}
	}
	float side = float(size * 2 + 1);
	bcol /= side * side;
	return bcol;
}

void fragment() {
	vec3 screen = texture(SCREEN_TEXTURE, SCREEN_UV).xyz;
	float intensity = 40.0;
	
	vec3 bloom = bloom_box(2, 2, 0.8, SCREEN_TEXTURE, SCREEN_UV).xyz;
	screen += bloom * bloom * intensity;
	
	ALBEDO = screen;
}
