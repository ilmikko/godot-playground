shader_type spatial;
render_mode unshaded, cull_disabled;

vec4 chromatic_aberration(vec2 direction, float radius, sampler2D tex, vec2 uv) {
	vec4 col = vec4(0);
	
	col += texture(tex, uv + direction * radius) * vec4(0.75, 0.0,  0.0, 1.0);
	col += texture(tex, uv)                      * vec4(0.25, 1.0, 0.25, 1.0);
	col += texture(tex, uv - direction * radius) * vec4( 0.0, 0.0, 0.75, 1.0);
	
	return col;
}

void fragment() {
	vec3 screen = chromatic_aberration(vec2(1.0, 0.0), 0.005, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}