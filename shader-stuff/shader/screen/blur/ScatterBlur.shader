shader_type spatial;
render_mode unshaded, cull_disabled;

vec2 rand_2(vec2 p) {
	p = vec2(
		dot(p, vec2(-127.1284271, 9.2718)),
		dot(p, vec2(200.11842, -30042.32))
	);
	return fract(cos(p)*58392.9492122);
}

// blur_scatter applies a very fast scattering blur effect to a texture.
//     scatter: Amount of scatter for the blur effect.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_scatter(float scatter, sampler2D tex, vec2 uv) {
	return texture(tex, uv + scatter*(rand_2(uv)-0.5), scatter);
}

void fragment() {
	vec3 screen = blur_scatter(0.005, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}