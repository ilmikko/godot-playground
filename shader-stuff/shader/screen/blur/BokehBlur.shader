shader_type spatial;
render_mode unshaded, cull_disabled;

float rand_h2(vec2 a) {
	return fract(sin(dot(a, vec2(-127.1284271, 9.2718)))*489321.493);
}

void add(sampler2D tex, vec2 uv, float factor, inout vec4 acol, inout vec4 weight) {
	vec4 col = texture(tex, uv);
	// Fake gamma as 2, as it's quick to compute here.
	col *= col;
	
	// Quadruple power for the bokeh effect.
	// TODO: Move this away to a bloom filter.
	vec4 col4 = col * col; col4 *= col4;
	vec4 bokeh = vec4(1.0) + col4 * factor;
	acol += col * bokeh;
	weight += bokeh;
}

// blur_bokeh approximates a Bokeh blur effect on a texture.
//  iterations: Amount of samples taken per pixel.
//      radius: Bokeh blur radius.
//      factor: Factor of the bokeh effect.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_bokeh(float iterations, float radius, float factor, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec2 scale = 1.0/texsize;
	
	float corners = 5.;
	float twist = 0.1;
	float stretch = 0.9;
	// Add randomness for larger radii to break the spiral pattern.
	float randomness = 0.3/radius;
	
	scale *= radius;
	float part = 6.28318530718/iterations;
	
	vec4 col = vec4(0);
	vec4 weight = vec4(0);
	
	// Always add the middle.
	add(tex, uv, factor, col, weight);
	
	for (float i = 0.0; i < iterations; i++) {
		float p = (i/iterations)*stretch;
		float angle = part * i * iterations/corners + p*i*twist;
		p = 1.0 - p*p;
		vec2 len = scale * p;
		vec2 offset = vec2(cos(angle), sin(angle)) * len;
		offset += rand_h2(uv)*scale*randomness;
		add(tex, uv + offset, factor, col, weight);
	}
	
	col /= weight;
	col = sqrt(col);
	return col;
}

void fragment() {
	vec3 screen = blur_bokeh(50, 20.0, 128.0, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}