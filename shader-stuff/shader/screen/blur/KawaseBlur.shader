shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_kawase calculates the Kawase blur for a texture. It will perform four
// calculations per iteration, for each corner of the square, so sample count
// will be four times iterations.
//  iterations: Amount of iterations. Value of 2 will have 2*4 = 8 samples.
//  separation: Amount of separation between sample points to create more blur
//              without the need of increasing samples.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_kawase(float iterations, float separation, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec2 scale = 1.0/texsize;
	vec2 scale_bias = scale*0.5;
	// Tweak LOD as appropriate.
	float lod = separation*0.25;
	
	vec4 col = vec4(0);
	for (float i = 0.0; i < iterations; i++) {
		vec2 duv = i*separation*scale+scale_bias;
		col += texture(tex, uv + duv.xy               , lod);
		col += texture(tex, uv + duv.xy * vec2(-1,  1), lod);
		col += texture(tex, uv + duv.xy * vec2(-1, -1), lod);
		col += texture(tex, uv + duv.xy * vec2( 1, -1), lod);
	}
	col /= (iterations * 4.);
	return col;
}

void fragment() {
	vec3 screen = blur_kawase(8, 2.5, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}