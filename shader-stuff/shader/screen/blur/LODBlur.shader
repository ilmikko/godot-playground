shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_lod applies a cheap blur based on texture LOD.
//         lod: Amount of LOD blur to pass to texture.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_lod(float lod, sampler2D tex, vec2 uv) {
	return texture(tex, uv, lod);
}

void fragment() {
	vec3 screen = blur_lod(2.0, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}