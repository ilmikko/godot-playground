shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_circle applies a fast circle blur effect to a texture.
// It can convincingly simulate lens blur in some conditions.
//  iterations: Amount of iterations / points on the circle arc.
//              This is performance bound, so prefer to keep it low.
//      radius: Circle blur radius. Not performance bound.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_circle(float iterations, float radius, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec2 scale = vec2(radius) / texsize;
	
	float weight = 1.0/iterations;
	float part = 6.28318530718/iterations;
	
	vec4 col = vec4(0);
	for (float i = 0.0; i < iterations; i++) {
		float angle = part * i;
		vec2 offset = vec2(cos(angle), sin(angle));
		col += texture(tex, uv + offset * scale) * weight;
	}
	return col;
}

void fragment() {
	vec3 screen = blur_circle(25.0, 10.0, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}