shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_box calculates a box blur for a texture.
// It's a fast blur to calculate keeping in mind the amount of samples taken.
//        size: Half of the length of the sample box.
//              A box size of 2 will have 2*2+1 = 25 samples.
//  separation: Amount of separation between sample points to create more blur
//              without the need of increasing samples.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_box(float size, float separation, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 bcol = vec4(0);
	for (float y = -size; y <= size; y++) {
		for (float x = -size; x <= size; x++) {
			bcol += texture(tex, uv + vec2(x, y) * separation/texsize);
		}
	}
	float side = size * 2.0 + 1.0;
	bcol /= side * side;
	return bcol;
}

void fragment() {
	vec3 screen = blur_box(1, 5, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}