shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_zoom applies a "zoom effect" on a texture, usually creating the
// illusion of speed.
//  iterations: Amount of iterations in the effect.
//              This is performance bound, so prefer to keep it low.
//      radius: Zoom radius.
//      center: Zoom center.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_zoom(float iterations, float radius, vec2 center, sampler2D tex, vec2 uv) {
	vec2 offset = uv - center;
	
	vec4 col = vec4(0);
	for (float i = 0.0; i < iterations; i++) {
		float p = i/iterations;
		float w = (1.0+i)/(1.0+iterations);
		col += texture(tex, uv - offset * p * radius) * w;
	}
	col /= iterations / 2.0;
	return col;
}

void fragment() {
	vec3 screen = blur_zoom(10.0, 0.5, vec2(0.5, 0.5), SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}