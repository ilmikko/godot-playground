shader_type spatial;
render_mode unshaded, cull_disabled;

// find_color_variance_2 finds a mean colour of a 3x3 region, and a variance
// between that mean in the colours.
// The pixels are indexed from top-left ([0,0], [0,1], [0,2], [1,0], ...).
// This uses a vec4 to return both the variation (float) and mean colour (vec3).
vec4 find_color_variance_2(int separation, int x0, int y0, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 mean = vec4(0);
	
	vec4 sort[9];
	
	// Calculate mean as usual, storing the original colours as well.
	for (int y = 0; y <= 2; y++) {
		for (int x = 0; x <= 2; x++) {
			vec4 col = texture(tex, uv + vec2(ivec2(2*x0+x, 2*y0+y)*separation)/texsize);
			sort[x+y*3] = col;
			mean += col;
		}
	}
	mean /= 9.0;
	
	// Calculate how much each colour varies from the mean.
	vec4 variance = vec4(0);
	for (int i = 0; i < 9; i++) {
		vec4 sm = sort[i]-mean;
		variance += sm * sm;
	}
	variance /= 9.0;
	
	return vec4(mean.rgb, length(variance));
}

// blur_kuwahara_2 calculates a 5x5 kuwahara filter for a texture.
// This means there are four subregions of 3x3 of which the variance is
// calculated.
//  separation: Separation between sample points.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec3 blur_kuwahara_2(int separation, sampler2D tex, vec2 uv) {
	float minVariance = 99999.0;
	
	vec3 col = vec3(0);
	for (int y = -1; y <= 0; y++) {
		for (int x = -1; x <= 0; x++) {
			vec4 colvar = find_color_variance_2(separation, x, y, tex, uv);
			if (colvar.w < minVariance) {
				minVariance = colvar.w;
				col = colvar.rgb;
			}
		}
	}
	
	return col;
}

void fragment() {
	vec3 screen = blur_kuwahara_2(5, SCREEN_TEXTURE, SCREEN_UV);

	ALBEDO = screen;
}