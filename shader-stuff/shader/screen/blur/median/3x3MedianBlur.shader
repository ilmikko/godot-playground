shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_median_1 calculates a fast 3x3 median blur for a texture.
// This is not mathematically correct, but achieves a good enough effect.
//  separation: Separation between sample points.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_median_1(int separation, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 sort[9];
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			sort[(x+1)+(y+1)*3] = texture(tex, uv + vec2(ivec2(x, y) * separation)/texsize);
		}
	}
	// Use a pregenerated network for fast sorting.
	// Best Known Arrangement with parallel operations
	// http://jgamble.ripco.net/cgi-bin/nw.cgi?inputs=9&algorithm=best&output=svg
	// [[0,1],[3,4],[6,7]]
	// [[1,2],[4,5],[7,8]]
	// [[0,1],[3,4],[6,7],[2,5]]
	// [[0,3],[1,4],[5,8]]
	// [[3,6],[4,7],[2,5]]
	// [[0,3],[1,4],[5,7],[2,6]]
	// [[1,3],[4,6]]
	// [[2,4],[5,6]]
	// [[2,3]]
	vec4 t = vec4(0);
	int a = 0, b = 0;
	
	a = 0; b = 1;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 3; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 6; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 1; b = 2;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 4; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 7; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 0; b = 1;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 3; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 6; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 2; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 0; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 1; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 5; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 3; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 4; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 2; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 0; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 1; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 5; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 2; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 1; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 4; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 2; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	a = 5; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	
	a = 2; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	return t;
}

void fragment() {
	vec3 screen = blur_median_1(5, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}