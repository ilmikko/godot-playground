shader_type spatial;
render_mode unshaded, cull_disabled;

// blur_median_2 calculates a fast 5x5 median blur for a texture.
// This is not mathematically correct, but achieves a good enough effect.
//  separation: Separation between sample points.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_median_2(int separation, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	vec4 sort[25];
	for (int y = -2; y <= 2; y++) {
		for (int x = -2; x <= 2; x++) {
			sort[(x+2)+(y+2)*5] = texture(tex, uv + vec2(ivec2(x, y) * separation)/texsize);
		}
	}
	// Use a pregenerated network for fast sorting.
	// Best Known Arrangement with parallel operations
	// http://jgamble.ripco.net/cgi-bin/nw.cgi?inputs=9&algorithm=best&output=svg
	// [[1,2],[4,5],[7,8],[10,11],[13,14],[16,17],[19,20],[21,22],[23,24]]
	// [[0,2],[3,5],[6,8],[9,11],[12,14],[15,17],[18,20],[21,23],[22,24]]
	// [[0,1],[3,4],[2,5],[6,7],[9,10],[8,11],[12,13],[15,16],[14,17],[18,19],[22,23],[20,24]]
	// [[0,3],[1,4],[6,9],[7,10],[5,11],[12,15],[13,16],[18,22],[19,23],[17,24]]
	// [[2,4],[1,3],[8,10],[7,9],[0,6],[14,16],[13,15],[18,21],[20,23],[11,24]]
	// [[2,3],[8,9],[1,7],[4,10],[14,15],[19,21],[20,22],[16,23]]
	// [[2,8],[1,6],[3,9],[5,10],[20,21],[12,19],[15,22],[17,23]]
	// [[2,7],[4,9],[12,18],[13,20],[14,21],[16,22],[10,23]]
	// [[2,6],[5,9],[4,7],[14,20],[13,18],[17,22],[11,23]]
	// [[3,6],[5,8],[14,19],[16,20],[17,21],[0,13],[9,22]]
	// [[5,7],[4,6],[14,18],[15,19],[17,20],[0,12],[8,21],[10,22]]
	// [[5,6],[15,18],[17,19],[1,14],[7,20],[11,22]]
	// [[16,18],[2,15],[1,12],[6,19],[8,20],[11,21]]
	// [[17,18],[2,14],[3,16],[7,19],[10,20]]
	// [[2,13],[4,17],[5,18],[8,19],[11,20]]
	// [[2,12],[5,17],[4,16],[3,13],[9,19]]
	// [[5,16],[3,12],[4,14],[10,19]]
	// [[5,15],[4,12],[11,19],[9,16],[10,17]]
	// [[5,14],[8,15],[11,18],[10,16]]
	// [[5,13],[7,14],[11,17]]
	// [[5,12],[6,13],[8,14],[11,16]]
	// [[6,12],[8,13],[10,14],[11,15]]
	// [[7,12],[9,13],[11,14]]
	// [[8,12],[11,13]]
	// [[9,12]]
	// [[10,12]]
	// [[11,12]]
	vec4 t = vec4(0);
	int a = 0, b = 0;
	
	a = 1; b = 2;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 11;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 13; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 16; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 19; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 21; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 23; b = 24;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 0; b = 2;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 6; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 11;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 12; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 15; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 18; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 21; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 22; b = 24;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 0; b = 1;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 2; b = 5;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 6; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 10;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 11;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 12; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 15; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 18; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 22; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 20; b = 24;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 0; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 6; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 10;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 11;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 12; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 13; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 18; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 19; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 24;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 4;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 10;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 0; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 13; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 18; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 20; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 24;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 3;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 10;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 19; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 20; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 16; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 10;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 20; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 12; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 15; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 12; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 13; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 16; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 9;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 13; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 23;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 3; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 8;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 16; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 0; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 7;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 14; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 15; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 0; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 6;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 15; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 17; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 22;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 16; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 2; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 1; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 6; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 21;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 17; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 2; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 20;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 2; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 5; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 3; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 4; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 19;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 18;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 7; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 17;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 5; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 6; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 16;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 6; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 8; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 10; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 15;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 7; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 9; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 14;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 8; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;

	a = 11; b = 13;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 9; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 10; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;


	a = 11; b = 12;
	t = max(sort[a],sort[b]);
	sort[a] = min(sort[a],sort[b]);
	sort[b] = t;
	
	return t;
}

void fragment() {
	vec3 screen = blur_median_2(5, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}