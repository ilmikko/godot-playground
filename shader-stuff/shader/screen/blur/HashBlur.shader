shader_type spatial;
render_mode unshaded, cull_disabled;

float rand(float r) {
	return sin(-30042.32*r+0.6213792);
}

float rand_h2(vec2 a) {
	return sin(dot(a, vec2(-127.1284271, 9.2718)));
}

// blur_hash applies a blur effect to a texture by hashing the sample multiple
// times. If iterations is one, it is roughly equivalent to ScatterBlur.
//  iterations: Amount of iterations (samples taken).
//        size: Amount of blur effect to apply.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_hash(float iterations, float size, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	float pixel = size/texsize.y;
	
	float r = rand_h2(uv);
	vec4 col = vec4(0.0);
	for (float i = 0.0; i < iterations; i++) {
		vec2 rv;
		r = rv.x = rand(r);
		r = rv.y = rand(r);
		col += texture(tex, uv+rv*pixel, pixel)/iterations;
	}
	return col;
}

void fragment() {
	vec3 screen = blur_hash(16, 2.0, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}