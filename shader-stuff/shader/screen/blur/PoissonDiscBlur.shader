shader_type spatial;
render_mode unshaded, cull_disabled;

float rand_h2(vec2 a) {
	return fract(sin(dot(a, vec2(-127.1284271, 9.2718)))*42180.512);
}

// rot2d rotates a vec2 by an angle.
vec2 rot2d(vec2 vec, float angle) {
	vec2 sc = vec2(sin(angle), cos(angle));
	return vec2(dot(vec, sc.yx * vec2(1, -1)), dot(vec, sc.xy));
}

// blur_poisson applies a poisson disc blur into a texture.
// the poisson disc is precalculated so the runtime is always static
// (27 iterations).
//      radius: Blur radius.
//         tex: Texture to blur.
//          uv: UV coordinates for the center pixel to blur around.
vec4 blur_poisson(float radius, sampler2D tex, vec2 uv) {
	vec2 texsize = vec2(textureSize(tex, 0));
	float scale = 1.0/texsize.y;
	
	vec2 poisson_samples[27];
	poisson_samples[0]  = vec2(-0.8835609,  2.5233910);
	poisson_samples[1]  = vec2(-1.3873750,  1.0563180);
	poisson_samples[2]  = vec2(-2.8544520,  1.3136450);
	poisson_samples[3]  = vec2( 0.6326182,  1.1456900);
	poisson_samples[4]  = vec2( 1.3315150,  3.6372970);
	poisson_samples[5]  = vec2(-2.1753070,  3.8857950);
	poisson_samples[6]  = vec2(-0.5396664,  4.1938000);
	poisson_samples[7]  = vec2(-0.6708734, -0.3687500);
	poisson_samples[8]  = vec2(-2.0839080, -0.6921188);
	poisson_samples[9]  = vec2(-3.2190280,  2.8546500);
	poisson_samples[10] = vec2(-1.8639330, -2.7422540);
	poisson_samples[11] = vec2(-4.1257390, -1.2830280);
	poisson_samples[12] = vec2(-3.3767660, -2.8184400);
	poisson_samples[13] = vec2(-3.9745530,  0.5459405);
	poisson_samples[14] = vec2( 3.1025140,  1.7176920);
	poisson_samples[15] = vec2( 1.3394100, -0.1663950);
	poisson_samples[16] = vec2( 2.9518870,  3.1866240);
	poisson_samples[17] = vec2( 2.8147270, -0.3216669);
	poisson_samples[18] = vec2( 0.7786853, -2.2356390);
	poisson_samples[19] = vec2(-0.7396695, -1.7024660);
	poisson_samples[20] = vec2( 0.4621856, -3.6252500);
	poisson_samples[21] = vec2( 4.1815410,  0.5883132);
	poisson_samples[22] = vec2( 4.2224400, -1.1102900);
	poisson_samples[23] = vec2( 2.1169170, -1.7894360);
	poisson_samples[24] = vec2( 1.9157740, -3.4258850);
	poisson_samples[25] = vec2( 3.1426860, -2.6563290);
	poisson_samples[26] = vec2(-1.1086320, -4.0234790);
	
	vec4 col = vec4(0);
	float r = 6.28318530718 * rand_h2(uv);
	for (int i = 0; i < 27; i++) {
		vec2 offset = rot2d(poisson_samples[i], r);
		col += texture(tex, uv + radius * offset * scale, radius * scale);
	}
	col /= 27.0;
	return col;
}

void fragment() {
	vec3 screen = blur_poisson(2.0, SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}