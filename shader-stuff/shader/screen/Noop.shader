shader_type spatial;
render_mode unshaded, cull_disabled;

void fragment() {
	vec3 screen = texture(SCREEN_TEXTURE, SCREEN_UV).xyz;

	ALBEDO = screen;
}