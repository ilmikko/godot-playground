shader_type spatial;

vec2 random2(vec2 p) {
	vec2 a = vec2(20.0, -4.0);
	vec2 b = vec2(0.3, -146.3);
	vec2 some = vec2(
		dot(p, a),
		dot(p, b)
	);
	return fract(sin(some));
}

float worley2(vec2 p) {
	float dist = 1.0;
	vec2 i_p = floor(p);
	vec2 f_p = fract(p);
	for (int y=-1;y<=1;y++) {
		for (int x=-1;x<=1;x++) {
			vec2 xy = vec2(float(x), float(y));
			vec2 rp = random2(i_p + xy);
			vec2 diff = xy + rp - f_p;
			dist = min(dist, length(diff));
		}
	}
	return dist;
}

void fragment() {
	vec2 direction = vec2(1.0, 0.0);
	ALBEDO = vec3(worley2((UV * 2.0) + (TIME * direction)));
}