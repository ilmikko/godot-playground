shader_type spatial;

void fragment() {
	float dist = length(UV - vec2(0.5));
	dist = 1.0 - smoothstep(0.35, 0.4, dist);
	ALBEDO = vec3(dist);
}