shader_type spatial;

void fragment() {
	float dist = max(max(UV.x, UV.y), max(1.0 - UV.x, 1.0 - UV.y)) - 0.5;
	dist = 1.0 - step(0.4, dist);
	ALBEDO = vec3(dist);
}