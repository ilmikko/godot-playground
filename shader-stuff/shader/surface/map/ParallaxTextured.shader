shader_type spatial;

uniform sampler2D tex;
uniform sampler2D depthmap;

// parallax computes a parallax effect on a UV map given a certain depth.
//    depth: The depth of the parallax effect.
//     view: View vector in tangent space (see below).
vec2 parallax(float depth, vec3 view) {
    return view.xy / view.z * -depth;
}

void fragment() {
	// NOT SURE WHY -BINORMAL IS NEEDED HERE.
	// I spent O(hour) trying to understand why the sample described on the
	// openGL website did not work as expected, but this is what you need to
	// use instead (if you find out why, please let me know).
	mat3 tbn = mat3(TANGENT, -BINORMAL, NORMAL);
	// convert vertex space to tangent space.
	vec3 view = normalize(-VERTEX*tbn);
	
	vec2 uv = UV;
	uv += parallax(texture(depthmap, uv).r * 0.05, view);
	ALBEDO = texture(tex, uv).rgb;
}