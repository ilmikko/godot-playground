shader_type spatial;

uniform sampler2D tex;
uniform sampler2D depthmap;

// parallax_steep computes a parallax effect on a UV map with multiple
// iterations.
//  iterations: The amount of layers to have.
//       depth: The offset between each layer.
//        view: View vector in tangent space (see below).
//          uv: UV map to apply parallax effect to.
vec2 parallax_steep(float iterations, float offset, vec3 view, vec2 uv) {
	float layer_depth = 1.0/iterations;
	vec2 delta_uv = view.xy/view.z/iterations;
	
	vec2  current_uv    = uv;
	float current_depth = texture(depthmap, current_uv).r;
	
	for (float depth = 0.0; depth < current_depth; depth += layer_depth) {
	    // shift texture coordinates along direction of P
	    current_uv -= delta_uv * offset;
	    // get depthmap value at current texture coordinates
	    current_depth = texture(depthmap, current_uv).r;
	}
	
	return current_uv;
}

void fragment() {
	// NOT SURE WHY -BINORMAL IS NEEDED HERE.
	// I spent O(hour) trying to understand why the sample described on the
	// openGL website did not work as expected, but this is what you need to
	// use instead (if you find out why, please let me know).
	mat3 tbn = mat3(TANGENT, -BINORMAL, NORMAL);
	// convert vertex space to tangent space.
	vec3 view = normalize(-VERTEX*tbn);
	
	vec2 uv = UV;
	uv = parallax_steep(5.0, 0.1, view, uv);
	ALBEDO = texture(tex, uv).rgb;
}