shader_type spatial;

// depth_box ray traces a box based on a depth (z value), view direction and uv
// coordinates of a surface.
//     depth: The depth of this box (away from eye into the surface normal)
//      view: The view direction
//        uv: UV map of the surface we're viewing "through"
vec3 depth_box(float depth, vec3 view, vec2 uv) {
	vec3 pos = vec3(uv * 2.0 - 1.0, depth);
	
	// No need to ray trace, also prevents div by 0 below.
	if (depth == 0.0) {
		return pos;
	}
	
	// Ray trace box
	vec3 id = vec3(1.0, 1.0, 2.0*depth/(1.0+depth)) / view;
	vec3 k = abs(id) - pos*id;
	pos += min(k.x, min(k.y, k.z)) * view;
	// Re-map the z coordinates according to depth.
	pos.z *= 1.0/depth;
	return pos;
}

void fragment() {
	// NOT SURE WHY -BINORMAL IS NEEDED HERE.
	// I spent O(hour) trying to understand why the sample described on the
	// openGL website did not work as expected, but this is what you need to
	// use instead (if you find out why, please let me know).
	mat3 tbn = mat3(TANGENT, -BINORMAL, NORMAL);
	// convert vertex space to tangent space.
	vec3 view = normalize(VERTEX*tbn);
	
	float tiling = 1.0;
	vec3 pos = depth_box(1.0, view, fract(UV*tiling));
	
	ALBEDO = vec3(pos);
}