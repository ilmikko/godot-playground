shader_type spatial;

// texture_depth specifies how the cubemap texture should be treated;
// see res://asset/texture/test/cubemap{2,4} for different depths.
uniform float texture_depth = 1.0;
uniform sampler2D tex;

// depth_box ray traces a box based on a depth (z value), view direction and uv
// coordinates of a surface.
//     depth: The depth of this box (away from eye into the surface normal)
//      view: The view direction
//        uv: UV map of the surface we're viewing "through"
vec3 depth_box(float depth, vec3 view, vec2 uv) {
	vec3 pos = vec3(uv * 2.0 - 1.0, depth);
	
	// No need to ray trace, also prevents div by 0 below.
	if (depth == 0.0) {
		return pos;
	}
	
	// Ray trace box
	vec3 id = vec3(1.0, 1.0, 2.0*depth/(1.0+depth)) / view;
	vec3 k = abs(id) - pos*id;
	pos += min(k.x, min(k.y, k.z)) * view;
	// Re-map the z coordinates according to depth.
	pos.z *= 1.0/depth;
	return pos;
}

// cubemap_interior maps normalized texture coordinates to a depth box for an
// interior shader.
// In order to render into a texture, the camera FOV will need to be 53.13
// degrees (equivalent to atan(0.5))
//     depth: The depth of this box (away from eye into the surface normal)
//      view: The view direction
//        uv: UV map of the surface we're viewing "through"
vec2 cubemap_interior(float depth, vec3 view, vec2 uv) {
	vec3 pos = depth_box(depth, view, uv);
	
	// Map perspective in texture to walls.
	float d = 2.0 + 1.0/texture_depth;
	float p = (0.5 / (pos.z + d)) - 0.5;
	// Normalize the side walls to a linear scale.
	// TODO: Isn't 100% correct, check if you can find out why
	float w = (1.0+(pos.z))*0.5;
	p *= (1.0 + (w*w)/d);
	return 0.5 + pos.xy * p;
}

void fragment() {
	// NOT SURE WHY -BINORMAL IS NEEDED HERE.
	// I spent O(hour) trying to understand why the sample described on the
	// openGL website did not work as expected, but this is what you need to
	// use instead (if you find out why, please let me know).
	mat3 tbn = mat3(TANGENT, -BINORMAL, NORMAL);
	// convert vertex space to tangent space.
	vec3 view = normalize(VERTEX*tbn);
	
	float tiling = 2.0;
	vec2 uv = cubemap_interior(0.5, view, fract(UV*tiling));
	ALBEDO = texture(tex, uv).rgb;
}