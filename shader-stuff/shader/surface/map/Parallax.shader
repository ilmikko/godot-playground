shader_type spatial;

// parallax computes a parallax effect on a UV map given a certain depth.
//    depth: The depth of the parallax effect.
//     view: View vector in tangent space (see below).
//       uv: UV map to apply parallax effect to.
vec2 parallax(float depth, vec3 view) {
    return view.xy / view.z * -depth;
}

void fragment() {
	// NOT SURE WHY -BINORMAL IS NEEDED HERE.
	// I spent O(hour) trying to understand why the sample described on the
	// openGL website did not work as expected, but this is what you need to
	// use instead (if you find out why, please let me know).
	mat3 tbn = mat3(TANGENT, -BINORMAL, NORMAL);
	// convert vertex space to tangent space.
	vec3 view = normalize(-VERTEX*tbn);
	
	ALBEDO = vec3(UV + parallax(0.1, view), 0.0);
}