shader_type spatial;

// https://io7m.github.io/r2/documentation/images/normal.png
// Looking at this picture, NORMAL means the y axis along the surface.
// TANGENT, BINORMAL and NORMAL are the 3 "axes" that make up the normal.

void fragment() {
	ALBEDO = vec3(NORMAL);
}