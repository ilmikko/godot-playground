shader_type spatial;

// https://io7m.github.io/r2/documentation/images/normal.png
// Looking at this picture, BINORMAL means the x axis along the surface.
// Apparently BINORMAL and BITANGENT are the same thing (latter is not in Godot)
// TANGENT, BINORMAL and NORMAL are the 3 "axes" that make up the normal.

void fragment() {
	ALBEDO = vec3(BINORMAL);
}