shader_type spatial;

// https://io7m.github.io/r2/documentation/images/normal.png
// Looking at this picture, TANGENT means the z axis along the surface.
// TANGENT, BINORMAL and NORMAL are the 3 "axes" that make up the normal.

void fragment() {
	ALBEDO = vec3(TANGENT);
}