shader_type spatial;

vec2 random2(vec2 vec) {
	vec2 a = vec2(20.0, -4.0);
	vec2 b = vec2(0.3, -146.3);
	vec2 c = vec2(-2.9, -0.2);
	vec += c;
	vec2 some = vec2(
		dot(vec, a),
		dot(vec, b)
	);
	return sin(some);
}

vec3 random3(vec3 vec) {
	vec3 a = vec3(20.0, -4.0, 0.62);
	vec3 b = vec3(0.3, -146.3, 0.00515138);
	vec3 c = vec3(-2.9, -0.2, 80.3218);
	vec3 d = vec3(0.4124, 25.0321, -820.0214);
	vec += d;
	vec3 some = vec3(
		dot(vec, a),
		dot(vec, b),
		dot(vec, c)
	);
	return sin(some);
}

float worley2(vec2 p) {
	float dist = 1.0;
	vec2 i_p = floor(p);
	vec2 f_p = fract(p);
	for (int y=-1;y<=1;y++) {
		for (int x=-1;x<=1;x++) {
			vec2 xy = vec2(float(x), float(y));
			vec2 rp = random2(i_p + xy);
			vec2 diff = xy + rp - f_p;
			dist = min(dist, length(diff));
		}
	}
	return dist;
}

void vertex() {
	vec3 vertex = VERTEX;
	vertex += NORMAL*cos(vertex.x*TIME*2.0)*sin(vertex.y*TIME*0.4)*0.1;
	VERTEX = vertex;
	COLOR = vec4(0, 1, 0, 1.0);
}

void fragment() {
	ALBEDO = COLOR.rgb;
}