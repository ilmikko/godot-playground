shader_type spatial;
// Render both front and back
render_mode cull_disabled;

varying float wind;

// DO NOT TOUCH //
// These should be always constant. //
const vec3 UP = vec3(0, 1, 0);
const vec3 RIGHT = vec3(1, 0, 0);

const float PI = 3.1415926535897;
const float DEG2RAD = (PI / 180.0);
// END DO NOT TOUCH //

// TODO: Detach these and get them from "actual" wind speeds
uniform float wind_scale = 1.2;
uniform float wind_speed = 1.0;

uniform float deg_sway_pitch_max = 80.0;
uniform float deg_sway_yaw_max   = 25.0;
uniform vec2  sway_randomness = vec2(0.1, 0.2);

uniform float specular_level : hint_range(0.0, 1.0) = 1.0;

// REMEMBER TO NORMALISE
uniform vec3 wind_direction = vec3(0, 0, 1);

uniform vec4 color_top : hint_color = vec4(1, 1, 1, 1);
uniform vec4 color_bot : hint_color = vec4(0, 0, 0, 1);

// Lower values = "neater" grass, higher values = "untidier" grass
uniform float rand_pitch : hint_range(0.0, 1.0) = 0.1;
uniform float rand_yaw : hint_range(0.0, 1.0) = 0.1;

// Magic: DO NOT TOUCH
mat3 mat3_from_axis_angle(float angle, vec3 axis) {
	float s = sin(angle);
	float c = cos(angle);
	float t = 1.0 - c;
	float x = axis.x;
	float y = axis.y;
	float z = axis.z;
	return mat3(
		vec3(t*x*x+c, t*x*y-s*z, t*x*z+s*y),
		vec3(t*x*y+s*z, t*y*y+c, t*y*z-s*x),
		vec3(t*x*z-s*y, t*y*z+s*z, t*z*z+c)
	);
}

///// DO NOT TOUCH /////
///// COPIED OVER FROM Wind.shader /////

vec2 random2(vec2 p) {
	vec2 a = vec2(20.0, -4.0);
	vec2 b = vec2(0.3, -146.3);
	vec2 some = vec2(
		dot(p, a),
		dot(p, b)
	);
	return fract(sin(some));
}

float worley2(vec2 p) {
	float dist = 1.0;
	vec2 i_p = floor(p);
	vec2 f_p = fract(p);
	for (int y=-1;y<=1;y++) {
		for (int x=-1;x<=1;x++) {
			vec2 xy = vec2(float(x), float(y));
			vec2 rp = random2(i_p + xy);
			vec2 diff = xy + rp - f_p;
			dist = min(dist, length(diff));
		}
	}
	return dist;
}

///// END DO NOT TOUCH /////

void vertex() {
	NORMAL = UP;
	vec3 vertex = VERTEX;
	mat3 to_model = inverse(mat3(WORLD_MATRIX));
	vec4 world = (WORLD_MATRIX * vec4(vertex, -1.0));
	// Get UV coordinates to wind shader by using world position.
	// -1.0 as the last parameter so that the wind flow is flipped (looks off otherwise).
	vec2 uv = world.xz * wind_scale;
	uv += wind_direction.xz * TIME * wind_speed;
	wind = worley2(uv);
	// Apply the wind influence (created in GrassFactory)
	wind *= UV2.y;
	
	vec3 wind_forward = to_model * wind_direction;
	vec3 wind_right = normalize(cross(wind_forward, UP));
	
	vec2 rand = random2(world.xz);
	
	vec2 sway_power = vec2(1, sin(TIME*0.4));
	
	float sway_pitch = rand.x * sway_randomness.x + (deg_sway_pitch_max * DEG2RAD) * wind * sway_power.x;
	float sway_yaw =   rand.y * sway_randomness.y + (deg_sway_yaw_max   * DEG2RAD) * wind * sway_power.y;
	
	// Width data set in GrassPlanter
	vertex.xz *= INSTANCE_CUSTOM.x;
	// Height data set in GrassPlanter
	vertex.y *=  INSTANCE_CUSTOM.y;
	
	mat3 rot_right = mat3_from_axis_angle(sway_pitch, wind_right);
	mat3 rot_forward = mat3_from_axis_angle(sway_yaw, wind_forward);
	
	vertex = rot_right * rot_forward * vertex;
	
	VERTEX = vertex;
	// UV2 is set in GrassFactory.gd
	COLOR = mix(color_bot, color_top, UV2.y);
}

void fragment() {
	// Flip the normals for back-culled faces.
	// If NORMAL is not set to up in vertex(), you can remove these lines.
	NORMAL *= FRONT_FACING ? 1.0 : -1.0;
	ALBEDO = COLOR.rgb;
	SPECULAR = specular_level;
	// Make more affected areas shinier
	ROUGHNESS = clamp(1.0 - (wind * 2.0), 1.0 - specular_level, 1.0);
}