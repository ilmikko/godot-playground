extends Object

static func mesh_tri(verts, uvs):
	var tri = []
	tri.resize(Mesh.ARRAY_MAX)
	tri[Mesh.ARRAY_VERTEX] = verts
	tri[Mesh.ARRAY_TEX_UV2] = uvs
	return tri

static func grass_blade_geom():
	# Basic triangle
	var verts = PoolVector3Array([
		Vector3(-0.5, 0.0, 0.0),
		Vector3(0.5, 0.0, 0.0),
		Vector3(0.0, 1.0, 0.0)
	])
	# UV contains info on whether we're at the tip or the base.
	var uvs = PoolVector2Array([
		Vector2(0.0, 0.0),
		Vector2(0.0, 0.0),
		Vector2(1.0, 1.0)
	])
	return [verts, uvs]

static func grass_blade():
	var geom = grass_blade_geom()
	# Construct triangles
	var mesh_tri = mesh_tri(geom[0], geom[1])
	
	var mesh = ArrayMesh.new()
	mesh.add_surface_from_arrays(Mesh.PRIMITIVE_TRIANGLES, mesh_tri)
	# Set bounding box to correct values
	mesh.custom_aabb = AABB(Vector3(-0.5, 0.0, -0.5), Vector3(0.5, 1.0, 1.0))

	return mesh
