extends Node
class_name Geom

static func RandVector2() -> Vector2:
	return Vector2(randf(), randf())

static func RandVector3() -> Vector3:
	return Vector3(randf(), randf(), randf())

static func RandnVector2() -> Vector2:
	return Vector2(randf()-0.5, randf()-0.5)*2

static func RandnVector3() -> Vector3:
	return Vector3(randf()-0.5, randf()-0.5, randf()-0.5)*2

static func Vector2(a = null, b = null) -> Vector2:
	if b != null:
		return Vector2(a, b)
	if a != null:
		return Vector2(a, a)
	return Vector2(1, 1)

static func Vector3(a = null, b = null, c = null) -> Vector3:
	if c != null:
		return Vector3(a, b, c)
	if b != null:
		return Vector3(a, b, b)
	if a != null:
		return Vector3(a, a, a)
	return Vector3(1, 1, 1)
