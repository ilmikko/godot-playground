extends RigidBody

var lift_max = 0.1655555

var acc_frame = 0.05
var acc_decay = 0.75
var acc_max = acc_frame / (1.0 - acc_decay)
var acc = acc_max

var vel_decay = 0.9997
var vel_decay_ang = 0.81
var vel_max = 19.413

var roll_frame = 0.021
var pitch_frame = 0.023
var yaw_frame = 0.01

func _integrate_forces(state):
	state.linear_velocity *= vel_decay
	state.angular_velocity *= vel_decay_ang
	
	var vel = state.linear_velocity.length()
	var lift_c = (2.0 / (1.0+(vel_max-vel)/vel_max)) - 1.0
	
	var lift = lift_max * lift_c
	
	apply_central_impulse(Vector3(0.0, 1.0, 0.0) * lift)
	apply_central_impulse(-global_transform.basis.x * acc)
	
	acc *= acc_decay
	
	if Input.is_action_pressed("veh_plane_pitch_dec"):
		apply_torque_impulse(global_transform.basis.z * pitch_frame)
	if Input.is_action_pressed("veh_plane_pitch_inc"):
		apply_torque_impulse(-global_transform.basis.z * pitch_frame)
	
	if Input.is_action_pressed("veh_plane_yaw_dec"):
		apply_torque_impulse(global_transform.basis.y * yaw_frame)
	if Input.is_action_pressed("veh_plane_yaw_inc"):
		apply_torque_impulse(-global_transform.basis.y * yaw_frame)
	
	if Input.is_action_pressed("veh_plane_roll_dec"):
		apply_torque_impulse(global_transform.basis.x * roll_frame)
	if Input.is_action_pressed("veh_plane_roll_inc"):
		apply_torque_impulse(-global_transform.basis.x * roll_frame)
	
	if Input.is_action_pressed("veh_plane_accelerate"):
		acc += acc_frame
	if Input.is_action_pressed("veh_plane_decelerate"):
		acc -= acc_frame
	
	Debug.show("Pos", global_transform)
	Debug.show("Altitude", global_transform.origin.y)
	Debug.show("Air Speed", vel)
	Debug.show("Air Speed Max", vel_max)
	Debug.show("Air Lift", lift)
	Debug.show("Air Lift Control", lift_c)
	Debug.show("Air Accel", acc)
	Debug.show("Air Accel Max", acc_max)
