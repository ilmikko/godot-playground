extends Active

signal camera_moved

const WALK_MODIFIER = 1
const RUN_MODIFIER = 6.5

onready var Person = $Person
onready var MouseGimbal = $Person/MouseGimbal
onready var CameraRoot = $Person/CameraRoot
onready var Camera = $Person/CameraRoot/Camera

func rotate_camera(basis):
	CameraRoot.global_transform.basis = basis
	Person.look(basis)
	emit_signal("camera_moved", Camera.global_transform)

func _activated(act):
	MouseGimbal.set_active(act)
	Person.set_active(act)
	Camera.current = act
	if act:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _input(event):
	if !active:
		return
	
	if not event is InputEventKey:
		return
	
	var mod = WALK_MODIFIER
	if Input.is_action_pressed("ped_sprint"):
		mod = RUN_MODIFIER
	
	var intent = Geo.Vector2(0)
	if Input.is_action_pressed("ped_move_left"):
		intent.x -= 1
	if Input.is_action_pressed("ped_move_right"):
		intent.x += 1
	if Input.is_action_pressed("ped_move_forwards"):
		intent.y -= 1
	if Input.is_action_pressed("ped_move_backwards"):
		intent.y += 1
	
	if Input.is_action_just_pressed("debug_1"):
		Person.die_by_collision(Vector3(100.0, 0.0, 0.0))
	
	Person.jump(Input.is_action_pressed("ped_jump"))
	
	intent = intent.normalized() * mod
	Debug.show("Player Ped Move Intent", intent)
	Person.move(intent)

func _on_MouseGimbal_rotated(basis):
	rotate_camera(basis)
