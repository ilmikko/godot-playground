extends RigidBody

var FLY_SPEED = 0.005
var FLY_SPEED_FALLOFF = 0.9
var FLY_FAST_MODIFIER = 7.0

export var active = false setget _activated

var view_basis = Basis()
var locked_rotation = Vector3(0, 0, 0)

func _activated(act):
	active = act
	can_sleep = !act
	sleeping = !act

func _integrate_forces(state):
	rotation = locked_rotation
	if !active:
		return
	
	var speed = FLY_SPEED
	state.linear_velocity *= FLY_SPEED_FALLOFF
	
	if Input.is_action_pressed("cam_move_fast"):
		speed *= FLY_FAST_MODIFIER
	
	if Input.is_action_pressed("cam_move_forwards"):
		apply_central_impulse(-view_basis.z * speed)
	if Input.is_action_pressed("cam_move_backwards"):
		apply_central_impulse(view_basis.z * speed)
	if Input.is_action_pressed("cam_move_left"):
		apply_central_impulse(-view_basis.x * speed)
	if Input.is_action_pressed("cam_move_right"):
		apply_central_impulse(view_basis.x * speed)

func _ready():
	_activated(active)
