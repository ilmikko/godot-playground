extends Spatial

var FLY_SPEED = 0.0025

signal camera_moved

onready var Movement = $Movement
onready var Camera = $Movement/Camera
onready var MouseGimbal = $Movement/MouseGimbal

export var active = false setget _activated

func rotate_camera(basis):
	Movement.view_basis = basis
	Camera.transform.basis = basis
	emit_signal("camera_moved", Movement.global_transform)

func _activated(act):
	active = act
	if is_inside_tree():
		Movement.active = act
		MouseGimbal.active = act
		Camera.current = true
	if act:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_MouseGimbal_rotated(basis):
	rotate_camera(basis)

func _ready():
	Movement.FLY_SPEED = FLY_SPEED
	_activated(active)
