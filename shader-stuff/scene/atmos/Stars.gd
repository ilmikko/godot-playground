tool
extends Spatial

const StarFactory = preload("res://script/factory/StarFactory.gd")

export(int) var count = 1000 setget _set_count
func _set_count(_count):
	count = _count
	rebuild()

export(float, 1.0, 100.0) var size = 100.0 setget _set_size
func _set_size(_size):
	size = _size
	rebuild()

export(float, 1.0, 1000.0) var radius = 100.0 setget _set_radius
func _set_radius(_radius):
	radius = _radius
	rebuild()

export(float, 0.0, 100.0) var variance = 0.0 setget _set_variance
func _set_variance(_variance):
	variance = _variance
	rebuild()

func rebuild():
	if !is_inside_tree():
		return
	
	if !$MultiMeshInstance.multimesh:
		var multimesh = MultiMesh.new()
		# Set format for multimesh data.
		multimesh.transform_format = MultiMesh.TRANSFORM_3D
		multimesh.set_custom_data_format(MultiMesh.CUSTOM_DATA_FLOAT)
		multimesh.set_color_format(MultiMesh.COLOR_NONE)
		$MultiMeshInstance.multimesh = multimesh

	var multimesh = $MultiMeshInstance.multimesh

	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh = StarFactory.star()
	multimesh.instance_count = count

	# Golden angle in radians
	var phi = 2.399963229728653

	# Generate points on a sphere.
	# TODO: Make seeded based on count (deterministic)
	for i in (count):
		var y = (i / float(count)) * 2.0 - 1.0

		var angle = phi * i
		var r = sqrt(1.0 - y*y)

		var x = cos(angle) * r
		var z = sin(angle) * r

		var pos    = Vector3(x, y, z)
		var scale   = randf()*size
		var basis  = Basis().scaled(Vector3(scale, scale, scale))
		var origin = pos * radius + Vector3(randf()-0.5, randf()-0.5, randf()-0.5) * 2.0 * variance
		multimesh.set_instance_transform(i, Transform(basis, origin))
		multimesh.set_instance_custom_data(i, Color(
			randf(), # Flicker offset
			randf(), # Flicker speed
			0, # Unused channel
			0  # Unused channel
		))

func _init():
	rebuild()

func _ready():
	rebuild()
