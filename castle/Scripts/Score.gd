extends Node

signal score_changed

var score = 0

func Add(s):
	Set(score+s)

func Change():
	emit_signal("score_changed", score)

func Get():
	return score

func Set(s):
	score = s
	Change()

func Sub(s):
	Set(score-s)

func _ready():
	Change()
