extends RigidBody2D

const ROTATION_SPEED = 0.2
const FORCE_SPEED = 2e5
const FORCE_JUMP = 2.5e6
const MAX_VELOCITY = Vector2(150, 2000)

func apply_force_horizontal(vec):
	if Input.is_action_pressed("ui_right"):
		vec.x += FORCE_SPEED
	if Input.is_action_pressed("ui_left"):
		vec.x -= FORCE_SPEED
	return vec

func apply_force_jump(vec):
	if Input.is_action_just_pressed("ui_up"):
		vec.y -= FORCE_JUMP
	return vec

func apply_force():
	var thrust = Vector2(0, 0)
	
	thrust = apply_force_horizontal(thrust)
	thrust = apply_force_jump(thrust)
	
	applied_force = thrust.rotated(rotation)

func apply_limits():
	linear_velocity.x = clamp(linear_velocity.x, -MAX_VELOCITY.x, MAX_VELOCITY.x)
	linear_velocity.y = clamp(linear_velocity.y, -MAX_VELOCITY.y, MAX_VELOCITY.y)

func apply_rotation():
	# Currently the player cannot rotate.
	rotation = 0

func _integrate_forces(state):
	apply_rotation()
	apply_force()
	apply_limits()

func _on_body_entered(contact):
	if contact.filename == "res://Coin/Coin.tscn":
		contact.PickUp()

func _ready():
	connect("body_entered", self, "_on_body_entered")
