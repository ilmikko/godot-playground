extends Control

onready var scoreLabel = $"CanvasLayer/PanelContainer/HBoxContainer/Score"

func _ready():
	Score.connect("score_changed", self, "_on_score_changed")

func _on_score_changed(s):
	scoreLabel.text = str(s)
