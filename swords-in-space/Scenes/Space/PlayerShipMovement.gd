extends KinematicBody

var velocity = Vector3()
var velocity_ang = 0

var camera

const ACCEL = 0.1
const ACCEL_ANG = 0.02

const VEL_MULTIPLIER = 0.9

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_movement_process(delta)

func _movement_process(delta):
	if Input.is_action_pressed("fly_forward"):
		velocity += Vector3(ACCEL, 0, 0)
	if Input.is_action_pressed("fly_backward"):
		velocity += Vector3(-ACCEL, 0, 0)
	if Input.is_action_pressed("fly_right"):
		velocity += Vector3(0, 0, ACCEL)
	if Input.is_action_pressed("fly_left"):
		velocity += Vector3(0, 0, -ACCEL)
	if Input.is_action_pressed("fly_roll_clockwise"):
		velocity_ang += ACCEL_ANG
	if Input.is_action_pressed("fly_roll_counterclockwise"):
		velocity_ang += -ACCEL_ANG

	velocity *= VEL_MULTIPLIER
	velocity_ang *= VEL_MULTIPLIER
	
	var framev = velocity.linear_interpolate(velocity, delta)
	
	translate(framev)
	rotate_x(velocity_ang)
	
var camera_anglev = 0
const mouse_sensitivity = 0.001

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(-event.relative.x * mouse_sensitivity)
		
		var changev = -event.relative.y * mouse_sensitivity
		camera_anglev += changev
		rotate_z(changev)
