# This library tries to mimic GL scripts with minimal changes to original code.
# It is helpful as then mathematical formulas from shaders can be re-used within
# the game. 
extends Object
class_name GL

static func dot(a, b):
	return a.dot(b);

static func fract(s):
	return s-glfloor(s);

static func glsin(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return sin(s);
	elif t == TYPE_VECTOR2:
		return Vector2(sin(s.x), sin(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(sin(s.x), sin(s.y), sin(s.z));

static func glfloor(s):
	var t = typeof(s);
	if t == TYPE_REAL or t == TYPE_INT:
		return floor(s);
	elif t == TYPE_VECTOR2:
		return Vector2(floor(s.x), floor(s.y));
	elif t == TYPE_VECTOR3:
		return Vector3(floor(s.x), floor(s.y), floor(s.z));

static func random(s) -> float:
	return random2(Vector2(4281.230, s)).y

static func random2(vec) -> Vector2:
	var a = Vector2(20.0, -4.0);
	var b = Vector2(0.3, -146.3);
	var c = Vector2(-2.9, -0.2);
	vec += c;
	var some = Vector2(
		dot(vec, a),
		dot(vec, b)
	);
	return glsin(some);

static func random3(vec) -> Vector3:
	var a = Vector3(20.0, -4.0, 0.62);
	var b = Vector3(0.3, -146.3, 0.00515138);
	var c = Vector3(-2.9, -0.2, 80.3218);
	var d = Vector3(0.4124, 25.0321, -820.0214);
	vec += d;
	var some = Vector3(
		dot(vec, a),
		dot(vec, b),
		dot(vec, c)
	);
	return glsin(some)
