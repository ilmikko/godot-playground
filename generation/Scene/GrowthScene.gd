extends Spatial

onready var Freelook = $FreelookController
onready var BioGraph = $UI/BioGraph
onready var CountGraph = $UI/CountGraph

func update_graph():
	var apples = get_tree().get_nodes_in_group("apples")
	var count = len(apples)
	var bm = 0
	for apple in apples:
		bm += apple.size()
	Debug.show("Apple count", count)
	Debug.show("Biomass", bm)
	CountGraph.tick(count)
	BioGraph.tick(bm)

func _ready():
	CountGraph.set_color(Color(1.0, 0.0, 0.0, 1.0))
	BioGraph.set_color(Color(1.0, 1.0, 0.2, 1.0))
	Freelook.FLY_SPEED = 0.004
	Freelook.set_active(true)

func _on_GraphTimer_timeout():
	update_graph()
