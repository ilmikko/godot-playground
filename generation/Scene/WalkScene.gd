extends Spatial

onready var Freelook = $FreelookController
onready var Walking = $"3rdPersonController"

var camera_transform = Transform()
var walking = false

func toggle_cam():
	walking = !walking
	
	var after
	var before
	
	if walking:
		before = Freelook
		after = Walking
	else:
		before = Walking
		after = Freelook
	
	before.set_active(false)
	after.set_active(true)

func _input(event):
	if Input.is_action_just_pressed("debug_1"):
		toggle_cam()

func _ready():
	toggle_cam()


func _on_3rdPersonController_camera_moved(transform):
	camera_transform = transform

func _on_FreelookController_camera_moved(transform):
	camera_transform = transform
