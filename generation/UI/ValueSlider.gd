extends Control

signal value_changed

export var label = ""
export var value = 0.0
export var min_value = 0.0
export var max_value = 1.0

onready var Label = $VBox/HSplit/Label
onready var Value = $VBox/HSplit/Value
onready var Range = $VBox/Range

func update_value(v):
	Value.text = str(v)
	value = v
	emit_signal("value_changed", v)

func _ready():
	Label.text = str(label)
	Range.value = value
	Range.min_value = min_value
	Range.max_value = max_value
	Range.step = (max_value-min_value)/200.0
	update_value(value)

func _on_Range_value_changed(v):
	update_value(v)
