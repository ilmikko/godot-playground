extends Control

signal ticked

const MAX_WIDTH = 1920/2
var COLOR = Color(1.0, 0.0, 0.0, 1.0)

var Line
var PrevLine

var x = MAX_WIDTH
var y = 0
var offset = 0
var last = 0

func new_line():
	if PrevLine != null:
		PrevLine.queue_free()
	if Line != null:
		Line.position.x -= MAX_WIDTH
		PrevLine = Line
	Line = Line2D.new()
	Line.position.x = MAX_WIDTH/2
	Line.width = 2.0
	Line.default_color = COLOR
	add_child(Line)

func scroll():
	x += 1
	if x > MAX_WIDTH:
		new_line()
		offset += x
		x = 0

func tick(val):
	scroll()
	y = -val * 0.5
	
	last = val
	
	Line.add_point(Vector2(x, y), x)
	emit_signal("ticked", val)
