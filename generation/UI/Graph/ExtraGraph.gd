extends Control

onready var BaseValue = $BaseValue
onready var MaxValue = $MaxValue
onready var MaxValue50 = $MaxValue50
onready var MinValue50 = $MinValue50

var val_max = 0

var val_min50 = 0
var val_min50_c = 0
var val_max50 = 0
var val_max50_c = 0

func set_color(color):
	BaseValue.COLOR = color
	MaxValue.COLOR = color * Color(0.75, 0.80, 1.25, 1.0)
	MaxValue50.COLOR = color * Color(1.0, 1.0, 1.0, 0.22)
	MinValue50.COLOR = color * Color(1.2, 1.2, 1.25, 0.22)

func tick(val):
	if val > val_max:
		val_max = val
	
	val_max50_c += 1
	if val > val_max50 || val_max50_c > 1500:
		val_max50_c = 0
		val_max50 = val
		
	val_min50_c += 1
	if val < val_min50 || val_min50_c > 1500:
		val_min50_c = 0
		val_min50 = val
	
	BaseValue.tick(val)
	MaxValue.tick(val_max)
	MaxValue50.tick(val_max50)
	MinValue50.tick(val_min50)
