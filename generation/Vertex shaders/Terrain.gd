extends Spatial

var t = 0
var speed = 4

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	t += delta*speed
	translation = Vector3(0, 0, -t)
