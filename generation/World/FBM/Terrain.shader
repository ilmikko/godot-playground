shader_type spatial;

uniform float scale = 2.2;
uniform float amplitude = 2.0;

uniform vec3 global;

const vec2 epsilon = vec2(0.01, 0.0);

float random(in vec2 _st) {
    return fract(sin(dot(_st.xy,
                         vec2(-21.2222,128.15922)))*
        22222.222222);
}

// Based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise(in vec2 _st) {
    vec2 i = floor(_st);
    vec2 f = fract(_st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}

float fbm(in vec2 _st) {
    float v = 0.0;
    float a = 0.5;
    
    // Rotate to reduce axial bias
    vec2 shift = vec2(50.0);
    mat2 rot = mat2(
		vec2(
			cos(0.5), sin(0.5)
		),
		vec2(
			-sin(0.5), cos(0.5)
		)
    );
    
    for (int i = 0; i < 2; i++) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    return v;
}

// MAKE SURE THIS IS CONSISTENT WITH TERRAINCELL.gd HEIGHT FUNCTION
float height(in float t, vec2 xy) {
	return cos(xy.x*scale) * sin(xy.y*scale) * amplitude;
}

// Helper function from https://www.youtube.com/watch?v=vm9Sdvhq6ho
// bt should be (0, n) for TANGENT
// bt should be (n, 0) for BINORMAL
// where n is something > epsilon, larger n = softer shadows
vec3 tangent(in float t, vec2 vec, vec2 bt) {
	return normalize(vec3(
		bt.x,
		height(t, vec.xy + epsilon.yx) - height(t, vec.xy - epsilon.yx),
		bt.y
	));
}

void vertex() {
	vec2 pos = global.xz + VERTEX.xz;
	VERTEX.y = height(TIME, pos);
	
	TANGENT.xyz = tangent(TIME, pos, vec2(0.0, epsilon.x*8.0));
	BINORMAL.xyz = tangent(TIME, pos, vec2(epsilon.x*8.0, 0.0));
	NORMAL.xyz = cross(TANGENT, BINORMAL);
	COLOR.xyz = VERTEX;
}

void fragment() {
	ALBEDO = vec3(1.0, 1.0, 1.0);
}