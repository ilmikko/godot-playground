extends Spatial

func update_collision(height):
	var trans = Transform()
	trans.origin.y = height
	transform = trans

func _on_TerrainCell_terrain_updated(height):
	update_collision(height)
