extends Spatial

const AMPLITUDE = 4.818;
const SCALE = 0.075;

signal terrain_updated

onready var MeshInstance = $MeshInstance
var material

func move(pos):
	transform = pos
	material.set_shader_param("global", pos.origin)
	var xy = Vector2(pos.origin.x, pos.origin.z)
	emit_signal("terrain_updated", height(0, xy))

func height(t, xy):
	return cos(xy.x*SCALE) * sin(xy.y*SCALE) * AMPLITUDE;

func _ready():
	material = MeshInstance.get_surface_material(0).duplicate()
	MeshInstance.set_surface_material(0, material)
	material.set_shader_param("amplitude", AMPLITUDE)
	material.set_shader_param("scale", SCALE)

func _on_PlaneController_moved(pos):
	move(pos)
