extends Spatial

const VELOCITY_FALLOFF = 0.80
const HEAD_RATIO = 0.3
const BODY_RATIO = 0.7

onready var Head = $DeadPersonHead/RigidBody
onready var Body = $DeadPersonBody/RigidBody

var initial_velocity = Vector3(0.0, 0.0, 0.0)

func _ready():
	Head.apply_central_impulse(initial_velocity * HEAD_RATIO)
	Body.apply_central_impulse(initial_velocity * BODY_RATIO)
	Head.apply_torque_impulse(Geo.RandnVector3()*0.01*initial_velocity)
	Body.apply_torque_impulse(Geo.RandnVector3()*0.3*initial_velocity)
