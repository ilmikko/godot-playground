extends Spatial

const APPLE_Y_OFFSET_MAX = 0.346
const APPLE_Y_OFFSET_MIN = 0.2
const APPLE_RADIUS_MIN = 1.5
const APPLE_RADIUS_MAX = 2.0

const APPLE_AGE_MIN = 100.0
const APPLE_AGE_MAX = 60.0

const MULTIPLY_SIZE_MAX = 0.4
const MULTIPLY_SIZE_MIN = 0.7
const MULTIPLY_AMOUNT_MIN = 1
const MULTIPLY_AMOUNT_MAX = 10

const MULTIPLY_ON_DEATH_CHANCE = 0.33

const SIZE_INCREASE_MIN = 0.01
const SIZE_INCREASE_MAX = 0.04
const SPAWN_SIZE_MAX = 0.25
const SPAWN_SIZE_MIN = 0.05
const GROW_DELAY_MIN = 0.01
const GROW_DELAY_MAX = 0.01

const CHILD_PENALTY = 0.1
const NEIGHBOUR_PENALTY = 0.015
const INFERTILE_LAND_PENALTY = 0.25

onready var APPLE = load("res://Actor/Apple/Apple.tscn")
onready var GrowTimer = $GrowTimer
onready var Area = $Area

var age = 0
var max_age = APPLE_AGE_MIN
var size = SPAWN_SIZE_MIN
var multiply_size = MULTIPLY_SIZE_MIN

func grow():
	var agep = (1 - (age/max_age))
	if size < multiply_size:
		var increase = SIZE_INCREASE_MIN + SIZE_INCREASE_MAX*randf()
		increase *= agep
		Debug.show("Grow size", agep)
		set_size(size + increase)
		age_checks()
		return
	
	var child_amount = MULTIPLY_AMOUNT_MIN + MULTIPLY_AMOUNT_MAX*randf()
	for _i in range(child_amount):
		if size < multiply_size:
			break
		size -= CHILD_PENALTY
		multiply()
	age_checks()

func die():
	if randf() < MULTIPLY_ON_DEATH_CHANCE:
		multiply()
	queue_free()

func fetch_overlapping_apples():
	var overlapping_apples = []
	var overlapping_areas = Area.get_overlapping_areas()
	for i in range(len(overlapping_areas)):
		var area = overlapping_areas[i]
		if area.is_in_group("apples"):
			overlapping_apples.append(area)
	return overlapping_apples

func multiply():
	var apple = APPLE.instance()
	var angle = randf()*TAU
	var radius = APPLE_RADIUS_MIN + APPLE_RADIUS_MAX*randf()
	var x = sin(angle)*radius
	var y = cos(angle)*radius
	get_parent().add_child(apple)
	apple.global_transform.origin = global_transform.origin + Vector3(x, 0, y)

func is_in_fertile_land():
	var overlapping_areas = Area.get_overlapping_areas()
	for i in range(len(overlapping_areas)):
		var area = overlapping_areas[i]
		if area.is_in_group("fertile_land"):
			return true
	return false

func set_size(s):
	Debug.show("Apple size", s)
	size = s
	scale = Geo.Vector3(s)

func age_checks():
	age += 1
	if age > max_age:
		die()
	
	for apple in fetch_overlapping_apples():
		size -= apple.size()*NEIGHBOUR_PENALTY
	
	if not is_in_fertile_land():
		size -= INFERTILE_LAND_PENALTY
	
	if size < 0:
		die()
	
	tick()

func tick():
	GrowTimer.wait_time = GROW_DELAY_MIN + GROW_DELAY_MAX*randf()
	GrowTimer.start()

func _enter_tree():
	pass

func _ready():
	max_age = APPLE_AGE_MIN + APPLE_AGE_MAX*randf()
	multiply_size = MULTIPLY_SIZE_MIN + MULTIPLY_SIZE_MAX*randf()
	# global_transform.origin.y = APPLE_Y_OFFSET_MIN + APPLE_Y_OFFSET_MAX*randf()*size
	set_size(size)
	tick()

func _on_GrowTimer_timeout():
	grow()
