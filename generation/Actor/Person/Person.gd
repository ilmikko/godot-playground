extends ActiveRigidBody

const WALK_SPEED = 0.2

const TURN_SPEED_BODY = 0.08
const TURN_SPEED_HEAD = 0.22

const JUMP_SPEED = 3.0
const JUMP_FALLOFF = 0.95
const JUMP_COOLDOWN_MAX = 25.0

const CUMULATIVE_VEL_FALLOFF = 0.98
const DEADLY_COLLISION_VELOCITY = 2.5
const DEAD_BODY = preload("res://Mesh/Person/Dead/DeadPersonMesh.tscn")

onready var AliveState = $State
onready var DownRay = $CollisionShape/DownRay

onready var Head = $State/PersonHead
onready var Body = $State/PersonBody

var cumulative_vel = Vector3(0.0, 0.0, 0.0)
var move_intent = Vector2(0.0, 0.0)
var look_intent = Basis()
var jump_intent = false
var jump_cooldown = 0.0
var dead = false

func collision(vel, _body):
	if dead:
		return
	Debug.show("Ped collision", vel.length())
	if vel.length() > DEADLY_COLLISION_VELOCITY:
		die_by_collision(vel)

func die():
	die_with_body(DEAD_BODY.instance())

func die_by_collision(vel):
	var DeadBody = DEAD_BODY.instance()
	DeadBody.initial_velocity = vel
	die_with_body(DeadBody)

func die_with_body(body):
	if dead:
		return
	dead = true
	
	Debug.show("Dead", "Dead!")
	
	var DeadState = Spatial.new()
	DeadState.add_child(body)
	Util.swap(AliveState, DeadState)
	
	# Prevent further movement to the the root of Person.
	# Gibs such as DeadBody will have their own physics roots.
	set_active(false)

func is_grounded():
	return DownRay.is_colliding()

func jump(intent=true):
	if dead:
		return
	
	jump_intent = intent

func move(intent):
	if dead:
		return
	
	move_intent = intent

func look(cam):
	if dead:
		return

	var yaw = Geo.basis_yaw_quat(cam)
	var pitch = Geo.basis_pitch_quat(cam)
	Debug.show("Look Cam", cam)
	Debug.show("Look Yaw", yaw)
	Debug.show("Look Pitch", pitch)
	look_intent = Basis(yaw)

func _integrate_forces(state):
	if !active:
		return
	
	cumulative_vel *= CUMULATIVE_VEL_FALLOFF
	cumulative_vel += state.linear_velocity
	
	jump_cooldown *= JUMP_FALLOFF
	jump_cooldown += abs(state.linear_velocity.y)
	
	if dead:
		return
	
	Debug.show("Jump value", jump_cooldown)
	
	_integrate_forces_turning(state)
	_integrate_forces_walking(state)
	_integrate_forces_jumping(state)

func _integrate_forces_turning(state):
	Head.global_transform.basis = Head.global_transform.basis.slerp(
		look_intent, TURN_SPEED_HEAD)
	var head_yaw = Geo.basis_yaw(Head.global_transform.basis)
	Body.global_transform.basis = Body.global_transform.basis.slerp(
		head_yaw, TURN_SPEED_BODY)

func _integrate_forces_jumping(_state):
	var grounded = is_grounded()
	Debug.show("Grounded", grounded)
	if !jump_intent:
		return
	if !grounded:
		return
	if jump_cooldown > JUMP_COOLDOWN_MAX:
		return
	jump_intent = false
	jump_cooldown += JUMP_COOLDOWN_MAX
	apply_central_impulse(global_transform.basis.y * JUMP_SPEED)

func _integrate_forces_walking(state):
	var ground_velocity = Vector2(state.linear_velocity.x, state.linear_velocity.z)
	# Cap how much force we get from input.
	# If we're moving very fast horizontally, then we won't listen to move intent.
	# BUG: If we're moving on a platform, it affects the calculations.
	var ground_velocity_cap = max(1.0-ground_velocity.length(), 0.0)
	var speed = WALK_SPEED * ground_velocity_cap
	Debug.show("Ground vel cap", ground_velocity_cap)
	apply_central_impulse(Body.global_transform.basis.x * move_intent.x * speed)
	apply_central_impulse(Body.global_transform.basis.z * move_intent.y * speed)

func _on_Person_body_entered(body):
	collision(cumulative_vel*(1.0-CUMULATIVE_VEL_FALLOFF), body)
