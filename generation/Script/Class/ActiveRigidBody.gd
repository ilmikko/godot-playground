extends RigidBody
class_name ActiveRigidBody

signal activated

var active = true

func set_active(v):
	active = v
	set_physics_process(v)
	can_sleep = !v
	sleeping = !v
	if v:
		mode = RigidBody.MODE_RIGID
	else:
		mode = RigidBody.MODE_STATIC
	emit_signal("activated", v)

func _ready():
	if has_method("_activated"):
		if connect("activated", self, "_activated"):
			print("Activated connect failed")
