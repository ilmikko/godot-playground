extends Spatial
class_name Active

signal activated

var active = true

func set_active(v):
	active = v
	emit_signal("activated", v)

func _ready():
	if has_method("_activated"):
		if connect("activated", self, "_activated"):
			print("Activated connect failed")
