extends Node

static func remove(n):
	n.queue_free()

static func remove_children(n):
	for child in n.get_children():
		child.queue_free()

static func swap(n, m):
	var parent = n.get_parent()
	remove(n)
	parent.add_child(m)
