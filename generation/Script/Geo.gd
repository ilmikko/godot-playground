extends Node

func quat_yaw(quat):
	quat.x = 0
	quat.y = quat.y
	quat.z = 0
	quat.w = quat.w
	return quat

func quat_pitch(quat):
	quat.x = 0
	quat.y = quat.y
	quat.z = quat.z
	quat.w = 0
	return quat

func basis_pitch_quat(basis):
	return quat_pitch(basis.get_rotation_quat())

func basis_pitch(basis):
	return Basis(basis_pitch_quat(basis))

func basis_yaw_quat(basis):
	return quat_yaw(basis.get_rotation_quat())

func basis_yaw(basis):
	return Basis(basis_yaw_quat(basis))

func RandVector2() -> Vector2:
	return Vector2(randf(), randf())

func RandVector3() -> Vector3:
	return Vector3(randf(), randf(), randf())

func RandnVector2() -> Vector2:
	return Vector2(randf()-0.5, randf()-0.5)*2

func RandnVector3() -> Vector3:
	return Vector3(randf()-0.5, randf()-0.5, randf()-0.5)*2

func Vector2(a = null, b = null) -> Vector2:
	if b != null:
		return Vector2(a, b)
	if a != null:
		return Vector2(a, a)
	return Vector2(1, 1)

func Vector3(a = null, b = null, c = null) -> Vector3:
	if c != null:
		return Vector3(a, b, c)
	if b != null:
		return Vector3(a, b, b)
	if a != null:
		return Vector3(a, a, a)
	return Vector3(1, 1, 1)
