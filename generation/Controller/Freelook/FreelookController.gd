extends Active

var FLY_SPEED = 0.0025

signal camera_moved

onready var FirstPersonController = $"Movement/1stPersonController"
onready var Movement = $Movement

func rotate_camera(basis):
	Movement.view_basis = basis
	emit_signal("camera_moved", Movement.global_transform)

func _activated(act):
	FirstPersonController.set_active(act)
	Movement.FLY_SPEED = FLY_SPEED
	Movement.set_active(act)

func _on_1stPersonController_camera_moved(transform):
	rotate_camera(transform.basis)
