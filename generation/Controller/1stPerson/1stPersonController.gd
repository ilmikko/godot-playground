extends Active

signal camera_moved

const MOUSE_SPEED = 0.01

onready var Gimbal = $MouseGimbal
onready var Camera = $Camera

func rotate_camera(basis):
	Camera.global_transform.basis = basis
	emit_signal("camera_moved", Camera.global_transform)

func set_basis(basis):
	Camera.global_transform.basis = basis
	Gimbal.set_basis(basis)

func _activated(act):
	Gimbal.set_active(act)
	Camera.current = act
	if act:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_MouseGimbal_rotated(basis):
	rotate_camera(basis)
