extends Active

signal rotated

const MOUSE_SPEED = 0.01

onready var Yaw = $Yaw
onready var Pitch = $Yaw/Pitch
onready var Gimbal = $Yaw/Pitch/Gimbal

func _input(event):
	if !active:
		return
	
	if event is InputEventMouseMotion:
		Yaw  .rotate(Vector3(0.0, 1.0, 0.0), (-event.relative.x/TAU) * MOUSE_SPEED)
		Pitch.rotate(Vector3(1.0, 0.0, 0.0), (-event.relative.y/TAU) * MOUSE_SPEED)
		emit_signal("rotated", Gimbal.global_transform.basis)
