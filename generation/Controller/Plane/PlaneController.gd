extends Spatial

signal moved

onready var PlaneBody = $Generic6DOFJoint/PlaneBody

func move_tick():
	var trans = Transform()
	trans.origin = PlaneBody.global_transform.origin
	trans.origin.y = 0
	emit_signal("moved", trans)

func _on_Timer_timeout():
	move_tick()
