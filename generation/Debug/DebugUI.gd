extends VBoxContainer

const LABEL = preload("res://Debug/Label.tscn")

var labels = {}

func create_label(id):
	if id in labels:
		return labels[id]

	var label = LABEL.instance()
	labels[id] = label
	add_child(label)
	return label

func _ready():
	var status = Debug.connect("update", self, "_on_debug_update")
	assert(status == OK)

func _on_debug_update(id, s):
	labels[id] = create_label(id)
	labels[id].text = id + ": " + str(s)
