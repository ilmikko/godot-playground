extends Node

var UI = preload("res://Debug/DebugUI.gd")

signal update

func show(id, s):
	emit_signal("update", id, s)

func _ready():
	add_child(UI.new())
