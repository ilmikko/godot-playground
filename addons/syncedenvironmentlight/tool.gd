tool
extends Spatial

const SCENE = preload("SyncedEnvironmentLight.tscn")
var scene

export(Environment) var env : Environment setget _set_env
func _set_env(_env):
	env = _env
	rebuild()

export(ProceduralSky) var sky : ProceduralSky setget _set_sky
func _set_sky(_sky):
	sky = _sky
	rebuild()

func rebuild():
	if !scene:
		scene = SCENE.instance()
		add_child(scene)
	if !scene.is_inside_tree():
		return
	if env:
		scene.env = env
	if sky:
		scene.sky = sky
	scene.rebuild()

func _ready():
	rebuild()

func _init():
	rebuild()
