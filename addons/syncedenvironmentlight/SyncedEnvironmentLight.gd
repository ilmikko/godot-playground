tool
extends Spatial

export var sky : ProceduralSky
export var env : Environment

onready var world_env = $WorldEnvironment
onready var light = $DirectionalLight

func rebuild():
	world_env.environment = env
	var euler = light.global_transform.basis.get_rotation_quat().get_euler()
	sky.sun_longitude =    - euler.y*360/TAU
	sky.sun_latitude = 180 + euler.x*360/TAU
	world_env.environment.background_sky = sky
