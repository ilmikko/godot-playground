tool
extends EditorPlugin

const BASE_CLASS_NAME = "Spatial"
const CLASS_NAME = "SyncedEnvironmentLight"
const TOOL = preload("tool.gd")
const ICON = "WorldEnvironment"

## DO NOT TOUCH BELOW ##

var gui
func _get_icon(icon):
	if !gui:
		gui = get_editor_interface().get_base_control()
	return gui.get_icon(icon, "EditorIcons")

func _enter_tree():
	   add_custom_type(CLASS_NAME, BASE_CLASS_NAME, TOOL, _get_icon(ICON))

func _exit_tree():
	   remove_custom_type(CLASS_NAME)
