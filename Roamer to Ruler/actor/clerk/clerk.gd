extends Node3D
class_name Clerk


@onready var controller = $basic_controller


enum ClerkMode {
	MODE_NONE, # Default enum value.
	MODE_SELLING, # Selling, standing still.
	MODE_STARE, # Staring at the player.
}
@export var mode : ClerkMode


func mode_selling():
	while true:
		await Game.time.timer_every_0_1s.timeout
		if mode != ClerkMode.MODE_SELLING: break
		
		# Point at random directions.
		# Pick a direction...
		var rand_vector = Vector3(randf() - 0.5, 0.0, randf() - 0.5).normalized()
		var up_vector = controller.global_transform.basis.y
		
		var v_z = rand_vector.normalized()
		var v_x = up_vector.cross(v_z).normalized()
		var v_y = v_z.cross(v_x)
		
		var target_basis = Basis(v_x, v_y, v_z)
		controller.intent_basis = target_basis
		controller.intent_move = Vector2(0.0, 0.0)


func mode_staring():
	for _i in range(50):
		await Game.time.timer_every_0_1s.timeout
		if mode != ClerkMode.MODE_STARE: break
		
		# Point at the player.
		# This is done by constructing a basis from the diff vector.
		var origin = controller.global_transform.origin
		var target = Game.player.player_body.global_transform.origin
		var up = controller.global_transform.basis.y
		
		controller.intent_basis = Math.basis_from(origin - target, up)
	
	mode = ClerkMode.MODE_SELLING


# Called when the node enters the scene tree for the first time.
func _ready():
	while true:
		await Game.time.timer_every_1s.timeout
		
#		match mode:
#			ClerkMode.MODE_SELLING:
#				await mode_selling()
#			ClerkMode.MODE_STARE:
#				await mode_staring()
