extends Node

signal stamina_changed(stamina)
signal stamina_depleted

@export var max_stamina = 100.0
@export var stamina = 100.0
@export var recharge_amount_per_second = 10.0
@export var stamina_cap = max_stamina: set = _set_stamina_cap

var recharging = false

func _physics_process(delta):
	charge(delta * recharge_amount_per_second)

func charge(amount):
	change(amount)
	if stamina == _max_possible_stamina(): recharging = false

func attempt_spend(amount):
	if !has_usable_stamina(): return false;

	if amount > stamina:
		_stamina_depleted()
		return false
	else:
		spend(amount)
		return true

func spend(amount):
	change(-amount)
	if stamina == 0: _stamina_depleted()

func has_usable_stamina():
	return stamina > 0 && !recharging

func change(amount=0):
	stamina = clamp(stamina + amount, 0, _max_possible_stamina())
	_emit_stamina_change()

func _emit_stamina_change():
	stamina_changed.emit(stamina)

func _max_possible_stamina():
	return min(stamina_cap, max_stamina)

func _set_stamina_cap(new_cap):
	stamina_cap = new_cap
	change()

func _stamina_depleted():
	recharging = true
	stamina_depleted.emit()
