extends Node3D


func speak(to_speak : String):
	$Label3D.text = to_speak
	await Game.time.wait(5.0)
	$Label3D.text = ""
