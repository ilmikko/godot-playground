extends Area3D

signal seen_valuable_thingy(body : PhysicsBody3D)

var bodies_within_area = {}


func body_entered(body : PhysicsBody3D):
	bodies_within_area[body] = true


func body_exited(body : PhysicsBody3D):
	bodies_within_area.erase(body)


func body_seen(body : PhysicsBody3D):
	if body.has_meta("value") && body.get_meta("value") > 0:
		# Valuable thing! Get it!
		emit_signal("seen_valuable_thingy", body)


# cast_to_target_with_dot does a raycast and returns a collider, with a catch:
# The dot product needs to be larger than the "dot" parameter.
# If "dot" is -1, acts like a regular raycast.
# If it's 0, only the "front" of the raycast is considered.
# If it's 1, the ray needs to be exactly perpendicular to the raycaster.
# In practice, if you want a cone of 45 degrees, you probably want a value of 0.5.
func cast_to_target_with_dot(target : Vector3, dot : float) -> PhysicsBody3D:
	var raycast = $RayCast3D
	
	var direction = raycast.to_local(target).normalized()
	
	# Calculate the dot product to see which side of the raycast the ray is going
	# to be on. If it is outside of the "cone", return null.
	var forwards = direction.dot(-raycast.global_transform.basis.z)
	if  forwards < dot:
		return null
	
	raycast.target_position = direction * VISION_DISTANCE
	return raycast.get_collider()


# Keep cycling through potentially visible objects within the area.
func _ready():
	while true:
		await Game.time.timer_every_1s.timeout
		# Get the closest body.
		var closest_body = null
		var closest_body_distance = INF
		
		for body in bodies_within_area:
			if !is_instance_valid(body): continue
			
			# See if it's visible.
			var collider = cast_to_target_with_dot(body.global_transform.origin, VISION_CONE)
			if  collider == null: continue
			body_seen(collider)
			break


# Controls
@export var VISION_DISTANCE = 5.0
@export var VISION_CONE = 0.5


func _on_body_entered(body):
	body_entered(body)


func _on_body_exited(body):
	body_exited(body)
