extends Node3D
class_name Townsperson


@onready var controller = $basic_controller


enum PersonMode {
	MODE_NONE, # Default enum value.
	MODE_ROAMING,
	MODE_STARE, # Stare at the player. That'll help.
	MODE_PICKUP, # Pick something up. (See variable "target")
}
@export var mode : PersonMode

# Target to pick up. Used by MODE_PICKUP.
var pickup_target : PhysicsBody3D


func drop_money():
	Game.world.drop_coin(controller.global_transform.origin)


func mode_pickup():
	while true:
		await Game.time.timer_every_0_1s.timeout
		if mode != PersonMode.MODE_PICKUP: break
		if pickup_target == null: break
		
		# Point at the target.
		# This is done by constructing a basis from the diff vector.
		var origin = controller.global_transform.origin
		var target = pickup_target.global_transform.origin
		
		var diff_vector = origin - target
		# Remove y-axis from the diff vector.
		diff_vector.y = 0
		var up_vector = controller.global_transform.basis.y
		
		var v_z = diff_vector.normalized()
		var v_x = up_vector.cross(v_z).normalized()
		var v_y = v_z.cross(v_x)
		
		var target_basis = Basis(v_x, v_y, v_z)
		controller.intent_basis = target_basis
		controller.intent_move = Vector2(-1.0, 0.0)


func mode_roaming():
	while true:
		await Game.time.timer_every_0_1s.timeout
		if mode != PersonMode.MODE_ROAMING: break
		
		# Pick a direction...
		var rand_vector = Vector3(randf() - 0.5, 0.0, randf() - 0.5).normalized()
		var up_vector = controller.global_transform.basis.y
		
		var v_z = rand_vector.normalized()
		var v_x = up_vector.cross(v_z).normalized()
		var v_y = v_z.cross(v_x)
		
		var target_basis = Basis(v_x, v_y, v_z)
		controller.intent_basis = target_basis
		controller.intent_move = Vector2(-1.0, 0.0)
		
		# Move for a bit...
		for _i in range(30):
			await Game.time.timer_every_0_1s.timeout
			if mode != PersonMode.MODE_ROAMING: break
			# Every tick, there's a chance of dropping money.
			if randf() < MONEY_DROP_CHANCE:
				drop_money()
	
		# Pause to ponder...
		controller.intent_move = Vector2(0.0, 0.0)
		for _i in range(randi() % 60):
			await Game.time.timer_every_0_1s.timeout
			if mode != PersonMode.MODE_ROAMING: break


func mode_staring():
	# Doesn't run forever. Eventually changes to roaming.
	for _i in range(50):
		await Game.time.timer_every_0_1s.timeout
		if mode != PersonMode.MODE_STARE: break
		
		# Point at the player.
		# This is done by constructing a basis from the diff vector.
		var origin = controller.global_transform.origin
		var target = Game.player.player_body.global_transform.origin
		var up = controller.global_transform.basis.y
		
		controller.intent_basis = Math.basis_from(origin - target, up)
	
	mode = PersonMode.MODE_ROAMING


const MONEY_DROP_CHANCE = 0.0005


# Called when the node enters the scene tree for the first time.
func _ready():
	while true:
		await Game.time.timer_every_0_5s.timeout
		
		match mode:
			PersonMode.MODE_ROAMING:
				await mode_roaming()
			PersonMode.MODE_STARE:
				await mode_staring()
			PersonMode.MODE_PICKUP:
				await mode_pickup()


func _on_vision_seen_valuable_thingy(body):
	# Only applies if roaming.
	if mode != PersonMode.MODE_ROAMING: return
	
	var speech = controller.get_node_or_null(controller.get_meta("speech_path"))
	if  speech == null: return
	
	pickup_target = body
	mode = PersonMode.MODE_PICKUP
	speech.speak("Ooh! Shiny!")


# TODO: Refactor bump_into and pick_up logic; currently it's copy-pasted between
# ....: player and townsperson.
func pick_up(body):
	# Only pick up things if mode is MODE_PICKUP.
	if mode != PersonMode.MODE_PICKUP: return
	body.queue_free()
	mode = PersonMode.MODE_ROAMING


func bump_into(body):
	if body.has_meta("value"):
		pick_up(body)


func _on_basic_controller_body_entered(body):
	bump_into(body)


func _on_area_3d_body_entered(body):
	bump_into(body)
