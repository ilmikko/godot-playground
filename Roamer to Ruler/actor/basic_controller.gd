extends RigidBody3D

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

# Controls where the actor is moving. Y -1 = backwards, Y +1 = forwards
@export var intent_move = Vector2(0, 0)
# Controls where the actor should be pointing at.
@onready var intent_basis = global_transform.basis
@export var intent_sprint = false

# Some properties of the basic controller.
@export var rotation_speed = 2.0
@export var movement_speed = 5.0
@export var sprint_speed = 6.0

@export var sprint_stamina_consumption_per_second = 35.0

@onready var stamina = get_node_or_null("stamina")

func _physics_process(delta):
	var direction = (global_transform.basis * Vector3(intent_move.y, 0, intent_move.x)).normalized()

	var velocity = Vector3(0, 0, 0)
	if direction:
		velocity.x = direction.x
		velocity.z = direction.z

	var speed = _calculate_speed(delta)
	global_transform.basis = global_transform.basis.orthonormalized().slerp(intent_basis.orthonormalized(), delta * rotation_speed)

	move_and_collide(velocity.normalized() * speed * delta)

func _calculate_speed(delta):
	if intent_sprint && stamina.attempt_spend(delta * sprint_stamina_consumption_per_second):
		
		return sprint_speed
	return movement_speed
