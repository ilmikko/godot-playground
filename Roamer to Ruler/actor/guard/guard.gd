extends Node3D


@onready var controller = $basic_controller

@export var raycast_path : NodePath
@onready var raycast = get_node(raycast_path)


# Controls
const VISION_DISTANCE = 10.0


enum GuardMode {
	PATROLLING,
	CHASING,
}

var mode : GuardMode = GuardMode.PATROLLING


# cast_to_target_with_dot does a raycast and returns a collider, with a catch:
# The dot product needs to be larger than the "dot" parameter.
# If "dot" is -1, acts like a regular raycast.
# If it's 0, only the "front" of the raycast is considered.
# If it's 1, the ray needs to be exactly perpendicular to the raycaster.
# In practice, if you want a cone of 45 degrees, you probably want a value of 0.5.
func cast_to_target_with_dot(target : Vector3, dot : float) -> PhysicsBody3D:
	var direction = raycast.to_local(target).normalized()
	
	raycast.target_position = direction * VISION_DISTANCE
	
	# Calculate the dot product to see which side of the raycast the ray is going
	# to be on. If it is outside of the "cone", return null.
	var forwards = direction.dot(-raycast.transform.basis.z)
	if  forwards < dot:
		return null
	
	return raycast.get_collider()


# Attempts to cast a ray to the player, if it hits, switches to CHASING.
func cast_to_player():
	var target = Game.player.player_body.global_transform.origin
	var collider = cast_to_target_with_dot(target, 0.5)
	if  collider == null: return
	if  collider.has_meta("player") && collider.get_meta("player"):
		mode = GuardMode.CHASING


func mode_chasing():
	# Point at the player.
	# This is done by constructing a basis from the diff vector.
	var origin = controller.global_transform.origin
	var target = Game.player.player_body.global_transform.origin
	
	var diff_vector = origin - target
	var up_vector = controller.global_transform.basis.y
	
	var v_z = diff_vector.normalized()
	var v_x = up_vector.cross(v_z).normalized()
	var v_y = v_z.cross(v_x)

	var target_basis = Basis(v_x, v_y, v_z)
	controller.intent_basis = target_basis
	controller.intent_move = Vector2(-1.0, 0.0)


func mode_patrolling():
	while true:
		await Game.time.timer_every_0_1s.timeout
		if mode != GuardMode.PATROLLING: break
		cast_to_player()
		
		# Pick a direction...
		var rand_vector = Vector3(randf() - 0.5, 0.0, randf() - 0.5).normalized()
		var up_vector = controller.global_transform.basis.y
		
		var v_z = rand_vector.normalized()
		var v_x = up_vector.cross(v_z).normalized()
		var v_y = v_z.cross(v_x)
		
		var target_basis = Basis(v_x, v_y, v_z)
		controller.intent_basis = target_basis
		controller.intent_move = Vector2(-1.0, 0.0)
		
		# Move for a bit...
		for _i in range(30):
			await Game.time.timer_every_0_1s.timeout
			if mode != GuardMode.PATROLLING: break
			cast_to_player()
	
		# Pause to ponder...
		controller.intent_move = Vector2(0.0, 0.0)
		for _i in range(randi() % 60):
			await Game.time.timer_every_0_1s.timeout
			if mode != GuardMode.PATROLLING: break
			cast_to_player()


# Called when the node enters the scene tree for the first time.
func _ready():
	while true:
		await Game.time.timer_every_0_5s.timeout
		
		match mode:
			GuardMode.CHASING:
				await mode_chasing()
			GuardMode.PATROLLING:
				await mode_patrolling()
