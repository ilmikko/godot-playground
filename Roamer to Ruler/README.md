# Roamer to Ruler

## MVP:

- Hide from guards = failure
- Get to safe place = victory
- Steal stuff on the way??? = score

## Features required for MVP

- [DONE] Movement
- [DONE] Enemies (guards)
- [DONE] Day / night
- [DONE?] World / village
- Score system
- Basic UI
- Some kind of timer

## Other features

- Early game: Building your safe place
  - Need a shelter
  - Need a bed
  - Need warmth
- Suspicious day jobs
  - Smuggling
  - Getting "rid" of someone
- Recruiting others to help in the day job / heists
- Have a non suspicious day job
  - Building stuff
  - Some mundane archival
  - Guard duty?
- General suspicion that you're the bad guy
  - You need to have protection
  - You need to bribe others
  - You need to help a guard do something criminal otherwise they will catch you
- Secret entrance to a thing
- Move barrels at day to plan for night heist
- Various heists:
  - Heist the church
  - Heist the rich people
  - Heist the castle
