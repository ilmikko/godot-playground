extends Node3D


func day_to_night():
	$Camera3D.make_current()
	$AnimationPlayer.play("day_to_night")
	await $AnimationPlayer.animation_finished
	Game.hideout()


func night_to_day():
	$Camera3D.make_current()
	$AnimationPlayer.play_backwards("day_to_night")
	await $AnimationPlayer.animation_finished
	Game.hideout()
