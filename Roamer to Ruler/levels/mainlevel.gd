extends Node3D


enum LevelState {
	STATE_DAYTIME,
	STATE_DAY_TO_NIGHT,
	STATE_NIGHTTIME,
	STATE_NIGHT_TO_DAY,
}

var state : LevelState = LevelState.STATE_NIGHTTIME


func load_level():
	match state:
		LevelState.STATE_DAYTIME:
			$current.add_child(DAY.instantiate())
		LevelState.STATE_NIGHTTIME:
			$current.add_child(NIGHT.instantiate())
		LevelState.STATE_DAY_TO_NIGHT:
			var time_passing = TIME_PASSING.instantiate()
			$current.add_child(time_passing)
			time_passing.day_to_night()
		LevelState.STATE_NIGHT_TO_DAY:
			var time_passing = TIME_PASSING.instantiate()
			$current.add_child(time_passing)
			time_passing.night_to_day()

func next_state():
	for c in $current.get_children():
		c.queue_free()

	match state:
		LevelState.STATE_DAYTIME:
			state = LevelState.STATE_DAY_TO_NIGHT
		LevelState.STATE_DAY_TO_NIGHT:
			state = LevelState.STATE_NIGHTTIME
		LevelState.STATE_NIGHTTIME:
			state = LevelState.STATE_NIGHT_TO_DAY
		LevelState.STATE_NIGHT_TO_DAY:
			state = LevelState.STATE_DAYTIME
	
	load_level()


func _ready():
	next_state()
	Game.hideout_accessed.connect(next_state)


const DAY = preload("res://levels/daytime.tscn")
const NIGHT = preload("res://levels/nighttime.tscn")
const TIME_PASSING = preload("res://levels/time_passing.tscn")
