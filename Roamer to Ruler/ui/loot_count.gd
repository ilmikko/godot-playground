extends RichTextLabel



func _ready():
	Game.score.score_changed.connect(_score_changed)


func _score_changed(new_score):
	text = str(new_score)

