extends ProgressBar

@onready var original_color = modulate

func _ready():
	await(Game.player.player_body.ready)
	var stamina = Game.player.player_body.stamina
	stamina.stamina_changed.connect(_on_stamina_changed)
	stamina.stamina_depleted.connect(_flash_red)


func _on_stamina_changed(new_stamina):
	value = (new_stamina / Game.player.player_body.stamina.max_stamina) * 100


func _flash_red():
	$AnimationPlayer.play("flash_red")
