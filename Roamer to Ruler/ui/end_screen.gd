extends Control

var loot_count = 0: set=_set_loot_count
var caught = false: set=_set_caught


func _enter_tree():
	get_tree().paused = true


func _set_loot_count(count):
	$ColorRect/VBoxContainer/CenterContainer2/RichTextLabel.text = "You found " + str(count) + " loot!"

func _set_caught(new_caught):
	$ColorRect/VBoxContainer/CenterContainer3/RichTextLabel.visible = new_caught
