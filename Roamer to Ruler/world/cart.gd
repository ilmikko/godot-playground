extends StaticBody3D


func body_entered(body):
	if body.has_meta("player") && body.get_meta("player"):
		Game.escaped()


func _on_area_3d_body_entered(body):
	body_entered(body)
