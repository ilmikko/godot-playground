extends StaticBody3D


func body_entered(body):
	if body.has_meta("player") && body.get_meta("player"):
		$AnimationPlayer.play("countdown")
		await $AnimationPlayer.animation_finished
		Game.hideout()


func body_exited(body):
	if body.has_meta("player") && body.get_meta("player"):
		$AnimationPlayer.stop()


func _on_area_3d_body_entered(body):
	body_entered(body)


func _on_area_3d_body_exited(body):
	body_exited(body)
