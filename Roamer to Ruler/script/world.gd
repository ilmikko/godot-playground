extends Node
class_name World

# Used to create elements (e.g. loot, dropped coins) in the world.
# Generally anything that interacts with the world should be done via this node.

func drop_coin(world_pos : Vector3):
	var coin = COIN.instantiate()
	add_child(coin)
	coin.global_transform.origin = world_pos


const COIN = preload("res://world/valuables/coin.tscn")
