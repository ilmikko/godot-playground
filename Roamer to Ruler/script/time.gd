extends Node
class_name GameTime


var t = 0.0


func _process(delta):
	t += delta


var timer_every_0_1s = Timer.new()
var timer_every_0_5s = Timer.new()
var timer_every_1s = Timer.new()


func wait(f : float):
	await get_tree().create_timer(f).timeout


func _enter_tree():
	add_child(timer_every_0_1s)
	timer_every_0_1s.wait_time = 0.1
	timer_every_0_1s.start()
	
	add_child(timer_every_0_5s)
	timer_every_0_5s.wait_time = 0.5
	timer_every_0_5s.start()
	
	add_child(timer_every_1s)
	timer_every_1s.wait_time = 1.0
	timer_every_1s.start()
