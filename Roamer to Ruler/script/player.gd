extends Node
class_name Player


var player


var player_body : get = _get_player_body
func _get_player_body():
	if player_body: return player_body
	player_body = player.get_node(player.get_meta("body"))
	return player_body


func register(_p):
	player = _p
