extends Node
class_name Math

static func basis_from(target : Vector3, up : Vector3):
	var v_z = target.normalized()
	var v_x = up.cross(v_z).normalized()
	var v_y = v_z.cross(v_x)

	var target_basis = Basis(v_x, v_y, v_z)
	return target_basis
