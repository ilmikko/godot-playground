extends Node


var player = Player.new()
var time = GameTime.new()
var score = Score.new()
var world = World.new()


signal hideout_accessed()


func _enter_tree():
	add_child(time)
	add_child(world)


func caught():
	var end_screen = preload("res://ui/end_screen.tscn").instantiate()
	end_screen.loot_count = score.score
	end_screen.caught = true
	get_tree().get_root().add_child(end_screen)


func hideout():
	Game.player.player_body.stamina.stamina_cap -= 20
	emit_signal("hideout_accessed")
