extends Node
class_name Score

signal score_changed(new_score)


var score = 0
var nights_survived = 0


func change(n):
	score += n
	score_changed.emit(score)
