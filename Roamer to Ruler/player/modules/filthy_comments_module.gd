extends Node3D


var last_filthy_comment = 0
var min_time_between_comments = 10 # seconds


var filthy_comments = [
	"You are filthy.",
	"Ew, gross.",
	"What a lowlife.",
	"You smell.",
	"You stink.",
	"Raggedy clothes.",
	"One without a home.",
	"My disgust is palpable.",
	"Ew.",
	"What a sad sight.",
	"Behold! A beggar!",
]
var i = randi()


func maybe_filthy_comment(body):
	var speech = body.get_node_or_null(body.get_meta("speech_path"))
	if  speech == null: return
	
	if Game.time.t - last_filthy_comment < min_time_between_comments:
		# No filthy comment; would be too soon.
		return
	
	last_filthy_comment = Game.time.t
	i += 1
	speech.speak(filthy_comments[i % len(filthy_comments)])


func maybe_stare(body):
	var townsperson = body.get_node_or_null(body.get_meta("townsperson_path"))
	if  townsperson == null: return
	
	townsperson.mode = Townsperson.PersonMode.MODE_STARE


func _on_body_entered(body):
	if body.has_meta("speech_path"):
		maybe_filthy_comment(body)
	if body.has_meta("townsperson_path"):
		maybe_stare(body)
