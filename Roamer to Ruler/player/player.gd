extends Node3D

func bump_into(body):
	if body.has_meta("value"):
		pick_up(body)
	if body.has_meta("guard") && body.get_meta("guard"):
		Game.caught()


func pick_up(body):
	# Play any sound via the animation player.
	if body.has_meta("pickup_sound"):
		$AnimationPlayer.play(body.get_meta("pickup_sound"))
	Game.score.change(body.get_meta("value"))
	body.queue_free()


func _input(event):
	if event is InputEventKey:         _input_event_key(event)
	if event is InputEventMouseMotion: _input_mouse_motion(event)


func _input_event_key(event : InputEventKey):
	var direction = Input.get_vector("move_forwards", "move_backwards", "move_left", "move_right")
	$basic_controller.intent_move = direction
	if event.is_action_pressed("sprint"):
		$basic_controller.intent_sprint = true
	if event.is_action_released("sprint"):
		$basic_controller.intent_sprint = false


func _input_mouse_motion(event : InputEventMouseMotion):
	var mouse  = event.velocity * MOUSE_INTENSITY
	var amount = -mouse.x
	$basic_controller.intent_basis = $basic_controller.intent_basis.rotated(Vector3.UP, amount)


func _enter_tree():
	Game.player.register(self)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _on_basic_controller_body_entered(body):
	bump_into(body)


func _on_area_3d_body_entered(body):
	bump_into(body)


const MOUSE_INTENSITY = 0.00002
