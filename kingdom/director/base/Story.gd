extends Node
class_name Story


var conditions = []
var state = 0


func condition_add(c):
	conditions_add([c])


func conditions_add(cs):
	conditions.append_array(cs)


func event_add(e):
	events_add([e])


func events_add(es):
	condition_add(NullCondition.new().then(es))
