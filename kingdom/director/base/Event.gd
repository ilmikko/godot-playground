extends Node
class_name Event


# Story that this event binds to.
var story


func run():
	push_error("Default event run() called for '%s'." % self)
