extends Node
class_name Condition


enum {
	BLOCKED,    # Condition is waiting.
	UNBLOCKED,  # Condition has passed.
	INFEASIBLE, # Condition will never pass. Not an error.
	ERROR,      # Condition has an error and cannot be determined if it passes or not.
}


# Story that this condition binds to.
var story
var nodes = []


# Every condition must have a check() method that returns one of the enum values.
func check():
	push_error("Default condition check() called for '%s'." % self)
	return ERROR


func then(_nodes):
	if !(_nodes is Array):
		_nodes = [_nodes]
	nodes = _nodes
	return self
