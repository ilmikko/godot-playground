extends Story

var EVENT_LEAVE = [StoryEndEvent.new()]


func _init(unit = null):
	event_add(ChoiceEvent.new([
		"What is it sire?"
	], {
		"Move the crates to build the house": 1,
		"Dismissed.": 2,
	}))
	condition_add(ChoiceCondition.new(1).then([
		CallMethodEvent.new(unit, "event_move_crates", null), StoryEndEvent.new()]))
	condition_add(ChoiceCondition.new(2).then(EVENT_LEAVE))
