extends Story

var DISMISSED = [StoryEndEvent.new()]

var unit


# Regular dialogue tree.
func dialogue():
	var options = {"Dismissed.": 999}
	var conditions = [ChoiceCondition.new(999).then(DISMISSED)]
	
	# Loop over Game.Treasury; everything >50 will be a dialog option.
	for k in Game.Treasury.keys():
		var amount = Game.Treasury.getX(k)
		if  amount < 50.0: continue
		var s = "Please send excess %s to Farawaylandia" % Resources.to_str(k)
		options[s] = k
		conditions.push_back(ChoiceCondition.new(k).then(sell_excess_dialogue(amount, k)))
	return [
		ChoiceEvent.new([
			"What is it, sire? What would you like me to do?"
		], options),
	] + conditions


# What is said at the very first time this character is met.
func greetings():
	if state > 0: return dialogue()
	
	return [
		ChoiceEvent.new([
			"Greetings, sire. I'm your Captain. I can take ships to far away lands."
		], {
			"Greetings.": 1,
		}),
		ChoiceCondition.new(1).then([StoryStateEvent.new(1)] + dialogue())
	]


func sell_excess_dialogue(amount, resource):
	var options = {"Never mind, forget it.": 999}
	var conditions = [ChoiceCondition.new(999).then(DISMISSED)]
	
	var i = 50
	while i < amount:
		options["Send %s crates." % i] = i
		conditions.push_back(ChoiceCondition.new(i).then([
			CallMethodEvent.new(unit, "event_send_shipment", [i, resource]),
			StoryEndEvent.new()
		]))
		i *= 2
	
	return [
		ChoiceEvent.new([
			"Indeed, sire. We have %s crates of %s available. How much would you like to send?" % [
				amount,
				Resources.to_str(resource),
			]
		], options),
	] + conditions


func rebuild():
	conditions = [NullCondition.new().then(greetings())]


func _init(_unit):
	unit = _unit
	rebuild()
