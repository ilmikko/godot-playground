extends Story

func _init(pawn):
	event_add(ChoiceEvent.new([
		"Yes, dear Sire?"
	], {
		"I praise you my dear citizen.": 1,
		"Farm": 2,
		"Carry on.": 3,
	}))
	condition_add(ChoiceCondition.new(1).then([
		CallMethodEvent.new(pawn, "event_praise", null),
		StoryEndEvent.new()
	]))
	condition_add(ChoiceCondition.new(2).then([
		CallMethodEvent.new(pawn, "event_farm", null),
		StoryEndEvent.new()
	]))
	condition_add(ChoiceCondition.new(3).then([
		CallMethodEvent.new(pawn, "event_leave", null),
		StoryEndEvent.new()
	]))
