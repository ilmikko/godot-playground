extends Event
class_name InventoryEvent


var delta


func run():
	for res in delta:
		var amount = delta[res]
		if  amount == 0: continue
		if  amount  > 0:
			Game.Economy.addX(res, amount)
		else:
			Game.Economy.remove(res, amount)


func _init(d):
	delta = d
