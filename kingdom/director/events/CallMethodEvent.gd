extends Event
class_name CallMethodEvent


var object
var method
var args


func run():
	if args == null:
		object.call(method)
	elif len(args) == 1:
		object.call(method, args[0])
	elif len(args) == 2:
		object.call(method, args[0], args[1])
	else:
		assert(false) #,"Oops, argument length not supported.")


func _init(o, m, a):
	object = o
	method = m
	args = a
