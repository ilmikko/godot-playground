extends Event
class_name StoryEvent
# StoryEvent begins new stories, but does not block on them.
# Shuffles the order of stories if multiple are passed.


var stories = []


func run():
	for story in stories:
		Game.Director.story_add(story)


func _init(_paths):
	if !(_paths is Array):
		_paths = [_paths]
	if len(_paths) > 1:
		_paths.shuffle()
	for path in _paths:
		stories.push_back(load(path).new())
