extends Event
class_name TinyTextEvent


var text = ""


func run():
	await Game.UI.Events.tiny_text_event(text)


func _init(t):
	text = t
