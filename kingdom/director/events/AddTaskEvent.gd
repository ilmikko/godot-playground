extends Event
class_name AddTaskEvent


var actor
var task


func run():
	actor.tasks_backlog.add_child(task)


func _init(actor, task):
	self.actor = actor
	self.task = task
