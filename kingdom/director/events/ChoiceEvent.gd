extends Event
class_name ChoiceEvent


var text = ""
var choices = {}


func run():
	await Game.UI.Events.Choice.choice_event(text, choices)


func _init(t, cs):
	text = t
	choices = cs
