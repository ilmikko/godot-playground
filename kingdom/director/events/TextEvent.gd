extends Event
class_name TextEvent


var text = ""


func run():
	await Game.UI.Events.text_event(text)


func _init(t):
	text = t
