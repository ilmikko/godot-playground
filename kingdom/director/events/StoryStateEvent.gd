extends Event
class_name StoryStateEvent


var state = 0


func run():
	story.state = state


func _init(s):
	state = s
