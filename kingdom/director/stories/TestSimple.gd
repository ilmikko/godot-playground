extends Story

func _init():
	# Events are run immediately.
	event_add(TextEvent.new("This is a simple event."))
	event_add(TextEvent.new("So is this."))
