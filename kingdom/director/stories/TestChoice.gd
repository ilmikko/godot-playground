extends Story

func _init():
	# Events are run immediately.
	event_add(ChoiceEvent.new([
		"This is a choice event.",
		"You must choose wisely.",
	], {
		"I am choosing 'wisely'.": 1,
		"Right.": 2,
		"Left.": 3,
	}))
	# Conditions need to be matched with an ID.
	condition_add(ChoiceCondition.new(1).then([
		TextEvent.new("You have chosen wisely."),
	]))
	condition_add(ChoiceCondition.new(2).then([
		TextEvent.new("You have chosen right."),
	]))
	condition_add(ChoiceCondition.new(3).then([
		TextEvent.new("You have chosen wrong."),
	]))
