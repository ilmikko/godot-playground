extends Story

func _init():
	events = {
		1: [
			ChoiceEvent.new([
				"The Captain Ysmard from Iron Isles comes to seek your council.",
				"Dear King O so mighty. I have lost my ship to pirates in your seas.",
				"All I plea is fifteen hundred gold from the treasury to sail back to my homeland.",
			], {
				"Who is this man?": 2,
				"Give him 1500 gold.": 3,
				"Send him away.": 4,
				"Pirates? We need to deal with them.": 5,
			}),
		],
		2: [],
		3: [],
		4: [],
		5: [],
	}
