extends Story

func _init():
	events_add([
		TinyTextEvent.new([
			"You feel curious."
		]),
		StoryEndEvent.new(),
	])
