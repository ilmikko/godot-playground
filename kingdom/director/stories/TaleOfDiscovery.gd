extends Story

var THE_THREE_SACRED_THINGS = UITextColor.new("The Three Sacred Things", Color(1, 0.5, 0))
var THE_CROWN_OF_COURAGE    = UITextColor.new("The Crown of Courage", Color(1, 0, 0))
var THE_WALLET_OF_WISDOM    = UITextColor.new("The Wallet of Wisdom", Color(0, 1, 0))
var THE_CANE_OF_CURIOUSITY  = UITextColor.new("The Cane of Curiousity", Color(0, 0, 1))
var PROFOUND_DETERMINATION  = UITextColor.new("profound determination", Color(0.6, 0.7, 0.6))

func _init():
	events_add([
		TextEvent.new([
			"The winds of change blow softly on the castle.", "\n",
			"With it, a paper note makes its way through the window.", "\n",
			"It lands on your table, and you gaze at it.", "\n",
			"\n", "It reads:", UITextPause.new(5), "\n",
			"Those Who Seek to Win This Game, Must Bring Forth ", THE_THREE_SACRED_THINGS, ":", "\n",
			THE_CROWN_OF_COURAGE, "\n",
			THE_WALLET_OF_WISDOM, "\n",
			"and ", THE_CANE_OF_CURIOUSITY, ".", UITextPause.new(15), "\n",
			"\n", "You are filled with ", PROFOUND_DETERMINATION, " to find these items.",
		]),
		StoryEvent.new([
			"res://director/stories/TheCaneOfCuriousity.gd",
			"res://director/stories/TheCrownOfCourage.gd",
			"res://director/stories/TheWalletOfWisdom.gd",
		]),
		StoryLockReleaseEvent.new(),
	])
	conditions_add([
		StoryEndCondition.new("res://director/stories/TheCaneOfCuriousity.gd").then(
			StoryEndCondition.new("res://director/stories/TheCrownOfCourage.gd").then(
				StoryEndCondition.new("res://director/stories/TheWalletOfWisdom.gd").then([
					# TODO: GameEndEvent?
					TextEvent.new([
						"Congratulations you have run out of developed content."
					]),
					StoryEndEvent.new()
				])
			)
		)
	])
