extends Story

func _init():
	events_add([
		TinyTextEvent.new([
			"You feel courageous."
		]),
		StoryEndEvent.new(),
	])
