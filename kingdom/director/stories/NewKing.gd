extends Story

var EVENT_GOLD = [
	InventoryEvent.new({
		Resources.GOLD: 1000,
	}),
	StoryEndEvent.new(),
]
var EVENT_FIRED = [
	ChoiceEvent.new([
		"But... but...",
	], {
		"Yes, you're a butt. Now get out of my sight.": 1,
		"Fear not, it was just a jest.": 2,
		"Guards! Seize him!": 3,
	}),
	ChoiceCondition.new(1).then([
		TinyTextEvent.new("The advisor leaves, holding back tears."),
		StoryEndEvent.new(),
	]),
	ChoiceCondition.new(2).then([
		ChoiceEvent.new([
			"...",
		], {
			"Now let's get to work.": 1,
		}),
		ChoiceCondition.new(1).then([
			StoryEndEvent.new(),
		]),
	]),
	ChoiceCondition.new(3).then([
		TinyTextEvent.new("Guards tackle the advisor, and drag him out of the room."),
		StoryEndEvent.new(),
	]),
]
var EVENT_NO   = [
	ChoiceEvent.new([
		"W... What do you mean 'No'?",
		"It's 1000 gold, your Sire.",
		"Surely you meant to say 'Yes'?",
	], {
		"No.": 1,
		"Yes.": 2,
		"You're fired.": 3,
	}),
	ChoiceCondition.new(1).then([
		ChoiceEvent.new([
			"Now I'm confused.",
			"Do you mean 'No', as in, 'No, I don't want it', or 'No, I meant to say yes'?",
		], {
			"No.": 1,
			"Yes.": 1,
		}),
		ChoiceCondition.new(1).then([
			TinyTextEvent.new("The advisor leaves without saying a word."),
			StoryEndEvent.new(),
		]),
	]),
	ChoiceCondition.new(2).then(EVENT_GOLD),
	ChoiceCondition.new(3).then(EVENT_FIRED),
]

func _init():
	event_add(ChoiceEvent.new([
		"Welcome to our new King!",
		"Please accept this humble gift of 1000 gold.",
	], {
		"Thank you!": 1,
		"No.": 2,
		"You're fired.": 3,
	}))
	condition_add(ChoiceCondition.new(1).then(EVENT_GOLD))
	condition_add(ChoiceCondition.new(2).then(EVENT_NO))
	condition_add(ChoiceCondition.new(3).then(EVENT_FIRED))
