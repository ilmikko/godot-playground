extends Story

func _init():
	# Events are run immediately.
	event_add(TextEvent.new("This is a simple event."))
	event_add(TextEvent.new("So is this."))
	# Conditions run events after some condition has passed.
	condition_add(WaitCondition.new(2.5).then([
		TextEvent.new("2.5 seconds have passed."),
		StoryEndEvent.new(),
	]))
