extends Story

func _init():
	events_add([
		TinyTextEvent.new([
			"You feel wise."
		]),
		StoryEndEvent.new(),
	])
