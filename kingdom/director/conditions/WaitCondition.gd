extends Condition
class_name WaitCondition


var wait_time = 0.0
var waiting_for = INF


func check():
	if Game.Director.t < waiting_for: return BLOCKED
	return UNBLOCKED


# Stores the waiting start time.
func wait_start(t):
	waiting_for = wait_time + t


func _init(t = 0.0):
	wait_time = t
