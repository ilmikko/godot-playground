extends Condition
class_name ChoiceCondition


var choice_id = -1;
# Any choices before this time do not count.
var latest_t  = 0.0;


func check():
	# The latest choice is stale.
	if latest_t >= Game.UI.Events.Choice.latest_choice_t: return BLOCKED
	# Player chose something, but it wasn't this one.
	if choice_id != Game.UI.Events.Choice.latest_choice: return INFEASIBLE
	return UNBLOCKED


func wait_for_choice(t):
	latest_t = t


func _init(id):
	choice_id = id
