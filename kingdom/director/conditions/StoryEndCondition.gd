extends Condition
class_name StoryEndCondition


var story_path


func check():
	# TODO: Check if this story has been finished.
	if story_path in Game.Director.stories_ended:
		return UNBLOCKED
	return BLOCKED


func _init(_story_path = ""):
	story_path = _story_path
