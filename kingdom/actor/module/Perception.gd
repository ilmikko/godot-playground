extends Node3D

# Handles perception of the world for an actor.


@export var body_path: NodePath
@onready var body = get_node_or_null(body_path)

var near_pawns = {}

func get_near_pawns():
	return near_pawns


func get_nearest_pawn():
	var origin = body.global_transform.origin
	
	var nearest_pawn = null
	var nearest_pawn_distance = INF
	
	for p in near_pawns:
		var dist = (origin - p.global_transform.origin).length()
		if  dist > nearest_pawn_distance:
			continue
		nearest_pawn = p
		nearest_pawn_distance = dist
	
	return nearest_pawn


func _on_Area_body_entered(_body):
	if _body == body:
		return
	if body is CharacterBody3D:
		near_pawns[_body] = _body


func _on_Area_body_exited(_body):
	if _body == body:
		return
	if _body is CharacterBody3D:
		near_pawns.erase(_body)
