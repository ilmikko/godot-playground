extends Node


@export var modules_path: NodePath
@onready var modules = get_node_or_null(modules_path)

@export var subordinates_group_name: String = ""


# Main task, father of all other tasks.
const Main = preload("res://actor/tasks/Main.gd")

const DelegationHandler = preload("res://actor/handlers/DelegationHandler.gd")

@onready var delegation_handler = DelegationHandler.new($"..", subordinates_group_name)

# The "main" task, handles subtasks.
var task = Main.new(modules, [])


func _ready():
	add_child(delegation_handler)
	task.modules = modules
	task.run()


# Returns whether the task handler can handle more tasks or not.
func is_busy() -> bool:
	return task.is_busy()


# Interrupts or delegates a task.
func handle(_task):
	if delegation_handler.attempt_to_delegate(_task):
		# Delegation successful.
		return
	
	print(self, "delegation unsuccessful")
	task.interrupt(_task)


# Causes the current task to idle, and the actor to reconsider their
# tasks.
func idle():
	task.idle()


# Interrupts a current task with a new one if there is one.
func interrupt(_task):
	if !_task:
		return
	
	# Easy: If there is no current task, there is nothing to interrupt.
	# start up the new task.
	if !task or !is_instance_valid(task):
		task.interrupt(_task)
		return
	
	# Ask the current task whether to interrupt or not.
	var response = await task.interrupt(_task)
	match response:
		BaseTask.INTERRUPT_IGNORE:
			print("Task %s ignored interrupt from task %s" % [task, _task])
		BaseTask.INTERRUPT_HANDLE:
			pass
		BaseTask.INTERRUPT_REPLACE:
			# Replace the task. Remove old tasks.
			if task != _task:
				task.queue_free()
			task.interrupt(_task)
		_:
			push_error("Unknown response from task %s: %s" % [task, response])


func ingest_node_tasks(node):
	for _task in node.get_children():
		if !(_task is BaseTask):
			continue
		
		task.interrupt(_task)
