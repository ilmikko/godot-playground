extends Node

@export var CalledByPlayerTask: Script = preload("res://actor/tasks/CalledByPlayer.gd")
@export var FarmTask: Script = preload("res://actor/tasks/Farm.gd")
@export var DialogueScript: Script = preload("res://director/discussions/PawnDiscussions.gd")


# Retain a copy of a DialogueScript.
var dialogue_script


func event_move_crates():
	var ship_cargos = get_tree().get_nodes_in_group("ship_cargo")
	for ship_cargo in ship_cargos:
		var task = load("res://actor/tasks/TakeItemToLocation.gd").new()
		task.item_path = ship_cargo.get_path()
		# TODO: Do not hardcode this
		task.target_location_path = "/root/Root/Port/HouseBeingBuilt"
		add_child(task)
		$"..".task.handle(task)


func event_leave():
	$"..".task.idle()


func event_farm():
	$"..".task.handle(FarmTask.new())


func event_praise():
	$"..".speech.speak("WHY THANK YOU SO MUCH MY KING I AM FOREVER GRATEFUL THAT YOU HAVE SAID THIS THING TO ME I AM JUST A HUMBLE CITIZEN OF THE WORLD OF UMM AND I THANK FOR YOUR TIME OH MY GOOOOOOOOOOOOOOOOOOOOD THIS IS IT I'M HYPERVENTILATING... BREATHE...")


func event_send_shipment(amount, resource):
	print("About to send shipment of %s of %s" % [amount, Resources.to_str(resource)])
	$"..".captain.prepare_ship(amount, resource)


func start_dialogue():
	if dialogue_script == null:
		dialogue_script = DialogueScript.new(self)
	var task = CalledByPlayerTask.new("You called?", dialogue_script)
	$"..".task.interrupt(task)
