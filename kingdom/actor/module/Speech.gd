extends Node3D

# Handles speech of an actor.


var text_intent = ""


func text_wait(text):
	var wait = 0.0
	for chunk in text:
		match chunk:
			".":
				wait += 0.8
			"!":
				wait += 0.8
			"?":
				wait += 0.8
			_:
				wait += 0.05
	return wait


# Returns the amount of time to wait, in milliseconds.
# Things like "." and " " cause a slight pause.
func pop_text(count = 1) -> float:
	var chunk = text_intent.substr(0, count)
	$Label3D.text += chunk
	text_intent    = text_intent.substr(count, -1)
	return text_wait(chunk)


func done_speaking():
	$Label3D.text = ""
	# Last yield to make the Godot script happy.
	await Game.TimeX.wait(0.1)


func speak(text : String):
	$Label3D.text = ""
	
	# *whistles*, etc. isn't speech - it gets displayed immediately.
	# However, we still wait for text_wait amount of time.
	if text[0] == "*":
		$Label3D.text = text
		await Game.TimeX.wait(text_wait(text))
		text_intent = ""
		return
	
	text_intent = text
	while text_intent != "":
		var t = pop_text()
		await Game.TimeX.wait(t)
