extends Node3D

# Handles movement for an actor.


@export var body_path: NodePath
@onready var body = get_node_or_null(body_path)

var body_and_target_collided = false


# Moves the body to a target_point, using navigation paths.
func move_to(target):
	var target_point = target if target is Vector3 else target.global_transform.origin
	var target_body = null if target is Vector3 else target
	
	while Game.Navigation == null:
		print("Waiting for navigation...")
		await Game.TimeX.wait(0.5)
#	var path = Game.Navigation.get_simple_path(
#		body.position, target_point# Game.Navigation.get_closest_point_to(target_point)
#	)
	var path = [] # TODO: Fix
	while path.size() > 0:
		# TODO: Hilariously broken
		# yield(rotate_toward(path[0]), "completed")
		await move_toward_point(path[0], target_body)
		path.remove(0)
	# Last yield to make the Godot script happy.
	await Game.TimeX.wait(0.02)


# Moves the body towards a target_point without regard to navigation paths.
func move_toward_point(target_point: Vector3, target_body):
	var move_speed = 10.0
	var close_enough = 1.5
	
	while true:
		var diff = target_point - body.global_transform.origin
		var direction = diff.normalized()
		if diff.length() < close_enough: break
		body.set_velocity(direction * move_speed)
		body.move_and_slide()
		body.velocity
		if target_body && collided_with_target(target_body): break
		
		await Game.TimeX.wait(0.02)
	
	# Last yield to make the Godot script happy.
	await Game.TimeX.wait(0.02)


func collided_with_target(target):
	for i in range(0, body.get_slide_collision_count() - 1):
		if target is Area3D && target.overlaps_body(body):
			return true
		if body.get_slide_collision(i).collider == target:
			return true
	return false


# Rotates the body back to its "default" rotation.
func rotate_default(rotation_speed = 0.1):
	var target = Transform3D()
	var current = body.global_transform.interpolate_with(target, rotation_speed)
	while current.basis.z.dot(target.basis.z) < 0.999:
		body.transform.basis = current.basis
		current = body.global_transform.interpolate_with(target, rotation_speed)
		await Game.TimeX.wait(0.05)
	
	# Last yield to make the Godot script happy.
	await Game.TimeX.wait(0.05)


# Rotates the body "toward" a target_point, without regard to the "up" axis.
# Attempts to align the Z basis vectors.
func rotate_toward(target_point : Vector3, rotation_speed = 0.1):
	var target = body.transform.looking_at(target_point, Vector3.UP)
	var current = body.transform.interpolate_with(target, rotation_speed)
	while current.basis.z.dot(target.basis.z) < 0.999:
		body.transform.basis = current.basis
		current = body.transform.interpolate_with(target, rotation_speed)
		await Game.TimeX.wait(0.05)
	
	# Last yield to make the Godot script happy.
	await Game.TimeX.wait(0.05)
