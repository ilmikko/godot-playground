extends Node3D

const LeaveKingdom = preload("res://actor/tasks/LeaveKingdom.gd")

const NO_FOOD_FLEE_CHANCE_PER_SECOND = 0.1

@export var body_path: NodePath
@onready var body = get_node_or_null(body_path)

func _ready():
	Game.PopulationManager.register_citizen(self)

func _on_not_enough_food(seconds_without_enough_food):
	if _should_flee_due_to_lack_of_food(seconds_without_enough_food):
		_flee()

func _should_flee_due_to_lack_of_food(seconds_without_enough_food):
	var chance_of_fleeing = seconds_without_enough_food * NO_FOOD_FLEE_CHANCE_PER_SECOND
	var number_between_1_and_100 = (randi() % 100) + 1
	return chance_of_fleeing > number_between_1_and_100

func _flee():
	$"..".task.task.abandon_task()
	Game.PopulationManager.unregister_citizen(self)
	var leave_kingdom = LeaveKingdom.new()
	leave_kingdom.actor = $"../.."
	$"..".task.interrupt(leave_kingdom)
