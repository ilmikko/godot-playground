extends Node

var actor

@onready var delegation_handler = $DelegationHandler

func handle_task(task):
	actor.tasks_backlog.add_child(task)
	_attempt_to_delegate_task()

func _ready():
	delegation_handler.subordinates_group_name = actor.subordinates_group_name

func next_task():
	if actor.tasks_backlog == null: return

	var backlog_tasks = actor.tasks_backlog.get_children()
	if backlog_tasks.size() == 0: return
	actor.task = backlog_tasks[0]
	actor.task.actor = actor
	actor.task.prepare()
	actor.task.get_parent().remove_child(actor.task)
	actor.add_child(actor.task)
	actor.task.run()


func _attempt_to_delegate_task():
	if actor.tasks_backlog == null: return

	var backlog_tasks = actor.tasks_backlog.get_children()
	if backlog_tasks.size() == 0: return
	var backlog_task = backlog_tasks[0]
	delegation_handler.attempt_to_delegate(backlog_task)
