extends Node
class_name DelegationHandler


# Which group to look for.
var subordinates_group_name = ""
# Which node owns this DelegationHandler.
var leader = null


func attempt_to_delegate(task) -> bool:
	var available_subordinates =  _available_subordinates()
	if  available_subordinates.is_empty(): return false
	
	
	var subordinate = available_subordinates.pop_front()
	var modules = subordinate_modules(subordinate)
	if  modules == null:
		push_warning("Subordinate was available but did not have modules.")
		return false
	
	
	modules.task.handle(task)
	return true


func subordinate_modules(subordinate):
	return subordinate.get_node_or_null("RegularModules")


func subordinate_is_close(subordinate):
	var diff = subordinate.global_transform.origin - leader.global_transform.origin
	if  diff.length() < 30:
		return true
	return false


func subordinate_is_free(subordinate):
	var modules = subordinate_modules(subordinate)
	if  modules != null:
		# TODO: Depends on who's asking.
		return !modules.task.is_busy()
	# Unknown what this is, so assume they're not free or cannot handle tasks.
	return false


func _available_subordinates():
	var available_subordinates = []
	
	for subordinate in _get_subordinates():
		if !subordinate_is_close(subordinate):
			continue
		if !subordinate_is_free(subordinate):
			continue
		available_subordinates.append(subordinate)
	
	print(self, "I have %s available subordinates" % len(available_subordinates))
	return available_subordinates


func _get_subordinates():
	return get_tree().get_nodes_in_group(subordinates_group_name)


func _init(_leader = null, _subordinate_group_name = ""):
	leader = _leader
	subordinates_group_name = _subordinate_group_name
