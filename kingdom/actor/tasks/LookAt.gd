extends BaseTask

@export var target: Vector3

func run():
	await modules.movement.rotate_toward(target, 0.1)
