extends BaseTask

@export var target_path: NodePath
@onready var target = get_node_or_null(target_path)

func run():
	var move_to_target = target.global_transform.origin
	if target.has_method("location_as_area"):
		move_to_target = target.location_as_area()
	await modules.movement.move_to(move_to_target)
