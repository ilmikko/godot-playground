extends BaseTask


var rambles = [
	"Look at those clouds.",
	"Hmm. It might rain tonight.",
	"Scratch that thought.",
	"*whistles*",
	"I wonder what's for dinner.",
	"They're coming for me.",
	"Hmm.. no. That's a terrible idea.",
	"What am I doing here?",
	"What is anyone doing anywhere?",
	"What is my purpose?",
	"Is this a simulation?",
	"What even IS a simulation?",
	"Huh! Anyway.",
	"What are you looking at?",
	"Spies.",
	"Good heavens I cannot move.",
	"Could someone help me move?",
	"Anyone?",
	"Hello?",
	"I see people walking past.",
	"Sir?",
	"Helloooooo?",
	"Why is no one talking to me?",
	"Am I on hidden camera?",
	"What is a camera?",
	"Sigh.",
]
var current_ramble_index = randi() % len(rambles)

func run():
	while !task_has_been_abandoned:
		current_ramble_index += 1
		var ramble = rambles[current_ramble_index % len(rambles)]
		await modules.speech.speak(ramble)
		await Game.TimeX.wait(2.0)
