extends BaseTask

@export var target_path: NodePath
@onready var target = get_node_or_null(target_path)

func run():
	await modules.movement.move_to(target)
