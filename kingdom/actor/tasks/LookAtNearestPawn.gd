extends BaseTask

const LookAt = preload("res://actor/tasks/LookAt.gd")

func run():
	while !task_has_been_abandoned:
		var nearest_pawn = modules.perception.get_nearest_pawn()
		
		if !nearest_pawn:
			await modules.speech.speak("I have no one to look at...")
			continue
		
		var look_at = LookAt.new()
		look_at.target = nearest_pawn.global_transform.origin
		await run_other_task(look_at)
		await modules.speech.speak("Oh! Hey %s" % nearest_pawn)
