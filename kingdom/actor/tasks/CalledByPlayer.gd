extends BaseTask

var speak
var dialogue_script


func run():
	var rotation_speed = 0.5
	var rotation_target = Game.Player.global_transform.origin
	await modules.movement.rotate_toward(rotation_target, rotation_speed)
	await speak_and_i_shall_deliver()


# Are you frigin kidding me?
func speak_and_i_shall_deliver():
	await Game.Director.story_run(dialogue_script)
	await modules.speech.speak(speak)


func _init(_speak = "", _dialogue_script = null):
	priority = 100
	speak = _speak
	dialogue_script = _dialogue_script
