extends "res://actor/tasks/BaseTask.gd"

const GoToSpatial = preload("res://actor/tasks/GoToSpatial.gd")
const GoToRigidBody = preload("res://actor/tasks/GoToRigidBody.gd")

@export var item_path: NodePath
@onready var item = get_node_or_null(item_path)
@export var target_location_path: NodePath
@onready var target_position = get_node_or_null(target_location_path)


func run():
	# Go to item
	await _go_to_item()
	# Pick it up
	_pick_up_item()
	# Go to target
	if task_has_been_abandoned: return
	await _go_to_target()
	# Drop it
	_drop_item()


func _go_to_item():
	var go_to_item = GoToRigidBody.new()
	go_to_item.target = item
	await run_other_task(go_to_item)


func _go_to_target():
	var go_to_target = GoToSpatial.new()
	go_to_target.target = target_position
	await run_other_task(go_to_target)


func _pick_up_item():
	item.get_parent().remove_child(item)
	modules.movement.add_child(item)
	item.transform.origin = Vector3(0, 3, 0)


func _drop_item():
	item.get_parent().remove_child(item)
	get_tree().get_root().add_child(item)
	item.global_transform = modules.movement.global_transform
	if target_position.has_method("item_dropped"):
		target_position.item_dropped(item)
