extends BaseTask

const GoToLocation = preload("res://actor/tasks/GoToLocation.gd")

func run():
	while !task_has_been_abandoned:
		var go_to_location = GoToLocation.new()
		go_to_location.modules = modules
		go_to_location.target = Game.Locations.get_random_wander_location().global_transform.origin
		await go_to_location.run()
