extends BaseTask


# Contains main logic for game pawns, and handles task interruptions and their
# priorities.

# Retains which tasks the actor has yet to do.
var task_backlog = []
var idle_tasks   = [preload("res://actor/tasks/LookAtDefault.gd").new()]
var current_task = null


func run():
	pass


func idle():
	if task_backlog.is_empty():
		# Insert "idle" tasks if there is nothing to do.
		interrupt(idle_tasks[randi()%len(idle_tasks)])
		return
	
	# Do remaining items in the backlog in random order.
	task_backlog.sort_custom(Callable(Sort.Tasks, "by_priority"))
	interrupt(task_backlog.pop_front())


# Add a new task to the backlog.
# Prioritise here - the task might be important!
func interrupt(_task):
	if is_busy():
		# Check if the new task is more important than the current task.
		if _task.priority >= current_task.priority:
			# New task is more important!
			task_backlog.push_back(current_task)
		else:
			# Current task is more important!
			task_backlog.push_back(_task)
			return INTERRUPT_HANDLE
	
	current_task = _task
	await run_other_task(current_task)
	current_task = null
	idle()
	return INTERRUPT_HANDLE


# Whether or not this task can accept new interrupts.
# Used for delegation. ("Are you busy?")
func is_busy() -> bool:
	# If I have no task, I'm not busy.
	if current_task == null:
		return false
	# If I have an idle task, I'm not busy.
	if current_task in idle_tasks:
		return false
	return true


func _init(_modules = null, _task_backlog = null):
	priority = 0
	modules = _modules
	task_backlog = _task_backlog
