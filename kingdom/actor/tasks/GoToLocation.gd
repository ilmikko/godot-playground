extends "res://actor/tasks/BaseTask.gd"

@export var target: Vector3


func interrupt(_task):
	# TODO: Stop movement on interrupt.
	# actor.movement.stop()
	return BaseTask.INTERRUPT_REPLACE


func run():
	await modules.movement.move_to(target)
