extends Node
class_name BaseTask

enum {
	# Task is busy and will drop the other task on the floor.
	INTERRUPT_IGNORE,
	# Task is handling the interrupt internally.
	INTERRUPT_HANDLE,
	# Task can be replaced with the new task.
	INTERRUPT_REPLACE,
}

@export var actor_path: NodePath
@onready var actor = get_node_or_null(actor_path)


# Relative priority of the task.
var priority = 0


# Should get populated at init time.
var modules = null

var task_has_been_abandoned = false

# Interrupt this task with some other task.
# The task itself has the ability to decide what to do with the interrupt.
# Note that base tasks always allow replacing themselves.
func interrupt(_task):
	return INTERRUPT_REPLACE


func run_other_task(_task):
	_task.modules = modules
	await _task.run()


func run():
	push_error("Base task run() called. This might mean that a task has not implemented run() correctly.")

func abandon_task():
	task_has_been_abandoned = true
