extends Node

signal not_enough_food(seconds_without_enough_food)
var citizens = []
var seconds_without_enough_food = 0

func register_citizen(citizen):
	citizens.append(citizen)
	connect("not_enough_food", Callable(citizen, "_on_not_enough_food"))

func unregister_citizen(citizen):
	citizens.erase(citizen)
	disconnect("not_enough_food", Callable(citizen, "_on_not_enough_food"))


func _on_FoodConsumptionTimer_timeout():
	Game.Economy.remove(Resources.FOOD, citizens.size())
	alert_citizens_of_food_shortages()


func alert_citizens_of_food_shortages():
	if Game.Economy.treasury.getX(Resources.FOOD) > 0: return
	
	seconds_without_enough_food += 1
	emit_signal("not_enough_food", seconds_without_enough_food)
