@tool
extends PanelContainer


@export var title = "": set = _set_title
func _set_title(t):
	title = t
	$HBoxContainer/Key.text = "%s: " % t


@export var value = "": set = _set_value
func _set_value(v):
	value = v
	$HBoxContainer/Value.text = str(v)
