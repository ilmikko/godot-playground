extends VBoxContainer

@onready var Conditions = $Conditions
@onready var DirectorTime = $DirectorTime
@onready var EventsAvailable = $EventsAvailable
@onready var StoriesActive = $StoriesActive
@onready var StoriesAvailable = $StoriesAvailable
@onready var EventLock = $EventLock
@onready var StoryLock = $StoryLock
