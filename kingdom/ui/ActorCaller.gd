extends Node


func _ready():
	Game.ActorCaller = self


func raycast_hit(node):
	# 1. Check if RegularModules has event_called_by_player.
	var modules = node.get_node_or_null("RegularModules")
	if  modules != null:
		if modules.has_method("event_called_by_player"):
			modules.event_called_by_player()
			return
	# 2. Check if node itself has event_called_by_player.
	if node.has_method("event_called_by_player"):
		node.event_called_by_player()
		return
	# End.


func _input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		var camera = Game.CameraX
		var raycast = camera.raycast
		
		var from = camera.project_ray_origin(event.position)
		var to = from + camera.project_ray_normal(event.position) * 100
		raycast.target_position = raycast.to_local(to)
		raycast.force_update_transform()
		raycast.force_raycast_update()
		var collider = raycast.get_collider()
		if collider:
			raycast_hit(collider)
