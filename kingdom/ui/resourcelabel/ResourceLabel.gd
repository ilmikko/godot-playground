extends Label3D


var requirements = {}: set = _set_requirements
func _set_requirements(reqs):
	requirements = reqs
	update_label()


func addX(type, amount):
	if !(type in requirements):
		requirements[type] = 0
	requirements[type] += amount
	update_label()


func sub(type, amount):
	requirements[type] -= amount
	update_label()


func update_label():
	if requirements.is_empty():
		text = ''
		return
	
	var s = "I need:\n"
	for resource in requirements:
		s += str(requirements[resource]) + " " + Resources.to_str(resource) + "\n"
	text = s


func _init():
	update_label()
