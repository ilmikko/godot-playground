extends Control

@onready var Choice = $MultipleChoice


func debug_event(message=""):
	var event = EVENT_TINY.instantiate()
	event.message = message
	event.timeout = 100000.0
	$SideEvents.add_child(event)
	await event.completed
	event.queue_free()


func tiny_text_event(message=""):
	var event = EVENT_TINY.instantiate()
	event.message = message
	$SideEvents.add_child(event)
	await event.completed
	event.queue_free()


func text_event(multiline_text=""):
	var event = EVENT_TEXT.instantiate()
	event.write_message(multiline_text)
	$CenterEvents.add_child(event)
	await event.completed
	event.queue_free()


const EVENT_TINY = preload("res://ui/event/EventTiny.tscn")
const EVENT_TEXT = preload("res://ui/event/EventText.tscn")
