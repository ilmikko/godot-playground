extends VBoxContainer


func display(resource, value):
	var label = Resources.to_str(resource)
	var t = get_node_or_null(label)
	if  t == null:
		t = StatKeyVal.instantiate()
		t.name = label
		t.title = label
		Game.UI.Stats.Treasury.add_child(t)
	t.value = value


const StatKeyVal = preload("res://ui/StatKeyVal.tscn")
