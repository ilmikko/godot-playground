extends PanelContainer

# Gets emitted when the event is done.
signal completed


func fire_completed():
	emit_signal("completed")


@export var message = "": set = _set_message
func _set_message(m):
	message = m


@export var timeout = 5.0: set = _set_timeout
func _set_timeout(s):
	timeout = s


func skip():
	fire_completed()


func tick():
	progress.value += timer.wait_time
	if progress.value >= progress.max_value:
		fire_completed()


func _gui_input(event):
	if !(event is InputEventMouseButton): return
	if !event.is_pressed(): return
	skip()


func _ready():
	progress.max_value = timeout
	$VBoxContainer/Text.text = str(message)
	$Timer.start()


@onready var progress = $VBoxContainer/ProgressBar
@onready var timer = $Timer


func _on_Timer_timeout():
	tick()
