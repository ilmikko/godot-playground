extends PanelContainer

# Gets emitted when the event is over.
signal completed


var text_event_queue = [];

# These keep track of where to write in a tick.
var current_label = null
var current_line  = null

# These keep track of what to write.
var current_color = null
var current_text  = ""

var wait = 0


func fire_completed():
	emit_signal("completed")


func waiting():
	if wait > 0:
		wait -= 1
		return true
	return false


func refresh_label():
	current_label = Label.new()
	if current_color != null:
		current_label.modulate = current_color
		current_color = null
	current_line.add_child(current_label)


func refresh_line():
	current_line = HBoxContainer.new()
	$VBoxContainer.add_child(current_line)
	refresh_label()


func pop_text(count = 1):
	if current_text == "\n":
		refresh_line()
		current_text = ""
		return
	
	current_label.text += current_text.substr(0, count)
	current_text        = current_text.substr(count, -1)


func pop_text_event():
	var new_event = text_event_queue.pop_front()
	
	if  new_event is String:
		refresh_label()
		current_text = str(new_event)
		if current_text == "\n":
			# Wait on newlines.
			wait = 10
		
		# Trim the end of the text to avoid double spacing.
		current_text = current_text.trim_prefix(" ").trim_suffix(" ")
		return
	
	if  new_event is UITextPause:
		wait = new_event.wait
		return
	
	if  new_event is UITextColor:
		current_text  = str(new_event.text)
		current_color = new_event.color
		refresh_label()
		return
	
	push_error("Unknown text event %s" % new_event)


# Skips a single line, or a wait time.
# If there's nothing to skip, ends the text event.
func skip():
	if (wait > 0):
		wait = 0
		tick()
		return
	
	if (len(current_text) > 0):
		current_label.text += current_text
		current_text = ""
		tick()
		return
	
	fire_completed()


func tick():
	if waiting(): return;
	
	# Make sure there's a new line.
	if current_line == null:
		refresh_line()
	
	# Make sure there is a label to write to.
	if current_label == null:
		refresh_label()
	
	# Read back text.
	if current_text != "":
		pop_text()
		return
	
	# Get new text event.
	if len(text_event_queue) > 0:
		pop_text_event()
		return
	
	# Nothing to do...


func _gui_input(event):
	if !(event is InputEventMouseButton): return
	if !event.is_pressed(): return
	skip()


func write_message(_text_event_queue):
	for c in $VBoxContainer.get_children():
		$VBoxContainer.remove_child(c)
	text_event_queue = _text_event_queue


func _on_Timer_timeout():
	tick()
