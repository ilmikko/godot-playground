extends Control

# Gets emitted when the event is over.
signal completed


func fire_completed():
	emit_signal("completed")


var latest_choice   = -1
var latest_choice_t = -INF


func choice_event(text, choices):
	latest_choice = -1
	show()
	display_text(text)
	display_choices(choices)
	await self.completed
	hide()


func choose(id):
	latest_choice_t = Game.Director.t;
	latest_choice = id;
	fire_completed()


func display_choices(choices):
	for c in $PanelContainer/VBoxContainer/Choices.get_children():
		$PanelContainer/VBoxContainer/Choices.remove_child(c)
	for text in choices:
		var butt = BUTTON.instantiate()
		butt.text = text
		butt.choice_id = choices[text]
		$PanelContainer/VBoxContainer/Choices.add_child(butt)


func display_text(text):
	var s = ""
	for line in text:
		s += str(line) + "\n"
	
	$PanelContainer/VBoxContainer/RichTextLabel.text = s


func _ready():
	hide()


var BUTTON = preload("res://ui/multiplechoice/MultipleChoiceButton.tscn")
