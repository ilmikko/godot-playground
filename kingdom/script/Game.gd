# Root script node - autoloaded.
extends Node

signal actor_caller_set(actor_caller)
signal navigation_set(navigation)
signal camera_set(camera)

var Debug    = preload("res://script/Debug.gd").new()
var Economy  = preload("res://script/Economy.gd").new()
var Director = preload("res://script/Director.gd").new()
var TimeX     = preload("res://script/Time.gd").new()
var Treasury = preload("res://script/Treasury.gd").new()
var Wealth   = preload("res://script/Wealth.gd").new()
var UI       = preload("res://ui/UI.tscn").instantiate()
var Navigation : set = _set_navigation
var CameraX : set = _set_camera
var ActorCaller : set = _set_actor_caller
var Locations
var Player

var PopulationManager = preload("res://managers/PopulationManager.tscn").instantiate()


func _enter_tree():
	seed(Time.get_ticks_usec())
	
	add_child(Debug)
	
	add_child(UI)
	add_child(TimeX)
	
	add_child(Economy)
	add_child(Director)
	add_child(Treasury)
	add_child(Wealth)
	
	add_child(PopulationManager)

func _process(delta):
	Director.tick(delta)
	Economy.tick(delta)

func _set_navigation(navigation):
	Navigation = navigation
	emit_signal("navigation_set", navigation)

func _set_camera(camera):
	CameraX = camera
	emit_signal("camera_set", camera)

func _set_actor_caller(actor_caller):
	ActorCaller = actor_caller
	emit_signal("actor_caller_set", actor_caller)
