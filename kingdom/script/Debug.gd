extends Node


func axis(origin):
	var axis = AXIS.instantiate()
	axis.global_transform.origin = origin
	add_child(axis)


const AXIS = preload("res://debug/Axis.tscn")
