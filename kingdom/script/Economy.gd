extends Node
class_name Economy


@onready var treasury = Game.Treasury


var sources = StorageDict.new()
var sinks   = StorageDict.new()


func addX(resource, amount):
	treasury.addX(resource, amount)


func add_source(resource, rate):
	sources.addX(resource, rate)


func remove(resource, amount):
	treasury.remove(resource, amount)


func add_sink(resource, rate):
	sinks.addX(resource, rate)


func tick(_delta):
	for k in sources.keys():
		addX(k, sources.getX(k))
	for k in sinks.keys():
		remove(k, sinks.getX(k))
