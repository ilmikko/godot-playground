extends Node
class_name StorageDict

# Only fires when value change is > 1.0
signal updated(resource, value)


var contents = {}


func addX(key, amount):
	var v = 0
	if key in contents:
		v = contents[key]
	v += amount
	if floor(v) != floor(v-amount):
		emit_signal("updated", key, v)
	contents[key] = v


func remove(key, amount):
	var v = 0
	if key in contents:
		v = contents[key]
	v -= amount
	if v < 0:
		# Resource exhaustion
		v = 0
	if floor(v) != floor(v+amount):
		emit_signal("updated", key, v)
	contents[key] = v


func getX(k):
	return contents[k]


func keys():
	return contents.keys()
