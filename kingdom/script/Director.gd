extends Node
class_name Director


signal story_ended


# TODO: Before any "major" event, give some lead time.
#     : Bonus points if this is somehow incorporated into the story.

# A "lock" for which story is currently active.
# Can be released for other stories in case some conditions (e.g. wait condition) are active.
var story_lock = null

# Only one thing can be happening at one time.
# The event_lock keeps track of this.
var event_lock = null

# The Director is in charge of which events happen in which order.
# An event makes something happen in the game.
var events_ready = []

# Conditions are waiting for something to happen.
var conditions_active = []

# Stories that have not ended yet.
var stories_active = []

# Stories that can be started when the story lock is released.
var stories_ready = []

# Story paths that have ended.
# TODO: This does NOT support doing a story multiple times.
var stories_ended = {}

# Optimizes by moving some conditions to their corresponding data structures.
func condition_add(condition):
	if  condition is ChoiceCondition:
		condition.wait_for_choice(t)
	if  condition is WaitCondition:
		condition.wait_start(t)
	
	conditions_active.push_back(condition)


func condition_remove(condition):
	conditions_active.erase(condition)


# TODO: Most of this can probably be refactored to use yield() so we can block on conditions.
func condition_tick():
	if len(conditions_active) == 0: return
	for condition in conditions_active:
		var result = condition.check()
		if  result == condition.BLOCKED: continue
		
		condition_remove(condition)
		
		# Already removed; nothing else to do here.
		if result == Condition.INFEASIBLE: continue
		
		if result == Condition.UNBLOCKED:
			for node in condition.nodes:
				node.story = condition.story
				if node is Event:
					event_add(node)
				elif node is Condition:
					condition_add(node)
				else:
					push_error("Condition '%s' returned unknown node: %s" % [condition, node])
		elif result == Condition.ERROR:
			push_error("Condition '%s' returned an error." % condition)
		else:
			push_error("Condition '%s' is in an unknown state %s" % [condition, result])


func event_add(event):
	events_ready.push_back(event)


func event_tick():
	if event_lock: return # An event is already going on.
	if len(events_ready) == 0: return
	
	# TODO: Don't just pick any event, pick an event from the current locked story.
	# ....: If there are no events in the current story, then release the lock.
	var next_event = events_ready.pop_at(0)
	
	event_lock = next_event
	var wait = await next_event.run()
	event_lock = null


func story_add(story):
	stories_active.push_back(story)
	stories_ready.push_back(story)
	Game.UI.Events.debug_event("Story '%s' has been added." % story)


func story_end(story):
	stories_active.erase(story)
	if story_lock == story:
		story_lock = null
	stories_ended[story.get_script().resource_path] = 1
	Game.UI.Events.debug_event("Story '%s' has ended." % story)
	emit_signal("story_ended", story)
	await Game.TimeX.wait(0.02)


# Yield this method to wait until the story is completed.
func story_run(story):
	story_add(story)
	await story_wait(story)


func story_wait(story):
	while true:
		var _story = await self.story_ended
		print("Story ended: %s vs %s" % [_story, story])
		if  _story != story:
			continue
		break
	return story


func story_lock_release(story):
	if story_lock == story:
		story_lock = null


func story_tick():
	if story_lock: return # A story is already going on.
	if len(stories_ready) == 0: return
	
	var next_story = stories_ready.pop_at(0)
	
	story_lock = next_story
	
	if next_story.has_method('rebuild'):
		next_story.rebuild()
	
	for condition in next_story.conditions:
		condition.story = next_story
		condition_add(condition)


var t = 0.0
func tick(d):
	t += d
	
	# Process any stories.
	story_tick()
	# Process any events.
	event_tick()
	# Process any conditions.
	condition_tick()
	
	# Print out debug stats of director.
	Game.UI.Stats.Debug.DirectorTime.value = t
	Game.UI.Stats.Debug.Conditions.value = conditions_active
	Game.UI.Stats.Debug.StoriesActive.value = stories_active
	Game.UI.Stats.Debug.EventsAvailable.value = events_ready
	Game.UI.Stats.Debug.StoriesAvailable.value = stories_ready
	Game.UI.Stats.Debug.EventLock.value = event_lock
	Game.UI.Stats.Debug.StoryLock.value = story_lock
