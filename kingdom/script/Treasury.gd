extends StorageDict
class_name Treasury


# Connect this treasury to the UI.
func _enter_tree():
	assert(self.connect("updated", Callable(self, "_resource_updated")) == OK)


func _resource_updated(resource, value):
	Game.UI.Stats.Treasury.display(resource, value)
