extends Node
class_name Wealth

signal updated

var amount = 0


# Connect this to the UI.
func _enter_tree():
	assert(self.connect("updated", Callable(self, "_wealth_updated")) == OK)


func addX(v):
	amount += v
	emit_signal("updated", amount)


func remove(v):
	amount -= v
	emit_signal("updated", amount)


func _wealth_updated(value):
	Game.UI.Stats.Wealth.value = value
