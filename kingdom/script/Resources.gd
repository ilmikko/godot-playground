extends Node
class_name Resources

enum {
	GOLD,
	WINE,
	MIGHT,
	FOOD,
	HAPPINESS,
	PEOPLE,
	WOOD,
	STONE,
}

static func to_str(resource):
	var to_str = {
		FOOD:      "Food",
		GOLD:      "Gold in Treasury",
		MIGHT:     "Kingdom Might",
		WINE:      "Barrels of Wine",
		PEOPLE:    "Population",
		HAPPINESS: "Happiness",
		WOOD:      "Crates of Wood",
		STONE:     "Barrels of Stone",
	}
	return to_str[resource]
