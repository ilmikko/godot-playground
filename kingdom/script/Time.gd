extends Node
class_name GameTime

# TODO: If paused, stop timers.
func wait(t):
	await get_tree().create_timer(t).timeout
