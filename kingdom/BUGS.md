# Bugs

## Movement tied to framerate

In `actor/module/Movement.gd` the `move_and_slide` is outside any `_physics_process`, meaning the characters move faster for some people.

## You can only add wood to building projects

## Navigation uses the old Godot navigation modules instead of the new ones
