extends Node3D


func location_as_area():
	return $DropOffArea


func add_resources(type, amount):
	$ResourceLabel.sub(type, amount)


func item_dropped(_item):
	# TODO: Make it not wood
	add_resources(Resources.WOOD, 20)


func _ready():
	$ResourceLabel.requirements = {
		Resources.WOOD:  50,
		Resources.STONE: 15,
	}
