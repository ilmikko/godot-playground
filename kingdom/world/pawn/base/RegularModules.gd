extends Node3D


# Resolve module names.
@onready var dialogue   = $Dialogue
@onready var task       = $Task
@onready var speech     = $Speech
@onready var perception = $Perception
@onready var movement   = $Movement

# TODO: Only load this module if the person is actually a captain.
@onready var captain    = get_node_or_null("Captain")


func _ready():
	# Get any "drag and dropped" tasks from the parent.
	task.ingest_node_tasks($"..")


func event_called_by_player():
	dialogue.start_dialogue()
