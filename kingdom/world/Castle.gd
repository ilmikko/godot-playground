extends MeshInstance3D

func _ready():
	Game.Economy.addX(Resources.PEOPLE,    10.0)
	Game.Economy.addX(Resources.MIGHT,     10000.0)
	Game.Economy.addX(Resources.HAPPINESS, 100.0)
	
	Game.Economy.add_sink(Resources.WINE, 1.0)
