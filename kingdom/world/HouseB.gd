extends MeshInstance3D

func _ready():
	Game.Wealth.addX(15000.0)
	Game.Economy.add_single(Resources.PEOPLE, 2.0)
	Game.Economy.add_single(Resources.HAPPINESS, 1.0)
	Game.Economy.add_sink(Resources.FOOD, 1.3)
