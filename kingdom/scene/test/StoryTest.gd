extends Node3D

# Called when the node enters the scene tree for the first time.
func _ready():
	Game.Director.story_add(load("res://director/stories/TaleOfDiscovery.gd").new())
	Game.Director.story_add(load("res://director/stories/NewKing.gd").new())
