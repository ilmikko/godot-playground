extends Node3D


@export var leave_kingdom_location_path: NodePath
@onready var leave_kingdom_location = get_node_or_null(leave_kingdom_location_path)


func _ready():
	Game.Locations = self


# Contains all wander locations.
var wander_locations = []


func add_wander_location(wl : WanderLocation):
	wander_locations.push_back(wl)


func get_random_wander_location() -> WanderLocation:
	return wander_locations[randi() % len(wander_locations)]
