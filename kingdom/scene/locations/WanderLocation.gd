extends Node3D
class_name WanderLocation

@export var locations_path: NodePath
@onready var locations = get_node(locations_path)

@export var location_name = ""

func _ready():
	locations.add_wander_location(self)
