# Kingdom

## Ideas

- Resource Economy
- "Realistic" medieval stuff
- Top-down ish

## "Pipelines"

Forest -> Wood -> Planks -> Barrels -> Wine -> Gold

Forest + People + Tools -> Wood
Wood + People + Tools -> Planks
Wood + People + Metal + Tools -> Barrels

## Game Loop

-> You enter a reign
-> Your reign ends - how well did you do?
-> Legacy, etc.

## "Stories"

"Why did the King order a stew?"
TBD
