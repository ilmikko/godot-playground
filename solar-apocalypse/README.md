# Solar Apocalypse

Save the animals from disaster!

## Features

* Solar System
	* Planets
		* With Gravity
	* A Sun

* A shop!

* Animals!
	* Procedurally Generated(TM)!

* Disaster!
	* Sun goes Supernova!
	* Meteor Storm!
	* Black hole!

* Spaceships!
	* Spaceperson!
	* Limited Cargo
