extends Spatial


func _enter_tree():
	$Area/CollisionShape.disabled = false
	# $Occluder.visible = true


func _exit_tree():
	$Area/CollisionShape.disabled = true
	# $Occluder.visible = false


func rebuild():
	if !is_inside_tree(): return
	
	var plane = PlaneMesh.new()
	plane.size = get_viewport().size * 0.00009
	$Occluder.mesh = plane


func ui_effect_set(area, state):
	if !area.is_in_group("ui_effect"): return;
	
	# Which effects?
	if area.is_in_group("ui_effect_underwater"):
		$Occluder.underwater = state


func _on_Area_area_entered(area):
	ui_effect_set(area, true)


func _on_Area_area_exited(area):
	ui_effect_set(area, false)


func _ready():
	rebuild()
