extends MeshInstance

export var underwater = false setget _set_underwater
func _set_underwater(_underwater):
	underwater = _underwater
	rebuild()

func rebuild():
	material_override.set_shader_param("underwater", underwater)
	var active = underwater;
	visible = active
