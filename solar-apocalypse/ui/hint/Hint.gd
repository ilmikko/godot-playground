extends Control


var current_center_label_key = null
var center_labels_by_key = {}
func set_center_label_with_key(key, text):
	current_center_label_key = key
	center_labels_by_key[key] = text
	_set_center_label(text)

func unset_center_label_with_key(key):
	center_labels_by_key.erase(key)
	if current_center_label_key == key:
		if center_labels_by_key.size() > 0:
			current_center_label_key = center_labels_by_key.keys()[0]
			_set_center_label(center_labels_by_key[current_center_label_key])
		else:
			current_center_label_key = null
			_set_center_label("")


var center_label = "" setget _set_center_label
func _set_center_label(text):
	if text:
		center_label = text
		$CenterLabel.text = text
		$CenterLabel.visible = true
	else:
		$CenterLabel.visible = false


var star_label = "" setget _set_star_label
func _set_star_label(sl):
	if sl:
		$StarLabel.text = sl
		$StarLabel.visible = true
	else:
		$StarLabel.visible = false
