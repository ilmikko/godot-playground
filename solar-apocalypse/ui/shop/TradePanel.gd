extends HBoxContainer

var shop_quantity setget _set_shop_quantity
func _set_shop_quantity(sq):
	shop_quantity = sq
	
	# Cannot buy if there's nothing to buy.
	$BuyButton.disabled = sq <= 0
	$ShopQuantity.text  = "%sx" % sq


var player_quantity setget _set_player_quantity
func _set_player_quantity(pq):
	player_quantity = pq
	
	# Cannot sell if there's nothing to sell.
	$SellButton.disabled = pq <= 0
	$PlayerQuantity.text = "%sx" % pq
