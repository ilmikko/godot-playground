extends HBoxContainer

signal selected


export(String) var title setget _set_title
export(ButtonGroup) var button_group setget _set_button_group


func _set_title(new_title):
	$TitleButton.text = new_title


func _set_button_group(new_button_group):
	button_group = new_button_group
	$TitleButton.group = new_button_group


func press():
	$TitleButton.pressed = true


var TradePanel setget , _get_TradePanel
func _get_TradePanel(): return $TradePanel


func _on_TitleButton_pressed():
	emit_signal("selected")

