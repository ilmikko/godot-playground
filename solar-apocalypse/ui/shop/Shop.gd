extends PanelContainer

var ShopItemTitle = preload("res://ui/shop/ShopItemTitle.tscn")

onready var item_list = $VSplit/ListColor/Margin/Scroll/ItemList

onready var item_title = $VSplit/ItemPanel/HBoxContainer/Margin/ItemArea/ItemTitle
onready var item_description = $VSplit/ItemPanel/HBoxContainer/Margin/ItemArea/ItemDescription
onready var item_cost = $VSplit/ItemPanel/HBoxContainer/Margin/ItemArea/ItemDescription

var button_group = ButtonGroup.new()

var shop_inventory
var player_inventory

var key_to_shop_item_title = {}

var displayed_item


func open():
	Runtime.Game.paused = true
	show()


func close():
	Runtime.Game.paused = true
	hide()


func show_inventory(inventory):
	Util.remove_children(item_list)
	_remove_displayed_item()
	
	player_inventory = Runtime.Game.Inventory
	shop_inventory = inventory
	
	for key in shop_inventory.items_by_key:
		var inventory_item = inventory.items_by_key[key]
		add_item(inventory_item)
	for key in player_inventory.items_by_key:
		var inventory_item = player_inventory.items_by_key[key]
		add_item(inventory_item)
	
	if item_list.get_child_count() == 1:
		item_list.get_child(0).press()


func add_item(inventory_item):
	var key = inventory_item.key
	
	if !(key in key_to_shop_item_title):
		key_to_shop_item_title[key] = ShopItemTitle.instance()
		key_to_shop_item_title[key].title = inventory_item.item_name
		key_to_shop_item_title[key].button_group = button_group
		key_to_shop_item_title[key].connect("selected", self, "_item_selected", [key])
		item_list.add_child(key_to_shop_item_title[key])
	
	var shop_item_title = key_to_shop_item_title[key]
	
	shop_item_title.TradePanel.shop_quantity   = shop_inventory.item_count_by_key(key)
	shop_item_title.TradePanel.player_quantity = player_inventory.item_count_by_key(key)


func remove_item(inventory_item):
	var key = inventory_item.key
	if !(key in key_to_shop_item_title): return

	shop_inventory.remove_item(inventory_item)
	if shop_inventory.item_count_by_key(inventory_item.key) <= 0:
		item_list.remove_child(key_to_shop_item_title[key])


func can_afford_item(inventory_item):
	for base_cost in inventory_item.base_costs:
		if !player_inventory.has_item_by_key(base_cost["key"], base_cost["quantity"]):
			print("Do not have ", base_cost["key"], " with quantity ", base_cost["quantity"])
			return false

	return true


func _item_selected(key):
	var inventory_item = key_to_shop_item_title[key]
	displayed_item = inventory_item
	item_title.text = inventory_item.item_name
	item_description.text =  inventory_item.description
	item_cost.text = _cost_string_for_item(inventory_item)


func _cost_string_for_item(inventory_item):
	var cost_string = ""
	for base_cost in inventory_item.base_costs:
		var key = base_cost["key"]
		var player_item_quantity = player_inventory.item_count_by_key(key)
		cost_string += "%s: %s (%s)" % [key, base_cost["quantity"], player_item_quantity]
	return cost_string


func _remove_displayed_item():
	displayed_item = null
	item_title.text = ""
	item_description.text = ""


func _on_PurchaseButton_pressed():
	if displayed_item == null || !can_afford_item(displayed_item): return
	
	remove_item(displayed_item)
	Runtime.Game.Inventory.add_item(displayed_item)
	_remove_displayed_item()
