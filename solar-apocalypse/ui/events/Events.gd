extends Control

const DISPLAY_MSEC = 3000

var displayed_events = []

func _ready():
	Runtime.Game.Events.connect("event", self, "display_event")

func display_event(event):
	var event_text_label = _event_text_label(event)
	displayed_events.append({ "event": event_text_label, "time": OS.get_ticks_msec() })
	$EventContainer.add_child(event_text_label)

func _event_text_label(event):
	var rich_text_label = RichTextLabel.new()
	rich_text_label.text = event
	rich_text_label.rect_min_size.y = 25
	rich_text_label.scroll_active = false
	return rich_text_label
	


func _on_FadeEventTimer_timeout():
	var check_time = OS.get_ticks_msec()
	for displayed_event in displayed_events:
		if displayed_event["time"] + DISPLAY_MSEC < check_time:
			$EventContainer.remove_child(displayed_event["event"])
			displayed_events.erase(displayed_event)
