tool
extends Control


export var achievement_color = Color(0.9529, 0.8705, 0) setget _set_achievement_color
func _set_achievement_color(color):
	achievement_color = color
	rebuild()

var description = "" setget _set_description
func _set_description(title):
	description = title
	$Center/PullDown/Container/MarginContainer/HBoxContainer/VBoxContainer/Description.text = title
	$Center/PullDown/Container/MarginContainer/HBoxContainer2/VBoxContainer/Description.text = title


func animate():
	$AnimationPlayer.play("AchievementUnlocked")


func rebuild():
	if !is_inside_tree(): return
	_get_achievement_element().modulate = achievement_color
	_get_star_element().modulate = achievement_color
	_get_particles().modulate = achievement_color
	$AnimationPlayer.current_animation_position = 0
	

var particles setget , _get_particles
func _get_particles():
	return $Center/PullDown/Container/MarginContainer/HBoxContainer/Particles
	
var star_element setget , _get_star_element
func _get_star_element():
	return $Center/PullDown/Container/MarginContainer/HBoxContainer2/Star

var achievement_element setget , _get_achievement_element
func _get_achievement_element():
	return $Center/PullDown/Container/MarginContainer/HBoxContainer2/VBoxContainer/Achievement
