extends Control


export var CC_TIMEOUT = 2.0


var items  = []
var timers = []


func push(text):
	items.append(text)
	var t = Timer.new()
	t.wait_time = CC_TIMEOUT
	t.connect("timeout", self, "_on_timer_timeout")
	add_child(t)
	timers.append(t)
	t.start()
	cc_render()


func cc_render():
	if items.empty():
		cc_hide()
		return
	
	cc_hide()
	cc_show()
	
	$MarginContainer/MarginContainer/VBoxContainer/Label.text = Util.join(items, "\n")


func cc_hide():
	visible = false
	
	$MarginContainer/MarginContainer/VBoxContainer/Label.text = ""


func cc_show():
	visible = true


func _ready():
	cc_hide()


func _on_timer_timeout():
	items.pop_front()
	remove_child(timers.pop_front())
	cc_render()
