extends Control


var targeter


func communicate(t, target_object):
	targeter = t
	$AnimationPlayer.play("Dial")


func comms_open(from, to):
	$AnimationPlayer.play("Communicate")
	$Panel/MarginContainer/HBoxContainer/VBoxContainer3/CommsText.text = Runtime.Expert.Comms.get_dialog(to)


func dial():
	targeter.dial()


var scanning = null
var scan_amount = 0.0
func scan(t, target_object):
	targeter = t
	$AnimationPlayer.play("Scan")
	$Panel/MarginContainer/HBoxContainer/VBoxContainer3/CommsText.text = ""
	visible = true
	scanning = target_object
	scan_amount = 0.0


func scan_progress():
	var info = Runtime.Expert.Scan.scan_object(targeter, scanning, scan_amount)
	
	var scan_map_text = ""
	for k in info:
		scan_map_text += k + ": " + str(info[k]) + "\n"
	$Panel/MarginContainer/HBoxContainer/VBoxContainer2/ScanMap.text = scan_map_text
	
	scan_amount += SCAN_SPEED
	if scan_amount > 1.0:
		scan_amount = 1.0
	$Panel/MarginContainer/HBoxContainer/VBoxContainer/ScanPercentage.text = "%s%%" % (scan_amount * 100.0)


func hide():
	visible = false
	$Panel/MarginContainer/HBoxContainer/VBoxContainer3/CommsText.text = ""
	$AnimationPlayer.stop()


const SCAN_SPEED = 0.05
