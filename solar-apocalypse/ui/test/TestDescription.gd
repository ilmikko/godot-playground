tool
extends Control

# Simple node to help keep track of our increasing number of Test scenes.
# Add this to your test scene and fill out the description.
# Dependencies might be used later to construct a test graph.

export(String, MULTILINE) var description setget _set_description
func _set_description(d):
	description = d
	$WindowDialog/VBoxContainer/DescriptionLabel.text = d


export(String, MULTILINE) var dependencies setget _set_dependencies
func _set_dependencies(d):
	dependencies = d
	$WindowDialog/VBoxContainer/DependenciesLabel.text = "Dependencies:\n" + d


func _ready():
	if Engine.is_editor_hint(): return
	$WindowDialog.popup()
