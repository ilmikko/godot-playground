extends Control


var Health setget _set_Health
func _set_Health(h):
	Health = h
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Values/Health.text = str(h)
	recenter()


var Material setget _set_Material
func _set_Material(m):
	Material = m
	$PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Values/Material.text = Materials.name(m)
	recenter()


var Title setget _set_Title
func _set_Title(t):
	Title = t
	$PanelContainer/MarginContainer/VBoxContainer/Title.text = str(t)
	recenter()


func recenter():
	$PanelContainer.set_anchors_and_margins_preset(Control.PRESET_BOTTOM_LEFT)
