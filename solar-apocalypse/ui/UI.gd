extends Control


var graphs = {}


func debug_toggle():
	$Debug.visible = !$Debug.visible


func game_pause():
	Runtime.UI.PauseMenu.set_state(true)


func game_resume():
	Runtime.UI.Shop.close()
	Runtime.UI.PauseMenu.set_state(false)


func Graph(title):
	if title in graphs: return graphs[title]
	var graph = Graph.instance()
	graph.title = title
	graph.width = 1.0
	graph.color = Color(1.0, 0.0, 1.0, 1.0)
	graphs[title] = graph
	$Debug/Graphs.add_child(graph)
	return graph


var Achievement setget , _get_Achievement
func _get_Achievement(): return $SafeAreas/Center/MainArea/Achievement


var ClosedCaptions setget , _get_ClosedCaptions
func _get_ClosedCaptions(): return $SafeAreas/Center/MainArea/ClosedCaptions


var Scan setget , _get_Scan
func _get_Scan(): return $SafeAreas/Center/MainArea/Scan


var Effect setget , _get_Effect
func _get_Effect(): return $SafeAreas/Full/Effect


var Hint setget , _get_Hint
func _get_Hint(): return $SafeAreas/Center/FocusArea/Hint


var KeyVal setget , _get_KeyVal
func _get_KeyVal(): return $Debug/KeyVal


var Loading setget , _get_Loading
func _get_Loading(): return $SafeAreas/Full/Loading


var Log setget , _get_Log
func _get_Log(): return $Debug/Log


var PauseMenu setget , _get_PauseMenu
func _get_PauseMenu(): return $SafeAreas/Full/PauseMenu

var Shop setget , _get_Shop
func _get_Shop(): return $SafeAreas/Full/Shop


var Journal setget , _get_Journal
func _get_Journal(): return $SafeAreas/Full/Journal


var Stats setget , _get_Stats
func _get_Stats(): return $SafeAreas/Center/MainArea/Stats


const Graph = preload("res://debug/graph/Graph.tscn")
