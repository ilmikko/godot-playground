extends Control

# TODO: This script is an ugly hack around the fact that we have 3 cameras
# where the journal might appear.

func show():
	if visible: return
	
	.show()
	
	var camera = Runtime.Expert.Camera.get_active_player_camera()
	if  camera == null:
		Debug.warn(self, "Tried to open journal with null camera!")
		return
	
	camera.BookOpenable.open()
	Runtime.Game.in_menu = true


func hide():
	if !visible: return
	
	.hide()
	
	var camera = Runtime.Expert.Camera.get_active_player_camera()
	if  camera == null:
		Debug.warn(self, "Tried to open journal with null camera!")
		return
	
	camera.BookOpenable.close()
	Runtime.Game.in_menu = false
