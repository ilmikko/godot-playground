extends PanelContainer

var entry setget _set_entry
func _set_entry(text):
	$MarginContainer/RichTextLabel.bbcode_text = "[center]%s[/center]" % text
