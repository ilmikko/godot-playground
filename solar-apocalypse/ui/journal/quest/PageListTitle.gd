extends HSplitContainer

signal title_pressed
signal archive_pressed

export(String) var title setget _set_title

func _set_title(new_title):
	$TitleButton.text = new_title


func _on_TitleButton_pressed():
	emit_signal("title_pressed")


func _on_ArchiveButton_pressed():
	emit_signal("archive_pressed")
