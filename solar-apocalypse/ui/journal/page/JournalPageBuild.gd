extends Control


func build(scene):
	var player_person = Runtime.Game.PlayerController.Actor
	Runtime.Handler.fetch(player_person, Runtime.Handler.Builder).build_packed = scene
	Runtime.UI.game_resume()
