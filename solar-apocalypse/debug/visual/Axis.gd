extends Spatial

onready var scene = $".."

func _process(_delta):
	Debug.text(self, "%s global X" % scene.name, global_transform.basis.x)
	Debug.text(self, "%s global Y" % scene.name, global_transform.basis.y)
	Debug.text(self, "%s global Z" % scene.name, global_transform.basis.z)
