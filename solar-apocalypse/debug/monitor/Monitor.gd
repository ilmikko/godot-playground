extends Node


func tick():
	tick_timing()
	
	# Memory
	Debug.text(self, "static mem used",  Performance.get_monitor(Performance.MEMORY_STATIC))
	Debug.text(self, "static mem max",   Performance.get_monitor(Performance.MEMORY_STATIC_MAX))
	Debug.text(self, "dynamic mem used", Performance.get_monitor(Performance.MEMORY_DYNAMIC))
	Debug.text(self, "dynamic mem max",  Performance.get_monitor(Performance.MEMORY_DYNAMIC_MAX))
	
	Debug.text(self, "message buffer mem peak", Performance.get_monitor(Performance.MEMORY_MESSAGE_BUFFER_MAX))
	
	# Counts
	Debug.text(self, "object count",   Performance.get_monitor(Performance.OBJECT_COUNT))
	Debug.text(self, "resource count", Performance.get_monitor(Performance.OBJECT_RESOURCE_COUNT))
	Debug.text(self, "node count",     Performance.get_monitor(Performance.OBJECT_NODE_COUNT))
	
	# Rendering
	Debug.text(self, "rendered verts",   Performance.get_monitor(Performance.RENDER_VERTICES_IN_FRAME))
	Debug.text(self, "rendered objects", Performance.get_monitor(Performance.RENDER_OBJECTS_IN_FRAME))
	Debug.text(self, "shader changes",   Performance.get_monitor(Performance.RENDER_SHADER_CHANGES_IN_FRAME))
	Debug.text(self, "mat changes",      Performance.get_monitor(Performance.RENDER_MATERIAL_CHANGES_IN_FRAME))
	Debug.text(self, "surf changes",     Performance.get_monitor(Performance.RENDER_SURFACE_CHANGES_IN_FRAME))
	
	Debug.text(self, "render texture mem used", Performance.get_monitor(Performance.RENDER_TEXTURE_MEM_USED))
	Debug.text(self, "render video mem used",   Performance.get_monitor(Performance.RENDER_VIDEO_MEM_USED))
	Debug.text(self, "render vertex mem used",  Performance.get_monitor(Performance.RENDER_VERTEX_MEM_USED))


func tick_timing():
	var fps = Performance.get_monitor(Performance.TIME_FPS)
	Debug.text(self,  "FPS", fps)
	Debug.graph(self, "FPS", fps)
	
	Debug.text(self, "time per frame",         Performance.get_monitor(Performance.TIME_PROCESS))
	Debug.text(self, "time per physics frame", Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS))
	
	var dpf = Performance.get_monitor(Performance.RENDER_DRAW_CALLS_IN_FRAME)
	Debug.text(self, "draw calls per frame", dpf)
	Debug.graph(self, "draw calls per frame", dpf)


func _on_Timer_timeout():
	tick()
