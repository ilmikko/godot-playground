extends Node
class_name Util


# Added because we usually don't want to go that far.
const MAX_ROOT_PARENT_LEVEL = 10

class Sorter:
	static func _by_node_path(a, b):
		if str(a.get_path()) > str(b.get_path()):
			return true
		return false


static func sort_nodes_by_path(nodes):
	nodes.sort_custom(Sorter, "_by_node_path")


static func control_center(control):
	control.set_anchors_and_margins_preset(
		Control.PRESET_CENTER,
		Control.PRESET_MODE_KEEP_SIZE
	)


static func file_exists(path):
	return File.new().file_exists(path)

static func get_collisions(node):
	return get_children_of_types(node, [CollisionShape, CollisionPolygon])

static func get_mesh_instances(node):
	return get_children_of_types(node, [MeshInstance])

static func get_nodes_of_types(node, types):
	var nodes = []
	if node_is_of_any_type(node, types):
		nodes.append(node)
	nodes += get_children_of_types(node, types)
	return nodes

static func get_children_of_types(node, types):
	var children_of_types = []
	for child in node.get_children():
		if node_is_of_any_type(child, types):
			children_of_types.append(child)

		children_of_types += get_children_of_types(child, types)
	return children_of_types

static func node_is_of_any_type(node, types) -> bool:
	for type in types:
		if node is type:
			return true
	return false

static func get_root_parent(node):
	var n = node
	
	if n == null || !n.is_in_group('has_root_parent'):
		return n
	
	var level = 0
	while n != null:
		var p = n.get_parent()
		if p == null: return n
		n = p
		level += 1
		
		if 'custom_root_parent' in n:
			return n.custom_root_parent
		
		if n.is_in_group('root_parent'):
			return n
		
		if level > MAX_ROOT_PARENT_LEVEL:
			push_warning("Max root parent level reached for node %s." % node)
			return node
	return node


static func get_root_parents(nodes):
	var root_parents = []
	for node in nodes:
		root_parents.append(get_root_parent(node))
	return root_parents


static func join(arr, delimiter=""):
	var text = ""
	if arr.empty(): return text
	
	text += arr[0]
	for i in range(1, arr.size()):
		text += "%s%s" % [ delimiter, arr[i] ]
	return text


static func move_child_under(child, new_parent):
	var old_parent = child.get_parent()
	if old_parent != null:
		old_parent.remove_child(child)
	new_parent.add_child(child)


static func remove(n):
	n.queue_free()


static func remove_children(n):
	for child in n.get_children():
		child.queue_free()


static func replace(n, m):
	var parent = n.get_parent()
	n.queue_free()
	parent.add_child(m)


static func replace_with_transform(n, m):
	var parent = n.get_parent()
	m.transform = n.transform
	parent.add_child(m)
	parent.remove_child(n)
	m.name = n.name
	n.queue_free()


static func replace_with_global_transform(parent, node, tf_node):
	parent.add_child(node)
	node.global_transform = tf_node.global_transform
	for c in tf_node.get_children():
		tf_node.remove_child(c)
		node.add_child(c)
	tf_node.get_parent().remove_child(tf_node)
	node.name = tf_node.name
	tf_node.queue_free()


static func replace_with_transform_and_children(n, m):
	var parent = n.get_parent()
	m.transform = n.transform
	parent.add_child(m)
	for c in n.get_children():
		n.remove_child(c)
		m.add_child(c)
	parent.remove_child(n)
	m.name = n.name
	n.queue_free()


static func key_names_for_action(action):
	# TODO: Make sure this works for multiple inputs
	var list = InputMap.get_action_list(action)
	if  list == null:     return "<none>"
	if  list.size() == 0: return "<none>"
	
	var event = list[0]
	if  event is InputEventMouseButton:
		return "MOUSE%s" % event.button_index
	else:
		return event.as_text()


static func nodes_paths(nodes):
	var nodes_paths = []
	
	for node in nodes:
		nodes_paths.append(node.get_path())
	
	return nodes_paths


static func save_content_to_file(content, save_path):
	var save_file = File.new()
	save_file.open(save_path, File.WRITE)
	save_file.store_string(content)
	save_file.close()

static func load_json_from_file(file_path):
	var file = File.new()
	file.open(file_path, File.READ)
	var content = parse_json(file.get_as_text())
	file.close()
	return content


static func merge_dictionaries(dict1, dict2):
	var new_dict = {}
	for key in dict1:
		new_dict[key] = dict1[key]
	for key in dict2:
		new_dict[key] = dict2[key]
	return new_dict


static func subtract_array(arr1, arr2):
	var arr2_dict = {}
	for v in arr2:
		arr2_dict[v] = true

	var only_in_arr1 = []
	for v in arr1:
		if not arr2_dict.get(v, false):
			only_in_arr1.append(v)
	return only_in_arr1

static func intersect_arrays(arr1, arr2):
	var arr2_dict = {}
	for v in arr2:
		arr2_dict[v] = true

	var in_both_arrays = []
	for v in arr1:
		if arr2_dict.get(v, false):
			in_both_arrays.append(v)
	return in_both_arrays


static func convert_to_ints(arr):
	var ints_arr = []
	for i in arr:
		ints_arr.append(int(i))
	return ints_arr
