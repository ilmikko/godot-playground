extends Node
class_name Scheduler


var t = 0


var schedule_sorted = []
var schedule_sorted_mutex = Mutex.new()


func tick(delta):
	t += delta
	if schedule_sorted.size() == 0: return
	if t < schedule_sorted[0][0]: return
	
	schedule_sorted_mutex.lock()
	var next = schedule_sorted.pop_front()
	schedule_sorted_mutex.unlock()
	
	var node = next[1]
	var method = next[2]
	var args = next[3]
	# TODO: Delta should be specific to how long this took
	if node == null or (not is_instance_valid(node)): return
	if args == null:
		node.call(method, delta)
	else:
		node.call(method, delta, args)


# TODO: Ability to de-schedule stuff we don't need
# TODO: Ensure this list stays sorted by the next schedulable event
func schedule(seconds, node, method, args):
	var item = [t + seconds, node, method, args]
	
	schedule_sorted_mutex.lock()
	schedule_critical(item)
	schedule_sorted_mutex.unlock()


func schedule_critical(item):
	if schedule_sorted.size() == 0:
		schedule_sorted.push_front(item)
		return
	
	for i in range(schedule_sorted.size()):
		if schedule_sorted[i][0] < item[0]: continue
		schedule_sorted.insert(i, item)
		return
	
	schedule_sorted.push_back(item)
