class_name Scene


# Returns the filepath of a node's scene.
static func filepath(node):
	if node == null or !is_instance_valid(node): return ""
	if node is PackedScene:
		node = node.instance()
	if node.filename: return node.filename
	var r = root(node)
	if  r == null: return ""
	return r.filename


# Returns the scene root or itself.
static func root(node):
	if node == null:       return null
	if node.owner == null: return node
	return node.owner


# Returns a parent body for a node, if it has any.
static func parent_body(node):
	var n = node
	while n != null:
		if n is PhysicsBody: return n
		n = n.get_parent()
	return null


# Returns a parent with a node or null if that node is not found.
static func parent_with_node(node, path):
	var n = node
	while n != null:
		var p = n.get_node_or_null(path)
		if  p != null: return n
		n = n.get_parent()
	return null


# Returns a parent with a property or null if property is not found.
static func parent_with_property(node, property):
	var n = node
	while n != null:
		var p = n.get(property)
		if  p != null: return n
		n = n.get_parent()
	return null
