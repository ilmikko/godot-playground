extends Node
class_name RateLimiter


var t = 0


var last_fires = {}


func tick(delta):
	t += delta


func limit(t_seconds, node, method):
	var k = [node,method]
	if k in last_fires and t-last_fires[k] < t_seconds: return
	last_fires[k] = t
	node.call(method, t)
