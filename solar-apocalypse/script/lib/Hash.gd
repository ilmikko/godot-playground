extends Node
class_name Hash

const HASH_SEED = 77

static func hash_basis(basis_seed : Basis) -> int:
	return hash_int(
		hash_int(
			(hash_vec3(basis_seed.x)+0)
			-
			(hash_vec3(basis_seed.y)+1)
		)
		-
		(hash_vec3(basis_seed.z)+2)
	)

static func hash_int(int_seed : int) -> int:
	var s = HASH_SEED + int_seed
	if s < 0:
		return -s
	return s

static func hash_string(string_seed : String) -> int:
	return hash_int(string_seed.hash())

static func hash_transform(transform_seed : Transform) -> int:
	return hash_int(
		(hash_basis(transform_seed.basis)+0)
		-
		(hash_vec3(transform_seed.origin)+1)
	)

static func hash_vec2(vec2_seed : Vector2) -> int:
	return hash_string("%s,%s" % [vec2_seed.x, vec2_seed.y])

static func hash_vec3(vec3_seed : Vector3) -> int:
	return hash_string("%s,%s,%s" % [vec3_seed.x, vec3_seed.y, vec3_seed.z])

static func hash(any_seed):
	if typeof(any_seed) == TYPE_BASIS:
		return hash_basis(any_seed)
	elif typeof(any_seed) == TYPE_INT:
		return hash_int(any_seed)
	elif typeof(any_seed) == TYPE_STRING:
		return hash_string(any_seed)
	elif typeof(any_seed) == TYPE_TRANSFORM:
		return hash_transform(any_seed)
	elif typeof(any_seed) == TYPE_VECTOR2:
		return hash_vec2(any_seed)
	elif typeof(any_seed) == TYPE_VECTOR3:
		return hash_vec3(any_seed)
	else:
		Debug.text(null, "Error", "Unknown seed type to hash: %d" % typeof(any_seed))
