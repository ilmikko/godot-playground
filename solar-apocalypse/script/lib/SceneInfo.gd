extends Node

# TODO: Cache
static func for_scene(scene):
	var node =  scene.instance() if scene is PackedScene else scene
	var dir = Directory.new()
	var fp = Scene.filepath(node)
	var info_path = "%s_info" % fp
	var info = {}
	if !(dir.open(info_path) == OK):
		Debug.warn(scene, "could not load info for %s" % scene)
		return info
	dir.list_dir_begin()
	
	print("load info for %s" % fp)
	
	var file_name = dir.get_next()
	while file_name != "":
		if !dir.current_is_dir():
			var new_info = Util.load_json_from_file("%s/%s" % [info_path, file_name])
			info = Util.merge_dictionaries(info, new_info)

		file_name = dir.get_next()
	return info
