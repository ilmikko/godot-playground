tool
extends Node

# Add IDs to this list which you don't want to emit debug messages.
# These will be matched by substring before printing them out.
var suppress = [
#	"/root/Runtime/UI/Monitor", # Global FPS monitor etc.
	"/root/VillageScene/GlobalEnvironment", # Environment stats
#	"Driver>>> Impact",     # Driver physics debug stats
	"Driver>>> Max Impact", # Driver physics debug stats
	"Observable>>> ", # Observable debug printout
#	">>> UNLOAD PLACEHOLDER:",    # LoadStage printout
#	">>> UNLOAD SCENE INSTANCE:", # LoadStage printout
#	">>> LOAD STAGE:",            # LoadStage printout
	"GroundSensor>>> ", # GroundSensor printout
]

const AXIS = preload("res://debug/visual/Axis.tscn")

# Filter specific messages. Disabled if empty.
var filter = ""

func is_suppressed(id) -> bool:
	for s in suppress:
		if s in id:
			return true
	return false


func identifier(n, k):
	var p = n.name
	if n.is_inside_tree():
		p = n.get_path()
	if p == "":
		p = "<anonymous>"
	return str(p) + ">>> " + str(k)


func graph(n, k, v):
	var id = identifier(n, k)
	if is_suppressed(id): return
	if filter != "" && !(filter in k): return
	if Engine.is_editor_hint():
		print("g[%s]: %s" % [id, v])
		return
	Runtime.UI.Graph(id).plot_next(v)


func graph_color(n, k, c):
	var id = identifier(n, k)
	if is_suppressed(id): return
	if filter != "" && !(filter in k): return
	if Engine.is_editor_hint(): return
	Runtime.UI.Graph(id).color = c


func text(n, k, v):
	var id = identifier(n, k)
	if is_suppressed(id): return
	if filter != "" && !(filter in k): return
	if Engine.is_editor_hint():
		print("kv[%s]: %s" % [id, v])
		return
	Runtime.UI.KeyVal.put(id, v)


func print(n, s):
	var msg = identifier(n, s)
	if is_suppressed(msg): return
	print(msg)
	if Engine.is_editor_hint(): return
	Runtime.UI.Log.print(msg)


func warn(n, s):
	var msg = identifier(n, s)
	if is_suppressed(msg): return
	push_warning(msg)
	if Engine.is_editor_hint(): return
	Runtime.UI.Log.print(msg)


func xyz(vec3):
	if vec3 is Node:
		vec3 = vec3.global_transform
	if Engine.is_editor_hint():
		print("xyz: %s" % vec3)
		return
	var axis = AXIS.instance()
	axis.global_transform.origin = vec3
	get_tree().root.add_child(axis)
