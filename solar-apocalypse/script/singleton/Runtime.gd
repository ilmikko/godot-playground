tool
extends Node

# Instantiated only once per runtime.

# Game-specific classes should go in res://game/Game.gd instead.
# Litmus test: Will my class reset when loading a new save?

var GameScene = preload("res://script/class/GameScene.gd").new()
var Expert    = preload("res://expert/Expert.gd").new()
var Handler   = preload("res://script/class/Handler.gd").new()
var Input     = preload("res://script/class/Input.gd").new()
var UI        = preload("res://ui/UI.tscn").instance()


func rebuild():
	# Needed for "Time" to work.
	add_child(GameScene)

	# if !is_inside_tree(): return
	
	# Don't add below modules in editor.
	if Engine.is_editor_hint(): return

	add_child(Expert, true)
	add_child(Input, true)
	add_child(UI, true)


func _enter_tree():
	rebuild()


var Game setget , _get_Game
func _get_Game():
	return GameScene.Game
