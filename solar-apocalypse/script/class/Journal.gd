extends Node
class_name Journal

var archived_ids = []
var archived setget, _get_archived

var active_ids = []
var active setget, _get_active


func _init():
	_create_journal_pages_node()
	add_to_group("serialize")
	name = "Journal"


func add_page(journal_page):
	var page_id = $JournalPages.get_children().size() + 1
	journal_page.id = page_id
	active_ids.append(journal_page.id)
	$JournalPages.add_child(journal_page, true)

	return page_id


func archive_page(page):
	active_ids.erase(page.id)
	archived_ids.append(page.id)


func activate_page(page):
	archived_ids.erase(page.id)
	active_ids.append(page.id)


func _get_archived():
	archived = []
	for archived_id in archived_ids:
		archived.append(_get_journal_page(archived_id))
	return archived

func _get_active():
	active = []
	for active_id in active_ids:
		active.append(_get_journal_page(active_id))
	return active


func _create_journal_pages_node():
	var journal_pages = Node.new()
	journal_pages.name = "JournalPages"
	add_child(journal_pages, true)


func _get_journal_page(page_id):
	return $JournalPages.get_node(str(page_id))
