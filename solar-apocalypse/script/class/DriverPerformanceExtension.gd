extends Node

# The maximum amount of angular force this character can emit.
export(float, 0, 100) var angular_ability = 0
export(float, 0, 100) var angular_ability_roll = 0

# Base angular damping amount.
export(float, 0, 100) var angular_damping = 0

# The maximum amount of linear force this character can emit.
export(float, 0, 100) var linear_ability = 0

# Base linear damping amount.
export(float, 0, 100) var linear_damping = 0

export(float, 0, 100) var fly_speed = 0

export(float, 0, 100) var keep_upright_intensity = 0

# The multiplier which will be used each frame to calculate the mouse control
# in the absence of mouse input.
export(float, 0, 100) var intent_rotate_decay_mult = 0
export(float, 0, 100) var crane_rotate_decay_mult = 0
