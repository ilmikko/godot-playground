extends "res://script/class/Inventory.gd"

const DriverPerformanceExtension = preload("res://script/class/DriverPerformanceExtension.gd")
const SpaceShip = preload("res://world/ship/SpaceShip.tscn")

func _add_scene_item(item):
	._add_scene_item(item)
	var item_name = Runtime.Expert.ItemName.for(item)
	Runtime.Game.Events.push_event("%s added to inventory" % item_name)

func _add_inventory_item(item):
	._add_inventory_item(item)
	# TODO: Eventually this should be something the user adds to the ship manually through some UI
	if item.item_script is DriverPerformanceExtension:
		var controller = Runtime.Game.BaseController.linked_controller
		var driver_performance_handler = Runtime.Handler.fetch(controller, Runtime.Handler.DriverPerformance)
		driver_performance_handler.add_extension(item.item_script)
