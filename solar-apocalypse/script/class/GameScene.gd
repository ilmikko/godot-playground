tool
extends Node

var loaded = {}

var SaveLoad = preload("res://script/class/SaveLoad.gd").new()
var Game     = null


# Changes to a new scene, unloading all the old scenes.
func change(scene):
	unload_all()
	if Game != null:
		remove_child(Game)
		Game.queue_free()
	Game = GAME.instance()
	add_child(Game)
	load_scene(scene)


func load_scene(scene):
	Runtime.Expert.Loading.load_and_change(scene)
	SaveLoad.call_deferred("load_game_defer", scene)
	loaded[scene] = true


func unload(scene):
	SaveLoad.save_scene(scene)
	loaded.erase(scene)
	scene.queue_free()


func unload_all():
	for scene in loaded:
		unload(scene)


# Saves all current scenes.
func save_all():
	# TODO: For all scenes, save scene
	SaveLoad.save_game()


func load_all():
	# TODO: For all scenes, load scene
	SaveLoad.load_game()


func _enter_tree():
	if Game == null:
		Game = GAME.instance()
	add_child(SaveLoad)
	add_child(Game)


const GAME = preload("res://game/Game.tscn")
