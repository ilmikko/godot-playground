extends Node

const SAVE_PATH = "saves"

var original_serializable_nodes_paths = []


func save_game():
	var serializable_nodes = _serializable_nodes()
	for serializable_node in serializable_nodes:
		_save_node(serializable_node)
	_save_main_data()
	_save_removed_nodes(serializable_nodes)
	_save_added_nodes(serializable_nodes)


func load_game():
	var data = _load_main_data()
	var scene = data["scene"]
	# TODO: Figure out if there's a race condition here.
	call_deferred("load_game_defer", scene)


func load_game_defer(scene):
	Runtime.Expert.Loading.root_load_end(scene)
	var serializable_nodes = _serializable_nodes()
	Util.sort_nodes_by_path(serializable_nodes)

	for serializable_node in serializable_nodes:
		_load_node(serializable_node)
	
	original_serializable_nodes_paths = Util.nodes_paths(serializable_nodes)
	
	_remove_removed_nodes()
	_add_added_nodes()


func save_scene(_scene):
	# TODO
	pass


func _remove_removed_nodes():
	var save_path = "user://" + SAVE_PATH + "/removed_nodes.json"
	if !Util.file_exists(save_path): return
	var removed_nodes_paths = Util.load_json_from_file(save_path)
	for removed_node_path in removed_nodes_paths:
		var node = get_node(removed_node_path)
		if not node: continue

		node.queue_free()


func _add_added_nodes():
	var save_path = "user://" + SAVE_PATH + "/added_nodes.json"
	if !Util.file_exists(save_path): return
	var added_nodes_paths = Util.load_json_from_file(save_path)
	for added_node_path in added_nodes_paths:
		var node_data = _load_node_data_from_path(added_node_path)
		var serializer = load(node_data["serializer"]).from_data(node_data)
		var last_seperator = node_data["node_path"].find_last("/")
		var parent_node_path = node_data["node_path"].substr(0, last_seperator)
		print(parent_node_path)
		get_node(parent_node_path).add_child(serializer.node)


func _load_node(node):
	var save_data = _load_node_data(node)

	if not save_data: return

	_serializer_for_node(node).deserialize(save_data)


func _load_node_data(node):
	var node_path = node.get_path()
	return _load_node_data_from_path(node_path)

func _load_node_data_from_path(node_path):
	var save_path = "user://" + SAVE_PATH + node_path + ".json"
	var save_file = File.new()
	if not save_file.file_exists(save_path): return false

	save_file.open(save_path, File.READ)
	var content = parse_json(save_file.get_as_text())
	save_file.close()
	return content


func _save_main_data():
	var save_path = "user://" + SAVE_PATH + "/main_data.json"
	var save_file = File.new()
	save_file.open(save_path, File.WRITE)
	save_file.store_string(to_json(_main_save_data()))
	save_file.close()


func _save_removed_nodes(current_serializable_nodes):
	var current_nodes_paths = Util.nodes_paths(current_serializable_nodes)
	var removed_nodes_paths = Util.subtract_array(original_serializable_nodes_paths, current_nodes_paths)
	var save_path = "user://" + SAVE_PATH + "/removed_nodes.json"
	Util.save_content_to_file(to_json(removed_nodes_paths), save_path)


func _save_added_nodes(current_serializable_nodes):
	var current_nodes_paths = Util.nodes_paths(current_serializable_nodes)
	var added_nodes_paths = Util.subtract_array(current_nodes_paths, original_serializable_nodes_paths)
	var save_path = "user://" + SAVE_PATH + "/added_nodes.json"
	Util.save_content_to_file(to_json(added_nodes_paths), save_path)


func _load_main_data():
	var save_path = "user://" + SAVE_PATH + "/main_data.json"
	return Util.load_json_from_file(save_path)


func _main_save_data():
	return {
		"scene": get_tree().get_current_scene().filename
	}


func _save_node(node):
	var save_data = _save_node_data(node)
	_write_save_data(save_data)


func _save_node_data(node):
	return _serializer_for_node(node).serialize()


func _write_save_data(save_data):
	var node_path = save_data["node_path"]
	var save_path = "user://" + SAVE_PATH + node_path + ".json"
	var last_seperator = save_path.find_last("/")
	var save_file = File.new()
	var dir_make_response = Directory.new().make_dir_recursive(save_path.substr(0, last_seperator))
	assert(dir_make_response == OK)
	save_file.open(save_path, File.WRITE)
	save_file.store_string(to_json(save_data))
	save_file.close()


func _serializer_for_node(node):
	var node_source = node.filename
	var scene_serializer_source = node_source.replace("res://", "res://serializers/")

	if "custom_serializer" in node:
		return node.custom_serializer
	elif File.new().file_exists(scene_serializer_source):
		return load(scene_serializer_source).new(node)

	var node_serializer_source = "res://serializers/node/" + node.get_class() + "Serializer.gd"
	
	if not File.new().file_exists(node_serializer_source):
		push_error("No serializer exists for the node" + node.get_path())

	return load(node_serializer_source).new(node)


func _serializable_nodes():
	return get_tree().get_nodes_in_group("serialize")

