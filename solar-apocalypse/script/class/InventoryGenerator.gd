extends Node

var Inventory = preload("res://script/class/Inventory.gd")
var InventoryItem = preload("res://script/class/InventoryItem.gd")
var SpaceShip = preload("res://world/ship/SpaceShip.tscn")
var DriverPerformanceExtension = preload("res://script/class/DriverPerformanceExtension.gd")

func generate():
	var inventory = Inventory.new()
	inventory.add_item(SpaceShip)
	inventory.add_item(_create_spaceship_speed_booster())
	return inventory

func _create_spaceship_speed_booster():
	var inventory_item = InventoryItem.new()
	inventory_item.key = "space_ship_golden_turbo"
	inventory_item.item_name = "Space Ship Golden Turbo"
	inventory_item.description = "Boosts the speed of your space ship by 5"
	inventory_item.base_costs = [{ "key": "gold_nugget", "quantity": 1 }]
	inventory_item.item_script = _create_driver_performance_extension_for_booster()
	return inventory_item

func _create_driver_performance_extension_for_booster():
	var dpe = DriverPerformanceExtension.new()
	dpe.fly_speed = 10.0
	return dpe
