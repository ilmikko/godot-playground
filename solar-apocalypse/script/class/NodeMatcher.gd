extends Node

var matching_group
var matching_class
var matching_node


func is_matching(node):
	return (
		(matching_group == null or (node and node.is_in_group(matching_group))) and
		(matching_class == null or (node and node is matching_class)) and
		(matching_node == null or matching_node == node)
	)
