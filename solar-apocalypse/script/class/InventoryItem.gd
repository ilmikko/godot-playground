extends Node

var item_name
var key setget set_key, get_key
var description
var base_costs
var quantity = 1
var item_script = null

func get_key():
	if key: return key

	return { "item_name": item_name, "key": key, "description": description, "base_costs": base_costs }.hash()

func set_key(new_key):
	key = new_key
