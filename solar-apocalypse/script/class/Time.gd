extends Node


enum {
	UPDATER_0_0_5,
	UPDATER_0_1,
	UPDATER_0_5,
	UPDATER_1_0,
	UPDATER_5_0,
}


var threshold = {
	UPDATER_0_0_5: 0.05,
	UPDATER_0_1:   0.1,
	UPDATER_0_5:   0.5,
	UPDATER_1_0:   1.0,
	UPDATER_5_0:   5.0,
}


var value = {
	UPDATER_0_0_5: threshold[UPDATER_0_0_5],
	UPDATER_0_1:   threshold[UPDATER_0_1],
	UPDATER_0_5:   threshold[UPDATER_0_5],
	UPDATER_1_0:   threshold[UPDATER_1_0],
	UPDATER_5_0:   threshold[UPDATER_5_0],
}


var scheduler = Scheduler.new()
var rate_limiter = RateLimiter.new()


var signal = {
	UPDATER_0_0_5: "at_least_0_0_5_seconds_passed",
	UPDATER_0_1:   "at_least_0_1_seconds_passed",
	UPDATER_0_5:   "at_least_0_5_seconds_passed",
	UPDATER_1_0:   "at_least_1_0_seconds_passed",
	UPDATER_5_0:   "at_least_5_0_seconds_passed",
}

# Updated approximately every 0.05s of game time. Use sparingly.
signal at_least_0_0_5_seconds_passed(n)
# Updates approximately every 0.1s of game time.
signal at_least_0_1_seconds_passed(n)
# Updates approximately every 0.5s of game time.
signal at_least_0_5_seconds_passed(n)
# Updates approximately every 1s of game time.
signal at_least_1_0_seconds_passed(n)
# Updates approximately every 5s of game time.
signal at_least_5_0_seconds_passed(n)


# now returns the total number of seconds from the game beginning.
var now setget _set_now, _get_now
func _set_now(_now):
	absolute = int(_now)
	relative = fmod(float(_now), 1.0)
	signal_updaters()
func _get_now():
	return absolute + int(relative)


func _increment_updater(updater, delta):
	value[updater] += delta
	var v = value[updater]
	var t = threshold[updater]
	if v < t: return
	var n = int(v/t)
	emit_signal(signal[updater], v)
	value[updater] -= n * t


var RELATIVE_TIME_MAX = 100 # 1e9


# Due to floats being annoying, we keep track of absolute (in seconds)
# and relative (also in seconds, but with fractions) times.
# relative can sometimes be reset if it grows too large, but the fractional
# content should not change.
# The total time can be derived from absolute + relative.
var absolute = 0
var relative = 0.0


func increment_updaters(delta):
	scheduler.tick(delta)
	rate_limiter.tick(delta)
	_increment_updater(UPDATER_0_0_5, delta)
	_increment_updater(UPDATER_0_1, delta)
	_increment_updater(UPDATER_0_5, delta)
	_increment_updater(UPDATER_1_0, delta)
	_increment_updater(UPDATER_5_0, delta)


func signal_updaters():
	# These are regular emit_signal strings because otherwise the linter
	# complains that the signals are "never emitted".
	emit_signal("at_least_0_0_5_seconds_passed", 0)
	emit_signal("at_least_0_1_seconds_passed",   0)
	emit_signal("at_least_0_5_seconds_passed",   0)
	emit_signal("at_least_1_0_seconds_passed",   0)
	emit_signal("at_least_5_0_seconds_passed",   0)


# Schedules something to happen after N seconds of game time.
# NB: If game time is skipped, method will be called AFTER the skip.
func after_s(seconds, obj, method, args=null):
	scheduler.schedule(seconds, obj, method, args)


func rate_limit_s(seconds, obj, method):
	rate_limiter.limit(seconds, obj, method)


func skip(d):
	absolute += int(d)
	increment_updaters(d)


func tick(delta):
	# Do not progress game time if we are in the editor.
	# TODO: This should also probably apply in places like the main menu etc.
	if Engine.is_editor_hint(): return

	relative += delta
	increment_updaters(delta)
	
	if  relative  > RELATIVE_TIME_MAX:
		absolute += int(RELATIVE_TIME_MAX)
		relative  = fmod(relative, 1.0)
	
	Debug.text(self, "Current game time", _get_now())
	Debug.text(self, "Current absolute time", absolute)
	Debug.text(self, "Current relative time", relative)


func _process(delta):
	tick(delta)
