extends Node

signal event(text)

func push_event(text):
	emit_signal("event", text)
