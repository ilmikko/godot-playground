extends Node

var Builder = preload("res://handler/builder/BuilderHandler.tscn")
var Health = preload("res://handler/health/HealthHandler.tscn")
var Wetness = preload("res://handler/wetness/WetnessHandler.tscn")
var Vitals = preload("res://handler/vitals/VitalsHandler.tscn")
var DriverPerformance = preload("res://handler/driverperformance/DriverPerformanceHandler.tscn")
# Which name to refer each handler with.
var names = {
	Builder: "BuilderHandler",
	Health: "HealthHandler",
	Wetness: "WetnessHandler",
	Vitals: "VitalsHandler",
	DriverPerformance: "DriverPerformanceHandler"
}

# Fetches a handler for a body, creating one if it doesn't exist.
func fetch(body, handler):
	var p = Util.get_root_parent(body)
	var h = p.get_node_or_null(names[handler])
	if h != null: return h
	
	h = handler.instance()
	h.name = names[handler]
	p.add_child(h, true)

	return h
