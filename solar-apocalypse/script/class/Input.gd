extends Node


func _input(_event):
	if Input.is_action_just_pressed("debug_console"):
		Runtime.UI.debug_toggle()
	
	# TODO: Decouple "pause" and "escape"
	if Input.is_action_just_released("pause"):
		if !Runtime.Game.in_menu:
			Runtime.UI.game_pause()
		else:
			Runtime.UI.game_resume()


func _ready():
	pause_mode = Node.PAUSE_MODE_PROCESS
