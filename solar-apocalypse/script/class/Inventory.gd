extends Node

const SceneInfo = preload("res://script/lib/SceneInfo.gd")
const InventoryItem = preload("res://script/class/InventoryItem.gd")

var items_by_key = {}


func remove_item(inventory_item):
	var key = inventory_item.key
	if !items_by_key.has(key): return

	items_by_key[key].quantity -= 1
	if items_by_key[key].quantity <= 0:
		items_by_key.erase(key)


func add_item(item):
	if item is InventoryItem:
		_add_inventory_item(item)
	else:
		_add_scene_item(item)


func _add_inventory_item(item):
	if items_by_key.has(item.key):
		items_by_key[item.key].quantity += item.quantity
	else:
		items_by_key[item.key] = item


func _add_scene_item(item):
	var inventory_item = InventoryItem.new()
	
	var scene_info = SceneInfo.for_scene(item)
	inventory_item.item_name =  _item_name(item, scene_info)
	inventory_item.description = _item_description(item, scene_info)
	inventory_item.base_costs = _item_base_cost(item, scene_info)
	inventory_item.key = _item_key(item, scene_info)
	var key = inventory_item.key

	if !items_by_key.has(inventory_item.key):
		items_by_key[key] = inventory_item
	else:
		items_by_key[key].quantity += 1

	if item is Node:
		item.queue_free()


func has_item_by_key(key, quantity=1):
	return items_by_key.has(key) && items_by_key[key].quantity >= quantity


func item_count_by_key(key):
	if !items_by_key.has(key): return 0
	return items_by_key[key].quantity


func _item_name(item, scene_info):
	return Runtime.Expert.ItemName.for(item)


func _item_description(_item, scene_info):
	return scene_info.get("description", "")


func _item_base_cost(_item, scene_info):
	return scene_info.get("shop", {}).get("base_costs", [])


func _item_key(_item, scene_info):
	return scene_info.get("inventory", {}).get("key", null)
