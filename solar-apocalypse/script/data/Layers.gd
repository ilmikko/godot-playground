class_name Layers

enum {
	COMMON
	OCCUPANCY
	SOUND
	INTERACTION
	CAMERA
	PLAYER
}

const ids = {
	COMMON: 0,
	OCCUPANCY: 15,
	SOUND: 16,
	INTERACTION: 17,
	CAMERA: 18,
	PLAYER: 19,
}

static func id(i):
	return ids[i]
