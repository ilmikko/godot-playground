extends Spatial


func send_sound(sound):
	Runtime.UI.ClosedCaptions.push(sound)


func sound_influence(node):
	node.receiver_enter(self)


func sound_influence_leave(node):
	node.receiver_leave(self)


func _on_Area_area_entered(area):
	var root_parent = Util.get_root_parent(area)
	if !root_parent.is_in_group("makes_sound"): return
	sound_influence(root_parent)


func _on_Area_area_exited(area):
	var root_parent = Util.get_root_parent(area)
	if !root_parent.is_in_group("makes_sound"): return
	sound_influence_leave(root_parent)
