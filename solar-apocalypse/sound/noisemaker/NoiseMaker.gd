extends Spatial


export var active = false setget _set_active
func _set_active(_active):
	active = _active
	rebuild()


var random_noises = [
	"*rabbit noises*",
	"*rabbit noises!*",
]
var receivers = []


func rebuild():
	if active:
		timer_start()
	else:
		timer_stop()


func receiver_enter(receiver):
	if (receiver in receivers): return
	if receivers.size() == 0:
		_set_active(true)
	receivers.append(receiver)


func receiver_leave(receiver):
	if !(receiver in receivers): return
	if receivers.size() == 1:
		_set_active(false)
	receivers.remove(receivers.find(receiver))


func random_noise():
	return random_noises[randi() % random_noises.size()]


func make_noise():
	var noise = random_noise()
	for receiver in receivers:
		receiver.send_sound(noise)


func timer_start():
	$Timer.wait_time = randf() * 3.0
	$Timer.start()


func timer_stop():
	$Timer.stop()


func _on_Timer_timeout():
	make_noise()
	timer_start()
