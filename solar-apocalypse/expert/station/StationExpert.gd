tool
extends Node


var r = RandomNumberGenerator.new()


# Randomizes a station for an ID.
func for_id(id):
	r.seed = id
	# TODO: Only some factions support some stations.
	return possible_stations[r.randi() % len(possible_stations)].instance()


func basis_for(id) -> Basis:
	r.seed = id
	return Basis(Vector3(r.randf(), r.randf(), r.randf()) * 2.0 - Vector3(1, 1, 1))


var possible_stations = [
	STATION_A,
	STATION_B,
]


const STATION_A = preload("res://world/station/StationA.tscn")
const STATION_B = preload("res://world/station/StationB.tscn")
