extends Node


func answer(_t, args):
	var targeter = args[0]
	var from = args[1]
	var body = args[2]
	
	targeter.answer(body)


func comms(from, targeter):
	targeter.comms(Runtime.Expert.TargetScan.get_scanned(targeter))


func comms_with(targeter, from, body):
	var fp = Scene.filepath(body)
	match fp:
		"res://world/ship/SpaceShip.tscn":
			Runtime.Game.Time.after_s(3.5, self, "answer", [targeter, from, body])
		"res://world/station/StationA.tscn":
			Runtime.Game.Time.after_s(1.9, self, "answer", [targeter, from, body])
		"res://world/station/StationB.tscn":
			Runtime.Game.Time.after_s(1.9, self, "answer", [targeter, from, body])
		_:
			return false
