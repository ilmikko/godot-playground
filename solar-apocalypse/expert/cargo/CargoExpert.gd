extends Node


var rng = RandomNumberGenerator.new()


const MAX_CARGO_WEIGHT = 200.0
const MIN_CARGO_WEIGHT = 10.0


# Calculates how much cargo in kgs a certain ship might hold.
func weight_for(ship) -> float:
	rng.seed = ship.id
	
	return MAX_CARGO_WEIGHT * rng.randf() * rng.randf() + MIN_CARGO_WEIGHT
