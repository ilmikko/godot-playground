extends Node


func attach_crane(body):
	var crane = CRANE.instance()
	
	match Scene.filepath(body):
		"res://actor/person/driver/Driver.tscn":
			Util.move_child_under(crane, body.CraneBase)
		_:
			Util.move_child_under(crane, body)
	
	return crane


# CraneExpert decides how and where to attach a crane to a node.
func attach(body, controller):
	controller.linked_controller = body
	
	# Try to use an existing crane.
	var crane = body.get_node_or_null("Crane")
	if  crane == null:
		crane = attach_crane(body)
	
	crane.attach(controller)


const CRANE = preload("res://player/camera/crane/Crane.tscn")
