extends Node

enum {
	UNKNOWN
	NONE
	INDEPENDENT
	CIVILIAN
	MERCHANT
	PIRATE
}


var r = RandomNumberGenerator.new()
var names = {
	UNKNOWN:     "Unknown",
	NONE:        "None",
	INDEPENDENT: "Independent",
	CIVILIAN:    "Civilian",
	MERCHANT:    "Merchant",
	PIRATE:      "Pirate",
}


func name(faction):
	return names[faction]


# Get a faction for an object.
# Probably also faction relationships when we get there.
func for(scene):
	var p = Scene.parent_with_node(scene, "FactionState")
	if  p != null: return p.get_node("FactionState").faction
	var fp = Scene.filepath(scene)
	match fp:
		"res://world/ship/SpaceShip.tscn": return CIVILIAN
	return INDEPENDENT


func for_id(id : int):
	r.seed = id
	var p = r.randf()
	if p < 0.3:
		return CIVILIAN
	elif p < 0.7:
		return PIRATE
	else:
		return NONE


# Returns true whether a faction hates another faction.
func hates(a, b):
	if a == PIRATE:
		if b in [CIVILIAN, MERCHANT]: return true
	return false
