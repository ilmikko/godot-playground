extends Node


# Determines which stats to show when certain things are scanned.


func scan_object(targeter, body, scan_amount):
	var info = {}
	
	info["Type"] = Runtime.Expert.ItemName.for(body)
	
	var fp = Scene.filepath(body)
	match fp:
		"res://world/field/Location.tscn":
			var id = body.id
			info["Name"] = Runtime.Expert.Star.name(id)
			info["Distance"] = "Unknown"
			info["Spectral Type"] = "Unknown"
			info["Classification"] = "Unknown"
			info["Faction"] = "Unknown"
			if scan_amount > 0.2:
				info["Distance"] = "%s AU" % (targeter.global_transform.origin - body.global_transform.origin).length()
			if scan_amount > 0.5:
				info["Spectral Type"] = Runtime.Expert.Star.spectral(id)
			if scan_amount > 0.65:
				info["Classification"] = Runtime.Expert.Star.classification(id)
			if scan_amount > 0.8:
				info["Faction"] = Runtime.Expert.Faction.name(Runtime.Expert.Faction.for_id(id))
		"res://world/space_stuff/asteroid/Asteroid.tscn", "res://world/space_stuff/asteroid/Asteroid2.tscn", "res://world/space_stuff/asteroid/Asteroid3.tscn", "res://world/space_stuff/asteroid/Asteroid4.tscn":
			info["Mass"] = "Unknown"
			info["Health"] = "Unknown"
			info["Nickel"] = "Unknown"
			info["Iron"] = "Unknown"
			info["Gold"] = "Unknown"
			if scan_amount > 0.2:
				info["Mass"] = Runtime.Expert.Mass.format(Runtime.Expert.Mass.for(body))
			if scan_amount > 0.4:
				info["Health"] = Runtime.Expert.Health.for(body).health
		"res://world/ship/SpaceShip.tscn":
			info["Mass"] = "Unknown"
			info["Status"] = "Unknown"
			info["Faction"] = "Unknown"
			info["Health"] = "Unknown"
			info["Shields"] = "Unknown"
			info["Armor"] = "Unknown"
			info["Cargo"] = "Unknown"
			if scan_amount > 0.2:
				# The cargo contributes to the mass, but we don't tell what it is.
				var cargo = Runtime.Expert.Cargo.weight_for(body)
				info["Mass"] = Runtime.Expert.Mass.format(cargo + Runtime.Expert.Mass.for(body))
			if scan_amount > 0.3:
				info["Status"] = "OK"
			if scan_amount > 0.4:
				info["Health"] = Runtime.Expert.Health.for(body).health
			if scan_amount > 0.45:
				info["Faction"] = Runtime.Expert.Faction.name(Runtime.Expert.Faction.for(body))
			if scan_amount > 0.5:
				info["Shields"] = "OK"
			if scan_amount > 0.6:
				info["Armor"] = "OK"
			if scan_amount > 0.9:
				info["Cargo"] = Runtime.Expert.Mass.format(Runtime.Expert.Cargo.weight_for(body))
		"res://world/station/StationA.tscn", "res://world/station/StationB.tscn":
			info["Mass"] = "Unknown"
			info["Shields"] = "Unknown"
			info["Armor"] = "Unknown"
			info["Status"] = "Unknown"
			info["Cargo"] = "Unknown"
			info["Faction"] = "Unknown"
	return info
