extends Node


var districts = {}
var district_current = null
var district_current_instance = null

var size = 4096.0


func add(district : Vector3, location, id : int):
	districts[district] = [location, id]


func fetch(district : Vector3):
	if !(district in districts): return null
	return districts[district]


func change(district : Vector3):
	Debug.text(self, "Current District", district)
	if district_current != null:
		Debug.print(self, "removed old district")
		remove_child(district_current_instance)
	district_current = fetch(district)
	if district_current == null: return
	district_current_instance = district_current[0].instance()
	district_current_instance.id = district_current[1]
	district_current_instance.transform.origin = district * size + Vector3(size, size, size) * 0.5
	Debug.print(self, "loaded district %s" % district)
	add_child(district_current_instance)


func remove(district : Vector3):
	districts.erase(district)
