extends Node


# Controls what happens when a targeter points at a body and the ray hits a position.
func pointing_at(targeter, body, pos):
	var t = TARGET.instance()
	t.name = "TARGET"
	body.add_child(t, true)
	
	Runtime.Expert.TargetScan.set_candidate(targeter, body)
	
	var fp = Scene.filepath(body)
	match fp:
		"res://world/field/Location.tscn":
			var id = body.id
			Runtime.UI.Hint.star_label = "%s" % Runtime.Expert.Star.name(id)
		"res://world/BlackHole.tscn":
			Runtime.UI.Hint.star_label = "???"
		"res://world/solar_system/SolarSystem.tscn":
			# TODO: More info when you're in the solar system.
			var id = Scene.root(body).id
			Runtime.UI.Hint.star_label = "Local Star: %s" % Runtime.Expert.Star.name(id)
		"res://world/station/StationA.tscn":
			var id = Scene.root(body).id
			Runtime.UI.Hint.star_label = "%s Space Station" % Runtime.Expert.Star.name(id)
		"res://world/station/StationB.tscn":
			var id = Scene.root(body).id
			Runtime.UI.Hint.star_label = "%s Space Station" % Runtime.Expert.Star.name(id)
		_:
			var name = Runtime.Expert.ItemName.for(body)
			if  name != "":
				Runtime.UI.Hint.star_label = name
			else:
				print("Unknown scene for targeter:", fp)
				Runtime.UI.Hint.star_label = "???"


func no_longer_pointing_at(targeter, body):
	Runtime.Expert.TargetScan.set_candidate(targeter, null)
	if  body == null or !is_instance_valid(body):
		Runtime.UI.Hint.star_label = ""
		return
	var t = body.get_node_or_null("TARGET")
	if  t == null: return
	t.queue_free()


const TARGET = preload("res://world/ui/Target.tscn")
