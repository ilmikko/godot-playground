tool
extends Node


var STAR_STARTING_SOLAR_SYSTEM = 9999
var custom_names = {
	STAR_STARTING_SOLAR_SYSTEM: "Umm",
}


var r = RandomNumberGenerator.new()


func name(id):
	if id in custom_names:
		return custom_names[id]
	r.seed = SEED_NAME + id
	var name_type = r.randf()
	var n = ""
	if   name_type < 0.1:
		# "Pure" name
		n = "%s %s" % [
			prefixes[r.randi() % len(prefixes)],
			names[r.randi() % len(names)],
		]
	elif name_type < 0.15:
		# "Someone's star"
		n = "%s's Star" % [
			owners[r.randi() % len(owners)]
		]
	elif name_type < 0.4:
		# name + number
		n = "%s %s" % [
			first[r.randi() % len(first)],
			number(r),
		]
	elif name_type < 0.6:
		# Code + name
		n = "%s %s" % [
			codes[r.randi() % len(codes)],
			names[r.randi() % len(names)],
		]
	elif name_type < 0.85:
		# "Pure" + roman number
		n = "%s %s" % [
			names[r.randi() % len(names)],
			romanized[r.randi() % 8],
		]
	else:
		# Code + Number
		n = "%s %s" % [
			codes[r.randi() % len(codes)],
			number(r),
		]
	return n


func spectral(id):
	var c = ""
	var s = size(id) / MAX_SIZE
	if s < 0.1:
		c += "Class M"
	elif s < 0.2:
		c += "Class K"
	elif s < 0.4:
		c += "Class G"
	elif s < 0.5:
		c += "Class F"
	elif s < 0.7:
		c += "Class A"
	elif s < 0.9:
		c += "Class B"
	else:
		c += "Class O"
	return c


func classification(id):
	var c = ""
	var s = size(id) / MAX_SIZE
	if s < 0.1:
		c += "Dwarf"
	elif s < 0.4:
		c += "Subdwarf"
	elif s < 0.5:
		c += "Main Sequence"
	elif s < 0.7:
		c += "Giant"
	elif s < 0.9:
		c += "Supergiant"
	else:
		c += "Hypergiant"
	return c


func number(r):
	if r.randf() < 0.6:
		return r.randi() % 10000
	return "%s-%s" % [r.randi() % 100, r.randi() % 10000]


func brightness(id):
	r.seed = SEED_BRIGHTNESS + id
	return r.randf() * r.randf() * MAX_BRIGHTNESS


func color(id):
	var s = size(id) / MAX_SIZE
	return COLOR_RAMP.interpolate(1.0 - s)


func size(id):
	r.seed = SEED_SIZE + id
	return r.randf() * MAX_SIZE


var prefixes = [
	"Alpha",
	"Beta",
	"Gamma",
	"Proxima",
	"Omega",
	"Epsilon",
	"Tau",
	"Zeta",
]

var names = [
	"Centauri",
	"Eridani",
	"Aquarii",
	"Leporis",
	"Circinus",
	"Ceti",
	"Hydri",
	"Cancri",
	"Indi",
	"Cygni",
	"Leonis",
	"Farcus",
	"Orcus",
	"Sagittarii",
	"Librus",
	"Berenices",
	"Aquarius",
]

var owners = [
	"Van Maanen",
	"Bernand",
	"Collinson",
	"Voiter",
	"Bravo",
	"Leopold",
	"Surgeon",
	"Keeyan",
	"Scholz",
]


var codes = [
	"AG",
	"EZ",
	"DEN",
	"DZ",
	"DX",
	"AD",
	"L"
]


var first = [
	"Wolf",
	"Lalande",
	"Luyten",
	"Ross",
	"Lacaille",
	"Struve",
	"Gliese",
]


var romanized = {
	0: "0",
	1: "I",
	2: "II",
	3: "III",
	4: "IV",
	5: "V",
	6: "VI",
	7: "VII",
	8: "VIII",
	9: "IX",
	10: "X",
}

const MAX_BRIGHTNESS = 1.0
const MAX_SIZE = 2.0
const SEED_SIZE       = 900.0
const SEED_BRIGHTNESS = 90.0
const SEED_COLOR      = 9.0
const SEED_NAME       = 9.1

const COLOR_RAMP = preload("res://expert/star/StarColor.tres")
