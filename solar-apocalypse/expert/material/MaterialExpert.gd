extends Node

# This expert determines a material for an arbitrary scene.
func for(scene):
	var mat = Materials.VOID
	
	var fp = Scene.filepath(scene)
	if fp in materials:
		return materials[fp]
	
	if fp:
		Debug.warn(self, "could not determine material for %s" % fp)
	
	return mat

# TODO: Let's unite all the scrappy stats (see ItemNameExpert).
var materials = {
	"res://actor/bee/driver/Driver.tscn": Materials.FLESH,
	"res://actor/person/Person.tscn": Materials.FLESH,
	"res://actor/person/driver/Driver.tscn": Materials.FLESH,
	"res://actor/person/skeleton/rigidbodies/arm/PersonArm.tscn": Materials.FLESH,
	"res://actor/person/skeleton/rigidbodies/head/PersonHead.tscn": Materials.FLESH,
	"res://actor/person/skeleton/rigidbodies/leg/PersonLeg.tscn": Materials.FLESH,
	"res://actor/person/skeleton/rigidbodies/upperbody/UpperBody.tscn": Materials.FLESH,
	"res://actor/rabbit/driver/Driver.tscn": Materials.FLESH,
	"res://debug/visual/CheckeredGround.tscn": Materials.METAL,
	"res://world/barrel/Barrel.tscn": Materials.WOOD,
	"res://world/buildingresources/Wood.tscn": Materials.WOOD,
	"res://world/buildingresources/Metal.tscn": Materials.METAL,
	"res://world/effect/gib/wood/GibWood1.tscn": Materials.WOOD,
	"res://world/effect/gib/wood/GibWood2.tscn": Materials.WOOD,
	"res://world/effect/gib/wood/GibWood3.tscn": Materials.WOOD,
	"res://world/exterior/ChoppingBlock.tscn": Materials.WOOD,
	"res://world/exterior/SignPost.tscn": Materials.WOOD,
	"res://world/foliage/tree/apple/AppleTree.tscn": Materials.WOOD,
	"res://world/foliage/tree/pine/PineTree.tscn": Materials.WOOD,
	"res://world/interior/chest/ChestRound.tscn": Materials.WOOD,
	"res://world/interior/table/StudyCabinetDoor.tscn": Materials.WOOD,
	"res://world/interior/table/StudyCabinetDrawer.tscn": Materials.WOOD,
	"res://world/interior/table/StudyCabinetShellWithStopper.tscn": Materials.WOOD,
	"res://world/interior/table/StudyCabinetShell.tscn": Materials.WOOD,
	"res://world/house/frame/RoofPillar2x.tscn": Materials.WOOD,
	"res://world/house/frame/WallPillar.tscn": Materials.WOOD,
	"res://world/house/frame/WallPillar2y.tscn": Materials.WOOD,
	"res://world/house/parts/Window.tscn": Materials.WOOD,
	"res://world/house/parts/door/Door.tscn": Materials.WOOD,
	"res://world/house/parts/exterior/Wall.tscn": Materials.WOOD,
	"res://world/house/parts/exterior/Wall0.5x.tscn": Materials.WOOD,
	"res://world/house/parts/exterior/Wall2x.tscn": Materials.WOOD,
	"res://world/house/parts/exterior/WallDoor.tscn": Materials.WOOD,
	"res://world/house/parts/exterior/WallWindow.tscn": Materials.WOOD,
	"res://world/house/parts/foundation/StoneFoundation2x1.tscn": Materials.STONE,
	"res://world/house/parts/interior/WallInteriorDivider.tscn": Materials.WOOD,
	"res://world/ship/SpaceShip.tscn": Materials.METAL,
	"res://world/tool/axe/Axe.tscn": Materials.METAL,
	"res://world/tool/farmhoe/FarmHoe.tscn": Materials.METAL,
	"res://world/tool/hammer/Hammer.tscn": Materials.METAL,
	"res://world/tool/pickaxe/Pickaxe.tscn": Materials.METAL,
	"res://world/tool/shovel/Shovel.tscn": Materials.METAL,
	"res://world/tool/sword/Sword.tscn": Materials.METAL,
}
