extends Node


func explode(body):
	var ex = EXPLOSION.instance()
	ex.force = 1.0
	ex.transform = body.global_transform
	get_tree().root.add_child(ex)
	body.queue_free()


var EXPLOSION = preload("res://world/effect/Explosion.tscn")
