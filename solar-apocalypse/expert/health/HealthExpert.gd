extends Node


# Average amount of health every body has.
var NORMAL_HEALTH = 100.0
# Minimum amount of health every body has.
var MIN_HEALTH = 5.0
# Amount of extra health from mass.
var MASS_HEALTH_MULTIPLIER = 0.2


func for(item):
	var health = item.get_node_or_null("HealthState")
	if  health != null: return health
	health = HEALTH_STATE.instance()
	health.health = max_health_for(item)
	item.add_child(health)
	return health


func lose(item, amount):
	var health = for(item)
	health.health -= amount
	if health.health <= 0:
		Runtime.Expert.Breakage.break(item)
	return health


func max_health_for(item):
	var h = NORMAL_HEALTH
	# The bigger the body is, the more health it has.
	h *= Runtime.Expert.Mass.for(item) * MASS_HEALTH_MULTIPLIER
	h += MIN_HEALTH
	return h


const HEALTH_STATE = preload("res://state/health/HealthState.tscn")
