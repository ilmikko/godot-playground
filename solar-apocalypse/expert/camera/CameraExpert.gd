extends Node

# This node answers questions such as: "give me the cameras that are currently
# connected to player viewports" or "what is the far clipping plane set to?".


var active_player_camera = null
var viewport_cameras = []

var fov       = 70.0
var clip_far  = 1000.0
var clip_near = 0.05


func add_viewport_camera(camera):
	if camera in viewport_cameras: return
	viewport_cameras.append(camera)
	Debug.text(self, "viewport_cameras", viewport_cameras)
	camera.current = true


func remove_viewport_camera(camera):
	viewport_cameras.erase(camera)
	Debug.text(self, "viewport_cameras", viewport_cameras)
	camera.current = false


# NB: This is a PlayerCamera. If you want active Cameras, see set_viewport_camera.
func set_active_player_camera(player_camera):
	active_player_camera = player_camera


# NB: This is a PlayerCamera. If you want active Cameras, see get_viewport_cameras.
func get_active_player_camera():
	return active_player_camera


func set_viewport_camera(camera):
	for c in viewport_cameras:
		remove_viewport_camera(c)
	viewport_cameras = []
	add_viewport_camera(camera)
	emit_signal("viewport_camera_changed", camera)


func get_viewport_cameras():
	return viewport_cameras
