extends Node

# This expert decides what happens when a tool hits a body.
func hit(instrument, body):
	hit_transform(instrument, body, instrument.global_transform)


func hit_damage_base(instrument_mat, body_mat):
	if body_mat in body_base_damages:
		return body_base_damages[body_mat]
	return DAMAGE_DEFAULT


func hit_damage_multiplier(instrument, body_mat):
	if !(instrument.filename in body_damage_mat_multipliers):
		return DAMAGE_MULTIPLIER_DEFAULT
	if !(body_mat in body_damage_mat_multipliers[instrument.filename]):
		return DAMAGE_MULTIPLIER_DEFAULT
	return body_damage_mat_multipliers[instrument.filename][body_mat]


func hit_sound(instrument_mat, body_mat):
	if body_mat in body_hit_sounds:
		return body_hit_sounds[body_mat]
	return SOUND_DEFAULT


func hit_transform(instrument, body, transform):
	hit_damage_body(instrument, body)


func hit_damage_body(instrument, body):
	var instrument_mat = Runtime.Expert.Material.for(instrument)
	var body_mat       = Runtime.Expert.Material.for(body)
	
	instrument.play_hit_sound(hit_sound(instrument_mat, body_mat))
	
	# Damage time!
	var damage = hit_damage_base(instrument_mat, body_mat)
	# Some tools have specific damage they make to certain materials.
	damage *= hit_damage_multiplier(instrument, body_mat)
	
	Debug.print(self, "Hit %s -> %s for %s damage" % [
		Materials.name(instrument_mat), Materials.name(body_mat), damage])
	
	Runtime.Expert.Health.lose(body, damage)
	
	# Punt rigidbodies away from the tool.
#	if body is RigidBody && body.mode == RigidBody.MODE_RIGID:
#		var diff = (body.global_transform.origin-instrument.global_transform.origin)
#		body.apply_central_impulse(diff * instrument.HIT_IMPULSE_AMOUNT)


var body_hit_sounds = {
	Materials.DIRT: SOUND_DIRT,
	Materials.WOOD: SOUND_WOOD,
}

var body_base_damages = {
	Materials.DIRT: 0.8,
	Materials.FLESH: 0.9,
	Materials.METAL: 0.08,
	Materials.STONE: 0.25,
	Materials.WOOD: 0.5,
}

var body_damage_mat_multipliers = {
	"res://world/tool/axe/Axe.tscn": {
		Materials.WOOD:  75.0,
		Materials.FLESH: 3.5,
	},
	"res://world/tool/hammer/Hammer.tscn": {
		Materials.WOOD:  0.0,
		Materials.FLESH: 0.6,
	},
	"res://world/tool/pickaxe/Pickaxe.tscn": {
		Materials.STONE: 25.0,
		Materials.FLESH: 2.5,
	},
	"res://world/tool/shovel/Shovel.tscn": {
		Materials.FLESH: 0.2,
	},
	"res://world/tool/sword/Sword.tscn": {
		Materials.FLESH: 10.0,
	},
}

const DAMAGE_DEFAULT = 0.5
const DAMAGE_MULTIPLIER_DEFAULT = 1.0

const SOUND_DEFAULT = preload("res://audio/Hit1.wav")
const SOUND_WOOD = preload("res://audio/HitWood1.wav")
const SOUND_DIRT = preload("res://audio/HitDirt.wav")
