extends Node

# Returns the rough extents of a physical body.
# Does not take into account local transforms.
func extents_no_transforms(body) -> Vector3:
	if !body:
		return Vector3(0, 0, 0)
	if body is StaticBody:
		return extents_any_body(body)
	if body is RigidBody:
		return extents_any_body(body)
	if body is KinematicBody:
		return extents_any_body(body)
	Debug.warn(self, "cannot calculate extents for %s (%s)" % [body, body.name])
	return Vector3(1.0, 1.0, 1.0)


func extents_any_body(body) -> Vector3:
	var collisions = Util.get_collisions(body)
	var v = Vector3(0.0, 0.0, 0.0)
	for c in collisions:
		var e = extents_collision(c)
		if  v.x < e.x:
			v.x = e.x
		if  v.y < e.y:
			v.y = e.y
		if  v.z < e.z:
			v.z = e.z
	return v


func extents_collision(col) -> Vector3:
	var s = col.shape
	if  s is BoxShape:
		return s.extents
	elif s is CylinderShape:
		return Vector3(s.radius, s.height * 0.5, s.radius)
	elif s is SphereShape:
		return Vector3(s.radius, s.radius, s.radius) * 2.0
	else:
		Debug.warn(self, "cannot calculate extents for collision %s (%s)" % [s, col])
		return Vector3(1.0, 1.0, 1.0)


# Returns the volume of a physical body.
func volume(body) -> float:
	if !body:
		return 0.0
	var volume = 0.0
	for child_body in Util.get_nodes_of_types(body, [KinematicBody, RigidBody, StaticBody]):
		volume += volume_any_body(child_body)
	return volume


# Returns the summed volumes of collisions for any
# RigidBody / StaticBody / KinematicBody.
func volume_any_body(body) -> float:
	var collisions = Util.get_collisions(body)
	var v = 0.0
	for c in collisions:
		v += volume_collision(c)
	return v


# Returns the volume for a single collision shape.
func volume_collision(col) -> float:
	var s = col.shape
	if s is BoxShape:
		return s.extents.x * s.extents.y * s.extents.z
	elif s is CylinderShape:
		return s.radius * s.height
	elif s is SphereShape:
		# 4.188790204786390 =~ 4/3 * PI
		return 4.188790204786390 * s.radius * s.radius * s.radius
	else:
		Debug.warn(self, "cannot calculate volume for collision %s (%s)" % [s, col])
		return 1.0
