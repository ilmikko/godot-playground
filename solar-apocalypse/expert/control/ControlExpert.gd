extends Node

# Handles the changes to controllers and the ability to control arbitrary
# objects. The BaseController is sacred and must not be destroyed.
# We will first extract the BaseController from the old_controller.
# We will attach BaseController to a ControlHandler.
# If an object does not have a ControlHandler, we must create one.


func base_controller_for(controller):
	match Scene.filepath(controller):
		"res://player/controller/base/BaseController.tscn":
			return controller
		_:
			return controller.BaseController


# Arbitrary rigidbody control.
func control_rigidbody(body, old_controller):
	Debug.print(self, "controlling %s" % body.get_path())
	var base_controller = base_controller_for(old_controller)
	Runtime.Expert.Crane.attach(body, base_controller)
	if  old_controller != base_controller:
		old_controller.queue_free()


func object(object, old_controller):
	for _try in range(10):
		if object == null: return
		if object is RigidBody && object.mode == RigidBody.MODE_RIGID:
			return control_rigidbody(object, old_controller)
		if 'Driver' in object:
			return control_rigidbody(object.Driver, old_controller)
		object = object.get_parent()
	Debug.warn(self, "does not know how to control %s" % object.get_path())
