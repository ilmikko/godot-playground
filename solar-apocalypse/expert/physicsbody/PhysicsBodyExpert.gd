extends Node
	


func make_staticbody(node):
	if node is StaticBody:
		# Node is already a StaticBody.
		return
	
	var new_body  = StaticBody.new()
	
	Util.replace_with_transform(node, new_body)
	
	if node is PhysicsBody:
		# Node is already a PhysicsBody, no need to recalc collisions.
		return new_body
	
	var new_collision = Runtime.Expert.Collision.simple_sphere(new_body)
	new_body.add_child(new_collision)
	
	return new_body

func make_rigidbody(node):
	# TODO: If node is a staticbody, use BreakageExpert logic
	var new_body
	
	if !(node is RigidBody):
		new_body = make_rigidbody_static(node)
	else:
		# Node is already a RigidBody.
		new_body = node
	
	new_body.mode = RigidBody.MODE_RIGID
	
	return new_body


func make_rigidbody_static(node):
	var new_body  = RigidBody.new()
	new_body.mode = RigidBody.MODE_STATIC
	
	var new_collision = Runtime.Expert.Collision.simple_sphere(new_body)
	new_body.add_child(new_collision)
	
	# Come up with a plausible mass for this thing.
	# TODO: Mass, not volume. Need to multiply based on material.
	new_body.mass = Runtime.Expert.Volume.volume_collision(new_collision)
	
	# Disable collisions so we can perform the body transform.
	var disabled_collisions = []
	for c in Util.get_collisions(node):
		if c.disabled: continue
		disabled_collisions.append(c)
		c.disabled = true
	
	Util.replace_with_global_transform(get_tree().root, new_body, node)
	
	# Re-enable collisions on the new body.
	for c in disabled_collisions: c.disabled = false
	
	return new_body
