extends Node

func apply_consumable_effect_to_actor(actor, item):
	var vitals_handler = Runtime.Handler.fetch(actor, Runtime.Handler.Vitals)
	
	 # TODO: Get consumable effect expert for this item instead of hardcoding
	var hunger_quench_amount = 0.1
	vitals_handler.get_node("Hunger").change_value_by(hunger_quench_amount)
	# TODO: Ensure the holder exists
	actor.HolderHandler.drop()
	Util.remove(item)
