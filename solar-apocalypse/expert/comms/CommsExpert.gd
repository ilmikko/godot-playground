extends Node


func get_dialog(body):
	var fp = Scene.filepath(body)
	match fp:
		"res://world/ship/SpaceShip.tscn":
			var answers = [
				"Greetings, Traveler.",
				"Hi there Hotshot!",
				"Good tidings to you Sir.",
				"Hello there.",
				"How's it flying?",
			]
			return answers[randi() % len(answers)]
		"res://world/station/StationA.tscn":
			var star_name = Runtime.Expert.Star.name(body.id)
			var answers = [
				"Welcome to the brilliant system of %s!" % star_name,
				"Greetings. Please fly safe.",
				"Best wares in the galaxy, right here in %s." % star_name,
				"Come visit our shop!",
				"The station of %s greets you." % star_name,
			]
			return answers[randi() % len(answers)]
		"res://world/station/StationB.tscn":
			var star_name = Runtime.Expert.Star.name(body.id)
			var answers = [
				"Welcome to the brilliant system of %s!" % star_name,
				"Greetings. Please fly safe.",
				"Best wares in the galaxy, right here in %s." % star_name,
				"Come visit our shop!",
				"The station of %s greets you." % star_name,
			]
			return answers[randi() % len(answers)]
		_:
			return "..."
