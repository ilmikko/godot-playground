extends Node


var rng = RandomNumberGenerator.new()


func for(body) -> float:
	if !body:
		return 0.0
	if 'Driver' in body:
		body = body.get_node("Driver")
	if body is RigidBody:
		return body.mass
	if body is StaticBody:
		return Runtime.Expert.Volume.for(body) * VOLUME_MASS_MULTIPLIER
	if body is KinematicBody:
		return Runtime.Expert.Volume.for(body) * VOLUME_MASS_MULTIPLIER
	Debug.warn(self, "cannot calculate mass for %s (%s)" % [body, body.name])
	return 1.0


func format(f):
	return "%skg" % f


const VOLUME_MASS_MULTIPLIER = 1.0
