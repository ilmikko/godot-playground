extends Node


func item_hint(hint, item_name, key):
	if  hint && item_name:
		hint = "%s %s" % [hint, item_name]
	if key:
		hint = "%s [%s]" % [hint, Util.key_names_for_action(key)]
	return hint


func interact_item_hint(item):
	var item_name = Runtime.Expert.ItemName.for(item)
	var hint = item.get("interact_hint")
	if  hint == null:
		hint = "Interact"
	var key  = "interact_primary"
	return item_hint(hint, item_name, key)


func control_item_hint(item):
	var item_name = Runtime.Expert.ItemName.for(item)
	var hint = item.get("control_hint")
	if  hint == null:
		hint = "Control"
	var key  = "control"
	return item_hint(hint, item_name, key)


func pick_up_item_hint(item):
	var item_name = Runtime.Expert.ItemName.for(item)
	var hint = "Pick Up"
	var key  = "interact"
	
	if item.is_in_group("equippable"):
		hint = "Equip"
	return item_hint(hint, item_name, key)


func object_pointing_at(controller, item):
	# The interact hint depends on what is pointing where.
	# For example, a freelookcamera pointing at a RigidBody cannot "Pick up"
	# the rigidbody, but a playercontroller pointing at the same thing can.
	
	Debug.text(self, "hint", "object %s pointing at item %s" % [Scene.filepath(controller), item])
	
	var item_health = Runtime.Expert.Health.for(item)
	var item_name = Runtime.Expert.ItemName.for(item)
	var item_mat = Runtime.Expert.Material.for(item)
	
	Runtime.UI.Stats.Health = item_health
	Runtime.UI.Stats.Title = item_name
	Runtime.UI.Stats.Material = item_mat
	Runtime.UI.Stats.visible = true
	
	match Scene.filepath(controller):
		"res://player/controller/freelook/FreelookController.tscn":
			# FreelookController always wants to control other things.
			return control_item_hint(item)
	
	# 1. Can we interact with it?
	if item.is_in_group("interactable"): return interact_item_hint(item)
	# 2. Can we control it?
	elif item.is_in_group("controllable"): return control_item_hint(item)
	# 3. Can we pick it up?
	elif item is RigidBody && item.mode == RigidBody.MODE_RIGID:
		return pick_up_item_hint(item)
