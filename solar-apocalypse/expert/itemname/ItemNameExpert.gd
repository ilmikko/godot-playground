extends Node


func for(item):
	var name = "Void"
	
	var fp = Scene.filepath(item)
	if fp in names:
		return names[fp]
	if fp:
		Debug.warn(self, "could not determine name for %s" % fp)
	
	return name


# TODO: Let's unite all the scrappy stats.
var names = {
	"res://actor/bee/driver/Driver.tscn": "Bee",
	"res://actor/rabbit/driver/Driver.tscn": "Rabbit",
	"res://actor/person/driver/Driver.tscn": "Person",
	"res://actor/person/skeleton/rigidbodies/arm/PersonArm.tscn": "Person",
	"res://actor/person/skeleton/rigidbodies/leg/PersonLeg.tscn": "Person",
	"res://actor/person/skeleton/rigidbodies/head/PersonHead.tscn": "Person",
	"res://actor/person/skeleton/rigidbodies/upperbody/UpperBody.tscn": "Person",
	"res://debug/visual/CheckeredGround.tscn": "Checkered Ground",
	"res://world/barrel/Barrel.tscn": "Barrel",
	"res://world/effect/gib/wood/GibWood1.tscn": "Wood",
	"res://world/effect/gib/wood/GibWood2.tscn": "Wood",
	"res://world/effect/gib/wood/GibWood3.tscn": "Wood",
	"res://world/exterior/Bucket.tscn": "Bucket",
	"res://world/exterior/ChoppingBlock.tscn": "Chopping Block",
	"res://world/foliage/gen/seed/MysteryPlantSeed.tscn": "Mystery Seed",
	"res://world/foliage/gen/seed/TangleWeedSeed.tscn": "Tangleweed Seed",
	"res://world/foliage/tree/pine/PineTree.tscn": "Pine Tree",
	"res://world/furniture/Chair.tscn": "Stool",
	"res://world/house/frame/RoofPillar2x.tscn": "Pillar",
	"res://world/house/frame/WallPillar.tscn": "Pillar",
	"res://world/house/frame/WallPillar2y.tscn": "Pillar",
	"res://world/house/parts/Window.tscn": "Window",
	"res://world/house/parts/door/Door.tscn": "Door",
	"res://world/house/parts/exterior/Wall.tscn": "Wall",
	"res://world/house/parts/exterior/Wall0.5x.tscn": "Wall",
	"res://world/house/parts/exterior/Wall2x.tscn": "Wall",
	"res://world/house/parts/exterior/WallDoor.tscn": "Wall",
	"res://world/house/parts/exterior/WallWindow.tscn": "Wall",
	"res://world/house/parts/foundation/StoneFoundation2x1.tscn": "Stone",
	"res://world/house/parts/interior/WallInteriorDivider.tscn": "Wall",
	"res://world/interior/chest/ChestRound.tscn": "Chest",
	"res://world/space_stuff/valuables/GoldNugget.tscn": "Gold Nugget",
	"res://world/space_stuff/asteroid/AsteroidFragment.tscn": "Space Rock",
	"res://world/interior/table/StudyCabinetDoor.tscn": "Drawer",
	"res://world/interior/table/StudyCabinetDrawer.tscn": "Drawer",
	"res://world/interior/table/StudyCabinetShellWithStopper.tscn": "Cabinet",
	"res://world/interior/table/StudyCabinetShell.tscn": "Cabinet",
	"res://world/kitchen/Plate.tscn": "Plate",
	"res://world/farming/DirtPile.tscn": "Dirt Pile",
	"res://world/planet_stuff/Shop.tscn": "Shop",
	"res://world/terrain/base/collision/Collision.tscn": "Ground",
	"res://world/space_stuff/asteroid/Asteroid.tscn": "Asteroid",
	"res://world/space_stuff/asteroid/Asteroid2.tscn": "Asteroid",
	"res://world/space_stuff/asteroid/Asteroid3.tscn": "Asteroid",
	"res://world/space_stuff/asteroid/Asteroid4.tscn": "Asteroid",
	"res://world/space_stuff/asteroid/AsteroidGold.tscn": "Asteroid",
	"res://world/ship/SpaceShip.tscn": "Space Ship",
	"res://world/field/Location.tscn": "Star",
	"res://world/tool/axe/Axe.tscn": "Axe",
	"res://world/tool/farmhoe/FarmHoe.tscn": "Farm Hoe",
	"res://world/tool/hammer/Hammer.tscn": "Hammer",
	"res://world/tool/pickaxe/Pickaxe.tscn": "Pickaxe",
	"res://world/tool/shovel/Shovel.tscn": "Shovel",
	"res://world/tool/sword/Sword.tscn": "Sword",
	"res://world/water/Pond.tscn": "Water",
	"res://world/workbench/Workbench.tscn": "Workbench",
	"res://world/buildingresources/Wood.tscn": "Wood resource",
	"res://world/exterior/SignPost.tscn": "SignPost",
}
