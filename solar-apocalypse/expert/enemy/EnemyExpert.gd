extends Node

# Decides who is a friend and who is a foe.
func is_enemy(me, other):
	var my_faction    = Runtime.Expert.Faction.for(me)
	var their_faction = Runtime.Expert.Faction.for(other)
	
	if their_faction == Runtime.Expert.Faction.INDEPENDENT: return false
	
	return Runtime.Expert.Faction.hates(my_faction, their_faction)
