extends Node

var candidates = {}
var scanned = {}


func scan(targeter):
	if !(targeter in candidates): return
	var t = TARGET_SCAN.instance()
	t.name = "TARGET_SCAN"
	var body = candidates[targeter]
	if  targeter in scanned && scanned[targeter] != null && is_instance_valid(scanned[targeter]):
		var tar = scanned[targeter].get_node_or_null("TARGET_SCAN")
		if  tar != null:
			tar.queue_free()
	scanned[targeter] = body
	targeter.scan(body)
	if  body == null: return
	body.add_child(t, true)


func get_scanned(targeter):
	if !(targeter in scanned): return null
	return scanned[targeter]


func set_candidate(targeter, body):
	candidates[targeter] = body


const TARGET_SCAN = preload("res://world/ui/TargetScan.tscn")
