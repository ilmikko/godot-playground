extends Spatial

export(PackedScene) var scene_packed setget _set_scene_packed

const ghost_material = preload("res://expert/ghost/GhostMaterial.tres")

var ghost

var materials_added = {}
var materials_needed
var scene_instance

func place():
	add_to_group("placed_ghost")

func add_resource(resource) -> bool:
	var resource_material = Runtime.Expert.Material.for(resource)
	if !_materials_needed().has(resource_material):
		return false

	if !materials_added.has(resource_material):
		materials_added[resource_material] = 0.0

	materials_added[resource_material] += Runtime.Expert.Volume.volume(resource)

	if _has_enough_of_each_material():
		var build = scene_packed.instance()
		get_parent().add_child(build)
		build.global_transform.origin = global_transform.origin
		queue_free()

	return true

func _has_enough_of_each_material():
	for material in _materials_needed():
		if !materials_added.has(material):
			return false
		if materials_added[material] < _materials_needed()[material]:
			return false
	return true

func _set_scene_packed(new_scene_packed):
	scene_packed = new_scene_packed
	ghost = scene_packed.instance()
	ghost.remove_from_group("root_parent")
	add_child(ghost)

	for mesh_instance in Util.get_mesh_instances(ghost):
		mesh_instance.material_override = ghost_material
	for rigid_body in Util.get_children_of_types(ghost, [RigidBody]):
		rigid_body.mode = RigidBody.MODE_STATIC
	for physics_body in Util.get_children_of_types(ghost, [PhysicsBody]):
		physics_body.set_collision_layer(Layers.id(Layers.COMMON))
		physics_body.set_collision_layer_bit(Layers.id(Layers.INTERACTION), true)

func _materials_needed():
	if materials_needed: return materials_needed

	var material = Runtime.Expert.Material.for(_scene_instance())
	var volume = Runtime.Expert.Volume.volume(_scene_instance())

	materials_needed = { material: volume }
	
	return materials_needed

func _scene_instance():
	if scene_instance: return scene_instance 
	return scene_packed.instance()
