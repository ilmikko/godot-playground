extends Spatial


func boom(body):
	if body is StaticBody: return
	Runtime.Expert.Breakage.break(body)


func _on_AreaOfImmediateDeathAndDestruction_body_entered(body):
	boom(body)
