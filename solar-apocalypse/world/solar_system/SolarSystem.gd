tool
extends Spatial


# TODO: Star spin
# TODO: Star intensity (based on brightness?)
# TODO: Star pulse
# TODO: Supernova


export var id = 0 setget _set_id
func _set_id(i):
	id = i
	_set_star_brightness(Runtime.Expert.Star.brightness(i))
	_set_star_color(Runtime.Expert.Star.color(i))
	_set_star_size(Runtime.Expert.Star.size(i))
	_set_faction(Runtime.Expert.Faction.for_id(i))
	_set_station(Runtime.Expert.Station.for_id(i))


export(float, 0.0, 1.0) var star_size = 1.0 setget _set_star_size
func _set_star_size(ss):
	star_size = ss
	rebuild_size()


export(float, 0.0, 1.0) var star_brightness = 1.0 setget _set_star_brightness
func _set_star_brightness(sb):
	star_brightness = sb
	star_color.a = sb
	rebuild_color()


export var star_color = Color(1, 1, 1) setget _set_star_color
func _set_star_color(sc):
	star_color = sc
	star_color.a = star_brightness
	rebuild_color()


export var faction = Runtime.Expert.Faction.NONE setget _set_faction
func _set_faction(f):
	faction = f
	rebuild_faction()


export(PackedScene) var station setget _set_station
func _set_station(s):
	if station == s: return
	station = s
	rebuild_station()


func rebuild_size():
	$Sun.scale = Vector3(1.0, 1.0, 1.0) + Vector3(1.0, 1.0, 1.0) * star_size
	dist_to_star_surface = (1.0 + star_size) * STAR_RADIUS
	# TODO: Randomize
	dist_to_surface_probe     = dist_to_star_surface + 50.0
	dist_to_near_star_station = dist_to_star_surface + 250.0
	dist_to_station           = dist_to_star_surface + 400.0


func rebuild_color():
	$Sun/MeshInstance.get_active_material(0).set_shader_param("albedo",   star_color)
	$Sun/MeshInstance.get_active_material(0).set_shader_param("emission", star_color)


func rebuild_faction():
	Util.remove_children($Faction)
	if faction == Runtime.Expert.Faction.PIRATE:
		$Faction.add_child(PIRATE_SHIPS.instance())


func rebuild_station():
	Util.remove_children($Station)
	# Move station outside of the sun.
	station.transform.origin = Vector3(dist_to_station, 0, 0)
	station.transform.basis = Runtime.Expert.Station.basis_for(id)
	station.id = id
	# Quick hack, TODO: Get another basis
	$Station.transform.basis = station.transform.basis
	$Station.add_child(station)


# Some helpers for creating the solar system.
var dist_to_star_surface = 0.0
var dist_to_station = 0.0
# TODO: Below are not used yet.
var dist_to_surface_probe = 0.0
var dist_to_near_star_station = 0.0
var dist_to_inner_planets = 0.0
var dist_to_outer_planets = 0.0
var dist_to_outer_rim = 0.0

const STAR_RADIUS = 50.0
const PIRATE_SHIPS = preload("res://world/space_stuff/RandomPirateSpawner.tscn")
