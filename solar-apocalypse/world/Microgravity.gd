extends Spatial


export var gravity_strength = 2.21 setget _set_gravity_strength
func _set_gravity_strength(gs):
	gravity_strength = gs
	$Area.gravity = gs


func enter_influence(body):
	if !body || !('primary_influencing_gravity' in body): return
	body.primary_influencing_gravity          = self
	body.primary_influencing_gravity_strength = gravity_strength


func exit_influence(body):
	if !body || !('primary_influencing_gravity' in body): return
	body.primary_influencing_gravity          = null
	body.primary_influencing_gravity_strength = gravity_strength


func _on_Area_body_entered(body):
	enter_influence(body)


func _on_Area_body_exited(body):
	exit_influence(body)
