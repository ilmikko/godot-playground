extends Spatial


var force = 1.0
# TODO: Push things with explosion force


func _ready():
	$Particles.one_shot = true
	$Particles2.one_shot = true
	$Particles.emitting = true
	$Particles2.emitting = true
	$AudioStreamPlayer3D.play()
	Runtime.Game.Time.after_s(1, self, "destroy")


func destroy(_t):
	queue_free()
