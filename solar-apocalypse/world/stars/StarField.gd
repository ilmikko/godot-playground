tool
extends Spatial

# TODO: Use MultiMeshInstance


export(int, 0, 1000) var star_count = 0 setget _set_star_count
func _set_star_count(sc):
	var d = sc - star_count
	if  d == 0: return
	if  d  > 0:
		add_stars(d)
	else:
		remove_stars(-d)
	
	if !(Runtime.Expert.Star.STAR_STARTING_SOLAR_SYSTEM in stars):
		var loc = STARTING_LOCATION.instance()
		add_star(Vector3(0, 0, 0), Runtime.Expert.Star.STAR_STARTING_SOLAR_SYSTEM, loc)
	
	star_count = sc


export (float, 0.0, 1000.0) var vast_scale = 1.0 setget _set_vast_scale
func _set_vast_scale(vs):
	if vast_scale == vs: return
	vast_scale = vs
	scale = Vector3(vs, vs, vs)
	for id in stars:
		stars[id].scale = Vector3(1.0, 1.0, 1.0) / vs


export var vast_offset = Vector3(0, 0, 0) setget _set_vast_offset
func _set_vast_offset(vo):
	vast_offset = vo
	$Stars.transform.origin = vo
#	for id in stars:
#		stars[id].star_distance = (vo-stars[id].transform.origin).length()


var stars = {}
var star_districts = {}
var occupied_districts = {}
var divisor = 64.0


func random_district(r : RandomNumberGenerator) -> Vector3:
	# TODO: This is a cube.
	var x = (r.randi() % 32) - 16
	var y = (r.randi() % 32) - 16
	var z = (r.randi() % 32) - 16
	return Vector3(x, y, z)


func add_star(district : Vector3, id : int, loc):
	if  id in stars: return
	if  district in occupied_districts: return
	occupied_districts[district] = id
	loc.id = id
	
	var pos = district + Vector3(0.5, 0.5, 0.5)
	
	loc.transform.origin = pos / divisor
	loc.distance         = (vast_offset-pos).length() / vast_scale
	loc.district = district
	
	stars[id] = loc
	star_districts[id] = district
	$Stars.add_child(loc)
	Runtime.Expert.District.add(district, loc, id)


func add_stars(n):
	var r = RandomNumberGenerator.new()
	
	for i in range(n):
		var id = star_count + i
		r.seed = id
		var loc = LOCATION.instance()
		add_star(random_district(r), id, loc)


func remove_stars(n):
	for i in range(n):
		var id = star_count - i - 1
		var star = stars[id]
		stars.erase(id)
		$Stars.remove_child(star)
		Runtime.Expert.District.remove(star_districts[id])
		star.queue_free()


const LOCATION = preload("res://world/field/Location.tscn")
const STARTING_LOCATION = preload("res://world/field/StartingLocation.tscn")
