extends Spatial


var BULLET_SPEED = 1.0


export(float, 0.01, 1.0) var rate_of_fire = 0.5

export(PackedScene) var bullet_scene


var firing = false setget _set_firing
func _set_firing(f):
	if f:
		_on_at_least_0_1_seconds_passed(0)
		if not Runtime.Game.Time.is_connected("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed"):
			Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")
	else:
		if Runtime.Game.Time.is_connected("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed"):
			Runtime.Game.Time.disconnect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func pewpew(_t):
	var bullet = bullet_scene.instance()
	bullet.add_collision_exception_with(Scene.parent_body(self))
	bullet.transform.basis    = $BulletSpawn.global_transform.basis
	bullet.transform.origin   = $BulletSpawn.global_transform.origin
	bullet.impulse_after_wake = $BulletSpawn.global_transform.basis.z * BULLET_SPEED
	var tree = get_tree()
	if  tree != null: tree.root.add_child(bullet)
	$AudioStreamPlayer3D.play()


func _on_at_least_0_1_seconds_passed(_t):
	Runtime.Game.Time.rate_limit_s(rate_of_fire, self, "pewpew")
