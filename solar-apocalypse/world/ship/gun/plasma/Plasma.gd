extends RigidBody


var impulse_after_wake = Vector3(0, 0, 0)
var damage = 75.0


func _ready():
	Runtime.Game.Time.after_s(1.0,  self, "die")
	Runtime.Game.Time.after_s(0.1, self, "enable_collision")
	call_deferred("deferred")


func collide(body):
	Runtime.Expert.Explosion.explode(self)
	if body is RigidBody:
		body.apply_central_impulse((self.global_transform.origin - body.global_transform.origin) * 0.1)
	print(Runtime.Expert.Health.lose(body, damage))


func deferred():
	mode = RigidBody.MODE_RIGID
	apply_central_impulse(impulse_after_wake)


func die(_t):
	queue_free()


func enable_collision(_t):
	$CollisionShape.disabled = false


func _on_Bullet_body_entered(body):
	collide(body)
