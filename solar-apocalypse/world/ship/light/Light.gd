extends Spatial


func _enter_tree():
	Runtime.Game.Time.connect("at_least_1_0_seconds_passed", self, "_on_at_least_1_0_seconds_passed")


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_1_0_seconds_passed", self, "_on_at_least_1_0_seconds_passed")


func _on_at_least_1_0_seconds_passed(_t):
	$MeshInstance.visible = !$MeshInstance.visible
	$Bloom.visible        = !$Bloom.visible
