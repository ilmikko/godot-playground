extends Spatial

signal detect(body)
signal lose(body)


func detect(body):
	emit_signal("detect", body)


func lose(body):
	emit_signal("lose", body)


func _on_Area_body_entered(body):
	detect(body)


func _on_Area_body_exited(body):
	lose(body)
