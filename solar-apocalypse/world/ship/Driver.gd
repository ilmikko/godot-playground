extends RigidBody


var id = 0
var performance = null

func performance():
	if performance != null: return performance

	performance = Runtime.Handler.fetch(self, Runtime.Handler.DriverPerformance)
	return performance
	
var intent_fire = false setget _set_intent_fire
func _set_intent_fire(ff):
	intent_fire = ff
	$Gun.firing = ff
	$Gun2.firing = ff
	
var intent_fire_secondary = false setget _set_intent_fire_secondary
func _set_intent_fire_secondary(fs):
	intent_fire = fs
	$MissileGun.targeter = $Crane.BaseController.PlayerCamera.Targeter
	$MissileGun.firing = fs


var intent_scan = false setget _set_intent_scan
func _set_intent_scan(ii):
	if intent_scan == ii: return
	intent_scan = ii
	if ii:
		Runtime.Expert.TargetScan.scan($Crane.BaseController.PlayerCamera.Targeter)


var intent_comms = false setget _set_intent_comms
func _set_intent_comms(ic):
	if intent_comms == ic: return
	intent_comms = ic
	if ic:
		Runtime.Expert.TargetComms.comms(self, $Crane.BaseController.PlayerCamera.Targeter)


var intent_jump_drive = false setget _set_intent_jump_drive
func _set_intent_jump_drive(ij):
	if intent_jump_drive == ij: return

	intent_jump_drive = ij
	var time = Runtime.Game.Time
	var event = 'at_least_0_0_5_seconds_passed'
	var method = '_jump_drive_turn_to_body'
	if intent_jump_drive:
		time.connect(event, self, method)
	elif time.is_connected(event, self, method):
		time.disconnect(event, self, method)

var intent_move = Vector2(0, 0) setget _set_intent_move
func _set_intent_move(im):
	intent_move = im
	
	if abs(intent_move.y) > 0.0 and !$AudioStreamPlayer3D.playing:
		$AudioStreamPlayer3D.playing = true
	$Particles.forwards = intent_move.y


var intent_rotate = Vector2(0, 0) setget _set_intent_rotate
func _set_intent_rotate(ir):
	intent_rotate = ir
	crane_rotate += ir

var intent_roll = 0 setget _set_intent_roll
func _set_intent_roll(ir):
	intent_roll = ir
	
	$Particles.roll_cw  = ir < 0
	$Particles.roll_ccw = ir > 0


# Points to a body where gravity is strongest.
# Updated by microgravity.gd.
var primary_influencing_gravity          = null
var primary_influencing_gravity_strength = 1.0


# How much rotation the actor wants to apply.
var angular_intent = Vector3(0, 0, 0)
# How much velocity the actor wants to apply.
var linear_intent = Vector3(0, 0, 0)

# Purely a visual cue; move the crane a little to let the player know which
# way they are rotating.
var crane_rotate = Vector2(0, 0)


func _integrate_forces(state):
	var basis = global_transform.basis
	_mouse_rotate_decay()
	_mouse_rotate_crane_movement()
	_integrate_forces_angular(basis, state)
	_integrate_forces_angular_damping(state)
	_integrate_forces_linear(basis, state)
	_integrate_forces_linear_damping(state)


func _integrate_forces_angular(basis, state):
	state.apply_torque_impulse(basis.x * -intent_rotate.x * performance().angular_ability)
	state.apply_torque_impulse(basis.y * -intent_rotate.y * performance().angular_ability * 2.0)
	state.apply_torque_impulse(basis.z * intent_roll * performance().angular_ability_roll)


func _integrate_forces_angular_damping(state):
	state.apply_torque_impulse(-state.angular_velocity * performance().angular_damping)


func _integrate_forces_linear(basis, state):
	if linear_intent.length() > performance().linear_ability:
		linear_intent = linear_intent.normalized() * performance().linear_ability
	
	var jump_speed_multiplier = 1
	if intent_jump_drive && _is_close_to_target_basis():
		intent_move.y = -1
		jump_speed_multiplier = 100
	$Particles.vel_angular = state.angular_velocity
	$Particles.vel_linear  = state.linear_velocity
	
	state.apply_central_impulse(
		basis.z * intent_move.y * performance().fly_speed * jump_speed_multiplier
	)
	state.apply_central_impulse(basis.x * intent_move.x * jump_speed_multiplier)

func _is_close_to_target_basis():
	var target_body = _jump_drive_target_body().global_transform
	var basis = global_transform.basis
	var target_basis = global_transform.looking_at(target_body.origin, basis.y).basis

	return (
		basis.tdoty(target_basis.y) > 0.98 &&
		basis.tdoty(target_basis.y) > 0.98 &&
		basis.tdoty(target_basis.y) > 0.98
	)
	

func _jump_drive_turn_to_body(delta):
	if !intent_jump_drive: return

	var body = _jump_drive_target_body()
	if body == null: return

	var dtf = global_transform
	var size = 4096
	# TODO: Only do this if it is a star
	# TODO: Extract these stuff to the DistrictExpert
	var btf_origin = body.district * size + Vector3(size, size, size) * 0.5
	var ttf = dtf.looking_at(btf_origin, dtf.basis.y)
	global_transform.basis = dtf.basis.slerp(ttf.basis, delta)

func _jump_drive_target_body():
	var targeter = $Crane.BaseController.PlayerCamera.Targeter
	return Runtime.Expert.TargetScan.get_scanned(targeter)

func _integrate_forces_linear_damping(state):
	state.apply_central_impulse(-state.linear_velocity * performance().linear_damping)


func _mouse_rotate_decay():
	intent_rotate *= performance().intent_rotate_decay_mult
	crane_rotate  *= performance().crane_rotate_decay_mult


func _mouse_rotate_crane_movement():
	$Crane.transform.origin = Vector3(crane_rotate.y, -crane_rotate.x, 0) * 0.005
