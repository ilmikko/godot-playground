extends RigidBody


# Which targeter to follow.
var targeter = null


func _ready():
	Runtime.Game.Time.after_s(1.0, self, "enable_collision")


func turn_to_face():
	var body = Runtime.Expert.TargetScan.get_scanned(targeter)
	if  body == null or !is_instance_valid(body): return
	var ttf = Transform().looking_at((body.global_transform.origin - global_transform.origin), global_transform.basis.y)
	global_transform.basis = ttf.basis


func _integrate_forces(state):
	state.apply_central_impulse(-global_transform.basis.z * 1.0)


func collide(body):
	Runtime.Expert.Explosion.explode(self)
	Runtime.Expert.Breakage.break(body)


func enable_collision(_t):
	$CollisionShape.disabled = false


func _on_Missile_body_entered(body):
	collide(body)


func _on_Timer_timeout():
	turn_to_face()
