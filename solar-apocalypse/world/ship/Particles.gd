extends Spatial

onready var mat_roll   = $RollCCWParticles.process_material
onready var mat_thrust = $ThrusterParticles.process_material

var roll_cw = false setget _set_roll_cw
func _set_roll_cw(rc):
	roll_cw = rc
	$RollCWParticles.emitting = rc


var roll_ccw = false setget _set_roll_ccw
func _set_roll_ccw(rc):
	roll_ccw = rc
	$RollCCWParticles.emitting = rc


var forwards = 0 setget _set_forwards
func _set_forwards(f):
	forwards = f
	$ThrusterParticles.emitting  = f < 0


var vel_angular = Vector3(0, 0, 0)
var vel_linear = Vector3(0, 0, 0) setget _set_vel_linear
func _set_vel_linear(vl):
	vel_linear = vl
	
	mat_roll.direction   = -Vector3(vl.y, vl.x, vl.z) * 0.03
	mat_thrust.direction = -Vector3(vl.y, vl.x, vl.z) * 0.01
