tool
extends Spatial


export(float, 0.0, 500.0) var radius = 1.0 setget _set_radius
func _set_radius(r):
	if radius == r: return
	radius = r
	rebuild()


export(int, 0, 10000) var count = 0 setget _set_count
func _set_count(c):
	if count == c: return
	count = c
	rebuild()


export(float, 0, 100) var size = 1.0 setget _set_size
func _set_size(s):
	if size == s: return
	size = s
	rebuild()


export(PackedScene) var instance_packed setget _set_instance_packed
func _set_instance_packed(ip):
	instance_packed = ip
	rebuild()


func rebuild():
	Util.remove_children(self)
	
	if instance_packed == null:
		Debug.warn(self, "instance_packed is null")
		return
	
	var rand = RandomNumberGenerator.new()
	rand.seed = 123
	
	var up = Vector3.UP
	
	# Generate points on a sphere.
	for i in range(count):
		var y = (i / float(count)) * 2.0 - 1.0
		var angle = PHI * i

		var r = sqrt(1.0 - y*y)

		var x = cos(angle) * r
		var z = sin(angle) * r
		
		var pos    = Vector3(x, y, z)
		
		var vy = pos.normalized()
		var vx = up.cross(vy).normalized()
		var vz = vy.cross(vx).normalized()
		
		var basis  = Basis(vx * size, vy * size, vz * size)
		var origin = pos * radius
		
		var rock = instance_packed.instance()
		rock.transform.origin = origin
		rock.transform.basis  = basis
		add_child(rock)


# Golden angle in radians
const PHI = 2.399963229728653
