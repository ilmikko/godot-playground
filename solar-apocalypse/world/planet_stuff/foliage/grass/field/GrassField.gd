tool
extends MultiMeshInstance

const OUTSIDE = 10000.0

export(Vector2) var field_size = Vector2(1.0, 1.0) setget _set_field_size
func _set_field_size(_field_size):
	field_size = _field_size
	rebuild()

export(int) var count = 1000 setget _set_count
func _set_count(_count):
	count = _count
	rebuild()

export(Vector2) var width_min_max = Vector2(0.01, 0.02) setget _set_width_min_max
func _set_width_min_max(_width_min_max):
	width_min_max = _width_min_max
	rebuild()

export(Vector2) var height_min_max = Vector2(0.04, 0.08) setget _set_height_min_max
func _set_height_min_max(_height_min_max):
	height_min_max = _height_min_max
	rebuild()

export(float) var bucket_partitions = 100.0 setget _set_bucket_partitions
func _set_bucket_partitions(_bucket_partitions):
	bucket_partitions = _bucket_partitions
	rebuild()

export(float) var bucket_partitions_visible = 25.0 setget _set_bucket_partitions_visible
func _set_bucket_partitions_visible(_bucket_partitions_visible):
	bucket_partitions_visible = _bucket_partitions_visible
	rebuild()

export(bool) var auto_rebuild = true setget _set_auto_rebuild
func _set_auto_rebuild(_auto_rebuild):
	auto_rebuild = _auto_rebuild
	rebuild()

# TODO: GrassField could also partition the geometry for different view
# distances and density. This would create a better illusion of depth. E.g.
# ########## #### #### ### ### ### ### # ## #  #   #   #    #       #      #

var bucket = {}
var scaling
var heightmap
var previous_pos = Geo.Vector3(0)

func bucket_add(bucket_id, index):
	if !bucket.has(bucket_id):
		bucket[bucket_id] = []
	if !bucket[bucket_id].has(index):
		bucket[bucket_id].append(index)


func bucket_or_empty(bucket_id):
	if !bucket.has(bucket_id):
		return []
	return bucket[bucket_id]


func bucket_id_to_tiling(id):
	var y = floor(id / bucket_partitions)
	var x =  fmod(id,  bucket_partitions)
	return Vector2(x, y)


func bucket_origin(bucket_id):
	var coord = bucket_id_to_tiling(bucket_id) / bucket_partitions * 2.0 - Geo.Vector2(1.0)
	return Geo.Vectorx0y(coord * field_size)


func bucket_rebuild(current_coord, previous_coord):
	var previous_observer_bucket = world_coordinates_to_bucket_id(previous_coord)
	var  current_observer_bucket = world_coordinates_to_bucket_id(current_coord)
	
	# Assumption: if the individual bucket of the observer has not changed,
	# there will be no diff in the rest of the buckets.
	if current_observer_bucket == previous_observer_bucket:
		return
	
	Debug.text(self, "Current Observer Bucket",  current_observer_bucket)
	Debug.text(self, "Previous Observer Bucket", previous_observer_bucket)
	
	# Assumption: The algorithm to generate a diff between the two bucket sets
	# will always have the same number of buckets removed and added.
	var previous_visible = buckets_visible_around_center(previous_coord)
	var  current_visible = buckets_visible_around_center(current_coord)
	
	if previous_visible.size() - current_visible.size() != 0:
		push_warning("WARN: GrassField bucket diff was not 0; this might be a bug in grass calc.")
		return;
	
	var removals = []
	var additions = []
	
	for id in previous_visible:
		# No-op, remains visible.
		if current_visible.has(id):
			continue
		removals.append(id)
	
	for id in current_visible:
		# No-op, is already visible.
		if previous_visible.has(id):
			continue
		additions.append(id)
	
	if removals.size() - additions.size() != 0:
		push_warning("WARN: GrassField add/removal diff was not 0; this might be a bug in grass calc.")
		return;
	
	var diff_size = removals.size()
	Debug.text(self, "GRASS DIFF", diff_size)
	if diff_size == 0:
		return
	
	Debug.text(self, "REM", "Removal %s has %s grass blades" % [removals[0], bucket_or_empty(removals[0]).size()])
	for i in (diff_size):
		bucket_switch(removals[i], additions[i])


func bucket_rebuild_geom(bucket_id, bucket_geom):
	var bucket_origin = bucket_origin(bucket_id)
	var geom_origin = Geo.Vectorxz(global_transform.origin)
	
	var rand = RandomNumberGenerator.new()
	rand.seed = Hash.hash(bucket_origin)
	
	for index in bucket_geom:
		bucket_add(bucket_id, index)
		
		# Randomize rotation
		var basis = Basis(Vector3.UP, rand.randf()*TAU)
		# Randomize position within bucket
		var offset = Geo.RandnVector2(rand) * 2.0 * field_size / bucket_partitions
		var origin = bucket_origin + Geo.Vectorx0y(offset)
		
		# Get the Y coordinate from heightmap if it's defined.
		if heightmap:
			var h = heightmap.height(geom_origin, Geo.Vectorxz(origin))
			origin += Geo.Vector3y0(h)
		
		multimesh.set_instance_transform(index, Transform(basis, origin))


func bucket_remove(bucket_id, index):
	if !bucket.has(bucket_id):
		bucket[bucket_id] = []
	bucket[bucket_id].remove(index);


func bucket_switch(old, new):
	# No-op switch.
	if old == new:
		return;
	
	bucket_rebuild_geom(new, bucket_or_empty(old))
	
	bucket[old] = []


func bucket_tiling_to_id(coord):
	return floor(coord.x) + floor(coord.y) * bucket_partitions


func buckets_visible_around_center(bucket_center):
	var bucket_id_set = {}
	var bucket_id = world_coordinates_to_bucket_id(bucket_center)
	var bucket_tile = bucket_id_to_tiling(bucket_id)
	
	for x in (bucket_partitions_visible):
		for y in (bucket_partitions_visible):
			var offset = Vector2(x, y) - Geo.Vector2(floor(bucket_partitions_visible/2.0))
			var new_tile = bucket_tile + offset
			
			# In order to hide errors with modulo (IDs going out of the initial tiling)
			# we will send the geometry somewhere off-screen. This should not matter
			# because we have a shader for calculating the size of the geometry, and
			# the meshes will be scaled to 0 in the distance. They will stay in memory
			# so that we can get them back when we need them.
			# I experimented with removing the geometry / leaving it at place, but
			# this creates problems with the diffing mechanism that assumes the same
			# amount of buckets are removed / added every change.
			if new_tile.x < 0.0 || new_tile.y < 0.0 || new_tile.x > bucket_partitions || new_tile.y > bucket_partitions:
				new_tile += field_size * OUTSIDE
			
			var id = bucket_tiling_to_id(new_tile)
			bucket_id_set[id] = true
	
	return bucket_id_set


func world_to_bucket_tiling(coord):
	return (coord/field_size * 0.5 + Geo.Vector2(0.5)) * bucket_partitions


func world_coordinates_to_bucket_id(coord):
	coord /= Geo.Vectorxz(scaling)
	return bucket_tiling_to_id(world_to_bucket_tiling(coord))


func observer_update(pos):
	scaling = global_transform.basis.get_scale()
	material_override.set_shader_param("observer_position", pos)
	bucket_rebuild(Geo.Vectorxz(pos), Geo.Vectorxz(previous_pos))
	previous_pos = pos


func rebuild():
	if !is_inside_tree():
		return
	
	if !multimesh:
		multimesh = VolatileMultiMesh.new()
		# Set format for multimesh data.
		multimesh.transform_format = MultiMesh.TRANSFORM_3D
		multimesh.set_custom_data_format(MultiMesh.CUSTOM_DATA_FLOAT)
		multimesh.set_color_format(MultiMesh.COLOR_NONE)
	
	var rand = RandomNumberGenerator.new()
	rand.seed = Hash.hash(global_transform)
	
	scaling = global_transform.basis.get_scale()
	material_override.set_shader_param("observer_position", Geo.Vector3(0))
	material_override.set_shader_param("view_size", 1.0 - bucket_partitions_visible/bucket_partitions)
	material_override.set_shader_param("scale_mod", Geo.Vector3(1.0) / scaling)
	
	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh = MeshFactory.grass_blade()
	multimesh.instance_count = count

	var start = buckets_visible_around_center(Geo.Vector2(0)).keys()
	var bucket_count = start.size()

	for index in (multimesh.instance_count):
		# We only want to distribute the geometry to buckets that are visible.
		var bucket_start_id = floor(index / float(multimesh.instance_count) * bucket_count)
		var bucket_id = start[bucket_start_id]
		bucket_add(bucket_id, index)
		multimesh.set_instance_custom_data(index, Color(
			width_min_max.x +rand.randf()*(width_min_max.y -width_min_max.x),
			height_min_max.x+rand.randf()*(height_min_max.y-height_min_max.x),
			0, # Unused channel
			0  # Unused channel
		))
	
	# Display a sample set of the grass at the start.
	for bucket_id in start:
		bucket_rebuild_geom(bucket_id, bucket[bucket_id])


func rebuild_auto():
	if !auto_rebuild:
		return
	rebuild()


func _init():
	rebuild_auto()


func _ready():
	rebuild_auto()
