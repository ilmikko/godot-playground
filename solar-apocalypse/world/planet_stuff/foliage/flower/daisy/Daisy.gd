tool
extends Spatial

export(float, 0.0, 0.2) var radius = 0.0 setget _set_radius
func _set_radius(_radius):
	radius = _radius
	rebuild()

export(float, 0.0, 180.0) var angle_top_leaf = 90.0 setget _set_angle_top_leaf
func _set_angle_top_leaf(_angle_top_leaf):
	angle_top_leaf = _angle_top_leaf
	rebuild()

export(float, 0.0, 1.0) var angle_variation = 0.1 setget _set_angle_variation
func _set_angle_variation(_angle_variation):
	angle_variation = _angle_variation
	rebuild()

export(float, 0.0, 100.0) var count = 10.0 setget _set_count
func _set_count(_count):
	count = _count
	rebuild()

export(Vector2) var width_min_max = Vector2(0.01, 0.02) setget _set_width_min_max
func _set_width_min_max(_width_min_max):
	width_min_max = _width_min_max
	rebuild()

export(Vector2) var height_min_max = Vector2(0.04, 0.08) setget _set_height_min_max
func _set_height_min_max(_height_min_max):
	height_min_max = _height_min_max
	rebuild()


var multimesh


func rebuild():
	if !is_inside_tree():
		return
	
	var rand = RandomNumberGenerator.new()
	rand.seed = Hash.hash(global_transform)
	
	if !multimesh:
		$Top/MultiMeshInstance.multimesh = VolatileMultiMesh.new()
		multimesh = $Top/MultiMeshInstance.multimesh
		# Set format for multimesh data.
		multimesh.transform_format = MultiMesh.TRANSFORM_3D
		multimesh.set_custom_data_format(MultiMesh.CUSTOM_DATA_FLOAT)
		multimesh.set_color_format(MultiMesh.COLOR_NONE)

	# Clear any previous data.
	multimesh.instance_count = 0
	multimesh.mesh = MeshFactory.grass_blade()
	multimesh.instance_count = count
	
	var angle_top_leaf_rad = angle_top_leaf * TAU/360

	for index in (multimesh.instance_count):
		var p = index/float(multimesh.instance_count)
		var angle = p*TAU
		var basis = Basis(Vector3.RIGHT, angle_top_leaf_rad).rotated(Vector3.UP, angle)
		basis = basis.rotated(Vector3.RIGHT,   (2.0*rand.randf()-1.0)*TAU*angle_variation)
		basis = basis.rotated(Vector3.FORWARD, (2.0*rand.randf()-1.0)*TAU*angle_variation)
		var origin = Vector3(sin(angle)*radius, 0, cos(angle)*radius)
		multimesh.set_instance_transform(index, Transform(basis, origin))
		multimesh.set_instance_custom_data(index, Color(
			rand.randf()*(width_min_max.y-width_min_max.x) + width_min_max.x,
			rand.randf()*(height_min_max.y-height_min_max.x) + height_min_max.x,
			0, # Unused channel
			0  # Unused channel
		))

func _ready():
	rebuild()

func _init():
	rebuild()
