extends StaticBody

const InventoryGenerator = preload("res://script/class/InventoryGenerator.gd")
const Inventory = preload("res://script/class/Inventory.gd")


export(Script) var inventory = InventoryGenerator.new().generate()


func _on_InteractionArea_body_entered(body):
	if body != Runtime.GameScene.Game.BaseController.linked_controller: return

	var shop = Runtime.UI.Shop
	shop.show_inventory(inventory)
	shop.open()
