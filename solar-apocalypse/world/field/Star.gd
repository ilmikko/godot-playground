tool
extends Spatial


var id = 0 setget _set_id
func _set_id(i):
	_set_star_brightness(Runtime.Expert.Star.brightness(i))
	_set_star_color(Runtime.Expert.Star.color(i))
	_set_star_size(Runtime.Expert.Star.size(i))
	id = i


var star_name       = ""


export(float, 0.0, 1.0) var star_size = 1.0 setget _set_star_size
func _set_star_size(ss):
	star_size = ss
	rebuild_size()


export(float, 0.0, 10.0) var star_brightness = 1.0 setget _set_star_brightness
func _set_star_brightness(sb):
	star_brightness = sb
	star_color.a = sb
	rebuild_color()


export var star_color = Color(1, 1, 1) setget _set_star_color
func _set_star_color(sc):
	star_color = sc
	rebuild_color()


var star_distance   = 1.0 setget _set_star_distance
func _set_star_distance(sd):
	star_distance = sd
	rebuild_color()


func rebuild_size():
	$Glow.scale         = Vector3(1.0, 1.0, 1.0) + Vector3(1.0, 1.0, 1.0) * star_size
	var sb = 0.5 * star_brightness / Runtime.Expert.Star.MAX_BRIGHTNESS
	$MeshInstance.scale = Vector3(2.0, 2.0, 2.0) + Vector3(10.0, 10.0, 10.0) * sb * sb * sb


func rebuild_color():
	if !is_inside_tree(): return
	star_color.a = star_brightness / Runtime.Expert.Star.MAX_BRIGHTNESS
	$MeshInstance.get_active_material(0).albedo_color = star_color
	$Glow.get_active_material(0).albedo_color         = Color(1.0, 1.0, 1.0)
	var d = max(0.0, 1.0 - star_distance)
	var bloom_color = Color(star_color.r, star_color.g, star_color.b, star_brightness * 0.005 * d)
	$Bloom.get_active_material(0).albedo_color        = bloom_color


func _enter_tree():
	rebuild_size()
	rebuild_color()
