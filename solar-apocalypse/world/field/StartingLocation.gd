tool
extends Area


var district

var id setget _set_id
func _set_id(i):
	id = i
	var star = STAR.instance()
	star.id = i
	$Location.add_child(star)


var distance setget _set_distance
func _set_distance(d):
	distance = d


func instance():
	return STARTING_SCENE.instance()


const STAR = preload("res://world/field/Star.tscn")
const STARTING_SCENE = preload("res://world/starting_solar_system/StartingSolarSystem.tscn")
