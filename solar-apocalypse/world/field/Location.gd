tool
extends StaticBody

var district

var id setget _set_id
func _set_id(i):
	id = i
	# The ID determines the type of this Location.
	# It could be a star, or a black hole.
	if id % 100 == 0:
		var blackhole = BLACK_HOLE.instance()
		blackhole.id = i
		$Location.add_child(blackhole)
	else:
		var star = STAR.instance()
		star.id = i
		$Location.add_child(star)


var distance setget _set_distance
func _set_distance(d):
	distance = d


func instance():
	if id % 100 == 0:
		return BLACK_HOLE_SCENE.instance()
	else:
		return SOLAR_SYSTEM_SCENE.instance()


const STAR = preload("res://world/field/Star.tscn")
const BLACK_HOLE = preload("res://world/field/BlackHole.tscn")
const BLACK_HOLE_SCENE = preload("res://world/BlackHole.tscn")
const SOLAR_SYSTEM_SCENE = preload("res://world/solar_system/SolarSystem.tscn")
