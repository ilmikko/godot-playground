extends Spatial


var tiered_detail_pos  = Vector3(0, 0, 0)
var tiered_detail_size = 1.0
var max_asteroid_count = 25


func _enter_tree():
	var r = RandomNumberGenerator.new()
	r.seed = Hash.hash_vec3(tiered_detail_pos)
	var asteroid_count = r.randi() % max_asteroid_count
	for _i in range(asteroid_count):
		var a = ASTEROID.instance()
		a.transform.origin = Geo.RandnVector3(r) * tiered_detail_size * 0.5
		a.transform.basis  = Basis(Geo.RandVector3(r).normalized())
		add_child(a)


const ASTEROID = preload("res://world/space_stuff/asteroid/Asteroid.tscn")
