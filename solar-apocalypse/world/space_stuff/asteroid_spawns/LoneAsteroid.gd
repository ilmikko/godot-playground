extends Spatial


var tiered_detail_pos  = Vector3(0, 0, 0)
var tiered_detail_size = 1.0


func _enter_tree():
	var r = RandomNumberGenerator.new()
	r.seed = Hash.hash_vec3(tiered_detail_pos)
	$Asteroid.transform.origin = Geo.RandnVector3(r) * tiered_detail_size * 0.5
	$Asteroid.transform.basis  = Basis(Geo.RandVector3(r).normalized())
