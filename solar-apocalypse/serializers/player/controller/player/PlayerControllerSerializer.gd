extends "res://serializers/node/NodeSerializer.gd"

var driver

func _init(node).(node):
	driver = node.get_node("PlayerPerson").Driver

func serialize():
	var data = .serialize()

	data["transform_origin"] = _origin_array()
	data["transform_basis"] = _basis_array()

	return data

func deserialize(data):
	var origin = array_to_vector3(data["transform_origin"])
	driver.transform.origin = origin

	var basis = array_to_basis(data["transform_basis"])
	driver.intent_look_basis = basis


func _origin_array():
	var origin = driver.transform.origin
	return vector3_to_array(origin)


func _basis_array():
	var basis = driver.intent_look_basis
	return basis_to_array(basis)
