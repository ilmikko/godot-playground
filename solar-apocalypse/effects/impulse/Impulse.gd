tool
extends Node


export(Vector3) var impulse = Vector3(0, 0, 0)


func _ready():
	get_parent().get_node("RigidBody").apply_central_impulse(impulse)
