extends Spatial


const InventoryGenerator = preload("res://script/class/InventoryGenerator.gd")
const GoldNugget = preload("res://world/space_stuff/valuables/GoldNugget.tscn")


func _ready():
	for i in range(0, 10):
		Runtime.Game.Inventory.add_item(GoldNugget)
	print("Items in inventory: ", Runtime.Game.Inventory.items_by_key["gold_nugget"].quantity)
