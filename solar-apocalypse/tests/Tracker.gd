tool
extends Spatial


export(NodePath) var target_path
var target = null


func _process(_delta):
	if target == null && target_path:
		target = get_node(target_path)
	
	var target_pos = target.global_transform.origin
	var self_pos   = global_transform.origin
	# global_transform.origin = target_pos / 20
	
	var d = self_pos - target_pos
	if  d.length() == 0:
		target_pos += Vector3(0.0000001, 0, 0)
	
	var up = Vector3.UP
	
	var vz = -target_pos.normalized()
	var vx = up.cross(vz).normalized()
	var vy = vz.cross(vx).normalized()
	
	transform.basis = Basis(vx, vy, vz)
