tool
extends Spatial

# Handles changes in the environment when moving over vast distances.
export(Environment) var environment = Environment.new()

var player_camera


export var nebula_color = Color(1, 0, 1) setget _set_nebula_color
func _set_nebula_color(nc):
	nebula_color = nc
	_set_background_color()
	_set_environment()
	_set_fog()


export(float, 0.0, 1.0) var nebula_amount = 0.5 setget _set_nebula_amount
func _set_nebula_amount(na):
	nebula_amount = na
	_set_background_color()
	_set_environment()
	_set_fog()
	environment.background_energy = na * nebula_energy_intensity


export(float, 0.0, 1.0) var space_density = 0.5 setget _set_space_density
func _set_space_density(sd):
	space_density = sd
	_set_fog()
	_set_particle_density()


func _set_background_color():
	environment.background_color  = nebula_color * nebula_amount * nebula_intensity


func _set_fog():
	environment.fog_enabled = true
	environment.fog_color = nebula_color * nebula_amount * Color(nebula_intensity, nebula_intensity, nebula_intensity, 1.0)
	var depth_begin = (1.0 - space_density) * depth_max_begin
	environment.fog_depth_begin = depth_begin
	environment.fog_depth_end = depth_begin + (1.0 - space_density) * depth_max_end + depth_minimum


func _set_environment():
	if !is_inside_tree(): return
	$WorldEnvironment.environment = environment


func _set_particle_density():
	if !player_camera:
		player_camera = Runtime.Expert.Camera.get_active_player_camera()
	if !player_camera: return
	


var nebula_intensity = 0.5
var nebula_energy_intensity = 2.5
var depth_minimum = 10.0
var depth_max_begin = 25.0
var depth_max_end = 50.0


func init():
	_set_environment()
