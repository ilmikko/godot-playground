extends Spatial

export(NodePath) var interaction_notifier_path

export(PackedScene) var build_packed setget _set_build_packed

var NodeMatcher = preload("res://script/class/NodeMatcher.gd")

var ghost
var pointer = null
var ghost_is_child = false


func remove_ghost():
	if !ghost_is_child: return
	ghost_is_child = false
	remove_child(ghost)



func interaction_triggered(actor, object, interaction_type):
	var holder_handler = get_node("../HolderHandler")
	var held_object = holder_handler.held_obj
	if object.add_resource(held_object):
		holder_handler.drop()
		held_object.queue_free()


func _on_Pointer_pointing_at(point):
	if ghost_is_child == false: add_child(ghost)
	ghost_is_child = true

	if ghost:
		ghost.global_transform.origin = point


func _on_Pointer_pointing_nowhere():
	remove_ghost()


func _set_build_packed(new_build_packed):
	build_packed = new_build_packed
	if ghost:
		remove_child(ghost)
	ghost = Runtime.Expert.Ghost.convert_to_ghost(build_packed)
	add_child(ghost)


func _input(event):
	if Input.is_action_just_pressed("build") and ghost:
		var ghost_global_transform = ghost.global_transform
		remove_child(ghost)
		get_tree().get_root().add_child(ghost)
		ghost.global_transform = ghost_global_transform
		ghost.place()
		ghost = null
