extends Spatial

# The maximum amount of angular force this character can emit.
export(float, 0, 100) var angular_ability = 0.04
export(float, 0, 100) var angular_ability_roll = 0.10

# Base angular damping amount.
export(float, 0, 100) var angular_damping = 0.01

# The maximum amount of linear force this character can emit.
export(float, 0, 100) var linear_ability = 0.2

# Base linear damping amount.
export(float, 0, 100) var linear_damping = 0.02

export(float, 0, 100) var fly_speed = 1.0

export(float, 0, 100) var keep_upright_intensity = 0.02

# The multiplier which will be used each frame to calculate the mouse control
# in the absence of mouse input.
export(float, 0, 100) var intent_rotate_decay_mult = 0.5
export(float, 0, 100) var crane_rotate_decay_mult = 0.4

var enabled_extensions = []

func add_extension(extension):
	enabled_extensions.append(extension)
	angular_ability += extension.angular_ability
	angular_ability_roll += extension.angular_ability_roll
	angular_damping += extension.angular_damping
	linear_ability += extension.linear_ability
	linear_damping += extension.linear_damping
	fly_speed += extension.fly_speed
	keep_upright_intensity += extension.keep_upright_intensity
	intent_rotate_decay_mult += extension.intent_rotate_decay_mult
	crane_rotate_decay_mult += extension.crane_rotate_decay_mult

func remove_extension(extension):
	enabled_extensions.append(extension)
	angular_ability -= extension.angular_ability
	angular_ability_roll -= extension.angular_ability_roll
	angular_damping -= extension.angular_damping
	linear_ability -= extension.linear_ability
	linear_damping -= extension.linear_damping
	fly_speed -= extension.fly_speed
	keep_upright_intensity -= extension.keep_upright_intensity
	intent_rotate_decay_mult -= extension.intent_rotate_decay_mult
	crane_rotate_decay_mult -= extension.crane_rotate_decay_mult
