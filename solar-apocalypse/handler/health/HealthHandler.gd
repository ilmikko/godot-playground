extends Spatial

signal health_changed(new, old)
signal health_reached_zero(old)


var body


# Set to 0.0 for health to be automatically derived by HealthExpert.
export var max_health = 0.0


var health = 1.0 setget _set_health
func _set_health(h):
	emit_signal("health_changed", h, health)
	if h <= 0.0:
		emit_signal("health_reached_zero", health)
		h = 0.0
		_health_reached_zero()
	
	health = h
	# Debug.text(body, "health", health)


func _health_reached_zero():
	# Runtime.Expert.Breakage.break(body)
	pass


func current():
	return health


func lose(h):
	# This can happen if the object breaks, for example.
	if body != get_parent():
		reset_health(get_parent())
	
	# Runtime.Expert.Breakage.prepare(body)
	_set_health(health - h)
	return health


func reset_health(b):
	body = b
	if max_health > 0.0:
		health = max_health
	else:
		#health = Runtime.Expert.Health.max_health(body)
		pass


func _ready():
	reset_health(get_parent())
