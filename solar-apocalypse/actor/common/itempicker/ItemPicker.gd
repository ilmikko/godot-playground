tool
extends Spatial


export(float, 0, 100) var radius = 1 setget _set_radius
func _set_radius(r):
	radius = r
	$PickableArea/CollisionShape.shape.radius = r


func _on_PickableArea_body_entered(body):
	if body.is_in_group("pickup_enabled"):
		Runtime.Game.Inventory.add_item(body)
