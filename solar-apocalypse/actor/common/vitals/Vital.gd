extends Spatial

signal state_changed(new_state)

enum TIME_UNITS {MINUTE, HOUR, DAY}

export(float, 0, 1) var value = 1
export(float) var decay_per_time_unit = 0
export(TIME_UNITS) var time_unit
export(Array, String) var state_names = []
export(Curve) var states_curve


func change_value_by(amount):
	_handle_vital_change(amount)

func decay_vital():
	_handle_vital_change(-_decay_per_minute())
	

func _handle_vital_change(amount_change):
	var old_state = _get_state()
	value += amount_change
	value = min(1, max(0, value))


	var new_state = _get_state()

	if old_state != new_state:
		emit_signal("state_changed", new_state)
	Debug.text(self, get_path(), "%s %f" % [new_state, value])

func _decay_per_minute():
	match time_unit:
		TIME_UNITS.MINUTE:
			return decay_per_time_unit
		TIME_UNITS.HOUR:
			return decay_per_time_unit / 60.0
		TIME_UNITS.DAY:
			return decay_per_time_unit / (60 * 24.0)

func _get_state():
	var state_value = states_curve.interpolate(value)
	var state_name_index = ceil(state_value*(len(state_names) - 1))
	return state_names[state_name_index]


func _on_Timer_timeout():
	decay_vital()
