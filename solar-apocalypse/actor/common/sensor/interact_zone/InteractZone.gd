tool
extends Area

export(float, 0, 4096) var radius = 5.0 setget _set_radius
func _set_radius(new_radius):
	radius = new_radius
	$CollisionShape.shape.radius = radius

func get_interactable_items(): 
	return get_overlapping_bodies() + get_overlapping_areas()
