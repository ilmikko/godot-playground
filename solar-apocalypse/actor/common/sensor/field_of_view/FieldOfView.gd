extends Area

signal item_detected

var root_parent


func rebuild():
	root_parent = Util.get_root_parent(self)
	$Timer.start()


func get_visible_items():
	var items_in_collision_shape = get_overlapping_bodies()
	var visible_items = []
	for item in items_in_collision_shape:
		if root_parent.is_a_parent_of(item): continue

		if item_is_visible(item):
			visible_items.append(item)
	return Util.get_root_parents(visible_items)


func item_is_visible(item):
	if not is_inside_fov(item): return false

	var item_collision_shapes = _get_item_collision_shapes(item)
	
	for item_collision_shape in item_collision_shapes:
		if _ray_to_point_collides_with_item(item_collision_shape.global_transform.origin, item):
			return true
	
	return false


func _ray_to_point_collides_with_item(point, item):
	var space_state = get_world().direct_space_state
	var ray_cast_hit_item = space_state.intersect_ray(
		global_transform.origin,
		point,
		[],
		collision_mask
	)

	return not ray_cast_hit_item.empty() and ray_cast_hit_item.collider == item


func _get_item_collision_shapes(item):
	var children =  item.get_children()
	var collision_shapes = []

	for child in children:
		if child is CollisionShape:
			collision_shapes.append(child)

	return collision_shapes


func is_inside_fov(item):
	var facing = -root_parent.global_transform.basis.z
	var direction_to_item = root_parent.global_transform.origin.direction_to(item.global_transform.origin)
	return direction_to_item.dot(facing) > 0


func _on_Timer_timeout():
	var visible_items = get_visible_items()
	for item in visible_items:
		emit_signal("item_detected", item)
