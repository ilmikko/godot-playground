extends Spatial

export(Script) var current_task_script = preload("res://actor/common/tasks/None.gd") setget _set_current_task_script
func _set_current_task_script(_current_task_script):
	current_task_script = _current_task_script
	rebuild()


var world_knowledge_base = WorldKnowledgeBase.new()
var current_task

var driver
var target_location
var target_rotation


func set_target_location(new_target_location):
	self.target_location = new_target_location
	driver.move_towards(target_location)


func _process(delta):
	current_task.process(delta)


func tick():
	# Thinking goes here.
	if !current_task:
		task_start()
	current_task.tick()


func task_start():
	current_task = current_task_script.new(self)
	current_task.start()


func rebuild():
	if !is_inside_tree():
		return
	
	$FieldOfView.rebuild()
	
	if !driver:
		driver = $".."
	
	task_start()


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _ready():
	Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")
	rebuild()


func _on_at_least_0_1_seconds_passed(_t):
	tick()


func set_task(new_task):
	current_task = new_task.new(self)


func task_completed(_is_successful):
	pass


# TODO: Add a way of removing bodies which are no longer at their old locations
func _on_FieldOfView_item_detected(item):
	world_knowledge_base.add_item(item)
