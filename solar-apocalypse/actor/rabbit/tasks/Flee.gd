extends "res://actor/common/Task.gd"
class_name Flee

var predator

const SAFE_DISTANCE = 20

var target_location

func _init(brain).(brain): pass

func tick():
	if !predator: return

	if _current_location_is_safe():
		brain.task_completed(true)


	target_location = _sample_location_away_from_point(_get_predator_location())
	brain.set_target_location(target_location)

func _sample_location_away_from_point(point):
	var distance_away_to_go = SAFE_DISTANCE
	var current_location = _get_current_location()
	var distance_between_points = current_location.distance_to(point)
	
	var point_x = point.x + (distance_away_to_go/distance_between_points) * (current_location.x - point.x)
	var point_y = 0
	var point_z = point.z +  (distance_away_to_go/distance_between_points) * (current_location.z - point.z)
	return Vector3(point_x, point_y, point_z)

func _target_is_safe_location():
	if !target_location: return false

	return target_location.distance_to(_get_predator_location()) > SAFE_DISTANCE

func _current_location_is_safe():
	return _get_current_location().distance_to(_get_predator_location()) > SAFE_DISTANCE

func _get_predator_location():
	return brain.world_knowledge_base.get_item_knowledge(predator).location

func _get_current_location():
	return brain.driver.global_transform.origin
