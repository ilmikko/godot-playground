tool
extends Spatial

export(Color) var color = Color(1, 0, 1, 1) setget _set_color
func _set_color(_color):
	color = _color
	rebuild()

export(float, 0.0, 1.0) var life setget _set_life
func _set_life(_life):
	life = _life
	rebuild()

var mat

func rebuild():
	if !is_inside_tree():
		return
	
	if !mat:
		mat = $MeshInstance.get_surface_material(0)
	
	mat.set_shader_param("fly_intensity", life)
	mat.set_shader_param("wing_color", color)
