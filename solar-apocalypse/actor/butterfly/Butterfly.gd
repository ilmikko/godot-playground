extends Spatial

export(float, 0.0, 1.0) var life = 1.0 setget _set_life
func _set_life(_life):
	life = _life
	rebuild()

var driver

func rebuild():
	driver = $Driver
	
	$Driver.life = life
