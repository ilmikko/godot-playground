tool
extends RigidBody

# The maximum amount of angular force this driver can emit.
var ANGULAR_ABILITY        = 0.01
# Base angular damping amount.
var ANGULAR_DAMPING        = 0.01
# Base gravity intensity. (TODO: Abstract this away)
var GRAVITY_INTENSITY      = 0.1
# Maximum amount of downwards hover force this driver can emit.
var HOVER_ABILITY_DOWN     = -0.05
# Maximum amount of upwards hover force this driver can emit.
var HOVER_ABILITY_UP       = 0.25
# Multiplier to keep upright signal.
var KEEP_UPRIGHT_INTENSITY = 0.05
# The maximum amount of linear force this driver can emit.
var LINEAR_ABILITY         = 0.05
# Base linear damping amount.
var LINEAR_DAMPING         = 0.01


# How much hovering force the actor wants to apply.
var intent_hover = 0.0
# How much rotation the actor wants to apply.
var intent_rotate = Vector2(0, 0)
# Where the actor wants to move. left-right, front-back.
var intent_move = Vector2(0, 0)


func _integrate_forces(state):
	_integrate_forces_angular(state)
	_integrate_forces_angular_damping(state)
	_integrate_forces_keep_upright(state)
	_integrate_forces_hover(state)
	_integrate_forces_linear(state)
	_integrate_forces_linear_damping(state)


func _integrate_forces_angular(state):
	state.apply_torque_impulse(global_transform.basis.x * intent_rotate.x * ANGULAR_ABILITY)
	state.apply_torque_impulse(global_transform.basis.y * intent_rotate.y * ANGULAR_ABILITY * 2.0)


func _integrate_forces_angular_damping(state):
	state.apply_torque_impulse(-state.angular_velocity * ANGULAR_DAMPING)


func _integrate_forces_keep_upright(state):
	var keep_upright = global_transform.basis.y.cross(Vector3.UP)
	
	Debug.text(self, "KUR", keep_upright)
	
	state.apply_torque_impulse(global_transform.basis.x * keep_upright.x * KEEP_UPRIGHT_INTENSITY)
	state.apply_torque_impulse(global_transform.basis.y * keep_upright.y * KEEP_UPRIGHT_INTENSITY)
	state.apply_torque_impulse(global_transform.basis.z * keep_upright.z * KEEP_UPRIGHT_INTENSITY)


func _integrate_forces_hover(state):
	var current_up = global_transform.basis.y;
	var target_up = Vector3.UP;
	# Maximum lift only occurs when up vectors are facing each other.
	var lift = max(target_up.dot(current_up), 0)
	Debug.text(self, "lift", lift)
	Debug.text(self, "life", life)
	Debug.text(self, "hover", intent_hover)
	state.apply_central_impulse(current_up * life * lift * clamp(intent_hover, HOVER_ABILITY_DOWN, HOVER_ABILITY_UP))


func _integrate_forces_linear(state):
	state.apply_central_impulse(global_transform.basis.x * intent_move.x * LINEAR_ABILITY)
	state.apply_central_impulse(global_transform.basis.z * intent_move.y * LINEAR_ABILITY)


func _integrate_forces_linear_damping(state):
	state.apply_central_impulse(-state.linear_velocity * LINEAR_DAMPING)


export(float, 0.0, 1.0) var hover_intensity = 0.1 setget _set_hover_intensity
func _set_hover_intensity(_hover_intensity):
	hover_intensity = _hover_intensity


export(float, 0.0, 1.0) var life = 0.0 setget _set_life
func _set_life(_life):
	life = _life
