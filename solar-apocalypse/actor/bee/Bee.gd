extends Spatial

export(float, 0.0, 1.0) var life = 1.0 setget _set_life
func _set_life(_life):
	life = _life
	rebuild()

var driver


func rebuild():
	driver = get_node_or_null("Driver")
	
	if driver == null: return
	
	$Driver.life         = life
	$Driver/Body.life    = life
	$Driver/Wings.life   = life
	$Driver/Stinger.life = life


func _ready():
	rebuild()
