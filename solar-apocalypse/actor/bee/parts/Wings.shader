shader_type spatial;
render_mode cull_disabled;

uniform float buzz_intensity : hint_range(0.0, 0.01) = 0.001;
uniform float fly_intensity  : hint_range(0.0, 1.0) = 1.0;

vec3 buzz(float t) {
	return vec3(sin(t*100.0), cos(t*110.0), 0.0);
}

void vertex() {
	VERTEX.y += abs(VERTEX.x * 0.5) * (cos(TIME*60.0) * 0.5 + 0.5) * fly_intensity;
	VERTEX   += buzz(TIME) * buzz_intensity;
}

void fragment() {
	vec2 off = vec2(0.5, 0.0);
	vec2 uv = UV * 2.0 - 1.0;
	float d = 0.0;
	
	d += step(0.6, 1.0 - length(uv + off));
	d += step(0.6, 1.0 - length(uv - off));
	
	ALBEDO = vec3(d);
	ALPHA  = d;
}