tool
extends Spatial

export(float, 0.0, 1.0) var life = 0.0 setget _set_life
func _set_life(_life):
	life = _life
	rebuild()

export(float, 0.0, 0.1) var buzz_intensity = 0.001 setget _set_buzz_intensity
func _set_buzz_intensity(_buzz_intensity):
	buzz_intensity = _buzz_intensity
	rebuild()

export(float, 0.0, 1.0) var fly_intensity = 1.0 setget _set_fly_intensity
func _set_fly_intensity(_fly_intensity):
	fly_intensity = _fly_intensity
	rebuild()

var mat

func rebuild():
	if !mat:
		mat = $MeshInstance.get_surface_material(0)
	mat.set_shader_param("buzz_intensity", life * buzz_intensity)
	mat.set_shader_param("fly_intensity", life * fly_intensity)
