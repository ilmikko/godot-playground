extends RigidBody
signal deadly_impact(velocity)


# The speed for various body parts to match player intent basis.
# TODO: This should be different for player drivers vs. actor drivers.
var TURN_SPEED_BODY   = 0.15
var TURN_SPEED_HEAD   = 0.22
var TURN_SPEED_INTENT = 0.75

# The maximum amount of angular force this character can emit.
var ANGULAR_ABILITY           = 0.01
# Base angular damping amount.
var ANGULAR_DAMPING           = 0.01
# Base gravity intensity. (TODO: Abstract this away)
var GRAVITY_INTENSITY         = 0.4
# The maximum amount of linear force this character can emit.
var LINEAR_ABILITY            = 3.8
# Base linear damping amount.
var LINEAR_DAMPING            = 0.03

var LINEAR_CUMULATIVE_FALLOFF = 0.45


# The minimum velocity considered to be deadly to send a signal.
var IMPACT_DEADLY_TOTAL_VELOCITY = 250.0


# The maximum amount of jump force this character can emit.
var JUMP_ABILITY                       = 5.0
# Damping to apply to the jump velocity calculation that tracks whether we are
# stationary on some object.
var JUMP_CUMULATIVE_DAMPING            = 0.4
var JUMP_CUMULATIVE_DELTA_DAMPING      = 0.65
# What is the vertical cumulative velocity we need to have not to be considered
# grounded any longer?
var JUMP_CUMULATIVE_GROUNDED_THRESHOLD = 0.3

# Maximum amount of walk force this character can emit.
var WALK_ABILITY             = 15.75
# Velocity cap - if our velocity is more than this, do not add more walking force.
var WALK_VELOCITY_MAX        = 2.55
var WALK_VELOCITY_SPRINT_MAX = 10.0
# Damping to apply to the cumulative force that keeps track of how much force
# the player has applied to walk, in order to calculate the stopping force.
var WALK_CUMULATIVE_DAMPING  = 0.55
# Damping to apply to walking force.
var WALK_DAMPING             = 0.45
# Maximum amount of sprint force this character can emit.
var WALK_SPRINT_ABILITY      = 1.75
# Damping to apply to the stopping force when the player finishes walking.
var WALK_STOP_DAMPING        = 0.55
# Multiplier to walk/sprint intents when the player is not on the ground.
var WALK_AIRBORNE_MULTIPLIER = 0.2


# How much velocity the actor wants to apply.
var intent_linear = Vector3(0, 0, 0)


var walk_cumulative  = Geo.Vector3(0)
var jump_cumulative  = 0.0
# What goes up must come down. This value tracks whether we have gone up and
# must come down shortly.
# TODO: Might be possible to re-use linear_cumulative now that it exists
var jump_cumulative_delta = 0.0


var linear_cumulative_velocity = Geo.Vector3(0)


# Ground normal to calculate relative velocity along the normals.
# TODO: Hook up GroundSensor to update this value for drivers.
var ground_normal = Vector3.UP
var ground_current = null


var actor setget , _get_actor
func _get_actor():
	return $".."


func rebuild():
	set_physics_process(active)
	$CollisionShape.disabled = !active
	can_sleep = !active
	sleeping = !active
	if active:
		mode = RigidBody.MODE_RIGID
	else:
		mode = RigidBody.MODE_STATIC

	if !actor:
		actor = $".."


func is_grounded():
	# Not grounded because of relative velocity.
	if (jump_cumulative_delta > JUMP_CUMULATIVE_GROUNDED_THRESHOLD):
		return false
	
	# Grounded in terms of the ground sensor.
	if ground_current != null:
		return true
	else:
		return false


func ground_changed(ground):
	ground_current = ground


var max_impact_timer = 0
var max_impact = 0.0


var body_total_velocity = {}
var body_total_collision_count = {}


func impact_with(body):
	if ! body_total_velocity.has(body):
		body_total_velocity[body] = Vector3(0, 0, 0)
	if ! body_total_collision_count.has(body):
		body_total_collision_count[body] = 0
	
	body_total_collision_count[body] += 1
	
	if  body is RigidBody:
		# If rigidbody, check which velocity they collided with us + our velocity.
		var body_velocity = (body.angular_velocity * 0.3 + body.linear_velocity)
		# Heavier objects will have more weight. (pun not intended.)
		body_velocity *= body.mass
		# If we have collided with this multiple times, don't add as much velocity.
		# This is to prevent us going over the threshold by simply pushing a barrel
		# for ~100 small collisions, versus getting hit by a barrel in ~2 collisions.
		body_velocity *= max(1.0 - (body_total_collision_count[body] / 10.0), 0.0)
		body_total_velocity[body] += body_velocity
		body_total_velocity[body] *= 0.98
	
	# TODO: Apply weight if we're sliding (walking)
	
	# Apply weight if we have been falling for a long time (fall damage)
	# Corollary: If we haven't been falling, collisions have less weight.
	# linear_cumulative_velocity *= abs(jump_cumulative_delta)
	
	var total_velocity = body_total_velocity[body] + linear_cumulative_velocity
	
#	Debug.text(self, "Impact: ", "%s = %s + %s (%s / %s)" % [
#		total_velocity.length(),
#		body_total_velocity[body].length(),
#		linear_cumulative_velocity.length(),
#		body_total_collision_count[body],
#		jump_cumulative_delta
#	])
	
	# TODO: This can be removed once we're confident the maths are OK here
	if total_velocity.length() > max_impact || max_impact_timer > 100:
		max_impact = total_velocity.length()
		max_impact_timer = 0
		#Debug.text(self, "Max Impact: ", "%s" % max_impact)
	else:
		max_impact_timer += 1
	
	if total_velocity.length() < IMPACT_DEADLY_TOTAL_VELOCITY: return
	#Debug.text(self, "Deadly impact: ", total_velocity.length())
	emit_signal("deadly_impact", total_velocity)


func _integrate_forces(state):
	#Debug.text(self, "Jump cumulative", jump_cumulative_delta)
	
	# _integrate_forces_gravity(state)
	_integrate_forces_angular_damping(state)
	_integrate_forces_linear_damping(state)
	
	if life == 0.0:
		state.linear_velocity *= 0.1
		return
	
	_integrate_forces_turning(state)
	_integrate_forces_walking(state)
	_integrate_forces_stopping(state)
	_integrate_forces_jumping(state)
	
	_integrate_forces_angular(state)
	# _integrate_forces_keep_upright(state)
	_integrate_forces_linear(state)
	
	_integrate_forces_track_velocity(state)


func _integrate_forces_angular(state):
	var intent_angular = Vector3(0, 0, 0)
	if intent_angular.length() > ANGULAR_ABILITY:
		intent_angular = intent_angular.normalized() * ANGULAR_ABILITY
	state.apply_torque_impulse(intent_angular)


func _integrate_forces_angular_damping(state):
	state.apply_torque_impulse(-state.angular_velocity * ANGULAR_DAMPING)


func _integrate_forces_gravity(state):
	state.apply_central_impulse(Vector3.DOWN * GRAVITY_INTENSITY)


func _integrate_forces_linear(state):
	if intent_linear.length() > LINEAR_ABILITY:
		intent_linear = intent_linear.normalized() * LINEAR_ABILITY
	state.apply_central_impulse(intent_linear * mass)
	intent_linear = Vector3(0, 0, 0)


func _integrate_forces_linear_damping(state):
	state.apply_central_impulse(-state.linear_velocity * LINEAR_DAMPING)


func _integrate_forces_track_velocity(state):
	linear_cumulative_velocity *= LINEAR_CUMULATIVE_FALLOFF
	linear_cumulative_velocity += state.linear_velocity


func _integrate_forces_turning(_state):
	var intent_body = Geo.basis_yaw(intent_basis).orthonormalized()
	$CollisionShape.global_transform.basis = \
		$CollisionShape.global_transform.basis \
		.slerp(intent_body, TURN_SPEED_BODY)
	
	var intent_main = intent_body
	intent_main.y += ground_normal
	intent_main = intent_main.orthonormalized()
	$Intent.global_transform.basis = \
		$Intent.global_transform.basis \
		.slerp(intent_main, TURN_SPEED_INTENT)


func _integrate_forces_jumping(state):
	jump_cumulative *= JUMP_CUMULATIVE_DAMPING
	jump_cumulative += abs(state.linear_velocity.y)
	jump_cumulative_delta *= JUMP_CUMULATIVE_DELTA_DAMPING
	var grounded = is_grounded()
	#Debug.text(self, "grounded", grounded)
	if !intent_jump || !grounded: return
	jump_cumulative_delta += JUMP_ABILITY * mass
	apply_central_impulse(global_transform.basis.y * JUMP_ABILITY * mass)
	_get_Skeleton().anim_jumping = true
	_get_Skeleton().anim_walking = false


func _integrate_forces_stopping(_state):
	if intent_move.length() != 0 || !is_grounded(): return
	walk_cumulative *= WALK_STOP_DAMPING
	apply_central_impulse(-walk_cumulative * mass)


func _integrate_forces_walking(state):
	walk_cumulative *= WALK_CUMULATIVE_DAMPING
	
	var dir = intent_move
	if  dir.length() == 0: return
	
	var grounded = is_grounded()
	#Debug.text(self, "grounded", grounded)
	if !grounded:
		dir *= WALK_AIRBORNE_MULTIPLIER
		_get_Skeleton().anim_walking = false
	else:
		_get_Skeleton().anim_walking = true
	
	var ability = WALK_ABILITY
	if intent_move_fast:
		ability = WALK_SPRINT_ABILITY
	
	dir *= ability
	dir *= WALK_DAMPING

	var intent = $Intent.global_transform.basis.x * dir.x + $Intent.global_transform.basis.z * dir.y
	
	var cap = WALK_VELOCITY_MAX
	if intent_move_fast:
		cap = WALK_VELOCITY_SPRINT_MAX
	
	var intent_mult = max(1.0 - state.linear_velocity.length() / cap, 0.0)
	
	intent *= intent_mult
	
	walk_cumulative += state.linear_velocity
	intent_linear   += intent


export var active = true setget _set_active
func _set_active(_active):
	active = _active
	rebuild()


var intent_attack = false setget _set_intent_attack
func _set_intent_attack(c):
	_get_Skeleton().anim_attacking = c


var intent_crouch = false setget _set_intent_crouch
func _set_intent_crouch(c):
	_get_Skeleton().anim_crouching = c


var intent_jump = false setget _set_intent_jump
func _set_intent_jump(i):
	intent_jump = i


var intent_kick = false setget _set_intent_kick
func _set_intent_kick(k):
	_get_Skeleton().anim_kicking = k


onready var intent_basis = global_transform.basis setget _set_intent_basis
func _set_intent_basis(ib):
	if ib == null: return
	intent_basis = ib
	_get_CraneBase().global_transform.basis = ib
	_get_Skeleton().anim_look_basis = ib


var intent_move = Vector2(0, 0) setget _set_intent_move
func _set_intent_move(im):
	intent_move = im
	_get_Skeleton().anim_walking = im.length() > 0


var intent_move_fast = false setget _set_intent_move_fast
func _set_intent_move_fast(imf):
	intent_move_fast = imf


var intent_interact_primary = false setget _set_intent_interact_primary
func _set_intent_interact_primary(ip):
	if intent_interact_primary == ip: return
	intent_interact_primary = ip
	if !ip: return
	_get_InteractionNotifier().interact(self, _get_PlayerCamera().Interactor, "primary")


var intent_interact_secondary = false setget _set_intent_interact_secondary
func _set_intent_interact_secondary(ip):
	if intent_interact_secondary == ip: return
	intent_interact_secondary = ip
	if !ip: return
	_get_InteractionNotifier().interact(self, _get_PlayerCamera().Interactor, "secondary")


export var life = 1.0 setget _set_life
func _set_life(l):
	life = l
	_get_Skeleton().life = l


var InteractionNotifier setget , _get_InteractionNotifier
func _get_InteractionNotifier(): return $InteractionNotifier


var Skeleton setget , _get_Skeleton
func _get_Skeleton(): return $CollisionShape/Skeleton


var Crane setget , _get_Crane
func _get_Crane(): return $CraneBase/Crane


var CraneBase setget , _get_CraneBase
func _get_CraneBase(): return $CraneBase


var PlayerCamera setget , _get_PlayerCamera
func _get_PlayerCamera(): return _get_Crane().player_camera


func _on_Driver_body_entered(body):
	impact_with(body)


func _on_GroundSensor_ground_changed(ground):
	ground_changed(ground)
