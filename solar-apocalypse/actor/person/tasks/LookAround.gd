extends "res://actor/common/Task.gd"

func _init(brain).(brain):
	Runtime.Game.Time.every("5s", self, "_every_5s")


func tick():
	pass


func _every_5s():
	driver.intent_basis = Basis().rotated(Vector3.UP, randf() * 10)


func finish():
	.finish()
