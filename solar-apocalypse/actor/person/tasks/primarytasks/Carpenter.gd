extends "res://actor/person/tasks/primarytasks/Person.gd"

var none_task = preload("res://actor/common/tasks/None.gd")
var hammer_task = preload("res://actor/person/tasks/Hammer.gd")

var DURATION_TO_HAMMER_FOR = Duration.new({"seconds": 10})

func _init(brain).(brain):
	_start_child_task(hammer_task)

func tick():
	.tick()
	if child_task is Hammer:
		handle_hammer_task()

func handle_hammer_task():
	if child_task_running_time.greater_than(DURATION_TO_HAMMER_FOR):
		_start_child_task(none_task)
