extends "res://actor/common/Task.gd"
class_name Hammer

func _init(brain).(brain):
	brain.attack()


func tick(): pass

func finish():
	.finish()
	brain.attack(false)
