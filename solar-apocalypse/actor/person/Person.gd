tool
extends "res://actor/Actor.gd"


var interact_hint      = "Speak To"


var dialog_tree_instance


func is_dead():
	return life <= 0.0


func kill():
	if is_dead(): return
	
	var d = _get_Driver()
	d.life = 0.0
	d.active = false
	life = 0.0

	_get_HolderHandler().drop()
	_get_Skeleton().ragdoll()


func rebuild():
	if !is_inside_tree(): return
	
	# Don't be interactable if dialog_tree is not there.
	if dialog_tree == null:
		interact_hint = ""
	
	if dialog_tree != null && dialog_tree_instance == null:
		dialog_tree_instance = dialog_tree.new()


func rebuild_life():
	_get_Driver().life = life
	_get_Skeleton().life = life
	
	if life <= 0.0:
		kill()


func _ready():
	rebuild()


export(Script) var dialog_tree setget _set_dialog_tree
func _set_dialog_tree(_dialog_tree):
	dialog_tree = _dialog_tree
	rebuild()


export(int, 0, 9999) var variant = 0 setget _set_variant
func _set_variant(v):
	if variant == v: return
	variant = v
	_get_Skeleton().variant = v


export(float, 0.0, 1.0) var life = 1.0 setget _set_life
func _set_life(l):
	if life == l: return
	life = l
	rebuild_life()


var HealthHandler setget , _get_HealthHandler
func _get_HealthHandler(): return $HealthHandler


var HolderHandler setget , _get_HolderHandler
func _get_HolderHandler(): return $HolderHandler


var Skeleton setget , _get_Skeleton
func _get_Skeleton(): return $Driver/CollisionShape/Skeleton


func _on_Driver_deadly_impact(_velocity):
	kill()


func _on_HealthHandler_health_reached_zero(_old):
	kill()


func _on_VitalsHandler_food_eaten(_food):
	$Driver/EatAudioStreamPlayer3D.play()
