tool
extends Spatial


var ANIM_CROUCH_OFFSET = 0.3
var ANIM_CROUCH_UPPER_ROTATION = 0.2
var ANIM_CROUCH_INTENSITY = 0.9


var ANIM_WALK_LEG_SPEED  = 9.0
var ANIM_WALK_LEG_AMOUNT = 0.46
var ANIM_WALK_ARM_SPEED  = ANIM_WALK_LEG_SPEED
var ANIM_WALK_ARM_AMOUNT = 0.15
var ANIM_WALK_CUMULATIVE_FALLOFF = 0.9
var ANIM_WALK_CUMULATIVE_KICKOFF = 7.0


var delta_target = 0.01


var t = 0
var anim_attacking_start_t = 0
var anim_jumping_start_t = 0
var anim_kicking_start_t = 0


var anim_walk_cumulative = 0.0


var arm_left_rotation
var arm_right_rotation
var leg_left_rotation
var leg_right_rotation


func disable_collisions():
	if driver == null: return
	for body in $SkeletonBodies.bodies:
		body.add_collision_exception_with(driver)


func ragdoll():
	set_process(false)
	$SkeletonBodies.unfreeze()
	$SkeletonBodies.remove_root_parents()


func _process(delta):
	if Engine.is_editor_hint(): return
	
	t += delta

	arm_left_rotation  = Vector3(0, 0, 0)
	arm_right_rotation = Vector3(0, 0, 0)
	leg_left_rotation  = Vector3(0, 0, 0)
	leg_right_rotation = Vector3(0, 0, 0)
	
	_process_attacking(delta)
	_process_crouching(delta)
	_process_jumping(delta)
	_process_kicking(delta)
	_process_walking(delta)
	
	$SkeletonBodies/PersonBody/Upper/ArmLeft/PersonArm.rotation  = arm_left_rotation
	$SkeletonBodies/PersonBody/Upper/ArmRight/PersonArm.rotation = arm_right_rotation
	
	$SkeletonBodies/PersonBody/Lower/PersonLegLeft.rotation  = leg_left_rotation
	$SkeletonBodies/PersonBody/Lower/PersonLegRight.rotation = leg_right_rotation


func _process_attacking(_delta):
	if !anim_attacking: return
	# We shouldn't be able to attack if we're holding an item (like a barrel)
	# with both our hands.
	# TODO: This shouldn't be handled here
	if anim_holding_left == 1.0 && anim_holding_right == 1.0: return
	
	var p = (t-anim_attacking_start_t)
	if  p > 1.0:
		anim_attacking_start_t = t
		return
	
	$SkeletonBodies/PersonBody/Upper/ArmRight/PersonArm.anim_attacking = p;


func _process_crouching(_delta):
	if anim_crouching:
		$SkeletonBodies/PersonBody/Upper.transform.origin.y = -ANIM_CROUCH_OFFSET
		$SkeletonBodies/PersonBody/Lower.transform.origin.y = -ANIM_CROUCH_OFFSET
		$SkeletonBodies/PersonBody/Upper.rotation = Vector3(1, 0, 0) * -ANIM_CROUCH_UPPER_ROTATION
		$SkeletonBodies/PersonBody/Lower/PersonLegLeft/PersonLeg.anim_crouching = ANIM_CROUCH_INTENSITY
		$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_crouching = ANIM_CROUCH_INTENSITY
	else:
		$SkeletonBodies/PersonBody/Upper.transform.origin.y = 0
		$SkeletonBodies/PersonBody/Lower.transform.origin.y = 0
		$SkeletonBodies/PersonBody/Upper.rotation = Vector3(0, 0, 0)
		$SkeletonBodies/PersonBody/Lower/PersonLegLeft/PersonLeg.anim_crouching = 0.0
		$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_crouching = 0.0


func _process_jumping(_delta):
	if !anim_jumping: return
	var p = (t-anim_jumping_start_t) * 1.5
	
	if p > 1.0:
		_set_anim_jumping(false)
		return
	
	$SkeletonBodies/PersonBody/Lower/PersonLegLeft/PersonLeg.anim_jumping = p
	$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_jumping = p


func _process_kicking(_delta):
	if !anim_kicking: return
	var p = (t-anim_kicking_start_t) * 1.5
	
	if p > 1.0:
		anim_kicking_start_t = t
		return
	
	$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_kicking = p


func _process_walking(delta):
	var falloff_pow = pow(ANIM_WALK_CUMULATIVE_FALLOFF, delta/delta_target);
	anim_walk_cumulative *= falloff_pow
	
	if anim_walking:
		anim_walk_cumulative += ANIM_WALK_CUMULATIVE_KICKOFF * delta
	
	if anim_walk_cumulative > 0:
		var amount_arm = cos(t * ANIM_WALK_ARM_SPEED) * ANIM_WALK_ARM_AMOUNT * anim_walk_cumulative
		var amount_leg = cos(t * ANIM_WALK_LEG_SPEED) * ANIM_WALK_LEG_AMOUNT * anim_walk_cumulative
		arm_left_rotation  += Vector3(0,  1, 0) * amount_arm
		arm_right_rotation += Vector3(0, -1, 0) * amount_arm
		leg_left_rotation  += Vector3(-1, 0, 0) * amount_leg
		leg_right_rotation += Vector3( 1, 0, 0) * amount_leg


func _ready():
	disable_collisions()


func _enter_tree():
	# We can only fetch the node once we're in the tree.
	# Assumes the node is in the parent tree.
	if driver == null && driver_path != null:
		driver = get_node(driver_path)


export var anim_attacking = false setget _set_anim_attacking
func _set_anim_attacking(a):
	if anim_attacking == a: return
	anim_attacking = a
	$SkeletonBodies/PersonBody/Upper/ArmRight/PersonArm.anim_attacking = 0.0
	anim_attacking_start_t = t


export var anim_crouching = false


export(float, 0.0, 1.0) var anim_holding_left  = 0.0 setget _set_anim_holding_left
func _set_anim_holding_left(a):
	anim_holding_left = a
	$SkeletonBodies/PersonBody/Upper/ArmLeft/PersonArm.anim_holding = a


export(float, 0.0, 1.0) var anim_holding_right = 0.0 setget _set_anim_holding_right
func _set_anim_holding_right(a):
	anim_holding_right = a
	$SkeletonBodies/PersonBody/Upper/ArmRight/PersonArm.anim_holding = a


export var anim_jumping = false setget _set_anim_jumping
func _set_anim_jumping(a):
	anim_jumping = a
	$SkeletonBodies/PersonBody/Lower/PersonLegLeft/PersonLeg.anim_jumping = 0.0
	$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_jumping = 0.0
	anim_jumping_start_t = t


export var anim_kicking = false setget _set_anim_kicking
func _set_anim_kicking(a):
	if anim_kicking == a: return
	anim_kicking = a
	$SkeletonBodies/PersonBody/Lower/PersonLegRight/PersonLeg.anim_kicking = 0.0
	anim_kicking_start_t = t


export var anim_look_basis = Basis() setget _set_anim_look_basis
func _set_anim_look_basis(b):
	if anim_look_basis == b: return
	anim_look_basis = b
	$SkeletonBodies/PersonBody/Upper/PersonHead.target_basis = b


export var anim_walking = false


export(float, 0.0, 1.0) var life setget _set_life
func _set_life(l):
	life = l
	$SkeletonBodies/PersonBody/Upper/PersonHead.life = l


export(Color) var skin_color setget _set_skin_color
func _set_skin_color(c):
	if skin_color == c: return
	$SkeletonBodies.skin_color = c


export(int, 0, 9999) var variant = 0 setget _set_variant
func _set_variant(v):
	if variant == v: return
	$SkeletonBodies.variant = v


var driver setget _set_driver
func _set_driver(d):
	if d == null: return
	driver      = d
	driver_path = null
export(NodePath) var driver_path setget _set_driver_path
func _set_driver_path(dp):
	if dp == null: return
	driver      = get_node(dp)
	driver_path = dp


var HolderArmLeft setget , _get_HolderArmLeft
func _get_HolderArmLeft(): return $SkeletonBodies/PersonBody/Upper/ArmLeft/PersonArm.Holder


var HolderArmRight setget , _get_HolderArmRight
func _get_HolderArmRight(): return $SkeletonBodies/PersonBody/Upper/ArmRight/PersonArm.Holder
