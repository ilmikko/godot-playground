shader_type spatial;

uniform float life    : hint_range(0.0, 1.0) = 0.0;
uniform float talking : hint_range(0.0, 1.0) = 0.0;
uniform vec4  color   : hint_color           = vec4(0.0, 0.0, 0.0, 1.0);

void vertex() {
	VERTEX.y *= 1.0 + life * cos(TIME * 1.3) * 0.1 + talking * cos(TIME * 22.0) * 0.3;
	
	float sway = cos(TIME * 0.2) * 0.5 + 0.5;
	sway = smoothstep(0.999, 1.0, sway) * life * (1.0 - talking);
	VERTEX.x += sway * cos(TIME * 5.1) * 0.02;
}

void fragment() {
	ALBEDO    = color.rgb;
	ROUGHNESS = 1.0 - life;
	METALLIC  = 0.6;
}