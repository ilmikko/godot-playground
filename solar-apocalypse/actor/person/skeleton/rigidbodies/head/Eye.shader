shader_type spatial;

uniform float life           : hint_range(0.0, 1.0) = 0.0;
uniform float sway_intensity : hint_range(0.0, 1.0) = 0.7;
uniform vec4  color          : hint_color           = vec4(0.0, 0.0, 0.0, 1.0);

void vertex() {
	float blink = life * cos(TIME) * 0.5 + 0.5;
	blink = smoothstep(0.992, 1.0, blink);
	VERTEX.y *= 0.1 + (1.0 - blink) * 0.9;
	
	vec2 sway = vec2(
		sin(TIME * 1.0)  * 0.01   + sin(TIME * 0.5)   * 0.005  + sin(TIME * 0.25)   * 0.0025,
		cos(TIME * 0.97) * 0.0017 + cos(TIME * 0.501) * 0.0017 + cos(TIME * 0.2481) * 0.0013
	);
	
	sway *= sway_intensity * life;
	
	VERTEX.x += sway.x;
	VERTEX.y += sway.y;
}

void fragment() {
	ALBEDO    = color.rgb;
	ROUGHNESS = 1.0 - life;
	METALLIC  = 0.6;
}