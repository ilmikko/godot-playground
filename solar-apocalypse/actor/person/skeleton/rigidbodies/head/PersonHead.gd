tool
extends Spatial


const AVAILABLE_VARIANTS = 4


export(int) var variant = 0 setget _set_variant
func _set_variant(_variant):
	variant = _variant
	rebuild()


export(float, 0.0, 1.0) var life = 0.0 setget _set_life
func _set_life(_life):
	if life == _life: return
	life = _life
	rebuild()


export(Material) var skin_material setget _set_skin_material
func _set_skin_material(_skin_material):
	skin_material = _skin_material
	rebuild()


export(Material) var target_basis setget _set_target_basis
func _set_target_basis(tb):
	target_basis = tb
	transform.basis = tb


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_life()
	rebuild_skin()
	rebuild_variants()


var eye_mat setget , _get_eye_mat
func _get_eye_mat():
	if !eye_mat:
		eye_mat = $Eyes/EyeLeft.material_override
	return eye_mat

var mouth_mat setget , _get_mouth_mat
func _get_mouth_mat():
	if !mouth_mat:
		mouth_mat = $Mouth/Mouth.material_override
	return mouth_mat


func rebuild_life():
	_get_eye_mat().set_shader_param("life", life)
	_get_mouth_mat().set_shader_param("life", life)


func rebuild_skin():
	$MeshInstance.material_override = skin_material


func rebuild_variants():
	Util.remove_children($Variants)
	var inst = load("res://actor/person/skeleton/rigidbodies/head/variant/HeadVariant%d.tscn" % (variant % AVAILABLE_VARIANTS)).instance()
	$Variants.add_child(inst)
