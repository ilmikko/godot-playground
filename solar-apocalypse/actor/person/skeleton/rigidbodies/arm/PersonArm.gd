tool
extends Spatial


const AVAILABLE_VARIANTS = 2

# How far into the attack animation we need to be in order to prime the held tool.
var   HOLDER_PRIME_ATTACK_THRESHOLD = 0.6

var   ANIM_HOLD_INTENSITY = 0.15

var   ANIM_ATTACK_INTENSITY_A = 0.9
var   ANIM_ATTACK_INTENSITY_B = 0.15


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_skin()
	rebuild_variants()
	
	# This method spams a lot; let's disable rebuilding it in the editor.
	if Engine.is_editor_hint(): return
	
	rebuild_anims()


func rebuild_anims():
	var base_attacking = Vector3(0,  0, 0)
	base_attacking    += Vector3(0,  0, 1) * sin(anim_attacking * anim_attacking * TAU) * (1.0 - anim_attacking) * 0.8
	
	var intensity_arm = 1.0
	var intensity_forearm = 0.9
	var intensity_hand = 0.1
	
	# Something interesting:
	# base_attacking    += Vector3(0, -1, 0) * sin(anim_attacking * anim_attacking * TAU) * sin(anim_attacking * PI) * 2.2
	# base_attacking    += Vector3(0,  0, 1) * sin(anim_attacking * anim_attacking * TAU) * sin(anim_attacking * PI) * 0.2
	# intensity_arm     = 1.1
	# intensity_forearm = 1.0
	
	if anim_holding > 0.0:
		base_attacking    += Vector3(0,  1, 0) * sin(anim_attacking * anim_attacking * TAU) * sin(anim_attacking * PI) * ANIM_ATTACK_INTENSITY_A
		# TODO: Variations of the below
		base_attacking    += Vector3(0, -1, 0) * (cos(anim_attacking * TAU) * 0.5 - 0.5) * ANIM_ATTACK_INTENSITY_B
	else:
		base_attacking    += Vector3(0, -1, 0) * sin(anim_attacking * TAU) * sin(anim_attacking * PI) * 2.2
		base_attacking    += Vector3(0,  0, 1) * sin(anim_attacking * TAU) * sin(anim_attacking * PI) * 0.8
		intensity_arm     = 1.1
		intensity_forearm = -0.3 + 1.3 * anim_attacking
	
	var base_holding   = Vector3(0,  1, 0) * anim_holding * TAU
	
	$Rotation.rotation                       = base_attacking * intensity_arm     + base_holding * ANIM_HOLD_INTENSITY
	$Rotation/Rotation.rotation              = base_attacking * intensity_forearm + base_holding * ANIM_HOLD_INTENSITY
	$Rotation/Rotation/Forearm/Hand.rotation = base_attacking * intensity_hand    + base_holding * ANIM_HOLD_INTENSITY
	var holder = _get_Holder()
	if  holder:
		holder.primed = anim_attacking > HOLDER_PRIME_ATTACK_THRESHOLD


func rebuild_skin():
	$Rotation/Rotation/Forearm/MeshInstance.material_override = skin_material
	$Rotation/Rotation/Forearm/Hand/MeshInstance.material_override = skin_material


func rebuild_variants():
	Util.remove_children($Rotation/Arm/Variants)
	var inst = load("res://actor/person/skeleton/rigidbodies/arm/variant/ArmVariant%d.tscn" % (variant % AVAILABLE_VARIANTS)).instance()
	$Rotation/Arm/Variants.add_child(inst)


func set_animations_based_on_held_object(o):
	if o == null:
		# TODO: Set anim_holding here based on what we're holding.
		return
	
	# Since Godot doesn't support multiple return variables, this is a bit ugly.
	# var person_arm_animation_variables = Runtime.Expert.ToolAnimation.person_arm_animation_variables(o)
	#ANIM_ATTACK_INTENSITY_A       = person_arm_animation_variables[0]
	#ANIM_ATTACK_INTENSITY_B       = person_arm_animation_variables[1]
	#HOLDER_PRIME_ATTACK_THRESHOLD = person_arm_animation_variables[2]


export(float, 0.0, 1.0) var anim_attacking = 0.0 setget _set_anim_attacking
func _set_anim_attacking(a):
	anim_attacking = a
	rebuild_anims()


export(float, 0.0, 1.0) var anim_holding = 0.0 setget _set_anim_holding
func _set_anim_holding(a):
	anim_holding = a
	rebuild_anims()


export(int, 0, 1e9) var variant = 0 setget _set_variant
func _set_variant(_variant):
	variant = _variant
	rebuild()


export(Material) var skin_material setget _set_skin_material
func _set_skin_material(_skin_material):
	skin_material = _skin_material
	rebuild()


var Holder setget , _get_Holder
func _get_Holder(): return $Rotation/Rotation/Forearm/Hand/Holder


func _on_HealthHandler_health_reached_zero(_old):
	# TODO: Make arm go limp or fall off
	pass


func _on_Holder_held_object_signal(held_object):
	set_animations_based_on_held_object(held_object)
