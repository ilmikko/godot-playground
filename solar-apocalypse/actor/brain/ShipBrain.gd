extends Spatial


export(NodePath) var driver_path
var                  driver


export(float, 0.0, 10.0) var roll_intensity = 1.0
export(float, 0.0, 10.0) var roll_duration = 1.0

export(float, 0.0, 10.0) var rotate_intensity = 1.0
export(float, 0.0, 10.0) var rotate_duration = 1.0


var attack_targets = {}

# Since the driver will slowly stop with rotation intents, we need to keep
# feeding the vector in repeatedly. This also makes actions seem more "intentional".
var intent_rotate_keep = Vector2(0, 0)


func attack(body):
	# Debug.print(self, "Attacking %s" % body)
	
	var off_angle = turn_to_face(body)
	shoot(off_angle)
	
	approach(off_angle, body)
	
	# 5% of 60% = 3% every 1s: change roll.
	if r.randf() < 0.05:
		attack_change_roll()


func attack_change_roll():
	driver.intent_roll   = r.randf()*0.1*roll_intensity
	Runtime.Game.Time.after_s(0.02 + 0.1*roll_duration*r.randf(), self, "attack_change_roll_stop")


func attack_change_roll_stop(_t):
	driver.intent_roll   = 0


func approach(off_angle, body):
	var dtf = driver.global_transform
	var btf = body.global_transform
	
	if off_angle > 0.5:
		# If we're totally off, stop and turn.
		driver.intent_move = Vector2(0, 0)
		return
	
	var distance = (dtf.origin - btf.origin).length()
	driver.intent_move = Vector2(0, -(0.1 + 1.5 * min(distance/100.0, 1.0)))


func roam():
	# We don't want to do stuff on every tick.
	# It's 0.1 so we can *react* quickly, but if there's nothing to react to,
	# we idle most of the time.
	driver.intent_fire = false
	Runtime.Game.Time.rate_limit_s(0.5, self, "roam_change_maybe")


func roam_change_maybe(_t):
	# 30% of every 1s: do nothing.
	if r.randf() < 0.3: return
	
	roam_change_speed()
	
	# 10% of 60% = 6% every 1s: change direction.
	if r.randf() < 0.1:
		roam_change_direction()
	# 10% of 60% = 6% every 1s: change roll.
	if r.randf() < 0.1:
		roam_change_roll()


func roam_change_direction():
	# print("changing directions....")
	var dir = Vector2(15.0*rotate_intensity*r.randf() - 5.0, 2.0*rotate_intensity*r.randf() - 0.5)
	
	if r.randf() < 0.5:
		intent_rotate_keep = Vector2(dir.x, dir.y)
	else:
		intent_rotate_keep = Vector2(dir.y, dir.x)
	
	Runtime.Game.Time.after_s(0.5 + 3.4*rotate_duration*r.randf(), self, "roam_change_direction_stop")


func roam_change_direction_stop(_t):
	intent_rotate_keep = Vector2(0, 0)


func roam_change_roll():
	driver.intent_roll   = r.randf()*3.0*roll_intensity
	Runtime.Game.Time.after_s(0.05 + 0.2*roll_duration*r.randf(), self, "roam_change_roll_stop")


func roam_change_roll_stop(_t):
	driver.intent_roll   = 0


func roam_change_speed():
	driver.intent_move = Vector2(0, -(0.3 + r.randf() * 0.5))


func shoot(off_angle):
	driver.intent_fire = off_angle < 0.3


func tick():
	if driver == null or (not is_instance_valid(driver)):
		queue_free()
		return
	
	driver.intent_rotate = intent_rotate_keep
	
	if attack_targets.size() > 0:
		# Pick closest target to attack.
		var closest_target      = null
		var closest_target_dist = 9999.0
		
		var pos = driver.global_transform.origin
		
		for t in attack_targets:
			var orig = t.global_transform.origin
			var dist = (pos - orig).length()
			if  dist < closest_target_dist:
				closest_target = t
				closest_target_dist = dist
		attack(closest_target)
		return
	
	roam()


func turn_to_face(body):
	var dtf = driver.global_transform
	var btf = body.global_transform
	var offset = Vector3(0.0, 0.0, -0.035) * body.linear_velocity
	
	var target = (btf.origin
		+ btf.basis.x * offset.x
		+ btf.basis.y * offset.y
		+ btf.basis.z * offset.z)
	
	var ttf = dtf.looking_at(target, dtf.basis.y)
	
	var d = (dtf.basis.z - ttf.basis.z)
	var off_angle = d.length()
	
	# If we're "close enough", stop putting as much intent.
	var force = 1.0
	if off_angle < 0.2:
		force = 0.2
	
	# intent_rotate_keep = Vector2(-d.y, d.x) * 4.0 * force
	# TODO: Use intent_rotate rather than absolute transforms.
	driver.global_transform.basis = ttf.basis
	
	return off_angle


var             r = RandomNumberGenerator.new()
export(int) var random_seed = 0 setget _set_random_seed
func _set_random_seed(s):
	random_seed = s
	r.seed = s


func _on_at_least_0_1_seconds_passed(_t):
	tick()


func _ready():
	driver = get_node(driver_path)
	Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _on_Radar_detect(body):
	if Runtime.Expert.Enemy.is_enemy(driver, body):
		attack_targets[body] = true


func _on_Radar_lose(body):
	if body in attack_targets:
		attack_targets.erase(body)
