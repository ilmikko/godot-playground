tool
extends RigidBody

# The maximum amount of angular force this character can emit.
var ANGULAR_ABILITY = 0.005
# Base angular damping amount.
var ANGULAR_DAMPING = 0.01
# The maximum amount of linear force this character can emit.
var LINEAR_ABILITY = 0.2
# Base linear damping amount.
var LINEAR_DAMPING = 0.02

var KEEP_UPRIGHT_INTENSITY = 0.02


var intent_move = Vector2(0, 0)
var intent_rotate = Vector2(0, 0)


# Points to a body where gravity is strongest.
# Updated by microgravity.gd.
var primary_influencing_gravity          = null
var primary_influencing_gravity_strength = 1.0


# How much rotation the actor wants to apply.
var angular_intent = Vector3(0, 0, 0)
# How much velocity the actor wants to apply.
var linear_intent = Vector3(0, 0, 0)


# TODO: Should be in brain, not in the driver
var target_rotation
export(Vector3) var target_location

# TODO: Should be in brain, not in the driver
func move_towards(new_target_location):
	target_location = new_target_location


func _integrate_forces(state):
	_integrate_forces_angular(state)
	_integrate_forces_angular_damping(state)
	_integrate_forces_keep_upright(state)
	_integrate_forces_linear(state)
	_integrate_forces_linear_damping(state)


func _integrate_forces_angular(state):
	state.apply_torque_impulse(global_transform.basis.x * -intent_rotate.x * ANGULAR_ABILITY)
	state.apply_torque_impulse(global_transform.basis.y * -intent_rotate.y * ANGULAR_ABILITY * 2.0)


func _integrate_forces_angular_damping(state):
	state.apply_torque_impulse(-state.angular_velocity * ANGULAR_DAMPING)


func _integrate_forces_keep_upright(state):
	if !primary_influencing_gravity: return
	
	var dir = (global_transform.origin - primary_influencing_gravity.global_transform.origin).normalized()
	var keep_upright = global_transform.basis.y.cross(dir)
	var direction    = global_transform.basis.y.dot(dir)
	
	Debug.text(self, "KUR", keep_upright)
	Debug.text(self, "KUR DIR", direction)
	
	state.apply_torque_impulse(global_transform.basis.x * keep_upright.x * KEEP_UPRIGHT_INTENSITY)
	state.apply_torque_impulse(global_transform.basis.y * keep_upright.y * KEEP_UPRIGHT_INTENSITY)
	state.apply_torque_impulse(global_transform.basis.z * keep_upright.z * KEEP_UPRIGHT_INTENSITY)


func _integrate_forces_linear(state):
	if linear_intent.length() > LINEAR_ABILITY:
		linear_intent = linear_intent.normalized() * LINEAR_ABILITY
	
	Debug.text(self, "P I B", primary_influencing_gravity)
	
	state.apply_central_impulse(global_transform.basis.z * intent_move.y)
	state.apply_central_impulse(global_transform.basis.x * intent_move.x)


func _integrate_forces_linear_damping(state):
	state.apply_central_impulse(-state.linear_velocity * LINEAR_DAMPING)
