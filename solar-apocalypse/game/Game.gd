tool
extends Spatial

var Achievement = preload("res://script/class/Achievement.gd").new()
var Quest       = preload("res://script/class/Quest.gd").new()
var Journal     = preload("res://script/class/Journal.gd").new()
var Time        = preload("res://script/class/Time.gd").new()
var Inventory   = preload("res://script/class/PlayerInventory.gd").new()
var Events   = preload("res://script/class/Events.gd").new()

func rebuild():
	# Add Time in editor as it's useful to have lighting update based on
	# changes to the time of day.
	add_child(Time)

	# Don't add below modules in editor.
	if Engine.is_editor_hint(): return

	add_child(Journal, true)
	
	# Game starts unpaused.
	_set_paused(false)


func _enter_tree():
	rebuild()


var in_menu setget _set_in_menu
func _set_in_menu(i):
	if in_menu == i: return
	in_menu = i
	print("IN_MENU %s" % i)
	if i:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


var paused setget _set_paused
func _set_paused(p):
	if paused == p: return
	paused = p
	_set_in_menu(p)
	
	get_tree().paused = p


var BaseController setget , _get_BaseController
func _get_BaseController():
	var base_controllers = get_tree().get_nodes_in_group("base-controller")
	var number_of_base_controllers = base_controllers.size()
	if number_of_base_controllers == 1:
		return base_controllers[0]
	
	if number_of_base_controllers == 0 :
		push_error("There is no base controller in this scene")

	push_error("There should only be one base controller in the scene. %s were found" % number_of_base_controllers)
