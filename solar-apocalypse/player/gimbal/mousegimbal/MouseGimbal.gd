extends Spatial

signal rotated(basis)


# TODO: Set this elsewhere (Game.settings?)
const MOUSE_SPEED = 0.01


var   basis_pitch = 0
var   basis_yaw   = 0


export(Basis) var basis = Basis() setget _set_basis, _get_basis
func _set_basis(b):
	basis = b
	basis_pitch = 0
	basis_yaw = 0
func _get_basis():
	return basis.rotated(
		basis.x, basis_pitch
	).rotated(
		basis.y, basis_yaw
	)


func rotated():
	emit_signal("rotated", _get_basis())


func _input(event):
	if event is InputEventMouseMotion:
		basis_yaw   += (-event.relative.x/TAU) * MOUSE_SPEED
		basis_pitch += (-event.relative.y/TAU) * MOUSE_SPEED
		rotated()


func _ready():
	rotated()
