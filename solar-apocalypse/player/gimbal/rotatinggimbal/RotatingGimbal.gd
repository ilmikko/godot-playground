extends Spatial


export var basis = Basis() setget _set_basis
func _set_basis(_basis):
	basis = _basis
	rebuild()


func rebuild():
	if !is_inside_tree(): return

	global_transform.basis = basis
