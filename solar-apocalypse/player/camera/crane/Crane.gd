extends Spatial


var player_camera


func attach(controller):
	# TODO: interact / point distance should be set globally.
	var distance_base = $ClipTargetRoot.distance
	var distance_interact = distance_base + 3.0
	
	player_camera = controller.PlayerCamera
	player_camera.Interactor.distance = distance_interact
	Util.move_child_under(controller, _get_Target())


# TODO: Setter for this should be attach().
var BaseController setget , _get_BaseController
func _get_BaseController(): return _get_Target().get_node("BaseController")


var Target setget , _get_Target
func _get_Target(): return $ClipTargetRoot/ClipTarget
