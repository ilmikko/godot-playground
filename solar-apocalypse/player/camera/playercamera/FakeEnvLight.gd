extends Spatial

# Assumes sun is at origin. How origin-al. Get it?
var point_at = Vector3(0, 0, 0)


func _process(delta):
	if transform.origin == point_at: return
	transform = transform.looking_at(point_at, Vector3.UP)
