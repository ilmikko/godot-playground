extends Spatial


func _enter_tree():
	Runtime.Expert.Camera.set_active_player_camera(self)
	Runtime.Expert.Camera.add_viewport_camera($SimpleCamera.Camera)
	# We can only fetch the node once we're in the tree.
	# Assumes the node is in the parent tree.
	if base_controller == null && base_controller_path != null:
		base_controller = get_node(base_controller_path)


func _exit_tree():
	Runtime.Expert.Camera.remove_viewport_camera($SimpleCamera.Camera)


func show_interact_hint(item):
	Runtime.UI.Hint.set_center_label_with_key(
		"player_camera_setter",
		Runtime.Expert.InteractHint.object_pointing_at(base_controller.linked_controller, item)
	)


func hide_interact_hint():
	Runtime.UI.Stats.visible = false
	Runtime.UI.Hint.unset_center_label_with_key("player_camera_setter")


var Camera setget , _get_Camera
func _get_Camera(): return $SimpleCamera.Camera


var FakeEnvironment setget , _get_FakeEnvironment
func _get_FakeEnvironment(): return $FakeEnvironment


var Interactor setget , _get_Interactor
func _get_Interactor(): return $Interactor


var Pointer setget , _get_Pointer
func _get_Pointer(): return $Pointer


var Tracker setget , _get_Tracker
func _get_Tracker(): return $Tracker


var Targeter setget , _get_Targeter
func _get_Targeter(): return $Targeter


var base_controller = null setget _set_base_controller
func _set_base_controller(c):
	base_controller      = c
	base_controller_path = null
export(NodePath) var base_controller_path = null setget _set_base_controller_path
func _set_base_controller_path(cp):
	base_controller      = get_node(cp)
	base_controller_path = cp


func _on_Interactor_pointing_at(object):
	show_interact_hint(object)


func _on_Interactor_pointing_nowhere():
	hide_interact_hint()
