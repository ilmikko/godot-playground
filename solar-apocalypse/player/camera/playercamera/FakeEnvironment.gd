extends Spatial


var district_size = Runtime.Expert.District.size
var divisor = 64.0


func _process(delta):
	if camera == null or (not is_instance_valid(camera)): return
	global_transform.origin = camera.global_transform.origin


func _physics_process(delta):
	if player == null or (not is_instance_valid(player)): return
	
	if not environment_moved:
		environment_moved = true
		Util.move_child_under(self, get_tree().root)
	
	$StarField.vast_offset = -player.local_coordinates / district_size / divisor


onready var player = Runtime.Expert.Tracker.get_player_trackers()[0]
onready var camera = get_parent()
var environment_moved = false
