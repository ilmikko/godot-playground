extends Spatial


# TODO: This all can go into FreelookCamera as the Driver.


onready var intent_basis = global_transform.basis setget _set_intent_basis
func _set_intent_basis(b):
	if b == null: return
	intent_basis = b
	$FreelookCamera/RotatingGimbal.global_transform.basis = b


var intent_control = false setget _set_intent_control
func _set_intent_control(c):
	if intent_control == c: return
	intent_control = c
	if !c: return
	
	# Start controlling whatever we're pointing at
	var object = _get_BaseController().PlayerCamera.Interactor.ray_object()
	# The way we control is to swap the base controller with the object's
	# ControllerHandler. If it doesn't have one, we need to make one.
	Runtime.Expert.Control.object(object, self)


var intent_move setget _set_intent_move
func _set_intent_move(m):
	intent_move = m
	$FreelookCamera.intent_fly_direction = m


var intent_move_fast setget _set_intent_move_fast
func _set_intent_move_fast(f):
	intent_move_fast = f
	$FreelookCamera.intent_fly_fast = f


var BaseController setget , _get_BaseController
func _get_BaseController(): return $FreelookCamera/RotatingGimbal/BaseController
