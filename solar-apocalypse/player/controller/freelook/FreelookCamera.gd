extends RigidBody


var FLY_FAST_MODIFIER = 50.0
var FLY_SPEED = 0.02
var FLY_SPEED_FALLOFF = 0.9


func _integrate_forces(state):
	state.linear_velocity *= FLY_SPEED_FALLOFF
	
	var speed = FLY_SPEED
	if intent_fly_fast:
		speed *= FLY_FAST_MODIFIER
	
	var view_basis = $RotatingGimbal.global_transform.basis
	
	apply_central_impulse(view_basis.x * intent_fly_direction.x * speed)
	apply_central_impulse(view_basis.z * intent_fly_direction.y * speed)


export var intent_fly_direction = Vector2(0, 0)
export var intent_fly_fast = false
