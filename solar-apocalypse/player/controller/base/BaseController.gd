extends Spatial

# The BaseController listens to all player input.
# It is up to the linked controllers to use this input in a meaningful way.

func _enter_tree():
	# We can only fetch the node once we're in the tree.
	# Assumes the node is in the parent tree.
	if linked_controller == null && linked_controller_path != null:
		_set_linked_controller(get_node(linked_controller_path))


# This method retrieves the basis from linked_controller and sets the input
# basis accordingly.
func rebuild_linked_controller():
	if linked_controller == null: return
	_get_MouseGimbal().basis = linked_controller.global_transform.basis
	_get_PlayerCamera().Tracker.body = linked_controller


func _input(event):
	_input_intent_interact_primary()
	_input_intent_interact_secondary()
	
	_input_intent_attack()
	_input_intent_control()
	_input_intent_crouch()
	_input_intent_fire()
	_input_intent_fire_secondary()
	_input_intent_jump()
	_input_intent_kick()
	_input_intent_move()
	_input_intent_move_fast()
	_input_intent_roll()
	_input_intent_rotate(event)
	_input_intent_comms()
	_input_intent_jump_drive()
	_input_intent_scan()


func _input_intent_interact_primary():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_interact_primary' in lc): return
	
	lc.intent_interact_primary = Input.is_action_pressed("interact_primary")
	Debug.text(self, "intent_interact_primary", lc.intent_interact_primary)


func _input_intent_interact_secondary():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_interact_secondary' in lc): return
	
	lc.intent_interact_secondary = Input.is_action_pressed("interact_secondary")
	Debug.text(self, "intent_interact_secondary", lc.intent_interact_secondary)


func _input_intent_attack():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_attack' in lc): return
	
	lc.intent_attack = Input.is_action_pressed("attack")
	Debug.text(self, "intent_attack", lc.intent_attack)


# Basis rotation for player camera.
func _input_intent_basis(basis):
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_basis' in lc): return
	
	lc.intent_basis = basis
	Debug.text(self, "intent_basis", lc.intent_basis)


# Control of actors and other objects.
func _input_intent_control():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_control' in lc): return
	
	lc.intent_control = Input.is_action_pressed("control")
	Debug.text(self, "intent_control", lc.intent_control)


func _input_intent_crouch():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_crouch' in lc): return
	
	lc.intent_crouch = Input.is_action_pressed("crouch")
	Debug.text(self, "intent_crouch", lc.intent_crouch)


# Pew pew!
func _input_intent_fire():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_fire' in lc): return
	
	lc.intent_fire = Input.is_action_pressed("fire")
	Debug.text(self, "intent_fire", lc.intent_fire)

# Pew pew!
func _input_intent_fire_secondary():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_fire_secondary' in lc): return
	
	lc.intent_fire_secondary = Input.is_action_pressed("fire_secondary")
	Debug.text(self, "intent_fire_secondary", lc.intent_fire_secondary)


func _input_intent_jump():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_jump' in lc): return
	
	lc.intent_jump = Input.is_action_pressed("jump")
	Debug.text(self, "intent_jump", lc.intent_jump)


func _input_intent_kick():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_kick' in lc): return
	
	lc.intent_kick = Input.is_action_pressed("kick")
	Debug.text(self, "intent_kick", lc.intent_kick)


func _input_intent_comms():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_comms' in lc): return
	
	lc.intent_comms = Input.is_action_pressed("comms")
	Debug.text(self, "intent_comms", lc.intent_comms)


func _input_intent_jump_drive():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_jump_drive' in lc): return
	
	lc.intent_jump_drive = Input.is_action_pressed("jump_drive")
	Debug.text(self, "intent_jump_drive", lc.intent_jump_drive)


func _input_intent_scan():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_scan' in lc): return
	
	lc.intent_scan = Input.is_action_pressed("scan")
	Debug.text(self, "intent_scan", lc.intent_scan)


# Move intent based on WASD.
func _input_intent_move():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_move' in lc): return
	
	var intent = Vector2(0, 0)
	if Input.is_action_pressed("move_left"):
		intent.x -= 1
	if Input.is_action_pressed("move_right"):
		intent.x += 1
	if Input.is_action_pressed("move_forwards"):
		intent.y -= 1
	if Input.is_action_pressed("move_backwards"):
		intent.y += 1
	
	lc.intent_move = intent.normalized()
	Debug.text(self, "intent_move", lc.intent_move)


# Fast move intent (sprint).
func _input_intent_move_fast():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_move_fast' in lc): return
	
	lc.intent_move_fast = Input.is_action_pressed("move_fast")
	Debug.text(self, "intent_move_fast", lc.intent_move_fast)


# Roll intent based on Q/E.
func _input_intent_roll():
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_roll' in lc): return
	
	var intent = 0
	if Input.is_action_pressed("roll_clockwise"):
		intent -= 1
	if Input.is_action_pressed("roll_counterclockwise"):
		intent += 1
	
	lc.intent_roll = intent
	Debug.text(self, "intent_roll", lc.intent_roll)



# Rotate intent based on mouse. Note that mouse events are very spurious; you
# most likely want to decay the signal over time.
func _input_intent_rotate(event):
	# This assign is needed in case linked_controller changes from under us.
	var lc = linked_controller
	if !lc || !('intent_rotate' in lc): return
	if !(event is InputEventMouseMotion): return
	
	var intent = Vector2(
		event.relative.y/TAU,
		event.relative.x/TAU
	)
	
	lc.intent_rotate = intent
	Debug.text(self, "intent_rotate", lc.intent_rotate)


var linked_controller = null setget _set_linked_controller
func _set_linked_controller(lc):
	linked_controller      = lc
	linked_controller_path = null
	rebuild_linked_controller()
export(NodePath) onready var linked_controller_path = null setget _set_linked_controller_path
func _set_linked_controller_path(lcp):
	linked_controller      = get_node(lcp)
	linked_controller_path = lcp
	rebuild_linked_controller()


var PlayerCamera setget , _get_PlayerCamera
func _get_PlayerCamera(): return $PlayerCamera


var MouseGimbal setget , _get_MouseGimbal
func _get_MouseGimbal(): return $MouseGimbal


func _on_MouseGimbal_rotated(basis):
	_input_intent_basis(basis)
