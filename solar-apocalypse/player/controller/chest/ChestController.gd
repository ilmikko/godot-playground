extends Spatial

export var active = false setget _set_active
func _set_active(_active):
	active = _active
	rebuild()

export var primary = true setget _set_primary
func _set_primary(_primary):
	primary = _primary
	rebuild()


export var SENSITIVITY = 0.12
export var VELOCITY_MAX = 0.022


var controlled_from = Vector3(0, 0, 1)


func rebuild():
	rebuild_gravity()


func rebuild_gravity():
	if active && primary:
		$RigidBody.gravity_scale = 0.0
	else:
		$RigidBody.gravity_scale = 1.0


# TODO: Move this to _integrate_forces on the rigid body - this will fix the
# TODO: input lag issues.
func move_hinge(intent):
	var top_basis    = $RigidBody.global_transform.basis
	var bottom_basis = $RigidBody2.global_transform.basis
	
	var intensity = 0
	intensity    += intent.x * -bottom_basis.z.dot(controlled_from)
	intensity    += intent.y * -bottom_basis.x.dot(controlled_from)
	
	$RigidBody.apply_torque_impulse(top_basis.z * intensity)


func _input(event):
	if !active:  return
	if !primary: return
	
	if event is InputEventMouseMotion:
		_input_mouse_move_hinge(event)
		return
	
	_input_keyboard_move_hinge(event)


func _input_keyboard_move_hinge(_event):
	var intent = Geo.Vector2(0)

	if Input.is_action_pressed("control_move_left"):
		intent.x -= 1
	if Input.is_action_pressed("control_move_right"):
		intent.x += 1
	if Input.is_action_pressed("control_move_forwards"):
		intent.y -= 1
	if Input.is_action_pressed("control_move_backwards"):
		intent.y += 1
	
	# move_hinge(intent.normalized().clamped(VELOCITY_MAX))
	move_hinge(intent.normalized())


func _input_mouse_move_hinge(event):
	var velocity = event.relative * SENSITIVITY
	
	move_hinge(velocity.clamped(VELOCITY_MAX))
