extends Spatial


var c = null


func tick():
	var oc = c
	c = $RayCast.get_collider()
	
	if oc == c: return
	
	Runtime.Expert.Target.no_longer_pointing_at(self, oc)
	
	if  c == null:
		Runtime.UI.Hint.star_label = ""
		return
	
	Runtime.Expert.Target.pointing_at(self, c, c.transform.origin)


func answer(other_side):
	$AudioStreamPlayer3D4.play()
	Runtime.UI.Scan.comms_open(self, other_side)


func comms(object):
	$AudioStreamPlayer3D2.play()
	Runtime.UI.Scan.communicate(self, object)
	Runtime.Expert.TargetComms.comms_with(self, self, object)


func dial():
	$AudioStreamPlayer3D3.play()


func scan(object):
	if object == null:
		Runtime.UI.Scan.hide()
		return
	$AudioStreamPlayer3D.play()
	Runtime.UI.Scan.scan(self, object)


func _enter_tree():
	Runtime.Game.Time.connect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _exit_tree():
	Runtime.Game.Time.disconnect("at_least_0_1_seconds_passed", self, "_on_at_least_0_1_seconds_passed")


func _on_at_least_0_1_seconds_passed(_t):
	tick()
