extends Area


var body : PhysicsBody


var local_coordinates    = Vector3(0, 0, 0)
var district_coordinates = Vector3(0, 0, 0)


func _enter_tree():
	Runtime.Expert.Tracker.add_player_tracker(self)


func _exit_tree():
	Runtime.Expert.Tracker.remove_player_tracker(self)


func _ready():
	body = Scene.parent_body(self)
	Runtime.Expert.District.change(district_coordinates)


func _physics_process(delta):
	if body == null: return
	
	var old_district = district_coordinates
	var district_size = Runtime.Expert.District.size
	
	local_coordinates = body.global_transform.origin
	district_coordinates = Vector3(
		floor(local_coordinates.x / district_size),
		floor(local_coordinates.y / district_size),
		floor(local_coordinates.z / district_size)
	)
	
	if district_coordinates != old_district:
		Runtime.Expert.District.change(district_coordinates)
	
	Debug.text(self, "COORD LOCAL", local_coordinates)
	Debug.text(self, "COORD DISTRICT", district_coordinates)
