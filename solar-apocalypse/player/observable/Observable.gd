tool
extends Spatial


# Emitted when the observable is being observed for the first time, and it
# should build soon.
signal observed(position)
# Emitted when the observable is no longer observed by any observers, and it
# should clean up immediately.
signal forgotten(position)


# Emitted when an observer is close by and more fine grained updates on
# its position are desired.
signal updated(position)
# Emitted when fine grained updates are either activated or deactivated.
signal updates_active(active, position)


var observers = {}
var observers_updating = {}

var self_pos
var nearest_observer
var nearest_observer_dist


func debug_update():
	Debug.text(self, "%s observed by" % self.name, observers)
	Debug.text(self, "%s updated by"  % self.name, observers_updating)


func updated_by(observer, active, pos):
	if active:
		# About to become non-empty.
		if observers_updating.size() == 0:
			emit_signal("updates_active", active, pos)
		observers_updating[observer] = active
	else:
		# About to become empty.
		if observers_updating.size() == 1:
			emit_signal("updates_active", active, pos)
		observers_updating.erase(observer)
	
	debug_update()


func observed_by(observer, pos):
	Debug.print(self, "observed by %s" % observer.get_path())
	if observers.size() == 0:
		emit_signal("observed", pos)
	observers[observer] = pos
	
	debug_update()


func forgotten_by(observer, pos):
	Debug.print(self, "forgotten by %s" % observer.get_path())
	if observers.size() == 1:
		emit_signal("forgotten", pos)
	observers.erase(observer)
	
	debug_update()


func position_update(observer, pos):
	observers[observer] = pos
	
	if observer == nearest_observer:
		emit_signal("updated", pos)
	
	debug_update()
	
	var current_dist = (self_pos - pos).length()
	if nearest_observer && current_dist >= nearest_observer_dist: return
	nearest_observer_dist = current_dist
	
	if nearest_observer == observer: return
	# Nearest observer change
	nearest_observer = observer


func _ready():
	self_pos = global_transform.origin


var radius = 1.0 setget _set_radius
func _set_radius(r):
	radius = r
	$Area/CollisionShape.shape.radius = r
