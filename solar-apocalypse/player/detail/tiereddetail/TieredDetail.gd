tool
extends Spatial


func clear_districts():
	for key in districts:
		Util.remove(districts[key])
	districts = {}


func clear_instances():
	for key in instances:
		Util.remove(instances[key])
	instances = {}


func district_del(pos, delta):
	var global_pos = pos + delta
	var key = district_key(global_pos)
	
	if districts.has(key):
		Util.remove(districts[key])
		districts.erase(key)
	
	if instances.has(key):
		Util.remove(instances[key])
		instances.erase(key)


func district_add(pos, delta):
	var global_pos = pos + delta
	var key = district_key(global_pos)
	
	# Debug display.
#	var box = DEBUG_BOX.instance()
#	box.color = Color(0.5, 0, 1, 0.02)
#	box.transform.origin = global_pos * size
#	box.scale = Vector3(1, 1, 1) * size * 0.5;
#
#	districts[key] = box
#	$DebugView.add_child(box)
	
	# TODO: Use InstanceLoadingExpert.
	if scene != null:
		var inst = scene.instance()
		inst.transform.origin = global_pos * size
		
		# Pass through some information if the scene supports it.
		if 'tiered_detail_pos'  in inst: inst.tiered_detail_pos  = global_pos
		if 'tiered_detail_size' in inst: inst.tiered_detail_size = size
		
		instances[key] = inst
		$Instances.add_child(inst)


func district_key(pos) -> String:
	# TODO: why not pos.round()?
	pos = Geo.clean_zeroes(pos)
	return "%s,%s,%s" % [pos.x, pos.y, pos.z]


# vec is an integer vector telling the new position.
func district_change(vec):
	if cell_position == vec: return
	var delta = vec-cell_position
	Debug.text(self, "%s District Rebuild" % self.name, delta)
	
	# We compute a simple delta of the grid changes.
	var district_delta = {}
	
	# TODO: This could be optimized from O(n**3)
	var old_districts = {}
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			for z in range(-expand_count, expand_count+1):
				old_districts[district_key(cell_position + Vector3(x, y, z))] = cell_position + Vector3(x, y, z)
	
	var new_districts = {}
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			for z in range(-expand_count, expand_count+1):
				new_districts[district_key(vec + Vector3(x, y, z))] = vec + Vector3(x, y, z)
	
	var removals = []
	var additions = []
	
	for id in old_districts:
		# No-op, remains visible.
		if new_districts.has(id): continue
		removals.append(old_districts[id])
	
	for id in new_districts:
		# No-op, is already visible.
		if old_districts.has(id): continue
		additions.append(new_districts[id])
	
	# Apply deltas.
	for d in additions:
		district_add(d, Vector3(0, 0, 0))
	for d in removals:
		district_del(d, Vector3(0, 0, 0))
	
	cell_position = vec


func rebuild_districts():
	Debug.warn(self, "District rebuild")
	clear_districts()
	clear_instances()
	Util.remove_children($Instances)
	for x in range(-expand_count, expand_count+1):
		for y in range(-expand_count, expand_count+1):
			for z in range(-expand_count, expand_count+1):
				district_add(cell_position, Vector3(x, y, z))


func rebuild_tracker():
	$Tracker.scale = Vector3(1, 1, 1) * 0.5 * size
	$Tracker.global_transform.origin = tracker_cell_position * size


func rebuild():
	if !is_inside_tree(): return
	
	rebuild_districts()
	rebuild_tracker()


func tracker_update(area):
	if area == null or !is_instance_valid(area): return
	last_tracker = area
	var new_pos = (area.global_transform.origin / size).round()
	if  tracker_cell_position == new_pos: return
	Debug.text(self, "Tracker cell position", tracker_cell_position)
	_set_tracker_cell_position(new_pos)
	district_change(tracker_cell_position)


func _enter_tree():
	Runtime.Game.Time.connect("at_least_0_5_seconds_passed", self, "_on_at_least_0_5_seconds_passed")
	Runtime.Expert.Tracker.connect_and_emit_last_signal("player_trackers_signal", self, "_on_TrackerExpert_trackers_changed")
	rebuild()


func _exit_tree():
	Util.remove_children($DebugView)
	Runtime.Game.Time.disconnect("at_least_0_5_seconds_passed", self, "_on_at_least_0_5_seconds_passed")
	Runtime.Expert.Tracker.disconnect("player_trackers_signal", self, "_on_TrackerExpert_trackers_changed")


var districts = {}
var instances = {}

export(int, 1, 5) var expand_count = 1 setget _set_expand_count
func _set_expand_count(_expand_count):
	expand_count = _expand_count
	rebuild()


export(float, 0.1, 10000.0) var size = 1.0 setget _set_size
func _set_size(_size):
	size = _size
	rebuild()


export(Vector3) var cell_position = Vector3(0, 0, 0) setget _set_cell_position
func _set_cell_position(_cell_position):
	cell_position = _cell_position
	rebuild_districts()


export var tracker_cell_position = Vector3(0, 0, 0) setget _set_tracker_cell_position
func _set_tracker_cell_position(c):
	tracker_cell_position = c
	rebuild_tracker()


var last_tracker = null


export(PackedScene) var scene


const DEBUG_BOX = preload("res://debug/visual/Box.tscn")


func _on_Area_area_exited(area):
	if !area.is_in_group('tracker'): return
	tracker_update(area)


func _on_at_least_0_5_seconds_passed(_t):
	tracker_update(last_tracker)


func _on_TrackerExpert_trackers_changed(ts):
	if ts.size() == 0: return
	tracker_update(ts[0])
