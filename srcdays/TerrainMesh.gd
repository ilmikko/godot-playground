tool
extends MeshInstance

func _ready():
	set_surface_material(0, get_surface_material(0).duplicate())

func _physics_process(delta):
	get_surface_material(0).set_shader_param("global", global_transform.origin)
