shader_type spatial;
render_mode unshaded;

void vertex() {
	vec2 xy = (WORLD_MATRIX * vec4(VERTEX, 0.0)).xz;
	COLOR.xyz = vec3(fract(xy), 0.0);
}

void fragment() {
	vec2 uv = COLOR.xy;
	
	float d = length(uv-vec2(0.5, 0.5))*2.0;
	
	vec2 suv = SCREEN_UV;
	vec3 scol = texture(SCREEN_TEXTURE, suv).xyz;
	
	ALBEDO = d*scol;
}