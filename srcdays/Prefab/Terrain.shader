shader_type spatial;

uniform float scale : hint_range(0.01, 0.1) = 0.1;
uniform float amplitude : hint_range(0.0, 10.0) = 0.1;

uniform vec3 global;

const vec2 epsilon = vec2(0.01, 0.0);

float height(vec2 vec) {
	return cos(vec.x*scale) * sin(vec.y*scale);
}

vec3 normal(vec2 vec) {
	return normalize(
		vec3(
			height(vec - epsilon.xy)-height(vec + epsilon.xy),
			2.0*epsilon.x,
			height(vec - epsilon.yx)-height(vec + epsilon.yx)
		)
	);
}

void vertex() {
	float height = height(global.xz + (WORLD_MATRIX * vec4(VERTEX, 0.0)).xz);
	VERTEX += NORMAL * height * amplitude;
	NORMAL = normal(VERTEX.xz) * amplitude;
	COLOR.xyz = vec3(abs(height/amplitude));
}

void fragment() {
	ALBEDO = COLOR.xyz;
}