shader_type spatial;
render_mode unshaded;

void fragment() {
	vec2 uv = SCREEN_UV;

	ALBEDO = texture(SCREEN_TEXTURE, vec2(uv.x, 1.0-uv.y)).xyz; 
}